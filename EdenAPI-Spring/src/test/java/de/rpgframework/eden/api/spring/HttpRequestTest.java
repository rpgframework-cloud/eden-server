package de.rpgframework.eden.api.spring;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.Connection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ContextConfiguration;

import de.rpgframework.eden.api.spring.rest.AccountRESTService;
import de.rpgframework.eden.logic.BackendAccess;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class HttpRequestTest {


	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	private BackendAccess backend;
	private Connection c;

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		backend = new BackendAccess("jdbc:hsqldb:mem:testdb", "SA","");
		c = backend.getInternalConnection();
	}

	@Test
	void greetingShouldReturnDefaultMessage() throws Exception {
		String value = this.restTemplate
				.withBasicAuth("jdoe", "secret")
				.getForObject("http://localhost:" + port + "/api/account",
				String.class);
		assertNotNull(value, "REST return value is null");
		assertThat(value).contains("Hello, World");
	}

}
