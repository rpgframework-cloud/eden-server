package de.rpgframework.eden.api.spring;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.api.spring.rest.AccountRESTService;

@SpringBootTest
@AutoConfigureMockMvc
class HttpRequestTest2 {

	@Autowired
	private MockMvc mockMvc;
	@Autowired private ObjectMapper objectMapper;

	@MockBean
	private AccountRESTService controller;

	@Test
	void greetingShouldReturnMessageFromService() throws Exception {
		EdenAccountInfo expect = new EdenAccountInfo();
		expect.setEmail("jane@doe.com");
		when(controller.getAccountData()).thenReturn(expect);
		String requestBody = objectMapper.writeValueAsString(expect);

		System.out.println("----------------------------");
		this.mockMvc.perform(get("/api/account").with(user("jdoe").password("secret")))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.email", is("jane@doe.com")))
		.andExpect(content().string(containsString("verified\":false")));
	}

}
