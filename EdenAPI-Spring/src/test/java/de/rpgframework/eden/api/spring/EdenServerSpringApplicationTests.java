package de.rpgframework.eden.api.spring;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import de.rpgframework.eden.api.spring.rest.AccountRESTService;

@SpringBootTest
class EdenServerSpringApplicationTests {

	@MockBean
	private AccountRESTService controller;

	@Test
	void contextLoads() {
		System.err.println("EdenServerSpringApplicationTests.contextLoads()");
		assertThat(controller).isNotNull();
	}

}
