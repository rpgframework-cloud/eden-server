package de.rpgframework.eden.api.spring;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.core.env.Environment;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import de.rpgframework.eden.base.JavaMailMailer;
import de.rpgframework.eden.base.Mailer;
import de.rpgframework.eden.base.MailerLoader;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.PlayerDatabase;
import de.rpgframework.reality.server.PlayerImpl;

/**
 *
 */
@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {

	private final static Logger logger = System.getLogger(MyUserDetailsService.class.getPackageName());

	private static PlayerDatabase database;
	static BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10, new SecureRandom());

	private static Map<String, PlayerImpl> userToPlayer;

	//-------------------------------------------------------------------
	/**
	 * @throws SQLException
	 */
	public MyUserDetailsService(Environment env) throws SQLException {
		userToPlayer = new HashMap<>();

		Properties config = new Properties();
		config.setProperty(BackendAccess.CFG_JDBC_URL , env.getProperty(BackendAccess.CFG_JDBC_URL));
		config.setProperty(BackendAccess.CFG_JDBC_USER, env.getProperty(BackendAccess.CFG_JDBC_USER));
		config.setProperty(BackendAccess.CFG_JDBC_PASS, env.getProperty(BackendAccess.CFG_JDBC_PASS));
		ensureProperty(config, env, Mailer.CFG_MAIL_HOST);
		ensureProperty(config, env, Mailer.CFG_MAIL_PASS);
		ensureProperty(config, env, Mailer.CFG_MAIL_USER);

		MailerLoader.setInstance(new JavaMailMailer(config));
		BackendAccess backend = new BackendAccess(config);
		BackendAccess.setInstance(backend);

		database = backend.getPlayerDatabase();
	}

	//-------------------------------------------------------------------
	private static void ensureProperty(Properties config, Environment env, String key) {
		if (env.getProperty(key)==null)
			throw new RuntimeException("Missing property '"+key+"' in application.properties");
		config.setProperty(key , env.getProperty(key));
	}

	//-------------------------------------------------------------------
	public static PlayerImpl getPlayer(UserDetails user) {
		PlayerImpl ret = userToPlayer.get(user.getUsername());
		if (ret==null) {
			try {
				ret = database.getPlayerByLogin(user.getUsername());
				if (ret!=null) {
					userToPlayer.put(user.getUsername(), ret);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.log(Level.INFO, "loadUserByUsername({0})", username);

		try {
			PlayerImpl player = database.getPlayerByLogin(username);
			logger.log(Level.INFO, "Player1 = {0}", player);
			if (player!=null) {
				logger.log(Level.INFO, "Player {0} has password {1}", player.getLogin(), player.getPassword());
				UserDetails ret = User.builder()
						.passwordEncoder( bCryptPasswordEncoder::encode)
						.username(player.getLogin())
						.password(player.getPassword())
						.build();
				userToPlayer.put(player.getLogin(), player);
				return ret;
			}
			player = database.getPlayerByEmail(username);
			logger.log(Level.INFO, "Player2 = {0}", player);
			if (player!=null) {
				logger.log(Level.INFO, "Player {0} has password {1}", player.getLogin(), player.getPassword());
				UserDetails ret =  User.builder()
						.passwordEncoder( bCryptPasswordEncoder::encode)
						.username(player.getLogin())
						.password(player.getPassword())
						.build();
				userToPlayer.put(player.getLogin(), player);
				return ret;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		throw new UsernameNotFoundException("Gibt's doch gar nicht");
	}

}
