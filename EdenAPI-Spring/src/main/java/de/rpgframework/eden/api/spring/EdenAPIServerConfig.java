package de.rpgframework.eden.api.spring;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.CachingUserDetailsService;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;

import io.swagger.v3.core.util.Json;

/**
 *
 */
@Configuration
@EnableWebMvc
@EnableWebSecurity
public class EdenAPIServerConfig {

	@Value("${jdbc.url}")
	private String jdbcURL;

	@Autowired
	private Environment env;

	private UserDetailsService userDetails;

	//-------------------------------------------------------------------
	/**
	 * @throws SQLException
	 */
	public EdenAPIServerConfig() {
		 Json.mapper().setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
	     Json.mapper().setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
	}

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http
			.cors(AbstractHttpConfigurer::disable)
			.csrf(AbstractHttpConfigurer::disable)
			//.csrf(custom -> custom.disable())
			.authorizeHttpRequests(authorize -> authorize
	            .requestMatchers(HttpMethod.GET,"/anon/*","/swagger-ui/*", "/v3/api-docs", "/v3/api-docs/*" ).permitAll()
	            .requestMatchers(HttpMethod.POST,"/api/account/" ).permitAll()
	            )
			.authorizeHttpRequests((requests) -> requests
				.requestMatchers("/api/**").authenticated()
				.anyRequest().authenticated()
			)
			.httpBasic(Customizer.withDefaults())
//			.formLogin((form) -> form
//				.loginPage("/login")
//				.permitAll()
//			)
			.logout((logout) -> logout.permitAll());

		return http.build();
	}

//	//-------------------------------------------------------------------
//	@Bean
//	public UserDetailsService userDetailsService() throws SQLException {
//		if (userDetails==null) {
//			MyUserDetailsService userDetails = new MyUserDetailsService(env);
//			this.userDetails = new CachingUserDetailsService(userDetails);
//		}
//		return userDetails;
//	}

}
