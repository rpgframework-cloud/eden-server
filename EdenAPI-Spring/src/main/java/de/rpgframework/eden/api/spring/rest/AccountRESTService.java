package de.rpgframework.eden.api.spring.rest;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.api.spring.MyUserDetailsService;
import de.rpgframework.eden.logic.AccountLogic;
import de.rpgframework.eden.logic.LogicResult;
import de.rpgframework.reality.server.PlayerImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 *
 */
@RestController
@Tag(name="Account service",description = "Manage user accounts")
public class AccountRESTService {

	private static class PostAccountInfo {
		private String login;
		private String firstName;
		private String lastName;
		private String email;
		private String secret;
		private String lang;
	}

	private final static Logger logger = System.getLogger("eden.account");

	@Autowired
	private UserDetailsService userDetails;

	//-------------------------------------------------------------------
	/**
	 */
	public AccountRESTService() {
		// TODO Auto-generated constructor stub
	}

    //-------------------------------------------------------------------
    @GetMapping("/api/account")
    @Operation(
    		summary="Get account information",
    		description = "Get account information for currently logged in user",
    		responses = {
    				@ApiResponse( responseCode ="200", description="Found and returned account data for session",
    						content= @Content(
    								mediaType = "application/json",
    								array=@ArraySchema(
    										schema=@Schema(implementation=EdenAccountInfo.class)))),
    				@ApiResponse( responseCode ="403", description="Not logged in", content = @Content(schema = @Schema(implementation = Void.class))),
    				@ApiResponse( responseCode ="500", description="A server side exception occcurred", content = @Content(schema = @Schema(implementation = Void.class)))
    		}
    )
   public EdenAccountInfo getAccountData() {
    	logger.log(Level.ERROR, "ToDo: getAccountData()");
    	System.err.println("ToDo: getAccountData()");

    	Object principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	   	logger.log(Level.WARNING, "principal = {0}",principal);

    	PlayerImpl player = MyUserDetailsService.getPlayer((UserDetails) principal);
	   	logger.log(Level.WARNING, "player = {0}",player);

	   	EdenAccountInfo info = new EdenAccountInfo();
	   	info.setEmail(player.getEmail());
	   	info.setLogin(player.getLogin());
	   	info.setFirstName(player.getFirstName());
	   	info.setLastName(player.getLastName());
	   	info.setVerified(player.isVerified());
	   	info.setLocale(player.getLocale());
     	return info;
    }

    //-------------------------------------------------------------------
    @PostMapping("/api/account/")
    @Operation(
    		summary="Create a new account",
    		description = "Create a new account using the sent data",
    		requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(schema = @Schema(implementation = PostAccountInfo.class))),
     		responses = {
    				@ApiResponse( responseCode ="200", description="Account created and mail with verification code has been sent",
    						content= @Content(
    								mediaType = "application/json",
    								schema = @Schema(implementation = Void.class))),
    				@ApiResponse( responseCode ="400", description="Invalid or insufficient data", content = @Content(schema = @Schema(implementation = Void.class))),
    				@ApiResponse( responseCode ="409", description="Already exists", content = @Content(schema = @Schema(implementation = Void.class))),
    				@ApiResponse( responseCode ="504", description="Failed sending verification email", content = @Content(schema = @Schema(implementation = Void.class))),
    				@ApiResponse( responseCode ="500", description="A server side exception occcurred", content = @Content(schema = @Schema(implementation = Void.class)))
    		}
    )
   public void createAccount(@RequestBody EdenAccountInfo data) {
    	logger.log(Level.WARNING, "ToDo: createAccount() "+data);
		LogicResult result = AccountLogic.createAccount(data);
		if (result.successObject!=null) {
	    	logger.log(Level.INFO, "All good {0}", result.status);
		} else {
	    	logger.log(Level.INFO, "Failed with code {1}: {0}", result.message, result.status);
			throw new ResponseStatusException(result.status.code(), result.message, null);
		}
   }

}
