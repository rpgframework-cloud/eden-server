package de.rpgframework.eden.api.spring.rest;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import de.rpgframework.eden.api.EdenPingInfo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@Tag(name="Public service",description = "Accessible without logging in")
public class PublicService {

	private final static Logger logger = System.getLogger(PublicService.class.getPackageName());

	@Value( "${server.hostname}" )
	private String hostname;

    //-------------------------------------------------------------------
    @GetMapping("/anon/ping")
    @Operation(
    		summary="Alive Check",
    		description = "Contact this endpoint to see if the server is up and running",
    		responses = {
    				@ApiResponse( responseCode ="200", description="Some server information",
    						content= @Content(
    								mediaType = "application/json",
    								schema=@Schema(implementation=EdenPingInfo.class))),
    				@ApiResponse( responseCode ="403", description="Not logged in", content = @Content(schema = @Schema(implementation = Void.class))),
    				@ApiResponse( responseCode ="500", description="A server side exception occcurred", content = @Content(schema = @Schema(implementation = Void.class)))
    		}
    )
   public EdenPingInfo ping() {
    	logger.log(Level.ERROR, "ToDo: ping()");

    	EdenPingInfo info = new EdenPingInfo();
    	info.setAccountCreationURL("http://"+hostname+"/api/account");
    	info.setMainSiteURL("https://go.away.com/");
     	return info;
    }

}
