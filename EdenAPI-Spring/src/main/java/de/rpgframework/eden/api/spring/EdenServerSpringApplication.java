package de.rpgframework.eden.api.spring;

import java.lang.System.Logger.Level;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class EdenServerSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdenServerSpringApplication.class, args);
	}

	@Bean
    ApplicationRunner applicationRunner(Environment environment) {
        return args -> {
            System.getLogger("BOOT").log(Level.INFO,"message from application.properties " + environment.getProperty("message-from-application-properties"));
        };
    }
}
