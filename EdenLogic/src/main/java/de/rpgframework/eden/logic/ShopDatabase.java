package de.rpgframework.eden.logic;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.reality.CatalogItem;
import de.rpgframework.reality.CatalogItem.Category;

/**
 * @author prelle
 *
 */
public class ShopDatabase {

	private final static Logger logger = BackendAccess.logger;

	public final static String TABLE = "Catalog";

	private final static String CREATE_SQL = "CREATE TABLE "+TABLE+" ("
			+"\n  id   VARCHAR(20) NOT NULL,"
			+"\n  category VARCHAR(20) NOT NULL,"
			+"\n  rules  VARCHAR(20) NOT NULL,"
			+"\n  lang  CHAR(2) NOT NULL,"
			+"\n  name  VARCHAR(40) NOT NULL,"
			+"\n  price  INT NOT NULL,"
			+"\n  state  ENUM('HIDDEN','AVAILABLE') DEFAULT 'HIDDEN',"
			+"\n  datasets  VARCHAR(255),"
			+"\n PRIMARY KEY(id)"
			+"\n)";

	private Connection con;
	private PreparedStatement insert;
	private PreparedStatement select;
	private PreparedStatement selectByID;

	//-------------------------------------------------------------------
	public ShopDatabase(Connection con) throws SQLException {
		this.con = con;
		setup();
	}

	//-------------------------------------------------------------------
	private void setup() throws SQLException {
		Statement stat = con.createStatement();
		if (con.getMetaData()!=null && con.getMetaData().getDatabaseProductName()!=null && con.getMetaData().getDatabaseProductName().contains("HSQL")) {
			logger.log(Level.INFO, "Enable MYSQL compatibility mode for HSQL");
			stat.execute("SET DATABASE SQL SYNTAX MYS true");
		}
		try {
			stat.execute("SELECT COUNT(*) FROM "+TABLE);
		} catch (SQLException e) {
			if (e.getSQLState().equals("42501") || e.getSQLState().equals("42S02")) {
				// Table does not exist yet
				logger.log(Level.DEBUG, "Create table "+TABLE);
				try {
					logger.log(Level.DEBUG, "Send "+CREATE_SQL);
					stat.executeUpdate(CREATE_SQL);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				logger.log(Level.WARNING, "sqlState = "+e.getSQLState());
				logger.log(Level.WARNING, "error code = "+e.getErrorCode());
				logger.log(Level.ERROR, "Error was",e);
				throw e;
			}
		}

		insert = con.prepareStatement("INSERT INTO "+TABLE+" VALUES (?,?,?,?,?,?,?,?)");
		select = con.prepareStatement("SELECT * FROM "+TABLE);
		selectByID = con.prepareStatement("SELECT * FROM "+TABLE+" WHERE id=?");
	}

	//-------------------------------------------------------------------
	public CatalogItem add(CatalogItem item) throws SQLException {
		insert.setString(1, item.getId());
		insert.setString(2, item.getCategory().name());
		insert.setString(3, item.getRules().name());
		insert.setString(4, item.getLanguage());
		insert.setString(5, item.getName());
		insert.setInt   (6, item.getPrice());
		insert.setString(7, item.getState().name());
		insert.setString(8, String.join(",",item.getDatasets()));
		logger.log(Level.DEBUG, "SEND "+insert);
		insert.executeUpdate();
		return item;
	}

	//-------------------------------------------------------------------
	private CatalogItem getHandleFromResultSet(ResultSet set) throws SQLException {
		CatalogItem ret = new CatalogItem(
				set.getString("id"),
				Category.valueOf(set.getString("category")),
				RoleplayingSystem.valueOf(set.getString("rules")),
				set.getString("lang"),
				set.getString("name"),
				set.getInt("price")
				);
		if (set.getString("rules")!=null)
			ret.setRules(RoleplayingSystem.valueOf(set.getString("rules")));
		if (set.getString("datasets")!=null)
			ret.setPlugins(Arrays.asList(set.getString("datasets").split(",")));
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Get all characters from all rule systems
	 * @param player
	 * @return
	 * @throws SQLException
	 */
	public List<CatalogItem> getCatalogItems() throws SQLException {
		List<CatalogItem> ret = new ArrayList<CatalogItem>();

//		select.setString(1, player.getUuid().toString());
		ResultSet set = select.executeQuery();
		while (set.next()) {
			ret.add(getHandleFromResultSet(set));
		}

		return ret;
	}

	//-------------------------------------------------------------------
	public CatalogItem getCatalogItem(String id) throws SQLException {
		selectByID.setString(1, id);
		ResultSet set = selectByID.executeQuery();
		try {
			while (set.next()) {
				return getHandleFromResultSet(set);
			}
		} finally {
			set.close();
		}
		return null;
	}

}
