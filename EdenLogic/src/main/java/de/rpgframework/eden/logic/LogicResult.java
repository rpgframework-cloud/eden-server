package de.rpgframework.eden.logic;

import de.rpgframework.eden.api.EdenStatus;

/**
 * @author prelle
 *
 */
public class LogicResult {

	public EdenStatus status;
	public String message;
	public Object successObject;

	//-------------------------------------------------------------------
	public LogicResult(EdenStatus status) {
		this.status = status;
	}

	//-------------------------------------------------------------------
	public LogicResult(EdenStatus status, String mess) {
		this.status = status;
		this.message= mess;
	}

	//-------------------------------------------------------------------
	public LogicResult(EdenStatus status, Object successObject) {
		this.status = status;
		this.successObject= successObject;
	}

}
