package de.rpgframework.eden.logic;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.character.Attachment;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.api.EdenStatus;
import de.rpgframework.eden.base.MailerLoader;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.server.PlayerImpl;
import de.rpgframework.reality.server.ServerCharacterHandle;

/**
 * @author prelle
 *
 */
public class AccountLogic {

	private final static Logger logger = System.getLogger("eden.api");

	private final static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(AccountLogic.class.getName(), Locale.ENGLISH, Locale.GERMAN);

	//-------------------------------------------------------------------
	/**
	 */
	public AccountLogic() {
		// TODO Auto-generated constructor stub
	}

    //-------------------------------------------------------------------
    private static String createNewCode() {
    	Random rand = new Random();
    	int n = rand.nextInt(10000);
    	if (n<10) return "000"+n;
    	if (n<100) return "00"+n;
    	if (n<1000) return "0"+n;
    	return String.valueOf(n);
    }

    //-------------------------------------------------------------------
    private static String getVerificationHTML(Locale loc, String code) {
    	StringBuffer html = new StringBuffer();
    	html.append("<h2>"+RES.getString("verificationmail.body1", loc)+"</h2>");
    	html.append("<p>"+RES.getString("verificationmail.body2", loc)+"</p>");
    	html.append("<p text-align=\"center\"; style=\"font-size: 200%\">"+code+"</p>");
    	html.append("<p>"+RES.getString("verificationmail.body3", loc)+"</p>");
    	return html.toString();
    }

    //-------------------------------------------------------------------
    private static String getWelcomeHTML(Locale loc, PlayerImpl player) {
    	StringBuffer html = new StringBuffer();
    	html.append("<h1>"+RES.getString("welcomemail.line1", loc)+" "+player.getFirstName()+" "+player.getLastName()+"</h1>");
    	html.append("<p>"+RES.getString("welcomemail.content", loc)+"</h1>");
    	return html.toString();
    }

    //-------------------------------------------------------------------
    public static LogicResult createAccount(EdenAccountInfo data) {
		logger.log(Level.INFO, "ENTER createAccount");
		logger.log(Level.INFO, "- email : "+data.getEmail());
		logger.log(Level.INFO, "- login : "+data.getLogin());
		if (data.getEmail()==null || data.getLogin()==null) {
			return new LogicResult(EdenStatus.INSUFFICENT_DATA, "Login or Email missing");
		}
		if (!data.getEmail().contains("@") || data.getEmail().length()<5) {
			return new LogicResult(EdenStatus.INSUFFICENT_DATA, "Not a valid email");
		}

		PlayerDatabase db = BackendAccess.getInstance().getPlayerDatabase();
		try {
			PlayerImpl player = db.getPlayerByEmail(data.getEmail());
			if (player!=null) {
	    		logger.log(Level.INFO, "Player found by email {0}", data.getEmail());
				if (!player.isVerified()) {
					// Account exists, but not verified yet, so may be fake
					Duration ago = Duration.between(player.getTimeCreated(), Instant.now());
					logger.log(Level.WARNING, "Player with email {0} already exists and is unverified since {1}", data.getEmail(), ago);
					if (ago.toHours()>=1) {
						logger.log(Level.WARNING, "Delete unverified outdated player (Login: {0}, Email: {1})", player.getLogin(), player.getEmail());
						db.deletePlayer(player);
						player=null;
					} else {
						return new LogicResult(EdenStatus.ALREADY_EXISTS, "A player with that email already exists");
					}
				} else {
					return new LogicResult(EdenStatus.ALREADY_EXISTS, "A player with that email already exists");
				}
			}
			if (player==null)
				player = db.getPlayerByLogin(data.getLogin());
			if (player!=null) {
	    		logger.log(Level.INFO, "Player found by login {0}", data.getLogin());
				if (!player.isVerified()) {
					// Account exists, but not verified yet, so may be fake
		    		logger.log(Level.INFO, "- created : "+player.getTimeCreated());
					Duration ago = Duration.between(player.getTimeCreated(), Instant.now());
					logger.log(Level.WARNING, "Player with login {0} already exists and is unverified since {1}", data.getEmail(), ago);
					if (ago.toHours()>=1) {
						logger.log(Level.WARNING, "Delete unverified outdated player (Login: {0}, Email: {1})", player.getLogin(), player.getEmail());
						db.deletePlayer(player);
						player=null;
					} else {
			    		logger.log(Level.INFO, "Trying to recreate a not-yet verified player with already existing login {0}", data.getLogin());
			    		return new LogicResult(EdenStatus.ALREADY_EXISTS, "A player with that login already exists");
					}
				} else {
		    		logger.log(Level.INFO, "Trying to recreate a verified player with already existing login {0}", data.getLogin());
		    		return new LogicResult(EdenStatus.ALREADY_EXISTS, "A player with that login already exists");
				}
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "Failed checking existing players", e);
			return new LogicResult(EdenStatus.INTERNAL_ERROR, e.toString());
		}
		logger.log(Level.INFO, "No player with login {0} yet",data.getLogin());

		// Create player
		String code = createNewCode();
		try {
			PlayerImpl player = db.createPlayer(data.getFirstName(), data.getLastName(), data.getEmail(), data.getLogin(), data.getSecret(), data.getLocale());
			player.setVerificationCode(code);
			db.updatePlayer(player);

			String error = MailCenter.send(
					data.getEmail(),
					RES.getString("verificationmail.subject", data.getLocale()),
					getVerificationHTML(data.getLocale(), code)
					);
//			List<String[]> header = new ArrayList<String[]>();
//			header.add(new String[] {"Reply-To","eden@rpgframework.de"});
//			String error = MailerLoader.getInstance().send(
//					"eden@rpgframework.de",
//					List.of("stefan@rpgframework.de"),
//					header,
//					RES.getString("verificationmail.subject", data.getLocale()),
//					"Read the HTML content, not this",
//					getVerificationHTML(data.getLocale(), code));
			if (error!=null) {
				logger.log(Level.WARNING, "Mail sending to {1} returns error: {0}", error, data.getEmail());
				db.deletePlayer(player);
				// Remove all line breaks from the message
				StringBuffer buf = new StringBuffer(error.trim());
				for (int i=0; i<error.length(); i++) {
					if (error.charAt(i)=='\n')
					buf.setCharAt(i, ' ');
				}
				error = buf.toString();
				logger.log(Level.INFO, "Reject account creation with reason: {0}", error);
				return new LogicResult(EdenStatus.MAIL_ERROR, error);
			}
			logger.log(Level.INFO, "Player {0} created successfully", data.getLogin());
			EdenAccountInfo answer = new EdenAccountInfo();
			answer.setEmail(data.getEmail());
			answer.setLogin(data.getLogin());
			answer.setFirstName(data.getFirstName());
			answer.setLastName(data.getLastName());
			MailCenter.send(
					"stefan@rpgframework.de",
					"Account "+data.getEmail()+" created",
					data.getFirstName()+" "+data.getLastName()+" (Language="+data.getLanguage()+")\n"+" Email: "+data.getEmail()
					);
			return new LogicResult(EdenStatus.OK, answer);
		} catch (SQLException e) {
			logger.log(Level.WARNING, "Error creating user in database", e);
			return new LogicResult(EdenStatus.INTERNAL_ERROR, e.toString());
		}
	}

    //-------------------------------------------------------------------
    public static LogicResult updateAccount(PlayerImpl player, EdenAccountInfo data) {
		logger.log(Level.INFO, "ENTER updateAccount");
		logger.log(Level.INFO, "- email : "+data.getEmail());
		logger.log(Level.INFO, "- player: "+player.getUuid());
		if (!data.getEmail().contains("@") || data.getEmail().length()<5) {
			return new LogicResult(EdenStatus.INSUFFICENT_DATA, "Not a valid email");
		}

		if (data.getEmail()!=null && !data.getEmail().equals(player.getEmail())) {
			player.setEmail(data.getEmail());
			logger.log(Level.INFO, "change email of player {0} to {1}", player.getUuid(), data.getEmail());
		}
		if (data.getFirstName()!=null && !data.getFirstName().equals(player.getFirstName())) {
			player.setFirstName(data.getFirstName());
			logger.log(Level.INFO, "change first name of player {0} to {1}", player.getUuid(), data.getFirstName());
		}
		if (data.getLastName()!=null && !data.getLastName().equals(player.getLastName())) {
			player.setLastName(data.getLastName());
			logger.log(Level.INFO, "change first name of player {0} to {1}", player.getUuid(), data.getLastName());
		}
		if (data.getSecret()!=null && !data.getSecret().equals(player.getPassword())) {
			player.setPassword(data.getSecret());
			logger.log(Level.INFO, "change password of player {0}", player.getUuid());
		}
		if (data.getLanguage()!=null && !data.getLanguage().equals(player.getLocale().getLanguage())) {
			player.setLocale(Locale.forLanguageTag(data.getLanguage()));
			logger.log(Level.INFO, "change language of player {0} to {1}", player.getUuid(), data.getLanguage());
		}

		try {
			BackendAccess.getInstance().getPlayerDatabase().updatePlayer(player);
	 		EdenAccountInfo pdu = new EdenAccountInfo();
			pdu.setEmail(player.getEmail());
			pdu.setFirstName(player.getFirstName());
			pdu.setLastName(player.getLastName());
			pdu.setLogin(player.getLogin());
			pdu.setLocale( ((PlayerImpl)player).getLocale());
			pdu.setModules(new HashMap<>());
			pdu.setVerified(player.isVerified());
			return new LogicResult(EdenStatus.OK, pdu);
		} catch (SQLException e) {
			logger.log(Level.WARNING, "Failed updating account",e);
			return new LogicResult(EdenStatus.INTERNAL_ERROR, "Failed updating server database. Contact admin!");
		}
    }

    //-------------------------------------------------------------------
    public static LogicResult sendVerificationCode(PlayerImpl player) {
		logger.log(Level.DEBUG, "ENTER sendVerificationCode({1}={0})",player.getUuid(), player.getLogin());
		try {
			if (player.isVerified()) {
				logger.log(Level.WARNING, "Request for a verification code for already verified player {0}", player.getEmail());
				return new LogicResult(EdenStatus.ALREADY_EXISTS, "Already verified");
			}

			EdenAccountInfo pdu = new EdenAccountInfo();
			pdu.setVerificationCode(player.getVerificationCode());
			pdu.setEmail(player.getEmail());
			pdu.setFirstName(player.getFirstName());
			pdu.setLastName(player.getLastName());
			pdu.setLocale( ((PlayerImpl)player).getLocale());
			pdu.setModules(new HashMap<>());
			pdu.setVerified(player.isVerified());


			String code = createNewCode();
			player.setVerificationCode(code);
			logger.log(Level.DEBUG, "created new verification code for player {0}", player.getEmail());

			PlayerDatabase db = BackendAccess.getInstance().getPlayerDatabase();
			db.updatePlayer(player);

			logger.log(Level.INFO, "Send code {0} to {1}", code, player.getEmail());
			String error = MailerLoader.getInstance().send(
				"eden@rpgframework.de",
				List.of(player.getEmail()),
				List.of(),
				RES.getString("verificationmail.subject", player.getLocale()),
				"Read the HTML content, not this",
				getVerificationHTML(((PlayerImpl)player).getLocale(), code));
			if (error!=null) {
				logger.log(Level.WARNING, "Mail sending to {1} returns error: {0}", error, player.getEmail());
				// Remove all line breaks from the message
				StringBuffer buf = new StringBuffer(error.trim());
				for (int i=0; i<error.length(); i++) {
					if (error.charAt(i)=='\n')
						buf.setCharAt(i, ' ');
				}
				error = buf.toString();
				logger.log(Level.INFO, "Reject account creation with reason: {0}", error);
				return new LogicResult(EdenStatus.MAIL_ERROR, error);
			}
			return new LogicResult(EdenStatus.OK, null);
		} catch (SQLException e) {
			logger.log(Level.WARNING, "Failed memorizing new verification code",e);
			return new LogicResult(EdenStatus.INTERNAL_ERROR, "Failed creating and memorizing new verification code. Contact admin!");
		} finally {
			logger.log(Level.DEBUG, "LEAVE sendVerificationCode({1}={0})",player.getUuid(), player.getLogin());
		}
    }

    //-------------------------------------------------------------------
	public static LogicResult verifyAccount(PlayerImpl player, String sentCode) {
		logger.log(Level.DEBUG, "ENTER verifyAccount({1}={0})",player.getUuid(), player.getLogin());
		try {
			if (player.getVerificationCode().equals(sentCode)) {
				player.setVerified(true);
				player.setVerificationCode(null);
				PlayerDatabase db = BackendAccess.getInstance().getPlayerDatabase();
				db.updatePlayer(player);
				logger.log(Level.INFO, "Successfully verified player {0} (UUID={1})", player.getLogin(), player.getUuid());

				MailCenter.send(
						player.getEmail(),
						RES.getString("welcomemail.subject", player.getLocale()), getWelcomeHTML(((PlayerImpl)player).getLocale(), player));
//				String error = MailerLoader.getInstance().send(
//						"eden@rpgframework.de",
//						List.of(player.getEmail()),
//						List.of(),
//						RES.getString("welcomemail.subject", player.getLocale()),
//						"Read the HTML content, not this",
//						getWelcomeHTML(((PlayerImpl)player).getLocale(), player));
				return new LogicResult(EdenStatus.OK);
			} else {
				logger.log(Level.INFO, "Wrong verification code for player {0} (UUID={1})", player.getLogin(), player.getUuid());
				return new LogicResult(EdenStatus.UNAUTHORIZED);
			}
		} catch (SQLException e) {
			logger.log(Level.INFO, "Failed verifying player {0} (UUID={1}).\nReason: {2}", player.getLogin(), player.getUuid(), e.toString());
			return new LogicResult(EdenStatus.INTERNAL_ERROR, e.toString());
		} finally {
			logger.log(Level.DEBUG, "LEAVE verifyAccount({1}={0})",player.getUuid(), player.getLogin());
		}
	}

	//-------------------------------------------------------------------
	public static LogicResult deleteAccount(PlayerImpl player) {
		logger.log(Level.INFO, "ENTER deleteAccount({1}={0})",player.getUuid(), player.getLogin());

		AttachmentDatabase attDB = BackendAccess.getInstance().getAttachmentDatabase();
		CharacterDatabase charDB = BackendAccess.getInstance().getCharacterDatabase();
		PlayerDatabase  playerDB = BackendAccess.getInstance().getPlayerDatabase();
		BoughtDatabase  boughtDB = BackendAccess.getInstance().getLicenseDatabase();
		try {
			for (CharacterHandle handle : charDB.listCharacters(player)) {
				logger.log(Level.INFO, "Delete character {0} (UUID={1})", handle.getName(), handle.getUUID());
				for (Attachment attach : attDB.getAttachments((ServerCharacterHandle) handle)) {
					attDB.delete(attach);
				}
				charDB.delete(handle);
			}

			// Delete all licenses
			List<String[]> catalogItems = new ArrayList<>();
			for (BoughtItem item : boughtDB.getBoughtItems(player)) {
				logger.log(Level.INFO, "Delete bought license for {0} from player {1}", item.getItemID(), player.getUuid());
				catalogItems.add(new String[] {item.getItemID(), item.getName()});
				boughtDB.deleteBoughtItem(item);
			}
			playerDB.deletePlayer(player);
			logger.log(Level.INFO, "Successfully deleted player {0} (UUID={1})", player.getLogin(), player.getUuid());

			MailCenter.send(
					"stefan@rpgframework.de",
					"Account "+player.getEmail()+" deleted",
					player.getFirstName()+" "+player.getLastName()+"<br/>\n"+" Email: "+player.getEmail()+
					"<br/>Der Account besaß folgende Lizenzen:\n"+
					String.join("</br>", catalogItems.stream().map(pair -> pair[0]+" \t"+pair[1]).toList() )
					);
			return new LogicResult(EdenStatus.OK);
		} catch (Throwable e) {
			logger.log(Level.ERROR, "Failed deleting account",e);
			return new LogicResult(EdenStatus.INTERNAL_ERROR, e.toString());
		} finally {
			logger.log(Level.INFO, "LEAVE deleteAccount({1}={0})",player.getUuid(), player.getLogin());
		}
	}
}
