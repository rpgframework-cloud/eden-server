package de.rpgframework.eden.logic;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.reality.server.PlayerImpl;
import de.rpgframework.reality.server.ServerCharacterHandle;

/**
 * @author prelle
 *
 */
public class CharacterDatabase {

	private final static Logger logger = BackendAccess.logger;

	public final static String TABLE = "Characters";

	private final static String CREATE_SQL = "CREATE TABLE "+TABLE+" ("
			+"\n  id       VARCHAR(40) NOT NULL,"
			+"\n  player   VARCHAR(40) NOT NULL,"
			+"\n  rules    VARCHAR(20) NOT NULL,"
			+"\n  name     VARCHAR(40) NOT NULL,"
			+"\n  descr    VARCHAR(255),"
			+"\n  modified TIMESTAMP NOT NULL,"
			+"\n  deleted  BOOLEAN ,"
			+"\n PRIMARY KEY(player,id),"
			+"\n FOREIGN KEY(player) REFERENCES Player(id)"
			+"\n)";

	private Connection con;
	private PreparedStatement insert;
	private PreparedStatement select;
	private PreparedStatement selectSpecific;
	private PreparedStatement selectByUUID;
	private PreparedStatement update;
	private PreparedStatement delete;

	//-------------------------------------------------------------------
	public CharacterDatabase(Connection con) throws SQLException {
		this.con = con;
		setup();
	}

	//-------------------------------------------------------------------
	private void setup() throws SQLException {
		Statement stat = con.createStatement();
		try {
			stat.execute("SELECT COUNT(*) FROM "+TABLE);
		} catch (SQLException e) {
			if (e.getSQLState().equals("42501") || e.getSQLState().equals("42S02")) {
				// Table does not exist yet
				logger.log(Level.DEBUG, "Create table "+TABLE);
				try {
					stat.executeUpdate(CREATE_SQL);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				logger.log(Level.WARNING, "sqlState = "+e.getSQLState());
				logger.log(Level.WARNING, "error code = "+e.getErrorCode());
				logger.log(Level.ERROR, "Error was",e);
				throw e;
			}
		}

		insert = con.prepareStatement("INSERT INTO "+TABLE+" (id,player,rules,name,descr,modified,deleted) VALUES(?,?,?,?,?,?,?)");
		select = con.prepareStatement("SELECT id,rules,name,descr,modified,deleted FROM "+TABLE+" WHERE player=?");
		selectSpecific = con.prepareStatement("SELECT id,rules,name,descr,modified,deleted FROM "+TABLE+" WHERE player=? AND rules=? AND name=?");
		selectByUUID = con.prepareStatement("SELECT id,rules,name,descr,modified,deleted FROM "+TABLE+" WHERE player=? AND id=?");
		update = con.prepareStatement("UPDATE "+TABLE+" SET name=?, deleted=?, modified=?, descr=? WHERE id=?");
		delete = con.prepareStatement("DELETE FROM "+TABLE+" WHERE id=?");
	}

	//-------------------------------------------------------------------
	@Deprecated
	public ServerCharacterHandle addCharacter(PlayerImpl player, RoleplayingSystem rules, String name) throws SQLException {
		UUID uuid = UUID.randomUUID();
		ServerCharacterHandle handle = new ServerCharacterHandle(player, uuid, rules, name);
		handle.setLastModified(Date.from(Instant.now()));
		return addCharacter(player, handle);
	}

	//-------------------------------------------------------------------
	public ServerCharacterHandle addCharacter(PlayerImpl player, ServerCharacterHandle handle) throws SQLException {
		insert.setString(1, handle.getUUID().toString());
		insert.setString(2, player.getUuid().toString());
		insert.setString(3, handle.getRuleIdentifier().name());
		insert.setString(4, handle.getName());
		insert.setString(5, handle.getShortDescription());
		insert.setTimestamp(6, java.sql.Timestamp.from(handle.getLastModified().toInstant()));
		insert.setBoolean(7, handle.isDeleted());
		logger.log(Level.INFO, "SQL: "+insert);
		insert.executeUpdate();
		logger.log(Level.INFO, "Created character '"+handle.getName()+"' for player '"+player.getLogin()+"'");

		BabylonEventBus.fireEvent(BabylonEventType.CHAR_ADDED, player, handle);
		return handle;
	}

	//-------------------------------------------------------------------
	private ServerCharacterHandle getHandleFromResultSet(PlayerImpl player, ResultSet set) throws SQLException {
		ServerCharacterHandle ret = new ServerCharacterHandle(
				player,
				UUID.fromString(set.getString("id")),
				RoleplayingSystem.valueOf(set.getString("rules")),
				set.getString("name"));
		ret.setLastModified( Date.from( set.getTimestamp("modified").toInstant() ));
		ret.setShortDescription(set.getString("descr"));
		ret.setDeleted(set.getBoolean("deleted"));
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Get all characters from all rule systems
	 * @param player
	 * @return
	 * @throws SQLException
	 */
	public List<CharacterHandle> listCharacters(PlayerImpl player) throws SQLException {
		List<CharacterHandle> ret = new ArrayList<CharacterHandle>();

		select.setString(1, player.getUuid().toString());
		ResultSet set = select.executeQuery();
		while (set.next()) {
			ret.add(getHandleFromResultSet(player,set));
		}

		return ret;
	}

	//-------------------------------------------------------------------
	public ServerCharacterHandle getCharacter(PlayerImpl player, RoleplayingSystem rules, String name) throws SQLException {
		selectSpecific.setString(1, player.getUuid().toString());
		selectSpecific.setString(2, rules.name());
		selectSpecific.setString(3, name);
		ResultSet set = selectSpecific.executeQuery();
		if (set.next()) {
			return getHandleFromResultSet(player, set);
		}
		return null;
	}

	//-------------------------------------------------------------------
	public ServerCharacterHandle getCharacter(PlayerImpl player, UUID uuid) throws SQLException {
		selectByUUID.setString(1, player.getUuid().toString());
		selectByUUID.setString(2, uuid.toString());
		ResultSet set = selectByUUID.executeQuery();
		if (set.next()) {
			return getHandleFromResultSet(player, set);
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void update(CharacterHandle handle) throws SQLException {
		update.setString (1, handle.getName());
		update.setBoolean(2, handle.isDeleted());
		update.setTimestamp(3, java.sql.Timestamp.from(handle.getLastModified().toInstant()));
		update.setString(4, handle.getShortDescription());
		update.setString(5, handle.getUUID().toString());
		logger.log(Level.TRACE, "Call {0}",update);
		update.executeUpdate();
	}

	//-------------------------------------------------------------------
	public void delete(CharacterHandle oldHandle) throws SQLException {
		delete.setString(1, oldHandle.getUUID().toString());
		logger.log(Level.TRACE, "Call {0}",delete);
		delete.executeUpdate();
	}


}
