package de.rpgframework.eden.logic;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Base64;
import java.util.Locale;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author prelle
 *
 */
public class PlayerDatabase {

	private final static String SECRET = "WLX8UMLKpMwY/gklqffzCQ==";
	private final static String ALGORITHM = "AES";

	private final static Logger logger = BackendAccess.logger;

	public final static String TABLE = "Player";

	private final static String CREATE_SQL = "CREATE TABLE "+TABLE+" ("
			+"\n  id   VARCHAR(40) NOT NULL,"
			+"\n  firstName  VARCHAR(40) NOT NULL,"
			+"\n  lastName  VARCHAR(40) NOT NULL,"
			+"\n  email  VARCHAR(40) NOT NULL,"
			+"\n  login  VARCHAR(40) NOT NULL,"
			+"\n  password  VARCHAR(88) NOT NULL,"
			+"\n  created  TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL ,"
			+"\n  verified  TINYINT DEFAULT 0,"
			+"\n  vCode  VARCHAR(5) NULL,"
			+"\n  recoverID  VARCHAR(40),"
			+"\n  lang  VARCHAR(2) NOT NULL,"
			+"\n PRIMARY KEY(id),"
			+"\n UNIQUE (email)"
			+"\n)";

	private static Cipher cipherEnc;
	private static Cipher cipherDec;

	private Connection con;
	private PreparedStatement insert;
	private PreparedStatement selectByUUID;
	private PreparedStatement selectByEmail;
	private PreparedStatement selectByLogin;
	private PreparedStatement update;
	private PreparedStatement verify;
	private PreparedStatement delete;

	//-----------------------------------------------------------------
	private static java.security.Key generateKeyFromString(final String secKey) throws Exception {
	    final byte[] keyVal = Base64.getDecoder().decode(secKey.getBytes());
	    final java.security.Key key = new SecretKeySpec(keyVal, ALGORITHM);
	    return key;
	}

	//-------------------------------------------------------------------
	public PlayerDatabase(Connection con) throws SQLException {
		this.con = con;

		try {
			final java.security.Key key = generateKeyFromString(SECRET);
			cipherEnc = Cipher.getInstance("AES");
			cipherEnc.init(Cipher.ENCRYPT_MODE, key);
			cipherDec = Cipher.getInstance("AES");
			cipherDec.init(Cipher.DECRYPT_MODE, key);
		} catch (Exception e) {
			logger.log(Level.ERROR, "Cannot set up Cipher",e);
		}

		setup();
	}

	//-------------------------------------------------------------------
	private void setup() throws SQLException {
		Statement stat = con.createStatement();
		try {
			stat.execute("SELECT COUNT(*) FROM "+TABLE);
		} catch (SQLException e) {
			if (e.getSQLState().equals("42501") || e.getSQLState().equals("42S02")) {
				// Table does not exist yet
				logger.log(Level.DEBUG, "Create table "+TABLE);
				stat.executeUpdate(CREATE_SQL);
			} else {
				logger.log(Level.WARNING, "sqlState = "+e.getSQLState());
				logger.log(Level.WARNING, "error code = "+e.getErrorCode());
				logger.log(Level.ERROR, "Error was",e);
				throw e;
			}
		}

		insert = con.prepareStatement("INSERT INTO "+TABLE+" (id, firstName, lastName, email, login, password,created, recoverID, lang) VALUES(?,?,?,?,?,?,now(),?,?)");
		selectByUUID  = con.prepareStatement("SELECT * FROM "+TABLE+" WHERE id=?");
		selectByEmail = con.prepareStatement("SELECT * FROM "+TABLE+" WHERE email=?");
		selectByLogin = con.prepareStatement("SELECT * FROM "+TABLE+" WHERE login=?");
		update = con.prepareStatement("UPDATE "+TABLE+" SET firstName=?, lastName=?, email=?, login=?, password=?, verified=?, vCode=?, recoverID=?, lang=? WHERE id=?");
		verify = con.prepareStatement("UPDATE "+TABLE+" SET verified=?, recoverID=?");
		delete = con.prepareStatement("DELETE FROM "+TABLE+" WHERE id=?");

	}

	//-------------------------------------------------------------------
	public final static String encodePassword(String passwd) {
		// Encode password
		byte[] encoded = null;
		try {
			encoded = cipherEnc.doFinal(passwd.getBytes("UTF-8"));
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed encoding password",e);
			return null;
		}
		String encStr  = Base64.getEncoder().encodeToString(encoded);
		return encStr;
	}

	//-------------------------------------------------------------------
	public PlayerImpl createPlayer(String firstName, String lastName, String email, String login, String passwd, Locale loc) throws SQLException {
		// Encode password
		String encStr  = encodePassword(passwd);
		logger.log(Level.WARNING, "Create encoding password "+encStr);
		UUID   recov   = UUID.randomUUID();

		UUID id = UUID.randomUUID();
		insert.setString(1, id.toString());
		insert.setString(2, firstName);
		insert.setString(3, lastName);
		insert.setString(4, email);
		insert.setString(5, login);
		insert.setString(6, encStr);
		insert.setString(7, recov.toString());
		insert.setString(8, loc.getLanguage());
		logger.log(Level.DEBUG, "SQL "+insert);
		insert.executeUpdate();

		PlayerImpl ret = new PlayerImpl(id);
		ret.setEmail(email);
		ret.setFirstName(firstName);
		ret.setLastName(lastName);
		ret.setLogin(login);
		ret.setPassword(passwd);
		ret.setRecoverUUID(recov);
		ret.setLocale(loc);

		return ret;
	}

	//-------------------------------------------------------------------
	private PlayerImpl getPlayerFromResultSet(ResultSet set) throws SQLException {
		logger.log(Level.TRACE, "ENTER getPlayerFromResultSet({0})",set);
		try {
			PlayerImpl ret = new PlayerImpl(UUID.fromString(set.getString("id")));
		ret.setFirstName(set.getString("firstName"));
		ret.setLastName(set.getString("lastName"));
		ret.setEmail(set.getString("email"));
		ret.setLogin(set.getString("login"));
		if  (set.getString("recoverID")!=null && !set.getString("recoverID").isBlank()) {
			try {
				ret.setRecoverUUID(UUID.fromString(set.getString("recoverID")));
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, "Failed parsing recoverID as UUID for character "+set.getString("id"));
			}
		}
		ret.setLocale(Locale.forLanguageTag(set.getString("lang")));
		// Encode password
		byte[] dec = Base64.getDecoder().decode(set.getString("password").getBytes());
		try {
			ret.setPassword(new String(cipherDec.doFinal(dec)));
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new SQLException("Failed decoding password",e);
		}

		ret.setTimeCreated( set.getTimestamp("created").toInstant());
		ret.setVerificationCode(set.getString("vCode"));
		ret.setVerified(set.getBoolean("verified"));
//		logger.log(Level.WARNING, "getPlayerFromResultset returns "+ret);
		return ret;
		} finally {
			logger.log(Level.TRACE, "LEAVE getPlayerFromResultSet({0})",set);
		}
	}

	//-------------------------------------------------------------------
	public PlayerImpl getPlayer(UUID uuid) throws SQLException {
		selectByUUID.setString(1, uuid.toString());
		ResultSet set = selectByUUID.executeQuery();
		if (set.next()) {
			return getPlayerFromResultSet(set);
		} else
			return null;
	}

	//-------------------------------------------------------------------
	public PlayerImpl getPlayerByEmail(String email) throws SQLException {
		selectByEmail.setString(1, email);
		ResultSet set = selectByEmail.executeQuery();
		if (set.next()) {
			return getPlayerFromResultSet(set);
		} else
			return null;
	}

	//-------------------------------------------------------------------
	public PlayerImpl getPlayerByLogin(String login) throws SQLException {
		PlayerImpl ret = null;

		try {
			synchronized(selectByLogin) {
				selectByLogin.setString(1, login);
				logger.log(Level.INFO, "Execute SQL "+selectByLogin);
				ResultSet set = selectByLogin.executeQuery();
				if (set.next()) {
					ret = getPlayerFromResultSet(set);
				}
				set.close();
			}
		} finally {
			logger.log(Level.DEBUG, "getPlayerByLogin({0}) returns {1}", login, ret);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public void updatePlayer(PlayerImpl player) throws SQLException {
		update.setString(1, player.getFirstName());
		update.setString(2, player.getLastName());
		update.setString(3, player.getEmail());
		update.setString(4, player.getLogin());
		update.setString(5, encodePassword(player.getPassword()));
		update.setInt   (6, player.isVerified()?1:0);
		update.setString(7, player.getVerificationCode());
		update.setString(8, (player.getRecoverUUID()!=null)?String.valueOf(player.getRecoverUUID()):null);
		update.setString(9, player.getLocale().getLanguage());
		update.setString(10, player.getUuid().toString());
		logger.log(Level.DEBUG, "SQL "+update);
		update.executeUpdate();
	}

//	//-------------------------------------------------------------------
//	public boolean verifyPlayer(Player value, UUID recoverUUID) throws SQLException {
//		PlayerImpl player = (PlayerImpl)value;
//		if (player.getRecoverUUID()==null || !player.getRecoverUUID().equals(recoverUUID)) {
//			logger.log(Level.DEBUG, "Verification failed for player "+value.getLogin()+": given UUID "+recoverUUID+" does not match "+player.getRecoverUUID());
//			return false;
//		}
//		verify.setInt   (1, player.isVerified()?1:0);
//		verify.setString(2, (player.getRecoverUUID()!=null)?String.valueOf(player.getRecoverUUID()):null);
//		logger.log(Level.DEBUG, "SQL "+verify);
//		verify.executeUpdate();
//		return true;
//	}

	//-------------------------------------------------------------------
	public void deletePlayer(PlayerImpl player) throws SQLException {
		delete.setString(1, player.getUuid().toString());
		logger.log(Level.DEBUG, "SQL "+delete);
		delete.executeUpdate();
	}

}
