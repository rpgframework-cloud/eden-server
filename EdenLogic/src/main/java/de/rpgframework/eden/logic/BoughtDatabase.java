package de.rpgframework.eden.logic;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author prelle
 *
 */
public class BoughtDatabase {

	private final static Logger logger = BackendAccess.logger;

	public final static String TABLE = "BoughtItems";

	private final static String CREATE_SQL = "CREATE TABLE "+TABLE+" ("
			+"\n  playerID   VARCHAR(40) NOT NULL,"
			+"\n  catalogID  VARCHAR(20) NOT NULL,"
			+"\n  timestamp  TIMESTAMP   NOT NULL,"
			+"\n  price      INT,"
			+"\n  transaction VARCHAR(255),"
			+"\n  license    VARCHAR(40),"
			+"\n PRIMARY KEY(playerID,catalogID),"
			+"\n FOREIGN KEY(catalogID) REFERENCES "+ShopDatabase.TABLE+"(id),"
			+"\n FOREIGN KEY(playerID) REFERENCES "+PlayerDatabase.TABLE+"(id)"
			+"\n)";

	private Connection con;
	private PreparedStatement select;
	private PreparedStatement insert;
	private PreparedStatement delete;

	//-------------------------------------------------------------------
	public BoughtDatabase(Connection con) throws SQLException {
		this.con = con;
		setup();
	}

	//-------------------------------------------------------------------
	private void setup() throws SQLException {
		Statement stat = con.createStatement();
		try {
			stat.execute("SELECT COUNT(*) FROM "+TABLE);
		} catch (SQLException e) {
			if (e.getSQLState().equals("42501") || e.getSQLState().equals("42S02")) {
				// Table does not exist yet
				logger.log(Level.DEBUG, "Create table "+TABLE);
				try {
					stat.executeUpdate(CREATE_SQL);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				logger.log(Level.WARNING, "sqlState = "+e.getSQLState());
				logger.log(Level.WARNING, "error code = "+e.getErrorCode());
				logger.log(Level.ERROR, "Error was",e);
				throw e;
			}
		}

		select = con.prepareStatement("SELECT * FROM "+TABLE+" WHERE playerID=? ORDER BY timestamp");
		insert = con.prepareStatement("INSERT INTO "+TABLE+" VALUES (?,?,?,?,?,?)");
		delete = con.prepareStatement("DELETE FROM "+TABLE+" WHERE playerID=? AND catalogID=?");
	}

	//-------------------------------------------------------------------
	private BoughtItem getHandleFromResultSet(ResultSet set) throws SQLException {
		BoughtItem ret = new BoughtItem(
				UUID.fromString(set.getString("playerID")),
				set.getString("catalogID"),
				set.getTimestamp("timestamp").toInstant(),
				set.getInt("price")
				);
		if (set.getString("transaction")!=null)
			ret.setTransactionID(set.getString("transaction"));
		if (set.getString("license")!=null)
			ret.setLicenseKey(UUID.fromString((set.getString("license"))));
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Get all characters from all rule systems
	 * @param player
	 * @return
	 * @throws SQLException
	 */
	public List<BoughtItem> getBoughtItems(PlayerImpl player) throws SQLException {
		List<BoughtItem> ret = new ArrayList<BoughtItem>();

		select.setString(1, player.getUuid().toString());
		ResultSet set = select.executeQuery();
		while (set.next()) {
			ret.add(getHandleFromResultSet(set));
		}

		if (!ret.stream().anyMatch(bought -> bought.getItemID().equals("schattenloads"))) {
			BoughtItem item = new BoughtItem(player.getUuid(), "schattenloads", Instant.now(), 0);
			ret.add(item);
		}

		return ret;
	}

	//-------------------------------------------------------------------
	public void add(BoughtItem item) throws SQLException {
		insert.setString(1, item.getPlayerUUID().toString());
		insert.setString(2, item.getItemID());
		insert.setTimestamp(3, Timestamp.from(item.getTimestamp()));
		insert.setInt(4, item.getPrice());
		insert.setString(5, item.getTransactionID());
		insert.setString(6, (item.getLicenseKey()!=null)?item.getLicenseKey().toString():null);
		logger.log(Level.DEBUG, "SEND "+insert);
		insert.executeUpdate();
	}

	//-------------------------------------------------------------------
	public void deleteBoughtItem(BoughtItem data) throws SQLException {
		delete.setString(1, data.getPlayerUUID().toString());
		delete.setString(2, data.getItemID().toString());
		logger.log(Level.INFO, "Call "+delete);
		delete.executeUpdate();
	}

}
