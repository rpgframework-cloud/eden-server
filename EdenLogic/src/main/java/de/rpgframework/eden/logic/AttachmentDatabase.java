package de.rpgframework.eden.logic;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import de.rpgframework.character.Attachment;
import de.rpgframework.character.Attachment.Format;
import de.rpgframework.character.Attachment.Type;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.reality.server.ServerCharacterHandle;

/**
 * @author prelle
 *
 */
public class AttachmentDatabase {

	private final static Logger logger = System.getLogger(AttachmentDatabase.class.getName());

	public final static String TABLE = "Attachments";

	private final static String CREATE_SQL = "CREATE TABLE "+TABLE+" ("
			+"\n  id     VARCHAR(40) NOT NULL,"
			+"\n  charac VARCHAR(40) NOT NULL,"
			+"\n  type   VARCHAR(20) NOT NULL,"
			+"\n  format VARCHAR(32) NOT NULL,"
			+"\n  name   VARCHAR(40) NOT NULL,"
			+"\n  data   BLOB NOT NULL,"
			+"\n  modified  TIMESTAMP NOT NULL,"
			+"\n PRIMARY KEY(id),"
			+"\n FOREIGN KEY(charac) REFERENCES Characters(id)"
			+"\n)";

	private Connection con;
	private PreparedStatement insert;
	private PreparedStatement select;
	private PreparedStatement selectSpecific;
	private PreparedStatement selectByUUID;
	private PreparedStatement update;
	private PreparedStatement delete;

	//-------------------------------------------------------------------
	public AttachmentDatabase(Connection con) throws SQLException {
		this.con = con;
		setup();
	}

	//-------------------------------------------------------------------
	private void setup() throws SQLException {
		Statement stat = con.createStatement();
		try {
			stat.execute("SELECT COUNT(*) FROM "+TABLE);
		} catch (SQLException e) {
			if (e.getSQLState().equals("42501") || e.getSQLState().equals("42S02")) {
				// Table does not exist yet
				logger.log(Level.DEBUG, "Create table "+TABLE);
				try {
					stat.executeUpdate(CREATE_SQL);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				logger.log(Level.WARNING, "sqlState = "+e.getSQLState());
				logger.log(Level.WARNING, "error code = "+e.getErrorCode());
				logger.log(Level.ERROR, "Error was",e);
				throw e;
			}
		}

		insert = con.prepareStatement("INSERT INTO "+TABLE+" (id,charac,type,format,name,data,modified) VALUES(?,?,?,?,?,?,?)");
		select = con.prepareStatement("SELECT * FROM "+TABLE+" WHERE charac=?");
		update = con.prepareStatement("UPDATE "+TABLE+" SET name=?, data=?, modified=? WHERE id=?");
		selectSpecific = con.prepareStatement("SELECT * FROM "+TABLE+" WHERE charac=? AND type=? AND format=?");
		selectByUUID = con.prepareStatement("SELECT * FROM "+TABLE+" WHERE charac=? AND id=?");
		delete = con.prepareStatement("DELETE FROM "+TABLE+" WHERE id=?");
	}

	//--------------------------------------------------------------------
	/**
	 * Adding an attachment to a character. Though the primary focus of this
	 * method is adding attachments only to characters of the local player,
	 * it may also be used for characters of other players as well. If this
	 * works is implementation dependant.
	 *
	 * How many instances (per format) of attachments are allowed, depends
	 * on the type: CHARACTER and BACKGROUND may only exist once, while
	 * REPORT may exist multiple times.
	 *
	 * @param handle Character to modify
	 * @param type   What us described in this attachment
	 * @param format What kind of data is it
	 * @param filename A proposed file name. May be null. May be ignored
	 *   by the implementation
	 * @param data Binary data of the attachment
	 * @return The created attachment
	 * @throws  IOException Error executing operation
	 */
	public void addAttachment(ServerCharacterHandle handle, Attachment value) throws SQLException {
		if (handle==null)
			throw new NullPointerException("CharacerHandle is NULL");
		UUID uuid = (value.getID()!=null) ? value.getID() : UUID.randomUUID();
		if (value.getID()==null)
			value.setID(uuid);
		long time = (value.getLastModified()!=null)?value.getLastModified().getTime():System.currentTimeMillis();
		insert.setString(1, uuid.toString());
		insert.setString(2, handle.getUUID().toString());
		insert.setString(3, value.getType().name());
		insert.setString(4, value.getFormat().name());
		insert.setString(5, value.getFilename());
		insert.setBytes(6, value.getData());
		insert.setTimestamp(7, new Timestamp(time));
		insert.executeUpdate();
		logger.log(Level.INFO, "Created attachment '"+value.getFilename()+"' for character '"+handle.getName()+"'");

		BabylonEventBus.fireEvent(BabylonEventType.CHAR_MODIFIED, handle);
	}

	//-------------------------------------------------------------------
	private Attachment parseResultSet(ServerCharacterHandle handle, ResultSet set) throws SQLException {
		UUID attachUUID = UUID.fromString(set.getString(1));
//		UUID characUUID = UUID.fromString(set.getString(2));
		Type type     = Type.valueOf(set.getString(3));
		Format format = Format.valueOf(set.getString(4));
		String filename = set.getString(5);
		byte[] data     = set.getBytes(6);
		Date   modified = new Date(set.getTimestamp(7).toInstant().toEpochMilli());

		Attachment attach = new Attachment(handle, attachUUID, type, format);
		attach.setFilename(filename);
		attach.setData(data);
		attach.setLastModified(modified);
		return attach;
	}

	//-------------------------------------------------------------------
	/**
	 * @param handle
	 * @param type
	 * @return
	 */
	public List<Attachment> getAttachments(ServerCharacterHandle handle) {
		List<Attachment> list = new ArrayList<Attachment>();
		ResultSet set = null;
		try {
			select.setString(1, handle.getUUID().toString());
			set = select.executeQuery();
			while (set.next()) {
				list.add(parseResultSet(handle, set));
			}
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed reading from database for character "+handle.getUUID(),e);
		} finally {
			try { set.close();  } catch (SQLException e) {}
		}
		return list;
	}

	//-------------------------------------------------------------------
	public Attachment getAttachment(ServerCharacterHandle handle, UUID uuid) {
		ResultSet set = null;
		try {
			selectByUUID.setString(1, handle.getUUID().toString());
			selectByUUID.setString(2, uuid.toString());
			logger.log(Level.TRACE, "SQL = "+selectByUUID);
			set = selectByUUID.executeQuery();
			while (set.next()) {
				return parseResultSet(handle, set);
			}
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed reading from database for character "+handle.getUUID(),e);
		} finally {
			try { set.close();  } catch (SQLException e) {}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public Attachment getAttachment(ServerCharacterHandle handle, Type type, Format format) {
		ResultSet set = null;
		try {
			selectSpecific.setString(1, handle.getUUID().toString());
			selectSpecific.setString(2, type.name());
			selectSpecific.setString(3, format.name());
			set = selectSpecific.executeQuery();
			while (set.next()) {
				return parseResultSet(handle, set);
			}
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed reading from database for character "+handle.getUUID(),e);
		} finally {
			try { set.close();  } catch (SQLException e) {}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void update(Attachment data) throws SQLException {
		update.setString(1, data.getFilename());
		update.setBytes(2, data.getData());
		update.setTimestamp(3, new Timestamp(data.getLastModified().getTime()));
		update.setString(4, data.getID().toString());
		update.executeUpdate();
		logger.log(Level.DEBUG, "Updated attachment {0}/{2} of character {1}", data.getID(), data.getParent().getUUID(), data.getFilename());
	}

	//-------------------------------------------------------------------
	public void delete(Attachment data) throws SQLException {
		delete.setString(1, data.getID().toString());
		logger.log(Level.TRACE, "Call {0}",delete);
		delete.executeUpdate();
	}

}
