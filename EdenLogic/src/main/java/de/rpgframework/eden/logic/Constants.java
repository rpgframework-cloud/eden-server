package de.rpgframework.eden.logic;

/**
 * @author prelle
 *
 */
public class Constants {

	public final static String ATTRIB_BACKEND = BackendAccess.class.getName();
	public final static String ATTRIB_LOGIC = AccountLogic.class.getName();

}
