package de.rpgframework.eden.logic;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.UUID;

import de.rpgframework.reality.ActivationKey;

/**
 * @author prelle
 *
 */
public class ActivationKeyDatabase {
	
	private final static Logger logger = BackendAccess.logger;
	
	public final static String TABLE = "ActivationKeys";
	
	private final static String CREATE_SQL = "CREATE TABLE "+TABLE+" ("
			+"\n  id         VARCHAR(40) NOT NULL,"
			+"\n  catalogID  VARCHAR(20) NOT NULL,"
			+"\n  timestamp  TIMESTAMP   NOT NULL,"
			+"\n PRIMARY KEY(id),"
			+"\n FOREIGN KEY(catalogID) REFERENCES "+ShopDatabase.TABLE+"(id)"
			+"\n)";

	private Connection con;
	private PreparedStatement select;
	private PreparedStatement insert;
	private PreparedStatement delete;
	
	//-------------------------------------------------------------------
	public ActivationKeyDatabase(Connection con) throws SQLException {
		this.con = con;
		setup();
	}
	
	//-------------------------------------------------------------------
	private void setup() throws SQLException {
		Statement stat = con.createStatement();
		try {
			stat.execute("SELECT COUNT(*) FROM "+TABLE);
		} catch (SQLException e) {
			if (e.getSQLState().equals("42501") || e.getSQLState().equals("42S02")) {
				// Table does not exist yet
				logger.log(Level.DEBUG, "Create table "+TABLE);
				try {
					stat.executeUpdate(CREATE_SQL);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				logger.log(Level.WARNING, "sqlState = "+e.getSQLState());
				logger.log(Level.WARNING, "error code = "+e.getErrorCode());
				logger.log(Level.ERROR, "Error was",e);
				throw e;
			}
		}
		
		select = con.prepareStatement("SELECT * FROM "+TABLE+" WHERE id=? ORDER BY timestamp");
		insert = con.prepareStatement("INSERT INTO "+TABLE+" VALUES (?,?,?)");
		delete = con.prepareStatement("DELETE FROM "+TABLE+" WHERE id=?");
	}
	
	//-------------------------------------------------------------------
	private ActivationKey getHandleFromResultSet(ResultSet set) throws SQLException {
		ActivationKey ret = new ActivationKey(
				UUID.fromString(set.getString("id")), 
				set.getString("catalogID"), 
				set.getTimestamp("timestamp").toInstant()
				);
		return ret;
	}
	
	//-------------------------------------------------------------------
	/**
	 * Get all characters from all rule systems
	 * @param player
	 * @return
	 * @throws SQLException
	 */
	public ActivationKey get(UUID key) throws SQLException {

		select.setString(1, key.toString());
		ResultSet set = select.executeQuery();
		try {
			while (set.next()) {
				return getHandleFromResultSet(set);
			}
		} finally {
			set.close();
		} 
		
		return null;
	}
	
	//-------------------------------------------------------------------
	public ActivationKey add(ActivationKey item) throws SQLException {
		insert.setString(1, item.getID().toString());
		insert.setString(2, item.getItemID());
		insert.setTimestamp(3, Timestamp.from(item.getTimestamp()));
		insert.executeUpdate();
		return item;
	}

	//-------------------------------------------------------------------
	public void remove(ActivationKey data) throws SQLException {
		delete.setString(1, data.getID().toString());
		delete.executeUpdate();
	}
	
}
