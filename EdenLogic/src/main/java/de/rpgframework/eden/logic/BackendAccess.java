package de.rpgframework.eden.logic;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import de.rpgframework.ResourceI18N;
import de.rpgframework.eden.base.MailerLoader;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author prelle
 *
 */
public class BackendAccess {

	final static Logger logger = System.getLogger("eden.db");

	private final static ResourceBundle RES = ResourceBundle.getBundle(BackendAccess.class.getName());

	private final static int DEFAULT_BACKEND_PORT = 8721;

	public final static String CFG_JDBC_URL   = "jdbc.url";
	public final static String CFG_JDBC_USER  = "jdbc.user";
	public final static String CFG_JDBC_PASS  = "jdbc.pass";
	public final static String CFG_SERVER_PORT= "server.port";
	public final static String CFG_SERVER_HOST= "server.hostname";

	private static BackendAccess instance;
	private static String hostportFrontend;
	private static String hostportBackend = String.valueOf(DEFAULT_BACKEND_PORT);

	private Connection con;
	private PlayerDatabase playerDB;
	private CharacterDatabase characDB;
	private AttachmentDatabase attachDB;
	private ShopDatabase shopDB;
	private BoughtDatabase boughtDB;
	private ActivationKeyDatabase activationDB;
	private LicenseLogic licenseLogic;

	//-------------------------------------------------------------------
	public static void setInstance(BackendAccess value) { instance = value; }
	public static BackendAccess getInstance() { return instance; }

	//-------------------------------------------------------------------
	public static void setHostPortFrontend(String value) { hostportFrontend = value; }
	public static String getHostPortFrontend() { return hostportFrontend; }

	//-------------------------------------------------------------------
	public static void setHostPortBackend(String value) { hostportBackend = value; }
	public static String getHostPortBackend() { return hostportBackend; }

	//-------------------------------------------------------------------
	public BackendAccess(String jdbcURL, String user, String pass) throws SQLException {
		logger.log(Level.TRACE, "ENTER BackendAccess.<init>");
		try {
			Properties pro = new Properties();
			pro.setProperty(BackendAccess.CFG_JDBC_URL, jdbcURL);
			pro.setProperty(BackendAccess.CFG_JDBC_USER, user);
			pro.setProperty(BackendAccess.CFG_JDBC_PASS, pass);
			con = DriverManager.getConnection(jdbcURL, user, pass);
			setupDatabaseKeepAlive();

			playerDB = new PlayerDatabase(con);
			characDB = new CharacterDatabase(con);
			attachDB = new AttachmentDatabase(con);
			shopDB   = new ShopDatabase(con);
			boughtDB = new BoughtDatabase(con);
			activationDB = new ActivationKeyDatabase(con);
			licenseLogic = new LicenseLogic(this);

			setInstance(this);
		} finally {
			logger.log(Level.TRACE, "LEAVE BackendAccess.<init>");
		}
	}

	//-------------------------------------------------------------------
	public BackendAccess(Properties config) throws SQLException {
		logger.log(Level.TRACE, "ENTER BackendAccess.<init>");
		try {
			String jdbcURL = config.getProperty(CFG_JDBC_URL, "localhost");
			String user    = config.getProperty(CFG_JDBC_USER);
			String pass    = config.getProperty(CFG_JDBC_PASS);
			if (user==null || pass==null)
				throw new IllegalArgumentException("Missing JDBC user and/or password in properties");

			logger.log(Level.INFO, "Connect as {0} to database at {1}", user, jdbcURL);
			System.err.println("Connect as "+user+" to database at "+jdbcURL);
			con = DriverManager.getConnection(jdbcURL, user, pass);
			setupDatabaseKeepAlive();

			playerDB = new PlayerDatabase(con);
			characDB = new CharacterDatabase(con);
			attachDB = new AttachmentDatabase(con);
			shopDB   = new ShopDatabase(con);
			boughtDB = new BoughtDatabase(con);
			activationDB = new ActivationKeyDatabase(con);
			licenseLogic = new LicenseLogic(this);

			setInstance(this);
		} finally {
			logger.log(Level.TRACE, "LEAVE BackendAccess.<init>");
		}
	}

	//-------------------------------------------------------------------
	private void connectToDatabase() {

	}

	//-------------------------------------------------------------------
	private void setupDatabaseKeepAlive() {
		// Interval for keep alive
		Timer timer = new Timer("DatabaseKeepAlive",true);
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				Statement stat = null;
				if (con==null) {
					logger.log(Level.ERROR, "Missing database connection");
					return;
				}
				try {
					stat = con.createStatement();
					ResultSet set = stat.executeQuery("SELECT * FROM "+PlayerDatabase.TABLE+" LIMIT 1");
					set.close();
				} catch (SQLException e) {
					logger.log(Level.WARNING, "Error in SQL keep alive",e);
				} finally {
					try { stat.close(); } catch (Exception e) {}
				}

			}}, 5*60000, 5*60000);
	}

	//-------------------------------------------------------------------
	public Connection getInternalConnection() {
		return con;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the playerDB
	 */
	public PlayerDatabase getPlayerDatabase() {
		return playerDB;
	}

	//-------------------------------------------------------------------
	public CharacterDatabase getCharacterDatabase() {
		return characDB;
	}

	//-------------------------------------------------------------------
	public AttachmentDatabase getAttachmentDatabase() {
		return attachDB;
	}

	//-------------------------------------------------------------------
	public ShopDatabase getShopDatabase() {
		return shopDB;
	}

	//-------------------------------------------------------------------
	public BoughtDatabase getLicenseDatabase() {
		return boughtDB;
	}

	//-------------------------------------------------------------------
	public LicenseLogic getLicenseLogic() {
		return licenseLogic;
	}

	//-------------------------------------------------------------------
	public ActivationKeyDatabase getActivationKeyDatabase() {
		return activationDB;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public Player createPlayer(String firstName, String lastName, String email, String login, String passwd, Locale lang) throws Exception {
		// Check if either email or login already exists
		PlayerImpl account = playerDB.getPlayerByLogin(login);
		if (account!=null)
			throw new RuntimeException("Account already exists");
		account = playerDB.getPlayerByEmail(email);
		if (account!=null)
			throw new RuntimeException("Account already exists");

		// Create
		account = playerDB.createPlayer(firstName, lastName, email, login, passwd, lang);
		logger.log(Level.INFO, "New account created: "+login+"  for "+email);

		String text = ResourceI18N.format(RES, "mail.template.new_account", firstName, lastName, login, "http://"+getHostPortFrontend()+"/extern/verify?id="+account.getUuid()+"&id2="+account.getRecoverUUID());

		if (MailerLoader.getInstance()!=null) {
			Thread worker = new Thread(new Runnable() {
				public void run() {
					String error = MailerLoader.getInstance().send(
							"eden@rpgframework.de",
							Arrays.asList(email),
							Collections.EMPTY_LIST,
							"Eden: Your account has been created",
							text,
							null // html
							);
					if (error==null) {
						logger.log(Level.DEBUG,"Mail sent to "+email);
					} else {
						logger.log(Level.WARNING, "Sending mail to '"+email+"' failed due to "+error);
						throw new RuntimeException("Sending mail failed: "+error);
					}
				}});
			worker.start();
		} else {
			logger.log(Level.WARNING, "Cannot send mail - no Mailer found");
		}

		return account;
	}

}
