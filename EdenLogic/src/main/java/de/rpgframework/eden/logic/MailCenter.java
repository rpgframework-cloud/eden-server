package de.rpgframework.eden.logic;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import de.rpgframework.eden.base.Mailer.MimeBody;
import de.rpgframework.eden.base.MailerLoader;
import de.rpgframework.reality.server.PlayerImpl;
import jakarta.mail.internet.MimeBodyPart;

/**
 *
 */
public class MailCenter {

	private static String baseTemplate = null;
	private static String logo = null;
	private static byte[] logoBytes = null;

	//-------------------------------------------------------------------
	static {
		try {
			baseTemplate = new String( MailCenter.class.getResourceAsStream("Skeleton.html").readAllBytes(), StandardCharsets.UTF_8 );

			logoBytes = MailCenter.class.getResourceAsStream("Logo.png").readAllBytes();
			logo = java.util.Base64.getEncoder().encodeToString(logoBytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	public static String send(String to, String subject, String content) {
		String html = baseTemplate.replace("{content}", content);

//		MimeBody logoBody = new MimeBody("image/png", logo, "Logo.png", "base64", "Logo");
		List<String[]> header = new ArrayList<String[]>();
		header.add(new String[] {"Reply-To","eden@rpgframework.de"});
		MimeBody logoBody = new MimeBody("image/png", logoBytes, "Logo.png", "base64", "Logo");
		return MailerLoader.getInstance().send(
				"eden@rpgframework.de",
				List.of(to),
				header,
				subject,
				"Read the HTML content, not this",
				html,
				logoBody);
	}

}
