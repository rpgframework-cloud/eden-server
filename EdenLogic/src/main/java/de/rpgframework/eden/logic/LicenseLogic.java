package de.rpgframework.eden.logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Security;
import java.security.spec.PKCS8EncodedKeySpec;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import org.bouncycastle.bcpg.ArmoredInputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.eden.api.EdenStatus;
import de.rpgframework.reality.ActivationKey;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.CatalogItem;
import de.rpgframework.reality.server.PlayerImpl;

public class LicenseLogic {

	private final static Logger logger = System.getLogger(LicenseLogic.class.getPackageName());

	private final static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(AccountLogic.class.getName(), Locale.ENGLISH, Locale.GERMAN);

	private BackendAccess backend;
	private Cipher rsa;
	private PrivateKey privateKey;

	//-------------------------------------------------------------------
	public LicenseLogic(BackendAccess parent) {
		this.backend = parent;
		// Prepare cryptography
		try {
			Security.addProvider(new BouncyCastleProvider());
			rsa = Cipher.getInstance("RSA","BC");
			privateKey = readKey();
		} catch (IOException e) {
			logger.log(Level.ERROR, "Could not decode public key for licenses",e);
		} catch (Exception e) {
			logger.log(Level.ERROR, "Could not initialize crypto framework - license management not working",e);
		}
	}

	//--------------------------------------------------------------------
	private static PrivateKey readKey() throws IOException {
		try {
			InputStream in = new ArmoredInputStream(ClassLoader.getSystemResourceAsStream("private.key"));
			byte[] encodedPrivateKey = new byte[in.available()];
			in.read(encodedPrivateKey);
			in.close();
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(
					encodedPrivateKey);
			return keyFactory.generatePrivate(privateKeySpec);
		} catch (Exception e) {
			throw new IOException(e);
		}
	}

	//--------------------------------------------------------------------
	public String encode(String toEncode) {
		try {
			rsa.init(Cipher.ENCRYPT_MODE, privateKey);
			byte[] encoded = rsa.doFinal(toEncode.getBytes(StandardCharsets.UTF_8));
			return Base64.getEncoder().encodeToString(encoded);
		} catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
			logger.log(Level.ERROR, "Failed encryption",e);
		}
		return null;
	}


	//-------------------------------------------------------------------
	public LogicResult useActivationKey(UUID playerID, UUID activationKey) {
		try {
			ActivationKey key = backend.getActivationKeyDatabase().get(activationKey);
			if (key==null)
				return new LogicResult(EdenStatus.NO_SUCH_ITEM, "Unknown activation key");

			PlayerImpl player = backend.getPlayerDatabase().getPlayer(playerID);
			if (player==null)
				return new LogicResult(EdenStatus.NO_SUCH_ITEM, "Unknown player");

			CatalogItem catItem = backend.getShopDatabase().getCatalogItem(key.getItemID());
			if (catItem==null)
				return new LogicResult(EdenStatus.INTERNAL_ERROR, "Activation key references unknown item "+key.getItemID());

			BoughtItem item = new BoughtItem(playerID, key.getItemID(), Instant.now(), catItem.getPrice());
			item.setLicenseKey(activationKey);
			item.setName(catItem.getName());
			backend.getLicenseDatabase().add(item);
			logger.log(Level.INFO, "User {0} activated license {1}", player.getLogin(), key.getItemID());

			backend.getActivationKeyDatabase().remove(key);
			logger.log(Level.DEBUG, "Remove {0} as unused activation key", key.getID());

			// TODO: Send email

			return new LogicResult(EdenStatus.OK, item);
		} catch (SQLException e) {
			logger.log(Level.ERROR,  "Error activating license",e);
			return new LogicResult(EdenStatus.INTERNAL_ERROR, e.toString());
		}
	}

	//-------------------------------------------------------------------
	public LogicResult getLicensedCatalogItems(PlayerImpl player) {
		BoughtDatabase licenses = BackendAccess.getInstance().getLicenseDatabase();
		try {
			return new LogicResult(EdenStatus.OK, licenses.getBoughtItems(player));
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error getting licensed datasets",e);
			return new LogicResult(EdenStatus.INTERNAL_ERROR, e.toString());
		}
	}

	//-------------------------------------------------------------------
	public LogicResult getLicensedDatasets(PlayerImpl player) {
		ShopDatabase shop = BackendAccess.getInstance().getShopDatabase();
		BoughtDatabase licenses = BackendAccess.getInstance().getLicenseDatabase();
		List<String> datasets = new ArrayList<>();

		try {
			List<BoughtItem> list = licenses.getBoughtItems(player);
			for (BoughtItem bought : list) {
				logger.log(Level.INFO, "bought item {0}", bought);
				CatalogItem item = shop.getCatalogItem(bought.getItemID());
				if (item==null) {
					logger.log(Level.ERROR, "Bought item {0} references unknown catalog item {1}", bought.getTransactionID(), bought.getItemID());
				} else {
					item.getDatasets().forEach( ds -> {
						if (!datasets.contains(ds)) {
							datasets.add(item.getRules().name()+"/"+ds+"/"+item.getLanguage());
						}
					});
				}
			}
			return new LogicResult(EdenStatus.OK, datasets);
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error getting licensed datasets",e);
			return new LogicResult(EdenStatus.INTERNAL_ERROR, e.toString());
		}
	}

}
