package de.rpgframework.eden.base;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @author prelle
 *
 */
public interface Mailer {

	public final static String CFG_MAIL_HOST   = "mail.host";
	public final static String CFG_MAIL_PREFIX = "mail.prefix";
	public final static String CFG_MAIL_USER   = "mail.user";
	public final static String CFG_MAIL_PASS   = "mail.pass";

	public class MimeBody {
		String mimeType;
		String content;
		byte[] contentBytes;
		String filename;
		String desc;
		String contentID;
		public MimeBody(String type, String content, String filename, String desc) {
			this.mimeType = type;
			this.content  = content;
			try {
				this.contentBytes = content.getBytes("UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			this.filename = filename;
			this.desc     = desc;
		}
		public MimeBody(String type, String content, String filename, String desc, String cid) {
			this(type, content, filename, desc);
			this.contentID = cid;
		}
		public MimeBody(String type, byte[] content, String filename, String desc) {
			this.mimeType = type;
			this.contentBytes  = content;
			this.filename = filename;
			this.desc     = desc;
		}
		public MimeBody(String type, byte[] content, String filename, String desc, String cid) {
			this(type, content, filename, desc);
			this.contentID = cid;
		}
		public String getMimeType() { return mimeType; }
		public String getContent() {return content;}
		public byte[] getContentBytes() {return contentBytes;}
		public String getFilename() { return filename; }
		public String getDescription() {return desc;}
		public String getContentID() {return contentID;}
	}

	//--------------------------------------------------------------
	/**
	 * Send a mail with a plain text content
	 *
	 * @param fromAddr
	 * @param toAddr
	 * @param header Extra headers to add
	 * @param subject Mail subject
	 * @param plain  Mail content
	 * @return NULL if mail has been sent successful otherwise error message
	 */
	public String send(String fromAddr, List<String> toAddr, List<String[]> header, String subject, String text, String html, MimeBody... attachments);

}
