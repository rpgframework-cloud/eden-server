package de.rpgframework.eden.base;

/**
 * @author spr
 *
 */
public class MailerLoader {

	private static Mailer instance = null;
	
	//-----------------------------------------------------------------
	public static Mailer getInstance() {
		return instance;
	}
	
	public static void setInstance(Mailer mailer) { instance = mailer; }

}
