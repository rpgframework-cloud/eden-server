package de.rpgframework.eden.base;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import jakarta.activation.CommandMap;
import jakarta.activation.DataHandler;
import jakarta.activation.MailcapCommandMap;
import jakarta.mail.Authenticator;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.SendFailedException;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import jakarta.mail.internet.MimePart;
import jakarta.mail.util.ByteArrayDataSource;

/**
 * @author prelle
 *
 */
public class JavaMailMailer implements Mailer {

	private static Logger logger = System.getLogger(JavaMailMailer.class.getPackageName());

	private String mailserverS;
	private Session session;

	/**
	 * Prefix to all mail subjects
	 */
	private String prefix;

	//---------------------------------------------------------
	/**
	 */
	public JavaMailMailer(Properties config) {
		if (logger.isLoggable(Level.TRACE)) {
			List<String> keys = new ArrayList<String>();
			config.keySet().forEach(k -> keys.add( (String)k));
			Collections.sort(keys);
			for (String k : keys) {
				logger.log(Level.TRACE, "  "+k+" = "+config.getProperty(k));
			}
		}

		mailserverS = config.getProperty(Mailer.CFG_MAIL_HOST, "smtp.gmail.com");
		prefix = config.getProperty(Mailer.CFG_MAIL_PREFIX);
		logger.log(Level.INFO, String.format("Mailserver: %s", mailserverS));

		String user = config.getProperty(CFG_MAIL_USER);
		String pass = config.getProperty(CFG_MAIL_PASS);

		Properties props = new Properties();
		try {
			InetAddress mailserver = InetAddress.getByName(mailserverS);
			props.put("mail.smtp.host", mailserver.getHostAddress());
			props.put("mail.host", mailserver.getHostAddress());
		} catch (UnknownHostException e) {
			logger.log(Level.ERROR, "Failed getting mailserver IP from '"+mailserverS+"': "+e);
		}

	    props.put("mail.transport.protocol", "smtp");
	    if (user!=null)
	    	props.put("mail.user", user);
	    props.put("mail.smtp.username", user);
	    props.put("mail.smtp.port", "587");
	    props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.connectiontimeout", "4000");
		props.put("mail.smtp.timeout", "15000");
		props.put("mail.smtp.localhost", "eden.rpgframework.de");
		props.put("mail.debug", "false");

//		props.put("mail.smtp.port", 25);
//		props.put("mail.smtp.connectiontimeout", "4000");
//		props.put("mail.smtp.auth", false);
//		props.put("mail.smtp.ehlo", false);
////		props.put("mail.smtp.socketFactory.port", 465);
////		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
////		props.put("mail.smtp.socketFactory.fallback", "false");
		if (logger.isLoggable(Level.DEBUG))
			logger.log(Level.DEBUG, "Properties = "+props);

		Authenticator auth = new SMTPAuthenticator(user, pass);

		// Get session
		logger.log(Level.INFO, "Configure JavaMail");
		session = Session.getDefaultInstance(props, auth);

		MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
		mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
		mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
		mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
		mc.addMailcap("image/jpeg;; x-java-content-handler=com.sun.mail.handlers.image_jpeg");
		mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed;x-java-fallback-entry=true");
		mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
	       CommandMap.setDefaultCommandMap(mc);
	}

	//-----------------------------------------------------------------
	/**
	 * @see de.qsc.monitoring.Mailer#send(java.lang.String, java.util.List, java.util.List, java.lang.String, de.qsc.monitoring.Mailer.MimeBody[])
	 */
	public String send(String from, List<String> toAddr, List<String[]> header, String subject, String plain, String html, MimeBody... attachments) {
		if (prefix!=null)
			subject = prefix+" "+subject;

        try {
			// Define message
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from, "Eden"));
			for (String to : toAddr)
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress("stefan.prelle@qsc.de"));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress("stefan@prelle.org"));
			message.setSubject(subject);
			try {
				if (html!=null)
					message.setDataHandler(new DataHandler(new ByteArrayDataSource(html, "text/html")));
				message.setDataHandler(new DataHandler(new ByteArrayDataSource(plain, "text/plain")));
//				message.setDataHandler(new DataHandler(new ByteArrayDataSource(html, "multipart/alternative")));
//				message.setDataHandler(new DataHandler(new ByteArrayDataSource(html, "multipart/related")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

//			DataSource ds = FileDataSource dsrc;

			MimeBodyPart textPlain = null;
			MimeBodyPart textHtml  = null;
			List<MimeBodyPart> attachParts = new ArrayList<>();
			if (plain!=null) {
	        	textPlain = new MimeBodyPart();
	        	textPlain.setText(plain, "UTF-8");
			}
			if (html!=null) {
				textHtml = new MimeBodyPart();
				textHtml.setContent(html, "text/html; charset=\"UTF-8\"");
//				textHtml.setContent(html, "text/html");
			}
			// Attachments
			logger.log(Level.DEBUG, "Add "+attachments.length+" attachments");
			for (MimeBody tmp : attachments) {
				MimeBodyPart attach = new MimeBodyPart();
				attach.setFileName(tmp.getFilename());
				if (tmp.getDescription()!=null)
					attach.setDescription(tmp.getDescription());
				if (tmp.getContentID()!=null)
					attach.setContentID("<"+tmp.getContentID()+">");

				if (tmp.getMimeType().startsWith("application")) {
					attach.setDataHandler(new DataHandler(new ByteArrayDataSource(tmp.getContentBytes(), tmp.getMimeType())));
//					attach.setDisposition(MimePart.ATTACHMENT);
					attach.setHeader("Content-Type", tmp.getMimeType()+"; name=\""+tmp.getFilename()+"\"");
					attach.setDataHandler(new DataHandler(new ByteArrayDataSource(tmp.getContentBytes(), tmp.getMimeType())));
				} else {
					attach.setContent(tmp.getContent(), tmp.getMimeType());
					attach.setDataHandler(new DataHandler(new ByteArrayDataSource(tmp.getContentBytes(), tmp.getMimeType())));
//					if (tmp.getContentID()!=null) {
						attach.setDisposition(MimePart.INLINE);
//					} else {
//						attach.setDisposition(MimePart.ATTACHMENT);
//					}
				}
				attachParts.add(attach);
			}

//			MimeMultipart alternative = new MimeMultipart("alternative");
//			if (textHtml !=null) alternative.addBodyPart(textHtml);
//			if (textPlain!=null) alternative.addBodyPart(textPlain);
//
//			MimeBodyPart wrap = new MimeBodyPart();
//			wrap.setContent(alternative);

			MimeMultipart related = new MimeMultipart("related");
			related.addBodyPart(textHtml);
			for (MimeBodyPart attachment : attachParts) {
				related.addBodyPart(attachment);
			}

			message.setContent(related);


			if (header!=null) {
				for (String[] pair : header)
					message.addHeader(pair[0], pair[1]);
			}

			// Send message
			Transport.send(message);
			return null;
		} catch (SendFailedException e) {
			logger.log(Level.ERROR, "Failed sending mail to "+toAddr+": "+e+" // "+e.getCause());
			Exception e2 = (Exception) e.getCause();
			return e2.getMessage().trim();
		} catch (MessagingException e) {
			logger.log(Level.ERROR, "Failed sending mail: "+e+" // "+e.getCause(),e);
			session=null;
			return e.getCause().getMessage().trim();
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed sending mail: "+e,e);
			session=null;
			return e.toString().trim();
		}

	}

}

class SMTPAuthenticator extends Authenticator {

	String username;
	String password;

	//-------------------------------------------------------------------
	public SMTPAuthenticator(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	//-------------------------------------------------------------------
	/**
	 * @see jakarta.mail.Authenticator#getPasswordAuthentication()
	 */
	@Override
	public PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username, password);
	}
}