package de.rpgframework.reality.server;

import java.util.Properties;

/**
 * @author prelle
 *
 */
public interface EdenAPIServer {

	public void configure(Properties config);

	public int getPort();

	public void start(Properties config) throws Exception;

	public void stop() throws Exception;

}
