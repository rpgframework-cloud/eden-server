package de.rpgframework.reality.server;

import java.time.Instant;
import java.util.Locale;
import java.util.UUID;

import de.rpgframework.reality.Player;

/**
 * @author prelle
 *
 */
public class PlayerImpl implements Player {
	
	private UUID   uuid;
	private String first;
	private String last;
	private String email;
	private String login;
	private String vCode;
	private transient String password;
	private transient Instant timeCreated;
	private transient boolean verified;
	private transient Locale locale;
	
	private transient UUID recoverUUID;

	//-------------------------------------------------------------------
	public PlayerImpl(UUID id) {
		this.uuid = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.Player#setFirstName(java.lang.String)
	 */
	@Override
	public Player setFirstName(String value) {
		this.first = value;
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.Player#getFirstName()
	 */
	@Override
	public String getFirstName() {
		return first;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.Player#setLastName(java.lang.String)
	 */
	@Override
	public Player setLastName(String value) {
		this.last = value;
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.Player#getLastName()
	 */
	@Override
	public String getLastName() {
		return last;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.Player#setEmail(java.lang.String)
	 */
	@Override
	public Player setEmail(String value) {
		this.email = value;
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.Player#getEmail()
	 */
	@Override
	public String getEmail() {
		return email;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.Player#setLogin(java.lang.String)
	 */
	@Override
	public Player setLogin(String value) {
		this.login = value;
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.Player#getLogin()
	 */
	@Override
	public String getLogin() {
		return login;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.Player#setPassword(java.lang.String)
	 */
	@Override
	public Player setPassword(String value) {
		this.password = value;
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.Player#getPassword()
	 */
	@Override
	public String getPassword() {
		return password;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the uuid
	 */
	public UUID getUuid() {
		return uuid;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the timeCreated
	 */
	public Instant getTimeCreated() {
		return timeCreated;
	}

	//-------------------------------------------------------------------
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Instant timeCreated) {
		this.timeCreated = timeCreated;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the verified
	 */
	public boolean isVerified() {
		return verified;
	}

	//-------------------------------------------------------------------
	/**
	 * @param verified the verified to set
	 */
	public Player setVerified(boolean verified) {
		this.verified = verified;
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the recoverUUID
	 */
	public UUID getRecoverUUID() {
		return recoverUUID;
	}

	//-------------------------------------------------------------------
	/**
	 * @param recoverUUID the recoverUUID to set
	 */
	public void setRecoverUUID(UUID recoverUUID) {
		this.recoverUUID = recoverUUID;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	//-------------------------------------------------------------------
	/**
	 * @param locale the locale to set
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the vCode
	 */
	public String getVerificationCode() {
		return vCode;
	}

	//-------------------------------------------------------------------
	/**
	 * @param vCode the vCode to set
	 */
	public void setVerificationCode(String vCode) {
		this.vCode = vCode;
	}

}
