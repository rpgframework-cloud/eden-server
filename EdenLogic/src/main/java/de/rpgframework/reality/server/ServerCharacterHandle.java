package de.rpgframework.reality.server;

import java.nio.file.Path;
import java.util.UUID;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class ServerCharacterHandle extends CharacterHandle {
	
	private transient PlayerImpl player;

	//-------------------------------------------------------------------
	public ServerCharacterHandle(PlayerImpl player, UUID uuid, RoleplayingSystem rules, String name) {
		this.player = player;
		this.uuid   = uuid;
		this.name   = name;
		this.rules  = rules;
	}

	//-------------------------------------------------------------------
	public PlayerImpl getPlayer() {
		return player;
	}

	@Override
	public Path getPath() {
		// TODO Auto-generated method stub
		return null;
	}
	
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.character.CharacterHandle#getAttachments()
//	 */
//	@Override
//	public List<Attachment> getAttachments() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.character.CharacterHandle#getAttachments(de.rpgframework.character.CharacterHandle.Type)
//	 */
//	@Override
//	public List<Attachment> getAttachments(Type type) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.character.CharacterHandle#getFirstAttachment(de.rpgframework.character.CharacterHandle.Type, de.rpgframework.character.CharacterHandle.Format)
//	 */
//	@Override
//	public Attachment getFirstAttachment(Type type, Format format) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.character.CharacterHandle#getFirstAttachmentParsed(de.rpgframework.character.CharacterHandle.Type, de.rpgframework.character.CharacterHandle.Format, java.lang.Class)
//	 */
//	@Override
//	public <T> T getFirstAttachmentParsed(Type type, Format format, Class<T> cls) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.character.CharacterHandle#getCharacter()
//	 */
//	@Override
//	public RuleSpecificCharacterObject getCharacter() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.character.CharacterHandle#getPath()
//	 */
//	@Override
//	public Path getPath() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.character.CharacterHandle#getImage()
//	 */
//	@Override
//	public byte[] getImage() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.character.CharacterHandle#setCharacter(de.rpgframework.character.RuleSpecificCharacterObject)
//	 */
//	@Override
//	public void setCharacter(RuleSpecificCharacterObject charac) throws IOException {
//		// TODO Auto-generated method stub
//
//	}


//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.character.CharacterHandle#setLastModified(java.util.Date)
//	 */
//	@Override
//	public void setLastModified(Date timestamp) {
//		// TODO Auto-generated method stub
//
//	}

}
