package de.rpgframework.eden.logic;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.rpgframework.eden.api.EdenStatus;
import de.rpgframework.reality.ActivationKey;

/**
 * @author prelle
 *
 */
class ShopDatabaseTest {

	private Connection c;
	private BackendAccess backend;

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		backend = new BackendAccess("jdbc:hsqldb:mem:testdb", "SA","");
		c = backend.getInternalConnection();
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
//		System.out.println("tearDown--------------------------------");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS ActivationKeys");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS BoughtItems");
		c.createStatement().executeUpdate("DROP TABLE IF EXiSTS Catalog");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS Attachments");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS Characters");
		c.createStatement().executeUpdate("DROP TABLE IF EXiSTS Player");
		c.close();
	}

	//-----------------------------------------------------------------
	private void prepareDatabase() throws SQLException {
		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Catalog (id, category, rules, lang, name, price, state, datasets) VALUES('SR6_DE_GRW_NEW','CHARGEN','SHADOWRUN6','de','Basislizenz',999,'AVAILABLE','CORE')");
		stat.execute("INSERT INTO Catalog (id, category, rules, lang, name, price, state, datasets) VALUES('SR6_DE_FEUER_FREI','CHARGEN','SHADOWRUN6','de','Feuer Frei',999,'AVAILABLE','CORE')");
		stat.execute("INSERT INTO Catalog (id, category, rules, lang, name, price, state, datasets) VALUES('SR6_DE_GRW','CHARGEN','SHADOWRUN6','de','Basislizenz',999,'HIDDEN','CORE')");
		stat.execute("INSERT INTO ActivationKeys (id, catalogID, timestamp) VALUES('898b11ce-58d0-4977-a30e-5b2e0794d6e8','SR6_DE_GRW_NEW',now())");
		stat.execute("INSERT INTO ActivationKeys (id, catalogID, timestamp) VALUES('15d897b1-d225-4cf1-8d29-245366384401','SR6_DE_GRW_NEW',now())");
		stat.execute("INSERT INTO ActivationKeys (id, catalogID, timestamp) VALUES('24e2a04f-2afc-49c1-b87f-52e90c76a8ff','SR6_DE_FEUER_FREI',now())");
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password, lang) VALUES('24e2a04f-2afc-49c1-b87f-52e90c76a900','Test','User','test@invalid.de','manni','XYlRXHvA4QbSwRVG5szjdg==','de')");
		stat.close();
	}

	//-----------------------------------------------------------------
	@Test
	void testActivateDirect() throws SQLException {
		System.out.println("---testActivate");

		prepareDatabase();

		UUID uuid = UUID.fromString("15d897b1-d225-4cf1-8d29-245366384401");
		ActivationKey activationKey = backend.getActivationKeyDatabase().get(uuid);
		assertNotNull(activationKey, "Failed finding activation key");

		backend.getActivationKeyDatabase().remove(activationKey);

		// Verify its gone
		Statement stat = c.createStatement();
		ResultSet set = stat.executeQuery("SELECT * FROM "+ActivationKeyDatabase.TABLE);
		assertTrue(set.next());
		set.close();
		set = stat.executeQuery("SELECT * FROM "+ActivationKeyDatabase.TABLE+" WHERE id='"+uuid.toString()+"'");
		assertFalse(set.next());
		set.close();
		stat.close();
	}

	//-----------------------------------------------------------------
	@Test
	void testActivateIndirect() throws SQLException {
		System.out.println("---testActivateIndirect");

		prepareDatabase();

		UUID key = UUID.fromString("15d897b1-d225-4cf1-8d29-245366384401");
		UUID player = UUID.fromString("24e2a04f-2afc-49c1-b87f-52e90c76a900");

		LogicResult result = backend.getLicenseLogic().useActivationKey(player, key);
		assertNotNull(result);
		assertEquals(EdenStatus.OK.code(), result.status.code());
		ActivationKey activationKey = backend.getActivationKeyDatabase().get(key);
		assertNull(activationKey, "Failed deleting activation key");

		// Verify its gone
		Statement stat = c.createStatement();
		ResultSet set = stat.executeQuery("SELECT * FROM "+ActivationKeyDatabase.TABLE);
		assertTrue(set.next());
		set.close();
		set = stat.executeQuery("SELECT * FROM "+ActivationKeyDatabase.TABLE+" WHERE id='"+key.toString()+"'");
		assertFalse(set.next());
		set.close();
		set = stat.executeQuery("SELECT * FROM "+BoughtDatabase.TABLE);
		assertTrue(set.next());
		set.close();
		stat.close();
	}

}
