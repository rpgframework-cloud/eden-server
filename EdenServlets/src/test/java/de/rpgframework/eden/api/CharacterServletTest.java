package de.rpgframework.eden.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.security.Authenticator;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.security.ServerAuthException;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.security.authentication.DeferredAuthentication;
import org.eclipse.jetty.security.authentication.FormAuthenticator;
import org.eclipse.jetty.security.authentication.SessionAuthentication;
import org.eclipse.jetty.server.Authentication;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.MultiMap;
import org.eclipse.jetty.util.security.Constraint;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.gson.Gson;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.BoughtDatabase;
import de.rpgframework.eden.logic.CharacterDatabase;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.eden.logic.PlayerDatabase;

/**
 * @author spr
 *
 */
class CharacterServletTest {

	private final static Logger logger = LogManager.getLogger("ServerTestPlayer");

	private final static String TESTUSER = "manni";
	private final static String TESTPASS = "muster";

	private static Connection c;
	private static BackendAccess backend;
	private static Server server;

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		backend = new BackendAccess("jdbc:hsqldb:mem:testdb", "SA","");

		server = new Server();
		ServerConnector connector = new ServerConnector(server);
		connector.setPort(8090);
		server.setConnectors(new Connector[] {connector});

		/*
		 * Prepare configuration
		 */
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");
		context.setAttribute(Constants.ATTRIB_BACKEND, backend);


		ServletHandler servletHandler = new ServletHandler();
		context.setServletHandler(servletHandler);
		servletHandler.addServletWithMapping(PlayerServlet.class, "/api/player/*");
		servletHandler.addServletWithMapping(AttachmentServlet.class, "/api/attachment/*");
		servletHandler.addServletWithMapping(CharacterServlet.class, "/api/character/*");

		TestSQLBasedLoginService loginService = new TestSQLBasedLoginService(null);
		Authenticator authenticator = getAuthenticator();
		//		context.setSecurityHandler(createSecurityHandler("FOO", allowedRoles, authenticator, "Realmchen", loginService));
		context.setSecurityHandler(createSecurityHandler("FOO", null, authenticator, "Realmchen", loginService));

		server.setHandler(context);
		server.start();
	}

	//-------------------------------------------------------------------
	private static Authenticator getAuthenticator() {
		return new BasicAuthenticator() {
			@SuppressWarnings("unchecked")
			public Authentication validateRequest(ServletRequest req, ServletResponse res, boolean mandatory) throws ServerAuthException {
				logger.debug("validateRequest(..,..,"+mandatory+")");
				HttpServletRequest request = (HttpServletRequest)req;
				if (!mandatory)
					return new DeferredAuthentication(this);

				HttpSession session = request.getSession(true);
				logger.debug("session="+session);

				// Look for cached authentication
				Authentication authentication = (Authentication) session.getAttribute(SessionAuthentication.__J_AUTHENTICATED);
				logger.debug("authentication="+authentication);
				if (authentication != null) {
					// Has authentication been revoked?
					if (authentication instanceof Authentication.User &&
							_loginService!=null &&
							!_loginService.validate(((Authentication.User)authentication).getUserIdentity()))
					{
						logger.debug("auth revoked {}",authentication);
						session.removeAttribute(SessionAuthentication.__J_AUTHENTICATED);
					} else {
						synchronized (session) {
							String j_uri=(String)session.getAttribute(FormAuthenticator.__J_URI);
							if (j_uri!=null) {
								//check if the request is for the same url as the original and restore
								//params if it was a post
								logger.debug("auth retry {}->{}",authentication,j_uri);
								StringBuffer buf = request.getRequestURL();
								if (request.getQueryString() != null)
									buf.append("?").append(request.getQueryString());

								if (j_uri.equals(buf.toString()))
								{
									MultiMap<String> j_post = (MultiMap<String>)session.getAttribute(FormAuthenticator.__J_POST);
									if (j_post!=null)
									{
										logger.debug("auth rePOST {}->{}",authentication,j_uri);
										Request base_request = Request.getBaseRequest(request);
										base_request.setContentParameters(j_post);
									}
									session.removeAttribute(FormAuthenticator.__J_URI);
									session.removeAttribute(FormAuthenticator.__J_METHOD);
									session.removeAttribute(FormAuthenticator.__J_POST);
								}
							}
						}
						logger.trace("auth {}",authentication);
						return authentication;
					}
				}
				return super.validateRequest(req, res, mandatory);
			}
		};
	}

	//-------------------------------------------------------------------
	protected static ConstraintSecurityHandler createSecurityHandler(String constraintName,
			String[] allowedRoles, Authenticator authenticator, String realm,
			LoginService loginService) {

		Constraint constraint = new Constraint();
		constraint.setName(constraintName);
		constraint.setRoles(new String[] {Roles.VALID_USER.name()});
		// This is telling Jetty to not allow unauthenticated requests through (very important!)
		constraint.setAuthenticate(true);

		ConstraintMapping cm = new ConstraintMapping();
		cm.setConstraint(constraint);
		cm.setPathSpec("/*");

		ConstraintSecurityHandler sh = new ConstraintSecurityHandler();
		sh.setAuthenticator(authenticator);
		sh.setLoginService(loginService);
		sh.setConstraintMappings(new ConstraintMapping[]{cm});
		sh.setRealmName(realm);

		return sh;
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS Attachments");
		c.createStatement().executeUpdate("DELETE FROM "+CharacterDatabase.TABLE);
		c.createStatement().executeUpdate("DELETE FROM "+BoughtDatabase.TABLE);
		c.createStatement().executeUpdate("DELETE FROM "+PlayerDatabase.TABLE);
		c.close();
		
		server.stop();
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		//		backend = new BackendAccess("jdbc:hsqldb:mem:testdb", "SA","");
		c = backend.getInternalConnection();
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
		c.createStatement().executeUpdate("DELETE FROM "+CharacterDatabase.TABLE);
		c.createStatement().executeUpdate("DELETE FROM "+PlayerDatabase.TABLE);
	}

	//-----------------------------------------------------------------
	@SuppressWarnings("unchecked")
	@Test
	void testList() throws Exception {
		// Write data to read
		UUID uuid = UUID.randomUUID();

		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password, lang) VALUES('"+uuid+"','Manfred','Mustermann','test@invalid.de','"+TESTUSER+"','"+PlayerDatabase.encodePassword(TESTPASS)+"'),'de'");
		stat.execute("INSERT INTO "+CharacterDatabase.TABLE+" (id, player, rules, name) VALUES('"+UUID.randomUUID()+"','"+uuid+"','SPLITTERMOND','Carimea')");
		stat.execute("INSERT INTO "+CharacterDatabase.TABLE+" (id, player, rules, name) VALUES('"+UUID.randomUUID()+"','"+uuid+"','SHADOWRUN6','Hello')");
		stat.execute("INSERT INTO "+CharacterDatabase.TABLE+" (id, player, rules, name) VALUES('"+UUID.randomUUID()+"','"+uuid+"','SPLITTERMOND','World')");
		stat.close();


		URL url = new URL("http://localhost:8090/api/character/");
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("GET");

		assertEquals(200, con.getResponseCode());

		List<CharacterHandle> handles = new ArrayList<CharacterHandle>();
		handles = (new Gson()).fromJson(new InputStreamReader(con.getInputStream()), handles.getClass());
		assertEquals(3, handles.size(), "Expected non-empty list");
	}

	//-----------------------------------------------------------------
	@Test
	void testAdd() throws Exception {
		// Write data to read
		UUID uuid = UUID.randomUUID();

		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password, lang) VALUES('"+uuid+"','Manfred','Mustermann','test@invalid.de','"+TESTUSER+"','"+PlayerDatabase.encodePassword(TESTPASS)+"'),'de'");
		stat.execute("INSERT INTO "+CharacterDatabase.TABLE+" (id, player, rules, name) VALUES('"+UUID.randomUUID()+"','"+uuid+"','SPLITTERMOND','Carimea')");
		stat.execute("INSERT INTO "+CharacterDatabase.TABLE+" (id, player, rules, name) VALUES('"+UUID.randomUUID()+"','"+uuid+"','SHADOWRUN6','Hello')");
		stat.close();

		// Prepare data to POST
		String name = "Hans Meier";
		RoleplayingSystem rules = RoleplayingSystem.CORIOLIS;
		

		URL url = new URL(String.format("http://localhost:8090/api/character/?name=%s&rules=%s",
				URLEncoder.encode(name, "UTF-8"), rules.name()));
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("POST");

		assertEquals(200, con.getResponseCode());

		CharacterHandle handle = (new Gson()).fromJson(new InputStreamReader(con.getInputStream()), CharacterHandle.class);
		assertNotNull(handle,"No character handle returned on POST");
		
		stat = c.createStatement();
		ResultSet set = stat.executeQuery("SELECT * FROM "+CharacterDatabase.TABLE+" WHERE player='"+uuid+"' AND name='"+handle.getName()+"'");
		assertTrue(set.next(), "Character not written do database");
		stat.close();
		
		// Now, writing another character with the same name should fail
		con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("POST");

		assertEquals(409, con.getResponseCode());
	}

	//-----------------------------------------------------------------
	@Test
	void testGet() throws Exception {
		// Write data to read
		UUID playerUUID = UUID.randomUUID();
		UUID charUUID = UUID.randomUUID();

		Statement stat = c.createStatement();
		stat.execute("INSERT INTO "+PlayerDatabase.TABLE+" (id, firstName, lastName, email, login, password,lang) VALUES('"+playerUUID+"','Manfred','Mustermann','test@invalid.de','"+TESTUSER+"','"+PlayerDatabase.encodePassword(TESTPASS)+"'),'de'");
		stat.execute("INSERT INTO "+CharacterDatabase.TABLE+" (id, player, rules, name) VALUES('"+charUUID+"','"+playerUUID+"','SPLITTERMOND','Carimea')");
		stat.close();

		// Get character
		URL url = new URL("http://localhost:8090/api/character/"+charUUID);
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("GET");

		assertEquals(200, con.getResponseCode());

		CharacterHandle handle = (new Gson()).fromJson(new InputStreamReader(con.getInputStream()), CharacterHandle.class);
		assertNotNull(handle,"No character handle returned on GET");
		
		assertEquals(charUUID, handle.getUUID());
	}

	//-----------------------------------------------------------------
	@Test
	void testRename() throws Exception {
		// Write data to read
		UUID playerUUID = UUID.randomUUID();
		UUID charUUID = UUID.randomUUID();

		Statement stat = c.createStatement();
		stat.execute("INSERT INTO "+PlayerDatabase.TABLE+" (id, firstName, lastName, email, login, password,lang) VALUES('"+playerUUID+"','Manfred','Mustermann','test@invalid.de','"+TESTUSER+"','"+PlayerDatabase.encodePassword(TESTPASS)+"','de')");
		stat.execute("INSERT INTO "+CharacterDatabase.TABLE+" (id, player, rules, name) VALUES('"+charUUID+"','"+playerUUID+"','SPLITTERMOND','Carimea')");
		stat.close();

		/* Load old data */
		URL url = new URL("http://localhost:8090/api/character/"+charUUID);
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("GET");

		assertEquals(200, con.getResponseCode());

		CharacterHandle handle = (new Gson()).fromJson(new InputStreamReader(con.getInputStream()), CharacterHandle.class);
		
		
		// Prepare data to PUT
		handle.setName("Henriette Meier");
		String json = (new Gson()).toJson(handle);

		url = new URL("http://localhost:8090/api/character/"+handle.getUUID());
		con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("PUT");
		con.setDoOutput(true);
		con.getOutputStream().write(json.getBytes("UTF-8"));

		assertEquals(200, con.getResponseCode());

		// Now verify database
		stat = c.createStatement();
		ResultSet set = stat.executeQuery("SELECT * FROM "+CharacterDatabase.TABLE+" WHERE id='"+handle.getUUID()+"'");
		assertTrue(set.next(), "Character not existing do database");
		assertEquals("Henriette Meier",set.getString("name"));
		stat.close();
	}

}
