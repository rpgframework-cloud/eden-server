package de.rpgframework.eden.api;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.security.AbstractLoginService;
import org.eclipse.jetty.security.authentication.SessionAuthentication;
import org.eclipse.jetty.server.UserIdentity;
import org.eclipse.jetty.util.security.Password;

import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author spr
 *
 */
public class TestSQLBasedLoginService extends AbstractLoginService {

	private final static Logger logger = LogManager.getLogger("login.file");
	
	private Map<String, UserIdentity> cache;

	//-----------------------------------------------------------------
	public TestSQLBasedLoginService(Properties pro) {
		super.setFullValidate(false);
		cache = new HashMap<>();
	}
	
	//-----------------------------------------------------------------
	/**
	 * @see org.eclipse.jetty.security.AbstractLoginService#login(java.lang.String, java.lang.Object, javax.servlet.ServletRequest)
	 */
	@Override
	public UserIdentity login(String username, Object credentials, ServletRequest request) {
		logger.info("login '"+username+"'");
		UserIdentity ident = cache.get(username);
		if (ident==null) {
			logger.debug("  not in cache");
			ident = super.login(username, credentials, request);
			logger.debug("result = "+ident);
			if (ident!=null)
				cache.put(username, ident);
		}
		
		if (ident!=null) {
			HttpSession session = ((HttpServletRequest) request).getSession(true);
			logger.debug("session = "+session);
			if (session != null) {
				//	            SessionAuthentication sessionAuth = new SessionAuthentication(getAuthMethod(), user, credentials);
//				SessionAuthentication sessionAuth = new SessionAuthentication("Basic", ident, credentials);
				SessionAuthentication sessionAuth = new SessionAuthentication("Basic", ident, credentials);
				session.setAttribute(SessionAuthentication.__J_AUTHENTICATED, sessionAuth);
			}			
		}
		return ident;
	}
	
//	//-----------------------------------------------------------------
//	/**
//	 * @see org.eclipse.jetty.security.AbstractLoginService#login(java.lang.String, java.lang.Object, javax.servlet.ServletRequest)
//	 */
//	@Override
//	public UserIdentity login(String username, Object credentials, ServletRequest request) {
//		logger.info("login '"+username+"'");
//
//		UserIdentity user = cache.get(username);
//		if (user!=null)
//			return user;
//		
//		UranosUser dbUser = null;
//		try {
//			dbUser = GraphDatabase.getInstance().get(UranosUser.class, username);
//		} catch (DatabaseException e) {
//			logger.error("Failed getting user from database",e);
//			return UserIdentity.UNAUTHENTICATED_IDENTITY;
//		}
//		if (dbUser==null) {
//			logger.warn("Login attempt from unknown user '"+username+"'");
////			QSCUserPrincipal userPrincipal = new QSCUserPrincipal(username, "?", new String[] {"ADMIN","SUPPORT"});
////			user = new DefaultUserIdentity(
////					null, 
////					userPrincipal, 
////					userPrincipal.getRoles());
////			cache.put(username, user);
////			HttpSession session = ((HttpServletRequest) request).getSession(true);
////			if (session != null) {
////				//	            SessionAuthentication sessionAuth = new SessionAuthentication(getAuthMethod(), user, credentials);
////				SessionAuthentication sessionAuth = new SessionAuthentication("Basic", user, credentials);
////				session.setAttribute(SessionAuthentication.__J_AUTHENTICATED, sessionAuth);
////			}
//			return user;
//		}
//
//		String[] roles = dbUser.getRoles().split(",");
//		QSCUserPrincipal userPrincipal = new QSCUserPrincipal(username, dbUser.getFirstName()+" "+dbUser.getLastName(), roles);
//		logger.debug("found user "+userPrincipal);
//		Subject subject = new Subject(false, 
//				new HashSet<Principal>(Arrays.asList(userPrincipal)), null, null);
//		logger.debug(".. HashLoginService would return "+super.login(username, credentials, request));;
//		user = new DefaultUserIdentity(subject, userPrincipal, userPrincipal.getRoles());
////				super.login(username, credentials, request);
//		
//		logger.warn("TODO: check password");
//
//		HttpSession session = ((HttpServletRequest) request).getSession(true);
//		if (session != null) {
//			//	            SessionAuthentication sessionAuth = new SessionAuthentication(getAuthMethod(), user, credentials);
//			SessionAuthentication sessionAuth = new SessionAuthentication("Basic", user, credentials);
//			session.setAttribute(SessionAuthentication.__J_AUTHENTICATED, sessionAuth);
//		}
//
//		return user;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.eclipse.jetty.security.AbstractLoginService#loadRoleInfo(org.eclipse.jetty.security.AbstractLoginService.UserPrincipal)
	 */
	@Override
	protected String[] loadRoleInfo(UserPrincipal user) {
		logger.debug("...loadRoleInfo("+user+")...");
//		try {
//			UranosUser dbUser = GraphDatabase.getInstance().get(UranosUser.class, user.getName());
//			if (dbUser==null) {
//				return new String[0];
////				return new String[] {"ADMIN","SUPPORT"};
//			} else {
//				return dbUser.getRoles().split(",");
//			}
//		} catch (DatabaseException e) {
//			logger.error("Failed getting user from database",e);
//			return new String[0];
//		}
		return new String[] {Roles.VALID_USER.name()};
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.eclipse.jetty.security.AbstractLoginService#loadUserInfo(java.lang.String)
	 */
	@Override
	protected UserPrincipal loadUserInfo(String userName) {
		logger.debug("...loadUserInfo("+userName+")...");
		
		try {
			PlayerImpl dbUser = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(userName);
			UserPrincipal userPrincipal = null;
			if (dbUser==null) {
				logger.warn("Unknown user: "+userName);
				return null;
//				userPrincipal = new QSCUserPrincipal(userName, "?", new String[] {"ADMIN","SUPPORT"});
			} else {
				userPrincipal = new UserPrincipal(
						userName,
						new Password(dbUser.getPassword())
						);
				logger.info("Loaded user "+userPrincipal);
			}
			return userPrincipal;
		} catch (Exception e) {
			logger.error("Failed getting user from database",e);
			return null;
		}
	}

}
