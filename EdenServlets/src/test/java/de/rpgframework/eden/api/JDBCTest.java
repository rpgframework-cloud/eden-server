package de.rpgframework.eden.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.PlayerDatabase;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author spr
 *
 */
class JDBCTest {

	private Connection c;
	private BackendAccess backend;

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		backend = new BackendAccess("jdbc:hsqldb:mem:testdb", "SA","");
		c = backend.getInternalConnection();
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
//		System.out.println("tearDown--------------------------------");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS Attachments");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS Characters");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS BoughtItems");
		c.createStatement().executeUpdate("DROP TABLE IF EXiSTS Player");
		c.close();
	}

	//-----------------------------------------------------------------
	@Test
	void testCreate() throws SQLException {
		PlayerDatabase playerDB = backend.getPlayerDatabase();
		
		Player player = playerDB.createPlayer("Manfred", "Mustermann", "test@invalid.de", "manni","muster", Locale.ENGLISH);
		assertNotNull(player);

		// Verify database
		Statement stat = c.createStatement();
		ResultSet set = stat.executeQuery("SELECT * FROM Player");
		assertTrue(set.next());
		assertEquals("Manfred", set.getString("firstName"));
		assertEquals( ((PlayerImpl)player).getUuid().toString(), set.getString("id"));
		assertFalse(set.next());
		set.close();
		stat.close();
		
		// Try  to create again - should fail
		try {
			playerDB.createPlayer("Manfred", "Mustermann", "test@invalid.de", "manni","muster", Locale.ENGLISH);
			fail("Database did not detect duplicate");
		} catch (SQLException e) {
			assertEquals("23505", e.getSQLState(),"Failed as expected, but with different SQL state");
		}
	}

	//-----------------------------------------------------------------
	@Test
	void testGet() throws SQLException {
		System.out.println("---testGet");
		// Write data to read
		UUID uuid = UUID.randomUUID();
		
		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password, lang) VALUES('"+uuid+"','Manfred','Mustermann','test@invalid.de','manni','XYlRXHvA4QbSwRVG5szjdg==','de')");
		stat.close();
		
		// Read it
		PlayerDatabase playerDB = backend.getPlayerDatabase();
		
		Player player = playerDB.getPlayer(uuid);
		assertNotNull(player);
		assertEquals(uuid, ((PlayerImpl)player).getUuid());
	}
	
}
