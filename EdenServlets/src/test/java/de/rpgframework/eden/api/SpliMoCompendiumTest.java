package de.rpgframework.eden.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.UUID;

import org.prelle.rpgframework.splittermond.data.SplittermondDataPlugin;

import de.rpgframework.eden.foundry.Module;
import de.rpgframework.eden.foundry.splimo.SplittermondCompendiumFactory;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;

public class SpliMoCompendiumTest {

	public static void main(String[] args) throws IOException {
		System.setProperty("logdir", "/tmp");
		Locale.setDefault(Locale.GERMANY);
		SplittermondDataPlugin plugin = new SplittermondDataPlugin();
		plugin.init( );
		
		Player player = new PlayerImpl(UUID.randomUUID());
		Module mod = SplittermondCompendiumFactory.create(player, Locale.GERMAN, false);
		byte[] zip = mod.fos.toByteArray();
		File file = new File("/tmp/splimo.zip");
		if (file.exists())
			file.delete();
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(zip);
		fos.close();
		System.out.println("Written to "+file.getAbsolutePath());
		System.exit(1);
	}

}
