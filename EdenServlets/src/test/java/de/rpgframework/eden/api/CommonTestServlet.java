package de.rpgframework.eden.api;

import java.sql.Connection;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.security.Authenticator;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.security.ServerAuthException;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.security.authentication.DeferredAuthentication;
import org.eclipse.jetty.security.authentication.FormAuthenticator;
import org.eclipse.jetty.security.authentication.SessionAuthentication;
import org.eclipse.jetty.server.Authentication;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.MultiMap;
import org.eclipse.jetty.util.security.Constraint;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import de.rpgframework.eden.backend.EdenBackend;
import de.rpgframework.eden.backend.EdenBackend.ServletWithMapping;
import de.rpgframework.eden.logic.ActivationKeyDatabase;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.BoughtDatabase;
import de.rpgframework.eden.logic.CharacterDatabase;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.eden.logic.PlayerDatabase;
import de.rpgframework.eden.logic.ShopDatabase;

/**
 * @author spr
 *
 */
class CommonTestServlet {

	protected final static Logger logger = LogManager.getLogger("BackendTest");

	protected final static String TESTUSER = "manni";
	protected final static String TESTPASS = "muster";

	protected static Connection c;
	protected static BackendAccess backend;
	protected static Server server;

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
		backend = EdenBackend.initialize("jdbc:hsqldb:mem:testdb", "SA","");

		server = new Server();
		ServerConnector connector = new ServerConnector(server);
		connector.setPort(8090);
		server.setConnectors(new Connector[] {connector});

		/*
		 * Prepare configuration
		 */
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");
		context.setAttribute(Constants.ATTRIB_BACKEND, backend);


		ServletHandler servletHandler = new ServletHandler();
		context.setServletHandler(servletHandler);
		for (ServletWithMapping map : EdenBackend.getServlets()) {
			servletHandler.addServletWithMapping(map.servlet, map.path);
		}

		TestSQLBasedLoginService loginService = new TestSQLBasedLoginService(null);
		Authenticator authenticator = getAuthenticator();
		//		context.setSecurityHandler(createSecurityHandler("FOO", allowedRoles, authenticator, "Realmchen", loginService));
		context.setSecurityHandler(createSecurityHandler("FOO", null, authenticator, "Realmchen", loginService));

		server.setHandler(context);
		server.start();
	}

	//-------------------------------------------------------------------
	private static Authenticator getAuthenticator() {
		return new BasicAuthenticator() {
			@SuppressWarnings("unchecked")
			public Authentication validateRequest(ServletRequest req, ServletResponse res, boolean mandatory) throws ServerAuthException {
				logger.debug("validateRequest(..,..,"+mandatory+")");
				HttpServletRequest request = (HttpServletRequest)req;
				if (!mandatory)
					return new DeferredAuthentication(this);

				HttpSession session = request.getSession(true);
				logger.debug("session="+session);

				// Look for cached authentication
				Authentication authentication = (Authentication) session.getAttribute(SessionAuthentication.__J_AUTHENTICATED);
				logger.debug("authentication="+authentication);
				if (authentication != null) {
					// Has authentication been revoked?
					if (authentication instanceof Authentication.User &&
							_loginService!=null &&
							!_loginService.validate(((Authentication.User)authentication).getUserIdentity()))
					{
						logger.debug("auth revoked {}",authentication);
						session.removeAttribute(SessionAuthentication.__J_AUTHENTICATED);
					} else {
						synchronized (session) {
							String j_uri=(String)session.getAttribute(FormAuthenticator.__J_URI);
							if (j_uri!=null) {
								//check if the request is for the same url as the original and restore
								//params if it was a post
								logger.debug("auth retry {}->{}",authentication,j_uri);
								StringBuffer buf = request.getRequestURL();
								if (request.getQueryString() != null)
									buf.append("?").append(request.getQueryString());

								if (j_uri.equals(buf.toString()))
								{
									MultiMap<String> j_post = (MultiMap<String>)session.getAttribute(FormAuthenticator.__J_POST);
									if (j_post!=null)
									{
										logger.debug("auth rePOST {}->{}",authentication,j_uri);
										Request base_request = Request.getBaseRequest(request);
										base_request.setContentParameters(j_post);
									}
									session.removeAttribute(FormAuthenticator.__J_URI);
									session.removeAttribute(FormAuthenticator.__J_METHOD);
									session.removeAttribute(FormAuthenticator.__J_POST);
								}
							}
						}
						logger.trace("auth {}",authentication);
						return authentication;
					}
				}
				return super.validateRequest(req, res, mandatory);
			}
		};
	}

	//-------------------------------------------------------------------
	protected static ConstraintSecurityHandler createSecurityHandler(String constraintName,
			String[] allowedRoles, Authenticator authenticator, String realm,
			LoginService loginService) {

		Constraint constraint = new Constraint();
		constraint.setName(constraintName);
		constraint.setRoles(new String[] {Roles.VALID_USER.name()});
		// This is telling Jetty to not allow unauthenticated requests through (very important!)
		constraint.setAuthenticate(true);

		ConstraintMapping cm = new ConstraintMapping();
		cm.setConstraint(constraint);
		cm.setPathSpec("/*");

		ConstraintSecurityHandler sh = new ConstraintSecurityHandler();
		sh.setAuthenticator(authenticator);
		sh.setLoginService(loginService);
		sh.setConstraintMappings(new ConstraintMapping[]{cm});
		sh.setRealmName(realm);

		return sh;
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS Attachments");
		c.createStatement().executeUpdate("DELETE FROM "+ActivationKeyDatabase.TABLE);
		c.createStatement().executeUpdate("DELETE FROM "+BoughtDatabase.TABLE);
		c.createStatement().executeUpdate("DELETE FROM "+ShopDatabase.TABLE);
		c.createStatement().executeUpdate("DELETE FROM "+CharacterDatabase.TABLE);
		c.createStatement().executeUpdate("DELETE FROM "+PlayerDatabase.TABLE);
		c.close();
		
		server.stop();
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		//		backend = new BackendAccess("jdbc:hsqldb:mem:testdb", "SA","");
		c = backend.getInternalConnection();
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS Attachments");
		c.createStatement().executeUpdate("DELETE FROM "+ActivationKeyDatabase.TABLE);
		c.createStatement().executeUpdate("DELETE FROM "+BoughtDatabase.TABLE);
		c.createStatement().executeUpdate("DELETE FROM "+ShopDatabase.TABLE);
		c.createStatement().executeUpdate("DELETE FROM "+CharacterDatabase.TABLE);
		c.createStatement().executeUpdate("DELETE FROM "+PlayerDatabase.TABLE);
	}

}
