package de.rpgframework.eden.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.google.gson.Gson;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.logic.BoughtDatabase;
import de.rpgframework.reality.ActivationKey;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.CatalogItem;
import de.rpgframework.reality.CatalogItem.Category;
import de.rpgframework.reality.CatalogItem.State;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author spr
 *
 */
class ShopServletTest extends CommonTestServlet {

	//-----------------------------------------------------------------
	UUID prepareDatabase() throws SQLException {
		CatalogItem catalog1 = backend.getShopDatabase().add(new CatalogItem("SpliMo_Bundle1", Category.CHARGEN_FVTT, RoleplayingSystem.SPLITTERMOND, "de", "Einsteigerbundle", 1995, State.AVAILABLE, "CORE","MSK"));
		CatalogItem catalog2 = backend.getShopDatabase().add(new CatalogItem("Die Magie"     , Category.CHARGEN     , RoleplayingSystem.SPLITTERMOND, "de", "Die Magie"       ,  995, State.AVAILABLE, "MAGIE"));
		CatalogItem catalog3 = backend.getShopDatabase().add(new CatalogItem("Grundregelwerk", Category.CHARGEN     , RoleplayingSystem.SHADOWRUN6  , "de", "Grundregelwerk"  , 2000, State.AVAILABLE, "CORE"));
		CatalogItem catalog4 = backend.getShopDatabase().add(new CatalogItem("Arkane Kräfte" , Category.CHARGEN     , RoleplayingSystem.SHADOWRUN6  , "de", "Arkane Kräfte"   , 2000, State.AVAILABLE, "SWYRD"));
		
		PlayerImpl player = backend.getPlayerDatabase().createPlayer("Manfred", "Mustermann", "test@invalid.de", TESTUSER, TESTPASS, Locale.GERMAN);

		BoughtItem bought1 = new BoughtItem(player.getUuid(), catalog1.getId(), Instant.now(), 2000);
		BoughtItem bought2 = new BoughtItem(player.getUuid(), catalog2.getId(), Instant.now(), 2000);
		BoughtItem bought3 = new BoughtItem(player.getUuid(), catalog3.getId(), Instant.now(), 2000);
		
		backend.getLicenseDatabase().add(bought1);
		backend.getLicenseDatabase().add(bought2);
		backend.getLicenseDatabase().add(bought3);
		
		return player.getUuid();
	}

	//-----------------------------------------------------------------
	@SuppressWarnings("unchecked")
	@Test
	void testList() throws Exception {
		// Write data to read
		prepareDatabase();


		URL url = new URL("http://localhost:8090"+ShopServlet.PREFIX);
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("GET");

		assertEquals(200, con.getResponseCode());

		List<BoughtItem> handles = new ArrayList<BoughtItem>();
		handles = (new Gson()).fromJson(new InputStreamReader(con.getInputStream()), handles.getClass());
		logger.info("Got "+handles);
		assertNotNull(handles);
		assertEquals(3, handles.size(), "Expected non-empty list");
	}

	//-----------------------------------------------------------------
	@SuppressWarnings("unchecked")
	@Test
	void testListByRules() throws Exception {
		// Write data to read
		prepareDatabase();


		URL url = new URL("http://localhost:8090"+ShopServlet.PREFIX+"?rules=SPLITTERMOND");
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("GET");

		assertEquals(200, con.getResponseCode());

		List<BoughtItem> handles = new ArrayList<BoughtItem>();
		handles = (new Gson()).fromJson(new InputStreamReader(con.getInputStream()), handles.getClass());
		logger.info("Got "+handles);
		assertNotNull(handles);
		assertEquals(2, handles.size(), "Expected list with 2 elements");
	}

	//-----------------------------------------------------------------
	@SuppressWarnings("unchecked")
	@Test
	void testListByCategory() throws Exception {
		// Write data to read
		prepareDatabase();

		URL url = new URL("http://localhost:8090"+ShopServlet.PREFIX+"?category=CHARGEN_FVTT");
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("GET");

		assertEquals(200, con.getResponseCode());

		List<BoughtItem> handles = new ArrayList<BoughtItem>();
		handles = (new Gson()).fromJson(new InputStreamReader(con.getInputStream()), handles.getClass());
		logger.info("Got "+handles);
		assertNotNull(handles);
		assertEquals(1, handles.size(), "Expected list with 1 elements");

		// Now with unsupported category
		url = new URL("http://localhost:8090"+ShopServlet.PREFIX+"?category=FVTT");
		con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("GET");

		assertEquals(200, con.getResponseCode());

		handles = new ArrayList<BoughtItem>();
		handles = (new Gson()).fromJson(new InputStreamReader(con.getInputStream()), handles.getClass());
		logger.info("Got "+handles);
		assertNotNull(handles);
		assertEquals(0, handles.size(), "Expected list with 0 elements");
	}

	//-----------------------------------------------------------------
	@Test
	void testUseActivationKey() throws Exception {
		// Write data to read
		UUID uuid = prepareDatabase();
		
		ActivationKey failActKey1 = new ActivationKey(UUID.randomUUID(), "SpliMo_Bundle1", Instant.parse("2021-05-03T10:15:30.00Z"));
		ActivationKey goodActKey2 = new ActivationKey(UUID.randomUUID(), "Arkane Kräfte" , Instant.parse("2021-06-03T10:16:35.00Z"));
		backend.getActivationKeyDatabase().add(failActKey1);
		backend.getActivationKeyDatabase().add(goodActKey2);

		/*
		 * Try using the license on something that has already been bought
		 */
		URL url = new URL(String.format("http://localhost:8090"+ShopServlet.PREFIX+"/?activationKey=%s", failActKey1.getID().toString()	));
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("POST");
		assertEquals(406, con.getResponseCode(), "Already owned not detected");

		/*
		 * Now try something not yet bought
		 */
		url = new URL(String.format("http://localhost:8090"+ShopServlet.PREFIX+"/?activationKey=%s", goodActKey2.getID().toString()	));
		con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("POST");
		assertEquals(200, con.getResponseCode(), con.getHeaderField("X-Reason"));

		BoughtItem handle = (new Gson()).fromJson(new InputStreamReader(con.getInputStream()), BoughtItem.class);
		assertNotNull(handle,"No item returned on POST");
		
		Statement stat = c.createStatement();
		ResultSet set = stat.executeQuery("SELECT * FROM "+BoughtDatabase.TABLE+" WHERE playerID='"+uuid+"' AND catalogID='"+handle.getItemID()+"'");
		assertTrue(set.next(), "Bought Item not written do database");
		stat.close();
		
		/*
		 * Now try the same - activation key has been used
		 */
		con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(new java.net.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication (TESTUSER, TESTPASS.toCharArray());
			}
		});
		con.setRequestMethod("POST");

		assertEquals(403, con.getResponseCode());
	}

}
