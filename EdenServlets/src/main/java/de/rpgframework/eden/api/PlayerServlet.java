package de.rpgframework.eden.api;

import java.io.IOException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.security.auth.Subject;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.reality.Player;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class PlayerServlet extends HttpServlet {
	
    private final static Logger logger = LogManager.getLogger("eden.api");
	
	public final static String PREFIX = "/api/player";

    private final static Pattern SPECIFIC = Pattern.compile(PREFIX+"/(.*)");

    private Gson gson;
    private BackendAccess backend;

    //-------------------------------------------------------------------
    /**
     * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
     */
    public void init(ServletConfig config) throws ServletException {
    	super.init(config);
    	logger.warn("Init called");
    	gson = new GsonBuilder().setPrettyPrinting().create();
    	
    	for (Enumeration<String> e= config.getInitParameterNames(); e.hasMoreElements(); ) {
    		logger.debug("  init parameter name = "+e.nextElement());
    	}
    	
    	logger.debug("Servlet context = "+config.getServletContext());
    	for (Enumeration<String> e= config.getServletContext().getAttributeNames(); e.hasMoreElements(); ) {
    		logger.debug("  attribute name = "+e.nextElement());
    	}
    	
    	backend = (BackendAccess) config.getServletContext().getAttribute(Constants.ATTRIB_BACKEND);
    }
    
    //-------------------------------------------------------------------
    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
            logger.debug("----------doGet "+request.getRequestURL()+"--------------------");
            logger.debug("User = "+request.getUserPrincipal());
            response.setContentType("text/html; charset=UTF-8");

            String uri = request.getRequestURI();
            Matcher matcher = SPECIFIC.matcher(URLDecoder.decode(uri, "UTF-8"));
            if (!matcher.matches()) {
            	logger.debug("URI does not match pattern: "+uri+"   pattern="+SPECIFIC);
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    return;
            }
            
            logger.debug("URI does match: "+request.getUserPrincipal().implies(new Subject()));
            
            try {
				Player player = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
				response.getWriter().println(gson.toJson(player));
                response.setStatus(HttpServletResponse.SC_OK);
                return;
			} catch (SQLException e) {
				logger.error("Failed accessing player",e);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
			}
    }
    
}
