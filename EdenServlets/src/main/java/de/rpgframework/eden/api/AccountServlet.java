package de.rpgframework.eden.api;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class AccountServlet extends HttpServlet {
	
	public final static String PREFIX = "/ping";

	//-------------------------------------------------------------------
	public class EdenAccountInfo {
		private String firstName;
		private String lastName;
		private String email;
		private Map<RoleplayingSystem, List<String>> modules;
		//-------------------------------------------------------------------
		public String getFirstName() {return firstName;}
		public String getLastName() { return lastName;}
		public String getEmail() { return email;}
		public Map<RoleplayingSystem, List<String>> getLicensedModules() { return modules;}
	}

	private final static Logger logger = LogManager.getLogger("eden.api");

	private final static Pattern SPECIFIC = Pattern.compile("/ping");

	private Gson gson;
	private BackendAccess backend;

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		logger.warn("Init called");
		gson = new GsonBuilder().setPrettyPrinting().create();

		for (Enumeration<String> e= config.getInitParameterNames(); e.hasMoreElements(); ) {
			logger.debug("  init parameter name = "+e.nextElement());
		}

		logger.debug("Servlet context = "+config.getServletContext());
		for (Enumeration<String> e= config.getServletContext().getAttributeNames(); e.hasMoreElements(); ) {
			logger.debug("  attribute name = "+e.nextElement());
		}

		backend = (BackendAccess) config.getServletContext().getAttribute(Constants.ATTRIB_BACKEND);
		logger.info("Backend is "+backend);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.debug("----------doGet "+request.getRequestURL()+"--------------------");
		response.setContentType("text/html; charset=UTF-8");

		Principal user = request.getUserPrincipal();
		logger.debug("user = "+user);
		try {
			PlayerImpl player = backend.getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
			if (player==null) {
				logger.error("Valid player '"+user.getName()+"' but did not find associated Player object");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}
			EdenAccountInfo pdu = new EdenAccountInfo();
			pdu.firstName = player.getFirstName();
			pdu.lastName  = player.getLastName();
			pdu.email     = player.getEmail();
			// Licenses
			Map<RoleplayingSystem, List<String>> licensed = new HashMap<RoleplayingSystem, List<String>>();
			licensed.put(RoleplayingSystem.SPLITTERMOND, Arrays.asList("CORE","JdG"));
			licensed.put(RoleplayingSystem.SHADOWRUN6, Arrays.asList("CORE"));
			pdu.modules   = licensed;
			
			response.getWriter().println(gson.toJson(pdu));
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		return;
	}

}
