package de.rpgframework.eden.api;

import java.io.IOException;
import java.net.URLDecoder;
import java.security.Principal;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.security.auth.Subject;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.character.Attachment;
import de.rpgframework.character.Attachment.Format;
import de.rpgframework.character.Attachment.Type;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;
import de.rpgframework.reality.server.ServerCharacterHandle;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class AttachmentServlet extends HttpServlet {
	
    private final static Logger logger = LogManager.getLogger("eden.api");

    private final static Pattern SPECIFIC = Pattern.compile("/attachment/(.*)");
	private final static Pattern WITHOUT_UUID = Pattern.compile("/attachment/*");

    private Gson gson;
    private BackendAccess backend;

    //-------------------------------------------------------------------
    /**
     * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
     */
    public void init(ServletConfig config) throws ServletException {
    	super.init(config);
    	logger.warn("Init called");
    	gson = new GsonBuilder().setPrettyPrinting().create();
    	
    	for (Enumeration<String> e= config.getInitParameterNames(); e.hasMoreElements(); ) {
    		logger.debug("  init parameter name = "+e.nextElement());
    	}
    	
    	logger.debug("Servlet context = "+config.getServletContext());
    	for (Enumeration<String> e= config.getServletContext().getAttributeNames(); e.hasMoreElements(); ) {
    		logger.debug("  attribute name = "+e.nextElement());
    	}
    	
    	backend = (BackendAccess) config.getServletContext().getAttribute(Constants.ATTRIB_BACKEND);
    }
    
    //-------------------------------------------------------------------
    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
            logger.debug("----------doGet "+request.getRequestURL()+"--------------------");
            logger.debug("User = "+request.getUserPrincipal());
            response.setContentType("text/html; charset=UTF-8");

            Principal user = request.getUserPrincipal();
            String uri = request.getRequestURI();
            Matcher matcher = SPECIFIC.matcher(URLDecoder.decode(uri, "UTF-8"));
            if (!matcher.matches()) {
            	logger.debug("URI does not match pattern: "+uri+"   pattern="+SPECIFIC);
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    return;
            }
            
            logger.debug("URI does match: "+user.implies(new Subject()));
            
            try {
				Player player = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
				if (player==null) {
					logger.error("Valid player '"+user.getName()+"' but did not find associated Player object");
	                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	                return;
				}
				BackendAccess.getInstance().getCharacterDatabase();
				
				response.getWriter().println(gson.toJson(player));
                response.setStatus(HttpServletResponse.SC_OK);
                return;
			} catch (SQLException e) {
				logger.error("Failed accessing player",e);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
			}
    }

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.debug("----------doPost "+request.getRequestURL()+"--------------------");
		logger.debug("User = "+request.getUserPrincipal());
		response.setContentType("text/html; charset=UTF-8");

		Principal user = request.getUserPrincipal();
		if (user==null) {
			logger.debug("PUT request for attachment, but no user object found");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.setHeader("Reason", "User object not found");
			return;
		}
		PlayerImpl player = null;
		try {
			player = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
		} catch (SQLException e) {
			logger.error("Failed accessing player",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}

		String uri = request.getRequestURI();
		Matcher matcher = WITHOUT_UUID.matcher(URLDecoder.decode(uri, "UTF-8"));
		if (!matcher.matches()) {
			logger.debug("User "+user+": URI does not match pattern: "+uri+"   pattern="+WITHOUT_UUID);
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		if (logger.isTraceEnabled())
			logger.trace("Params = "+request.getParameterMap());
		String name = request.getParameter("name");
		String charUUID = request.getParameter("char");
		if (charUUID==null) {
			logger.debug("User "+user+": Parameter 'char' missing");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "char parameter missing");
			return;
		}
		ServerCharacterHandle handle = null;
		try {
			handle = (ServerCharacterHandle)BackendAccess.getInstance().getCharacterDatabase().getCharacter(player, UUID.fromString(charUUID));
		} catch (SQLException e) {
			logger.error("Failed accessing character",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
		if (handle==null) {
			logger.debug("User "+user+": Parameter 'char' points to unknown character");
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			response.setHeader("Reason", "Parameter 'char' points to unknown character");
			return;
		}
		
		String type_p = request.getParameter("type");
		if (type_p==null) {
			logger.debug("User "+user+": Parameter 'type' missing");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "rules parameter missing");
			return;
		}
		Type type = null;
		try {
			type = Type.valueOf(type_p);
		} catch (Exception e) {
			logger.debug("User "+user+": Parameter 'type' invalid");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "type parameter invalid");
			return;
		}
		String format_p = request.getParameter("format");
		if (format_p==null) {
			logger.debug("User "+user+": Parameter 'format' missing");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "format parameter missing");
			return;
		}
		Format format = null;
		try {
			format = Format.valueOf(format_p);
		} catch (Exception e) {
			logger.debug("User "+user+": Parameter 'format' invalid");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "format parameter invalid");
			return;
		}


		logger.info(String.format("User '%s' adds attachment '%s', type=%s, format=%s", user.getName(), name,  type, format));

		// Read data
		byte[] data = request.getInputStream().readAllBytes();
		
		try {
			// Ensure character does not exist yet
			Attachment attach = backend.getAttachmentDatabase().addAttachment(handle, type, format, name, data);
			response.getWriter().println(gson.toJson(attach));
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		} catch (SQLException e) {
			logger.error("Failed creating attachment",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}
    
}
