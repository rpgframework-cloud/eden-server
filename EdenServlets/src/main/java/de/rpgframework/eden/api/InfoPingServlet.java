package de.rpgframework.eden.api;

import java.io.IOException;
import java.util.Enumeration;
import java.util.regex.Pattern;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.Constants;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class InfoPingServlet extends HttpServlet {

	//-------------------------------------------------------------------
	private class EdenPingInfo {
		private String mainSiteURL;
		private String accountCreationURL;
		//-------------------------------------------------------------------
		public String getMainSiteURL() {return mainSiteURL;}
		public String getAccountCreationURL() { return accountCreationURL;}
	}

	private final static Logger logger = LogManager.getLogger("eden.api");

	private final static Pattern SPECIFIC = Pattern.compile("/ping");

	private Gson gson;
	private BackendAccess backend;

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		logger.warn("Init called");
		gson = new GsonBuilder().setPrettyPrinting().create();

		for (Enumeration<String> e= config.getInitParameterNames(); e.hasMoreElements(); ) {
			logger.debug("  init parameter name = "+e.nextElement());
		}

		logger.debug("Servlet context = "+config.getServletContext());
		for (Enumeration<String> e= config.getServletContext().getAttributeNames(); e.hasMoreElements(); ) {
			logger.debug("  attribute name = "+e.nextElement());
		}

		backend = (BackendAccess) config.getServletContext().getAttribute(Constants.ATTRIB_BACKEND);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.debug("----------doGet "+request.getRequestURL()+"--------------------");
		response.setContentType("text/html; charset=UTF-8");

		EdenPingInfo pdu = new EdenPingInfo();
		pdu.accountCreationURL = "http://localhost:8000/create";
		pdu.mainSiteURL = "http://localhost:8000";

		response.getWriter().println(gson.toJson(pdu));
		response.setStatus(HttpServletResponse.SC_OK);
		return;
	}

}
