package de.rpgframework.eden.backend;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.Servlet;

import de.rpgframework.eden.api.AccountServlet;
import de.rpgframework.eden.api.CharacterServlet;
//import de.rpgframework.eden.api.CharacterServlet2;
import de.rpgframework.eden.api.PlayerServlet;
import de.rpgframework.eden.api.ShopServlet;
import de.rpgframework.eden.foundry.CompendiumServlet;
import de.rpgframework.eden.logic.BackendAccess;

/**
 * @author prelle
 *
 */
public class EdenBackend {
	
	public static class ServletWithMapping {
		public Class<? extends  Servlet> servlet;
		public String path;
		public ServletWithMapping(Class<? extends  Servlet> servlet, String path) {
			this.servlet = servlet;
			this.path    = path;
		}
	}
	
	private static List<ServletWithMapping> servlets;
	
	//-------------------------------------------------------------------
	static {
		servlets = new ArrayList<EdenBackend.ServletWithMapping>();
//		servlets.add(new ServletWithMapping(InfoPingServlet.class, "/ping"));
		servlets.add(new ServletWithMapping(AccountServlet.class, AccountServlet.PREFIX));
		servlets.add(new ServletWithMapping( PlayerServlet.class, PlayerServlet.PREFIX+"/*"));
//		servlets.add(new ServletWithMapping(AttachmentServlet.class, "/character/*/attachment/*"));
		servlets.add(new ServletWithMapping(CharacterServlet.class, CharacterServlet.PREFIX+"/*"));
//		servlets.add(new ServletWithMapping(CharacterServlet2.class, "/character/*"));
		servlets.add(new ServletWithMapping(CompendiumServlet.class, "/foundry/*"));
		servlets.add(new ServletWithMapping(ShopServlet.class, ShopServlet.PREFIX+"/*"));
		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
	}

	//-------------------------------------------------------------------
	public static BackendAccess initialize(String jdbcURL, String user, String pass) throws SQLException {
		Properties pro = new Properties();
		pro.setProperty(BackendAccess.CFG_JDBC_URL, jdbcURL);
		pro.setProperty(BackendAccess.CFG_JDBC_USER, user);
		pro.setProperty(BackendAccess.CFG_JDBC_PASS, pass);
		return new BackendAccess(pro);
	}

	//-------------------------------------------------------------------
	public static BackendAccess initialize(Properties pro) throws SQLException {
		return new BackendAccess(pro);
	}
	
	//-------------------------------------------------------------------
	public static List<ServletWithMapping> getServlets() {
		return new ArrayList<EdenBackend.ServletWithMapping>(servlets);
	}
}
