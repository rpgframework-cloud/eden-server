package de.rpgframework.eden.backend;

import java.util.ArrayList;
import java.util.List;

import com.paypal.core.PayPalEnvironment;
import com.paypal.core.PayPalHttpClient;
import com.paypal.orders.AddressPortable;
import com.paypal.orders.AmountBreakdown;
import com.paypal.orders.AmountWithBreakdown;
import com.paypal.orders.ApplicationContext;
import com.paypal.orders.Item;
import com.paypal.orders.Money;
import com.paypal.orders.Name;
import com.paypal.orders.OrderRequest;
import com.paypal.orders.PurchaseUnitRequest;
import com.paypal.orders.ShippingDetail;

import de.rpgframework.reality.CatalogItem;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author Stefan
 *
 */
public class Shop {
    
    // Creating a sandbox environment
    private static PayPalEnvironment environment;
    
    // Creating a client for the environment
    static PayPalHttpClient client;

	//-------------------------------------------------------------------
	public Shop() {
		// TODO Auto-generated constructor stub
	}
	
	//-------------------------------------------------------------------
	public void buy(PlayerImpl user, List<CatalogItem> items, String cancelURL, String returnURL) {
		float priceWithoutVAT = 0;
		for (CatalogItem item : items) {
			priceWithoutVAT += item.getPrice();
		}
		float tax = priceWithoutVAT*0.19f;
		float total = priceWithoutVAT + tax;
		
		String totalS = String.format("%d", priceWithoutVAT);
		
		
		OrderRequest orderRequest = new OrderRequest();
		orderRequest.checkoutPaymentIntent("CAPTURE");

		ApplicationContext applicationContext = new ApplicationContext().brandName("RPGFramework Eden").landingPage("BILLING")
				.cancelUrl(cancelURL).returnUrl(returnURL).userAction("CONTINUE")
				;
		orderRequest.applicationContext(applicationContext);

		List<PurchaseUnitRequest> purchaseUnitRequests = new ArrayList<PurchaseUnitRequest>();
		PurchaseUnitRequest purchaseUnitRequest = new PurchaseUnitRequest().referenceId("PUHF")
				.description("Eden Items").customId("CUST-HighFashions").softDescriptor("Modules")
				.amountWithBreakdown(new AmountWithBreakdown().currencyCode("EUR").value("15.50")
						.amountBreakdown(new AmountBreakdown().itemTotal(new Money().currencyCode("EUR").value("13.00"))
								.shipping(new Money().currencyCode("EUR").value("2.00"))
								.handling(new Money().currencyCode("EUR").value("1.00"))
								.taxTotal(new Money().currencyCode("EUR").value("0.50"))
								.shippingDiscount(new Money().currencyCode("EUR").value("1.00"))))
				.items(new ArrayList<Item>() {
					{
						add(new Item().name("T-shirt").description("Green XL").sku("sku01")
								.unitAmount(new Money().currencyCode("EUR").value("5.00"))
								.tax(new Money().currencyCode("EUR").value("00.10")).quantity("1")
								.category("PHYSICAL_GOODS"));
						add(new Item().name("Shoes").description("Running, Size 10.5").sku("sku02")
								.unitAmount(new Money().currencyCode("EUR").value("4.00"))
								.tax(new Money().currencyCode("EUR").value("0.20")).quantity("2")
								.category("PHYSICAL_GOODS"));
					}
				})
				.shippingDetail(new ShippingDetail().name(new Name().fullName("John Doe"))
						.addressPortable(new AddressPortable().addressLine1("123 Townsend St").addressLine2("Floor 6")
								.adminArea2("San Francisco").adminArea1("CA").postalCode("94107").countryCode("US")));
		purchaseUnitRequests.add(purchaseUnitRequest);
		orderRequest.purchaseUnits(purchaseUnitRequests);
//		return orderRequest;
		
	}

}
