package de.rpgframework.eden.foundry.sr6;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.logging.log4j.LogManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.eden.foundry.CompendiumEntry;
import de.rpgframework.eden.foundry.Language;
import de.rpgframework.eden.foundry.Module;
import de.rpgframework.eden.foundry.Pack;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.PageReference;
import de.rpgframework.reality.Player;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.AdeptPower;
import de.rpgframework.shadowrun.Quality;
import de.rpgframework.shadowrun6.Shadowrun6Core;
import de.rpgframework.shadowrun6.foundry.FVTTAdeptPower;
import de.rpgframework.shadowrun6.foundry.FVTTQuality;
import de.rpgframework.shadowrun6.foundry.FVTTSpell;
import de.rpgframework.shadowrun6.foundry.FVTTWeapon;
import de.rpgframework.shadowrun6.items.ItemTemplate;
import de.rpgframework.shadowrun6.items.ItemType;
import de.rpgframework.shadowrun6.items.SR6ItemAttribute;

/**
 * @author prelle
 *
 */
public class Shadowrun6CompendiumFactory {

	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	//-------------------------------------------------------------------
	public static String createSourceText(DataItem item, Locale loc) {
		List<String> elements = new ArrayList<>();
		boolean shorted = item.getPageReferences().size()>2;
		String language = loc.getLanguage();
		for (PageReference ref : item.getPageReferences()) {
			if (!ref.getLanguage().equals(language))
				continue;
			if (shorted) {
				elements.add( ref.getProduct().getShortName(Locale.getDefault())+" "+ref.getPage() );
			} else {
				elements.add( ref.getProduct().getName(Locale.getDefault())+" "+ref.getPage() );
			}
		}

		return String.join(", ", elements);
	}

	//-------------------------------------------------------------------
	public static Module create(Player player, Locale locale, boolean shallow) throws IOException {
		LogManager.getLogger("shadowrun6.compendium").info("Create");
		if (locale==null)
			throw new NullPointerException("Locale");
		Module module = new Module();
		module.setName("shadowrun6-data");
		module.setTitle("Shadowrun6 Daten");
		if (shallow) {
			module.setVersion("0.0.3"); // For JSON
		} else {
			module.setVersion("0.0.2");  // For ZIP
		}
		module.setMinimumCoreVersion("0.8.0");
		module.setCompatibleCoreVersion("0.8.9");
		module.setAuthor("Stefan Prelle");

		module.fos = new ByteArrayOutputStream();
        ZipOutputStream zipOut = new ZipOutputStream(module.fos);

		createAdeptPowers(module, zipOut, locale, shallow);
		createQualities  (module, zipOut, locale, shallow);
		createSpells     (module, zipOut, locale, shallow);
		createWeapons    (module, zipOut, locale, shallow);

		for (Language lang : module.getLanguages()) {
        	ZipEntry zipEntry = new ZipEntry(lang.getPath());
        	zipOut.putNextEntry(zipEntry);
         	zipOut.write(gson.toJson(lang.keys).getBytes(Charset.forName("UTF-8")));
		}

        // module.json
		module.setManifest("http://"+player.getLogin()+":"+player.getPassword()+"@"+BackendAccess.getHostPortBackend()+"/api/foundry/shadowrun6/compendium/module.json");
		module.setDownload(module.getManifest().substring(0, module.getManifest().length()-12)+".zip");
		LogManager.getLogger("shadowrun6.compendium").info("manifest = "+module.getManifest());
		LogManager.getLogger("shadowrun6.compendium").info("Download = "+module.getDownload());
		String json = gson.toJson(module);
    	ZipEntry zipEntry = new ZipEntry("module.json");
    	zipOut.putNextEntry(zipEntry);
    	zipOut.write(json.getBytes());

		zipOut.close();
    	module.fos.close();
		return module;
	}

	//-------------------------------------------------------------------
	private static void createAdeptPowers(Module module, ZipOutputStream zipOut, Locale locale, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("shadowrun6-powers");
		pack.setLabel("Adept Powers");
		pack.setEntity("Item");
		pack.setPath("packs/adeptpowers.db");
		pack.setSystem("shadowrun6-eden");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (AdeptPower tmp : Shadowrun6Core.getItemList(AdeptPower.class)) {

			module.addTranslation("de", "adeptpower."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "adeptpower."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));
			module.addTranslation("de", "adeptpower."+tmp.getId()+".src", createSourceText(tmp, Locale.GERMAN));
			module.addTranslation("en", "adeptpower."+tmp.getId()+".desc", tmp.getDescription(Locale.ENGLISH));
			module.addTranslation("en", "adeptpower."+tmp.getId()+".name", tmp.getName(Locale.ENGLISH));
			module.addTranslation("en", "adeptpower."+tmp.getId()+".src", createSourceText(tmp, Locale.ENGLISH));
			CompendiumEntry entry = new CompendiumEntry();
			entry._id  = tmp.getId();
			entry.name = tmp.getName(locale);
			entry.type = "adeptpower";

			FVTTAdeptPower data = new FVTTAdeptPower();
			data.genesisID   = tmp.getId();
			data.activation	 = tmp.getActivation().name().toLowerCase();
			data.cost        = tmp.getCostForLevel(1);
			entry.data = data;

			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

    	ZipEntry zipEntry = new ZipEntry("packs/adeptpowers.db");
    	zipOut.putNextEntry(zipEntry);
    	zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
		return;
	}

	//-------------------------------------------------------------------
	private static void createSpells(Module module, ZipOutputStream zipOut, Locale locale, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("shadowrun6-spells");
		pack.setLabel("Zauber");
		pack.setEntity("Item");
		pack.setPath("packs/spells.db");
		pack.setSystem("shadowrun6-eden");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (ASpell spell : Shadowrun6Core.getItemList(ASpell.class)) {

			module.addTranslation("de", "spell."+spell.getId()+".desc", spell.getDescription(Locale.GERMAN));
			module.addTranslation("de", "spell."+spell.getId()+".name", spell.getName(Locale.GERMAN));
			module.addTranslation("de", "spell."+spell.getId()+".src", createSourceText(spell, Locale.GERMAN));
			module.addTranslation("en", "spell."+spell.getId()+".desc", spell.getDescription(Locale.ENGLISH));
			module.addTranslation("en", "spell."+spell.getId()+".name", spell.getName(Locale.ENGLISH));
			module.addTranslation("en", "spell."+spell.getId()+".src", createSourceText(spell, Locale.ENGLISH));
			CompendiumEntry entry = new CompendiumEntry();
			entry._id = spell.getId();
			entry.name = spell.getName(locale);
			entry.type = "spell";

			FVTTSpell data = new FVTTSpell();
			data.genesisID = spell.getId();
			data.data.category  = spell.getCategory().name();
			data.data.drain = spell.getDrain();
			data.data.type  = spell.getType().name();
			data.data.range = spell.getRange().name();
			entry.data = data;

			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

    	ZipEntry zipEntry = new ZipEntry("packs/spells.db");
    	zipOut.putNextEntry(zipEntry);
    	zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
		return;
	}

	//-------------------------------------------------------------------
	private static void createQualities(Module module, ZipOutputStream zipOut, Locale locale, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("shadowrun6-qualities");
		pack.setLabel("Qualities");
		pack.setEntity("Item");
		pack.setPath("packs/qualities.db");
		pack.setSystem("shadowrun6-eden");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (Quality tmp : Shadowrun6Core.getItemList(Quality.class)) {

			module.addTranslation("de", "quality."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "quality."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));
			module.addTranslation("de", "quality."+tmp.getId()+".src", createSourceText(tmp, Locale.GERMAN));
			module.addTranslation("en", "quality."+tmp.getId()+".desc", tmp.getDescription(Locale.ENGLISH));
			module.addTranslation("en", "quality."+tmp.getId()+".name", tmp.getName(Locale.ENGLISH));
			module.addTranslation("en", "quality."+tmp.getId()+".src", createSourceText(tmp, Locale.ENGLISH));
			CompendiumEntry entry = new CompendiumEntry();
			entry._id = tmp.getId();
			entry.name = tmp.getName(locale);
			entry.type = "quality";

			FVTTQuality data = new FVTTQuality();
			data.genesisID   = tmp.getId();
			data.category    = tmp.getType().name();
			data.level       = tmp.hasLevel();
			entry.data = data;

			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

    	ZipEntry zipEntry = new ZipEntry("packs/qualities.db");
    	zipOut.putNextEntry(zipEntry);
    	zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
		return;
	}

	//-------------------------------------------------------------------
	private static void createWeapons(Module module, ZipOutputStream zipOut, Locale locale, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("shadowrun6-weapons");
		pack.setLabel("Weapons");
		pack.setEntity("Item");
		pack.setPath("packs/weapons.db");
		pack.setSystem("shadowrun6-eden");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (ItemTemplate tmp : Shadowrun6Core.getItemList(ItemTemplate.class)) {
			if (!ItemType.isWeapon(tmp.getItemType()))
					continue;

			module.addTranslation("de", "item."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "item."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));
			module.addTranslation("de", "item."+tmp.getId()+".src", createSourceText(tmp, Locale.GERMAN));
			module.addTranslation("en", "item."+tmp.getId()+".desc", tmp.getDescription(Locale.ENGLISH));
			module.addTranslation("en", "item."+tmp.getId()+".name", tmp.getName(Locale.ENGLISH));
			module.addTranslation("en", "item."+tmp.getId()+".src", createSourceText(tmp, Locale.ENGLISH));
			CompendiumEntry entry = new CompendiumEntry();
			entry._id  = tmp.getId();
			entry.name = tmp.getName(locale);
			entry.type = "gear";

			FVTTWeapon data = new FVTTWeapon();
			data.genesisID  = tmp.getId();
			if (tmp.getItemType()!=null)
				data.type       = tmp.getItemType().name();
			if (tmp.getItemSubtype()!=null)
				data.subtype    = tmp.getItemSubtype().name();
			if (tmp.getAttribute(SR6ItemAttribute.AVAILABILITY)!=null)
				data.availDef   = tmp.getAttribute(SR6ItemAttribute.AVAILABILITY).getRawValue();
			if (tmp.getAttribute(SR6ItemAttribute.SKILL)!=null)
				data.skill      = tmp.getAttribute(SR6ItemAttribute.SKILL).getRawValue();
			if (tmp.getAttribute(SR6ItemAttribute.SKILL_SPECIALIZATION)!=null)
				data.skillSpec  = tmp.getAttribute(SR6ItemAttribute.SKILL_SPECIALIZATION).getRawValue();

			if (tmp.getAttribute(SR6ItemAttribute.DAMAGE)!=null)
				data.dmgDef     = tmp.getAttribute(SR6ItemAttribute.DAMAGE).getRawValue();
			entry.data = data;

			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

    	ZipEntry zipEntry = new ZipEntry("packs/weapons.db");
    	zipOut.putNextEntry(zipEntry);
    	zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
		return;
	}

//	//-------------------------------------------------------------------
//	private static void createRituals(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
//		Pack pack = new Pack();
//		pack.setName("shadowrun6-rituals");
//		pack.setLabel("Rituals");
//		pack.setEntity("Item");
//		pack.setPath("packs/rituals.db");
//		pack.setSystem("shadowrun6-eden");
//		module.getPacks().add(pack);
//
//		if (shallow)
//			return;
//
//		StringBuffer buf = new StringBuffer();
//		Gson gson = new GsonBuilder().create();
//		for (Ritual spell : Shadowrun6Core.getItemList(Ritual.class)) {
//
//			module.addTranslation("de", "spell."+spell.getId()+".desc", spell.getDescription(Locale.GERMAN));
//			module.addTranslation("de", "spell."+spell.getId()+".name", spell.getName(Locale.GERMAN));
//			module.addTranslation("de", "spell."+spell.getId()+".src", createSourceText(spell, Locale.GERMAN));
//			module.addTranslation("en", "spell."+spell.getId()+".desc", spell.getDescription(Locale.ENGLISH));
//			module.addTranslation("en", "spell."+spell.getId()+".name", spell.getName(Locale.ENGLISH));
//			module.addTranslation("en", "spell."+spell.getId()+".src", createSourceText(spell, Locale.ENGLISH));
//			CompendiumEntry entry = new CompendiumEntry();
//			entry._id = spell.getId();
//			entry.name = spell.getName(Locale.ENGLISH);
//			entry.type = "spell";
//
//			FVTTRitual data = new FVTTRitual();
//			data.genesisID = spell.getId();
//			data.category  = spell.getCategory().name();
//			data.drain = spell.getDrain();
//			data.type  = spell.getType().name();
//			data.range = spell.getRange().name();
//			entry.data = data;
//
//			buf.append(gson.toJson(entry));
//			buf.append('\n');
//		}
//
//    	ZipEntry zipEntry = new ZipEntry("packs/spells.db");
//    	zipOut.putNextEntry(zipEntry);
//    	zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
//		return;
//	}

}
