package de.rpgframework.eden.foundry.splimo;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.Mastership;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.Power;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellEnhancementType;
import org.prelle.splimo.SpellSchoolEntry;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureFeature;
import org.prelle.splimo.creature.CreatureFeatureValue;
import org.prelle.splimo.items.Availability;
import org.prelle.splimo.items.Complexity;
import org.prelle.splimo.items.FeatureList;
import org.prelle.splimo.items.FeatureType;
import org.prelle.splimo.items.ItemSubType;
import org.prelle.splimo.items.ItemTemplate;
import org.prelle.splimo.items.SMItemAttribute;
import org.prelle.splimo.persist.WeaponDamageConverter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.eden.foundry.CompendiumEntry;
import de.rpgframework.eden.foundry.Language;
import de.rpgframework.eden.foundry.Module;
import de.rpgframework.eden.foundry.Pack;
import de.rpgframework.eden.foundry.TokenEntry;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.foundry.ItemData;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.PageReference;
import de.rpgframework.reality.Player;
import de.rpgframework.splittermond.foundry.FVTTDataItem;
import de.rpgframework.splittermond.foundry.JSONSkillValue;

/**
 * @author prelle
 *
 */
public class SplittermondCompendiumFactory {

	private final static String VALIDCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	private final static Random RANDOM = new Random();

	private final static Logger logger = LogManager.getLogger("foundry.splittermond");
	private final static String IMGROOT = "/home/data/splittermond";

	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	@Deprecated
	private final static String createRandomID() {
		StringBuffer buf = new StringBuffer();
		for (int i=0; i<16; i++) {
			buf.append(VALIDCHARS.charAt(RANDOM.nextInt(VALIDCHARS.length())));
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public static String createSourceText(DataItem item) {
		List<String> elements = new ArrayList<>();
		boolean shorted = item.getPageReferences().size()>2;
		String language = Locale.getDefault().getLanguage();
		for (PageReference ref : item.getPageReferences()) {
			if (!ref.getLanguage().equals(language))
				continue;
			if (shorted) {
				elements.add( ref.getProduct().getShortName(Locale.getDefault())+" "+ref.getPage() );
			} else {
				elements.add( ref.getProduct().getName(Locale.getDefault())+" "+ref.getPage() );
			}
		}

		return String.join(", ", elements);
	}

	//-------------------------------------------------------------------
	public static Module create(Player player, Locale locale, boolean shallow) throws IOException {
		Module module = new Module();
		module.setName("splittermond-data");
		module.setTitle("Splittermond Daten");
		if (shallow) {
			module.setVersion("0.0.3"); // For JSON
		} else {
			module.setVersion("0.0.2");  // For ZIP
		}
		module.setMinimumCoreVersion("0.8.0");
		module.setCompatibleCoreVersion("0.8.99");
		module.setAuthor("Stefan Prelle");

		module.fos = new ByteArrayOutputStream();
		ZipOutputStream zipOut = new ZipOutputStream(module.fos);

		createSpells(module, zipOut, shallow);
		createFeatureTypes(module, zipOut, shallow);
		createCreatureFeatureTypes(module, zipOut, shallow);
		createCreatures(module, zipOut, shallow);
		createItems(module, zipOut, shallow);
		createPowers(module, zipOut, shallow);
		createMasterships(module, zipOut, shallow);

		for (Language lang : module.getLanguages()) {
			ZipEntry zipEntry = new ZipEntry(lang.getPath());
			zipOut.putNextEntry(zipEntry);
			zipOut.write(gson.toJson(lang.keys).getBytes(Charset.forName("UTF-8")));
		}
		//		Language lang= new Language();
		//		lang.setName("splittermond-translations");
		//		lang.setLang("de");
		//		lang.setPath("./lang/de.json");
		//		module.getLanguages().add(lang);

		// module.json
		module.setManifest("http://"+player.getLogin()+":"+player.getPassword()+"@"+BackendAccess.getHostPortBackend()+"/api/foundry/splittermond/compendium/module.json");
		module.setDownload(module.getManifest().substring(0, module.getManifest().length()-12)+".zip");
		String json = gson.toJson(module);
		ZipEntry zipEntry = new ZipEntry("module.json");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(json.getBytes());

		zipOut.close();
		module.fos.close();
		return module;
	}

	//-------------------------------------------------------------------
	private static void createSpells(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-spells");
		pack.setLabel("Zauber");
		pack.setEntity("Item");
		pack.setPath("packs/spells.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (Spell spell : SplitterMondCore.getItemList(Spell.class)) {

			module.addTranslation("de", "spell."+spell.getId()+".desc", spell.getDescription(Locale.GERMAN));
			module.addTranslation("de", "spell."+spell.getId()+".name", spell.getName(Locale.GERMAN));
			module.addTranslation("de", "spell."+spell.getId()+".enhan", spell.getEnhancementDescription(Locale.GERMAN));
			//			de.rpgframework.eden.foundry.splimo.Spell converted = new de.rpgframework.eden.foundry.splimo.Spell();
			CompendiumEntry entry = new CompendiumEntry();
			entry._id  = spell.getId();
			entry.name = spell.getName(Locale.GERMAN);
			entry.type = "spell";
			entry.img  = "icons/svg/mystery-man.svg";

			de.rpgframework.splittermond.foundry.Spell data = new de.rpgframework.splittermond.foundry.Spell();
			data.genesisId   = spell.getId();
//			data.description = spell.getDescription(Locale.GERMAN);
			data.difficulty = spell.getDifficultyString();
			data.costs = SplitterTools.getFocusString(spell.getCost());
			data.castDuration = spell.getCastDurationString();
			data.range = spell.getCastRange()+"m";
			data.castRangeMeter = spell.getCastRange();
			data.castTicks = spell.getCastDurationTicks();
			if (spell.getEffectRange()!=null) {
				data.effectArea = spell.getEffectRangeString();
			}
			data.types = spell.getTypes().stream().map( s -> s.name()).collect(Collectors.toList());

			List<String> availableIn = new ArrayList<>();
			for (SpellSchoolEntry entry2 : spell.getSchools()) {
				availableIn.add( entry2.getSchool().getId()+" "+entry2.getLevel());
			}
			data.availableIn = String.join(",", availableIn);
			data.enhancementCosts = spell.getEnhancementString();
			data.enhancementDescription = spell.getEnhancementDescription(Locale.GERMAN);
			for (SpellEnhancementType type : spell.getEnhancementtype()) {
				switch (type) {
				case CHANNELIZED_FOCUS: data.degreeOfSuccessOptions.channelizedFocus=true; break;
				case EXHAUSTED_FOCUS  : data.degreeOfSuccessOptions.exhaustedFocus=true; break;
				case CONSUMED_FOCUS   : data.degreeOfSuccessOptions.consumedFocus=true; break;
				case DAMAGE           : data.degreeOfSuccessOptions.damage=true; break;
				case RELEASETIME      : data.degreeOfSuccessOptions.castDuration=true; break;
				case SPELLDURATION    : data.degreeOfSuccessOptions.effectDuration=true; break;
				case EFFECT_RANGE     : data.degreeOfSuccessOptions.effectrange=true; break;
				case RANGE            : data.degreeOfSuccessOptions.range=true; break;
				}
			}
			LogManager.getLogger("splittermond").error("Spell '"+spell.getId()+"': "+availableIn);
			//			if (spell.getCost().getChannelled()>0) {
			//				data.costK = spell.getCost().getChannelled() - spell.getCost().getConsumed();
			//			} else {
			//				data.costE = spell.getCost().getExhausted() - spell.getCost().getConsumed();
			//			}
			//			data.costV = spell.getCost().getConsumed();
			entry.data = data;

			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

		ZipEntry zipEntry = new ZipEntry("packs/spells.db");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
		return;
	}

	//-------------------------------------------------------------------
	private static void addImages(ZipOutputStream zipOut, String type, String id, CompendiumEntry entry) throws IOException {
		// Check for image
		String imgPath = "images/"+type+"/" + id + ".jpg";
		Path path = Paths.get(IMGROOT, imgPath);
		if (Files.exists(path)) {
			logger.warn("Found image "+path);
			FileInputStream fins = new FileInputStream(path.toFile());
			entry.img = "modules/splittermond-data/" + imgPath;
			ZipEntry zipEntry = new ZipEntry(imgPath);
			zipOut.putNextEntry(zipEntry);
			zipOut.write(fins.readAllBytes());
			fins.close();
		} else {
			logger.warn("Missing image "+path);
		}

		// Check for token
		String tokPath = "tokens/"+type+"/" + id + ".png";
		Path token = Paths.get(IMGROOT, tokPath);
		if (Files.exists(token)) {
			logger.warn("Found token "+tokPath);
			FileInputStream fins = new FileInputStream(token.toFile());
			entry.token.img = "modules/splittermond-data/" + tokPath;
			ZipEntry zipEntry = new ZipEntry(tokPath);
			zipOut.putNextEntry(zipEntry);
			zipOut.write(fins.readAllBytes());
			fins.close();
		} else {
			logger.warn("Missing token "+path);
			if (Files.exists(path)) {
				logger.warn("Found image "+path);
				FileInputStream fins = new FileInputStream(path.toFile());
				entry.token.img = "modules/splittermond-data/" + tokPath;
				ZipEntry zipEntry = new ZipEntry(tokPath);
				zipOut.putNextEntry(zipEntry);
				zipOut.write(fins.readAllBytes());
				fins.close();
			}
		}

	}

	//-------------------------------------------------------------------
	private static void createCreatures(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-creatures");
		pack.setLabel("Kreaturen");
		pack.setEntity("Actor");
		pack.setPath("packs/creatures.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (Creature tmp : SplitterMondCore.getItemList(Creature.class)) {
			module.addTranslation("de", "creature."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "creature."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));

			//			de.rpgframework.eden.foundry.splimo.Spell converted = new de.rpgframework.eden.foundry.splimo.Spell();
			CompendiumEntry entry = new CompendiumEntry();
			entry._id  = tmp.getId();
			entry.name = tmp.getName(Locale.GERMAN);
			entry.type = "npc";
			entry.token= new TokenEntry();
			entry.token.displayName=20;
			entry.token.displayBars=20;
			entry.token.bar1 = new TokenEntry.AttributeBar("data.healthBar");
			entry.token.bar2 = new TokenEntry.AttributeBar("data.focusBar");

			addImages(zipOut, "creature", tmp.getId(), entry);

			de.rpgframework.splittermond.foundry.Lifeform data = new de.rpgframework.splittermond.foundry.Lifeform();
			if (data.skills==null)
				data.skills = new LinkedHashMap<String,JSONSkillValue>();
			data.attributes.charisma.value = tmp.getAttribute(Attribute.CHARISMA).getModifiedValue();
			data.attributes.agility.value = tmp.getAttribute(Attribute.AGILITY).getModifiedValue();
			data.attributes.intuition.value = tmp.getAttribute(Attribute.INTUITION).getModifiedValue();
			data.attributes.constitution.value = tmp.getAttribute(Attribute.CONSTITUTION).getModifiedValue();
			data.attributes.mystic.value = tmp.getAttribute(Attribute.MYSTIC).getModifiedValue();
			data.attributes.strength.value = tmp.getAttribute(Attribute.STRENGTH).getModifiedValue();
			data.attributes.mind.value = tmp.getAttribute(Attribute.MIND).getModifiedValue();
			data.attributes.willpower.value = tmp.getAttribute(Attribute.WILLPOWER).getModifiedValue();

			data.derivedAttributes.bodyresist.value = tmp.getAttribute(Attribute.RESIST_BODY).getModifiedValue();
			data.derivedAttributes.mindresist.value = tmp.getAttribute(Attribute.RESIST_MIND).getModifiedValue();
			data.derivedAttributes.defense.value = tmp.getAttribute(Attribute.RESIST_DAMAGE).getModifiedValue();
			data.derivedAttributes.size.value = tmp.getAttribute(Attribute.SIZE).getModifiedValue();
			data.derivedAttributes.healthpoints.value = tmp.getAttribute(Attribute.LIFE).getModifiedValue();
			data.derivedAttributes.focuspoints.value = tmp.getAttribute(Attribute.FOCUS).getModifiedValue();
			data.derivedAttributes.speed.value = tmp.getAttribute(Attribute.SPEED).getModifiedValue();

			data.level = tmp.getLevelAlone()+"/"+tmp.getLevelGroup();
			data.type  = String.join(", ",tmp.getCreatureTypes().stream().map(f -> f.getName(Locale.GERMAN)).collect(Collectors.toList()));
			data.damageReduction.value = tmp.getAttribute(Attribute.DAMAGE_REDUCTION).getModifiedValue();

			// SKills
			for (SMSkillValue sval : tmp.getSkillValues()) {
				SMSkill skill = sval.getSkill();
				JSONSkillValue jVal = new JSONSkillValue();
				jVal.value = sval.getModifiedValue();
//				jVal.points = sval.getDistributed();
				if (skill==null) {
					logger.error("Null skill for "+tmp);
				}
				int a1 = tmp.getAttribute((skill.getAttribute()!=null)?skill.getAttribute():Attribute.AGILITY).getModifiedValue();
				int a2= tmp.getAttribute((skill.getAttribute2()!=null)?skill.getAttribute2():Attribute.STRENGTH).getModifiedValue();
				jVal.points = jVal.value - a1 - a2;
				jVal.sortKey  = skill.getType().ordinal()+"-"+skill.getName();
				data.skills.put(skill.getId(), jVal);

				// Masteries
				for (MastershipReference ref : sval.getMasterships()) {
					de.rpgframework.splittermond.foundry.Mastership master = new de.rpgframework.splittermond.foundry.Mastership();
					master.genesisId = ref.getMastership().getId();
					master.description = ref.getMastership().getDescription(Locale.GERMAN);
					master.availableIn = sval.getSkill().getName(Locale.GERMAN);
					master.skill = sval.getSkill().getId();
					master.source = createSourceText(ref.getMastership());

					ItemData<de.rpgframework.splittermond.foundry.Mastership> foundry = new ItemData<de.rpgframework.splittermond.foundry.Mastership>(ref.getName(Locale.GERMAN), "mastery", master);
					entry.addItems(foundry);
				}
			}
			entry.data = data;

			// Spells
			for (SpellValue item : tmp.getSpells()) {
				de.rpgframework.splittermond.foundry.Spell spell = new de.rpgframework.splittermond.foundry.Spell();
				spell.genesisId = item.getResolved().getId();
				for (SpellSchoolEntry entry2 : item.getResolved().getSchools()) {
					if (item.getSkill()==entry2.getSchool())
						spell.skillLevel = entry2.getLevel();
				}

				spell.skill= item.getSkill().getId();
				spell.availableIn = spell.skill+" "+spell.skillLevel;
				spell.difficulty = item.getResolved().getDifficultyString();
				spell.castDuration = item.getResolved().getCastDurationString();
				spell.range = item.getResolved().getCastRange()+"m";
				spell.castRangeMeter = item.getResolved().getCastRange();
				spell.castTicks= item.getResolved().getCastDurationTicks();
				spell.costs = SplitterTools.getFocusString( item.getResolved().getCost() );
				spell.effectDuration = item.getResolved().getSpellDuration();
				spell.enhancementDescription = item.getResolved().getEnhancementDescription(Locale.GERMAN);
				spell.enhancementCosts = item.getResolved().getEnhancementString();
				if (item.getResolved().getEffectRange()!=null) {
					spell.effRangeMeter = item.getResolved().getEffectRange();
					spell.effectArea = item.getResolved().getEffectRange()+"m";
				}

				ItemData<de.rpgframework.splittermond.foundry.Spell> foundry = new ItemData<de.rpgframework.splittermond.foundry.Spell>(item.getResolved().getName(), "spell", spell);
				entry.addItems(foundry);
			}

			// Creature Features
			for (CreatureFeatureValue item : tmp.getFeatures()) {
				de.rpgframework.splittermond.foundry.CreatureFeatureType feat = new de.rpgframework.splittermond.foundry.CreatureFeatureType();
				feat.genesisId = item.getModifyable().getId();
				ItemData<de.rpgframework.splittermond.foundry.CreatureFeatureType> foundry = new ItemData<de.rpgframework.splittermond.foundry.CreatureFeatureType>(item.getNameWithRating(Locale.GERMAN), "npcfeature", feat);

				entry.addItems(foundry);
			}


			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

		ZipEntry zipEntry = new ZipEntry("packs/creatures.db");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
		return;
	}

	//-------------------------------------------------------------------
	private static void createItems(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-weapons");
		pack.setLabel("Ausrüstung/Waffen");
		pack.setEntity("Item");
		pack.setPath("packs/equip-weapons.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);
		pack = new Pack();
		pack.setName("splittermond-armor");
		pack.setLabel("Ausrüstung/Rüstungen");
		pack.setEntity("Item");
		pack.setPath("packs/equip-armors.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);
		pack = new Pack();
		pack.setName("splittermond-equip");
		pack.setLabel("Ausrüstung/Sonstiges");
		pack.setEntity("Item");
		pack.setPath("packs/equip-other.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		WeaponDamageConverter dmgConv = new WeaponDamageConverter();
		StringBuffer bufWeapon = new StringBuffer();
		StringBuffer bufArmor  = new StringBuffer();
		StringBuffer bufOther  = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (ItemTemplate tmp : SplitterMondCore.getItemList(ItemTemplate.class)) {
			module.addTranslation("de", "item."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "item."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));

			//			de.rpgframework.eden.foundry.splimo.Spell converted = new de.rpgframework.eden.foundry.splimo.Spell();
			CompendiumEntry entry = new CompendiumEntry();
			entry._id  = tmp.getId();
			entry.name = tmp.getName(Locale.GERMAN);
			switch (tmp.getType()) {
			case WEAPON_CLOSE: case WEAPON_RANGED:
				entry.type = "weapon";
				break;
			case ARMOR:
				entry.type = "armor";
				break;
			default:
				entry.type = "equipment";
			}

			de.rpgframework.splittermond.foundry.Gear data = new de.rpgframework.splittermond.foundry.Gear();
			try {
				switch (tmp.getType()) {
				case WEAPON_RANGED:
				case WEAPON_CLOSE:
					if (tmp.getAttribute(SMItemAttribute.SPEED)!=null)
						data.weaponSpeed = tmp.getAttribute(SMItemAttribute.SPEED).getDistributed();
					if (tmp.getAttribute(SMItemAttribute.DAMAGE)!=null)
						data.damage = dmgConv.write(tmp.getAttribute(SMItemAttribute.DAMAGE).getDistributed());
					if (tmp.getAttribute(SMItemAttribute.ATTRIBUTE1)!=null) {
						data.attribute1 = Attribute.valueOf(tmp.getAttribute(SMItemAttribute.ATTRIBUTE1).getRawValue()).name().toLowerCase();
					}
					if (tmp.getAttribute(SMItemAttribute.ATTRIBUTE2)!=null) {
						data.attribute2 = Attribute.valueOf(tmp.getAttribute(SMItemAttribute.ATTRIBUTE2).getRawValue()).name().toLowerCase();
					}
					if (tmp.getAttribute(SMItemAttribute.SKILL)!=null) {
						data.skill = tmp.getAttribute(SMItemAttribute.SKILL).getRawValue();
						if (data.skill.startsWith("W")) {
							System.out.println("SplittermondCompendiumFoundry: "+data.skill+" for "+tmp);
						}
					}
					break;
				case ARMOR:
					if (tmp.getSubType()==ItemSubType.SHIELD)
						entry.type = "shield";
					if (tmp.getAttribute(SMItemAttribute.DEFENSE)!=null)
						data.defenseBonus = tmp.getAttribute(SMItemAttribute.DEFENSE).getDistributed();
					if (tmp.getAttribute(SMItemAttribute.DAMAGE_REDUCTION)!=null)
						data.damageReduction = tmp.getAttribute(SMItemAttribute.DAMAGE_REDUCTION).getDistributed();
					if (tmp.getAttribute(SMItemAttribute.SPEED)!=null)
						data.tickMalus = tmp.getAttribute(SMItemAttribute.SPEED).getDistributed();
					if (tmp.getAttribute(SMItemAttribute.HANDICAP)!=null)
						data.handicap = tmp.getAttribute(SMItemAttribute.HANDICAP).getDistributed();
					break;
				default:
				}
				if (tmp.getAttribute(SMItemAttribute.AVAILABILITY)!=null) {
					switch (((Availability)tmp.getAttribute(SMItemAttribute.AVAILABILITY).getValue())) {
					case VILLAGE:    data.availability="village"; break;
					case SMALL_TOWN: data.availability="town"; break;
					case LARGE_TOWN: data.availability="city"; break;
					case CAPITAL   : data.availability="metropolis"; break;
					}
				}
				if (tmp.getAttribute(SMItemAttribute.COMPLEXITY)!=null)
					data.complexity  = ((Complexity)tmp.getAttribute(SMItemAttribute.COMPLEXITY).getValue()).getID();
				if (tmp.getAttribute(SMItemAttribute.LOAD)!=null)
					data.weight        = tmp.getAttribute(SMItemAttribute.LOAD).getDistributed();
				if (tmp.getAttribute(SMItemAttribute.PRICE)!=null)
					data.price        = tmp.getAttribute(SMItemAttribute.PRICE).getDistributed();
				if (tmp.getAttribute(SMItemAttribute.RIGIDITY)!=null)
					data.hardness     = tmp.getAttribute(SMItemAttribute.RIGIDITY).getDistributed();
				if (tmp.getAttribute(SMItemAttribute.FEATURES)!=null) {
					FeatureList list = tmp.getAttribute(SMItemAttribute.FEATURES).getValue();
					List<String> newList = new ArrayList<String>();
					list.forEach( (feat) -> newList.add(feat.getName(Locale.GERMAN)));
					data.features     = String.join(", ", newList);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			entry.data = data;

			switch (tmp.getType()) {
			case WEAPON_CLOSE: case WEAPON_RANGED:
				bufWeapon.append(gson.toJson(entry));
				bufWeapon.append('\n');
				break;
			case ARMOR:
				bufArmor.append(gson.toJson(entry));
				bufArmor.append('\n');
				break;
			default:
				bufOther.append(gson.toJson(entry));
				bufOther.append('\n');
			}
		}

		ZipEntry zipEntry = new ZipEntry("packs/equip-weapons.db");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(bufWeapon.toString().getBytes(Charset.forName("UTF-8")));
		zipEntry = new ZipEntry("packs/equip-armors.db");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(bufArmor.toString().getBytes(Charset.forName("UTF-8")));
		zipEntry = new ZipEntry("packs/equip-other.db");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(bufOther.toString().getBytes(Charset.forName("UTF-8")));
	}

	//-------------------------------------------------------------------
	private static void createPowers(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-powers");
		pack.setLabel("Stärken");
		pack.setEntity("Item");
		pack.setPath("packs/powers.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (Power tmp : SplitterMondCore.getItemList(Power.class)) {
			module.addTranslation("de", "strength."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "strength."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));

			//			de.rpgframework.eden.foundry.splimo.Spell converted = new de.rpgframework.eden.foundry.splimo.Spell();
			CompendiumEntry entry = new CompendiumEntry();
			entry._id  = tmp.getId();
			entry.name = tmp.getName(Locale.GERMAN);
			entry.type = "strength";

			de.rpgframework.splittermond.foundry.Power data = new de.rpgframework.splittermond.foundry.Power();
			data.genesisId = tmp.getId();
			data.source = createSourceText(tmp);
			data.level   = tmp.hasLevel();
			data.description = tmp.getDescription(Locale.GERMAN);
			switch (tmp.getSelectable()) {
			case MULTIPLE:
				data.multiSelectable = true;
				break;
			case GENERATION:
				data.onCreationOnly = true;
				break;
			}
			entry.data = data;

			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

		ZipEntry zipEntry = new ZipEntry("packs/powers.db");
		zipOut.putNextEntry(zipEntry);
		zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
	}

	//-------------------------------------------------------------------
	private static void createMasterships(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-mastership");
		pack.setLabel("Meisterschaften");
		pack.setEntity("Item");
		pack.setPath("packs/masterships.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (Mastership tmp : SplitterMondCore.getItemList(Mastership.class)) {
			module.addTranslation("de", "mastery."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "mastery."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));

			//			de.rpgframework.eden.foundry.splimo.Spell converted = new de.rpgframework.eden.foundry.splimo.Spell();
			CompendiumEntry entry = new CompendiumEntry();
			entry._id  = tmp.getId();
			entry.name = tmp.getName(Locale.GERMAN);
			entry.type = "mastery";

			de.rpgframework.splittermond.foundry.Mastership data = new de.rpgframework.splittermond.foundry.Mastership();
			data.genesisId = tmp.getId().toLowerCase();
			data.source = createSourceText(tmp);
			data.availableIn   = tmp.getSkill().getId();
			data.level   = tmp.getLevel();
			data.description = tmp.getDescription(Locale.GERMAN);
			entry.data = data;

			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

		ZipEntry zipEntry = new ZipEntry(pack.getPath());
		zipOut.putNextEntry(zipEntry);
		zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
	}

	//-------------------------------------------------------------------
	private static void createFeatureTypes(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-featuretypes");
		pack.setLabel("Merkmale");
		pack.setEntity("Item");
		pack.setPath("packs/featuretypes.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (FeatureType tmp : SplitterMondCore.getItemList(FeatureType.class)) {
			module.addTranslation("de", "featuretype."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "featuretype."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));

//			//			de.rpgframework.eden.foundry.splimo.Spell converted = new de.rpgframework.eden.foundry.splimo.Spell();
//			CompendiumEntry entry = new CompendiumEntry();
//			entry._id  = tmp.getId();
//			entry.name = tmp.getName(Locale.GERMAN);
//			entry.type = "featuretype";
//
//			de.rpgframework.splittermond.foundry.FeatureType data = new de.rpgframework.splittermond.foundry.FeatureType();
//			data.id = tmp.getId().toLowerCase();
//			data.source = createSourceText(tmp);
//			data.validTypes = tmp.getApplyableItemTypes().stream().map(t -> t.name()).collect(Collectors.toList());
//			data.level   = tmp.hasLevel();
//			entry.data = data;
//
//			buf.append(gson.toJson(entry));
//			buf.append('\n');
		}

		ZipEntry zipEntry = new ZipEntry(pack.getPath());
		zipOut.putNextEntry(zipEntry);
		zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
	}

	//-------------------------------------------------------------------
	private static void createCreatureFeatureTypes(Module module, ZipOutputStream zipOut, boolean shallow) throws IOException {
		Pack pack = new Pack();
		pack.setName("splittermond-creaturefeaturetypes");
		pack.setLabel("Kreaturmerkmale");
		pack.setEntity("Item");
		pack.setPath("packs/creaturefeaturetypes.db");
		pack.setSystem("splittermond");
		module.getPacks().add(pack);

		if (shallow)
			return;

		StringBuffer buf = new StringBuffer();
		Gson gson = new GsonBuilder().create();
		for (CreatureFeature tmp : SplitterMondCore.getItemList(CreatureFeature.class)) {
			module.addTranslation("de", "npcfeature."+tmp.getId()+".desc", tmp.getDescription(Locale.GERMAN));
			module.addTranslation("de", "npcfeature."+tmp.getId()+".name", tmp.getName(Locale.GERMAN));

			//			de.rpgframework.eden.foundry.splimo.Spell converted = new de.rpgframework.eden.foundry.splimo.Spell();
			CompendiumEntry entry = new CompendiumEntry();
			entry._id  = tmp.getId();
			entry.name = tmp.getName(Locale.GERMAN);
			entry.type = "npcfeature";

			de.rpgframework.splittermond.foundry.CreatureFeatureType data = new de.rpgframework.splittermond.foundry.CreatureFeatureType();
			data.genesisId = tmp.getId().toLowerCase();
			data.source = createSourceText(tmp);
			data.description = tmp.getDescription(Locale.GERMAN);
			data.level   = tmp.hasLevel();
			entry.data = data;

			buf.append(gson.toJson(entry));
			buf.append('\n');
		}

		ZipEntry zipEntry = new ZipEntry(pack.getPath());
		zipOut.putNextEntry(zipEntry);
		zipOut.write(buf.toString().getBytes(Charset.forName("UTF-8")));
	}

}
