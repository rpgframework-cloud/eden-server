package de.rpgframework.eden.api;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.httprpc.Description;
import org.httprpc.RequestMethod;
import org.httprpc.ResourcePath;
import org.httprpc.WebService;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.http.CharacterService;
import de.rpgframework.reality.server.PlayerImpl;
import de.rpgframework.reality.server.ServerCharacterHandle;

/**
 * @author stefa
 *
 */
@SuppressWarnings("serial")
@WebServlet(urlPatterns={"/auth/character/*"})
@Description("Modify characters and their corresponding files")
public class CharacterServlet2 extends WebService implements CharacterService {

	private final static Logger logger = LogManager.getLogger(CharacterServlet2.class);

	private BackendAccess backend;

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		logger.warn("Init called");
		backend = (BackendAccess) config.getServletContext().getAttribute(Constants.ATTRIB_BACKEND);
	}

	@RequestMethod("GET")
	@ResourcePath("sum")
	@Description("Addiert zwei Zahlen")
	public double getSum(@Description("Zahl 1") double a, double b) {
		System.err.println("CharacterService.getSum");
		return a + b;
	}

	@RequestMethod("GET")
	@ResourcePath("sum")
	public double getSum(List<Double> values) {
		System.err.println("CharacterService.getSum(List)");
		double total = 0;

		for (double value : values) {
			total += value;
		}

		return total;
	}

	//-------------------------------------------------------------------
	@Override
	@RequestMethod("GET")
	@ResourcePath("/")
	@Description("Returns a list of all characters for the user")
	public List<CharacterHandle> getCharacters() throws IOException {
		HttpServletRequest request = super.getRequest();
		Principal user = request.getUserPrincipal();

		// Get player object
		try {
			PlayerImpl player = backend.getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
			if (player==null) {
				logger.error("Authenticatef user '"+user.getName()+"' but did not find associated Player object");
				throw new NullPointerException("Player object not found"); 
			}

			// Return all characters
			List<CharacterHandle> ret = backend.getCharacterDatabase().listCharacters(player);
			logger.debug("Return list of "+ret.size()+" characters for "+player);
			return ret;
		} catch (SQLException e) {
			logger.error("Failed accessing database: "+e);
			throw new IOException("Failed accessing database",e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.http.CharacterService#getCharacters(RoleplayingSystem)
	 */
	@Override
	@RequestMethod("GET")
	@ResourcePath("/")
	@Description("Returns a list of all characters of the specific RPG for the user")
	public List<CharacterHandle> getCharacters(@Description("The RPG to limit characters to") RoleplayingSystem rules) throws IOException {
		HttpServletRequest request = super.getRequest();
		Principal user = request.getUserPrincipal();

		// Get player object
		try {
			PlayerImpl player = backend.getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
			if (player==null) {
				logger.error("Authenticatef user '"+user.getName()+"' but did not find associated Player object");
				throw new NullPointerException("Player object not found"); 
			}

			// Return all characters
			List<CharacterHandle> ret = backend.getCharacterDatabase().listCharacters(player);
			ret = ret.stream().filter(ch -> ch.getRuleIdentifier()==rules).collect(Collectors.toList());
			logger.debug("Return list of "+ret.size()+" characters");
			return ret;
		} catch (SQLException e) {
			logger.error("Failed accessing database: "+e);
			throw new IOException("Failed accessing database",e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.http.CharacterService#createCharacter(RoleplayingSystem, String)
	 */
	@Override
	@RequestMethod("POST")
	@ResourcePath("")
	@Description("Create a new character")
	public CharacterHandle createCharacter(
			@Description("Roleplaying system") RoleplayingSystem rules, 
			@Description("Character name - must be unique within rules") String name) throws IOException {
		HttpServletRequest request = super.getRequest();
		Principal user = request.getUserPrincipal();
		if (user==null) {
			logger.debug("PUT request for character, but no user object found");
			throw new NoSuchElementException("User object not found"); 
		}
		PlayerImpl player = null;
		try {
			player = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
		} catch (SQLException e) {
			logger.error("Failed accessing player",e);
			throw new IOException("Failed accessing player in database: "+e,e); 
		}

		logger.info(String.format("User '%s' adds character '%s' for %s", user.getName(), name,  rules));

		try {
			// Ensure character does not exist yet
			ServerCharacterHandle handle = backend.getCharacterDatabase().getCharacter(player, rules, name);
			if (handle!=null) {
				logger.warn("User "+user+" tries to add already existing character "+rules+"/"+name);
				throw new IllegalStateException("Character already exists");
			}

			handle = backend.getCharacterDatabase().addCharacter(player, rules, name);
			return handle;
		} catch (SQLException e) {
			logger.error("Failed creating character",e);
			throw new IOException("Database error creating character: "+e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.http.CharacterService#modifyCharacter(CharacterHandle)
	 */
	@Override
	@RequestMethod("PUT")
	@ResourcePath("?:uuid")
	@Description("Update the character")
	public void modifyCharacter(
			@Description("UUID") String uuid, 
			@Description("Updated character handle") CharacterHandle handle) throws IOException {
		HttpServletRequest request = super.getRequest();
		Principal user = request.getUserPrincipal();
		if (user==null) {
			logger.debug("PUT request for character, but no user object found");
			throw new NoSuchElementException("User object not found"); 
		}
		PlayerImpl player = null;
		try {
			player = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
		} catch (SQLException e) {
			logger.error("Failed accessing player",e);
			throw new IOException("Failed accessing player in database: "+e,e); 
		}

		try {
			// Find old handle for that UUID
			ServerCharacterHandle oldHandle = (ServerCharacterHandle) backend.getCharacterDatabase().getCharacter(player, UUID.fromString(uuid));
			if (oldHandle==null) {
				logger.error("Trying to update non-existint character "+handle.getUUID());
				throw new IOException("Trying to update non-existing character"); 
			}
			logger.info(String.format("User '%s' modifies character '%s' for %s", user.getName(), oldHandle.getName(), oldHandle.getRuleIdentifier()));

			// Now update
			oldHandle.setName(handle.getName());
			oldHandle.setRuleIdentifier(handle.getRuleIdentifier());
			oldHandle.setLastModified(handle.getLastModified());
			backend.getCharacterDatabase().update(oldHandle);
		} catch (SQLException e) {
			throw new IOException("Database error: "+e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.http.CharacterService#deleteCharacter(java.lang.String)
	 */
	@Override
	@RequestMethod("DELETE")
    @ResourcePath("?:uuid")
	public void deleteCharacter(@Description("UUID") String uuid) throws IOException {
		HttpServletRequest request = super.getRequest();
		Principal user = request.getUserPrincipal();

		// Get player object
		try {
			PlayerImpl player = backend.getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
			if (player==null) {
				logger.error("Authenticatef user '"+user.getName()+"' but did not find associated Player object");
				throw new NullPointerException("Player object not found"); 
			}

			CharacterHandle handle = backend.getCharacterDatabase().getCharacter(player, UUID.fromString(uuid));
			if (handle!=null) {
				logger.info("User "+user+" deleted character "+handle.getName());
				backend.getCharacterDatabase().delete(handle);
			} else {
				logger.info("User "+user+" deleted  a non-existing character "+uuid);
			}
		} catch (SQLException e) {
			logger.error("Failed accessing database: "+e);
			throw new IOException("Failed accessing database",e);
		}
	}

}
