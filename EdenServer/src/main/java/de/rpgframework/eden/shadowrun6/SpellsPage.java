package de.rpgframework.eden.shadowrun6;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.router.Route;

import de.rpgframework.eden.PageHierarchy;
import de.rpgframework.eden.gui.DataItemLibraryLayout;
import de.rpgframework.eden.intern.lookup.LookupLayout;
import de.rpgframework.eden.renderer.SR6SpellRenderer;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.ASpell.Category;
import de.rpgframework.shadowrun.ASpell.Range;
import de.rpgframework.shadowrun6.Shadowrun6Core;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Route(value=PageHierarchy.PATH_LOOKUP_SHADOWRUN6_SPELLS, layout = LookupLayout.class)
@StyleSheet("frontend://styles/shadowrun6.css")
public class SpellsPage extends DataItemLibraryLayout<ASpell> {

	private static Logger logger = LogManager.getLogger(SpellsPage.class);

	private Select<Range> filterSkill;
	private Select<Category> filterType;
	
	private Collator collate ;
	
	//-------------------------------------------------------------------
	public SpellsPage() {
		UI.getCurrent().getPage().setTitle(getTranslation("navigation.lookup.shadowrun6.spells"));
		
		super.renderer = new SR6SpellRenderer(UI.getCurrent().getLocale(), new I18NProvider() {
			public String getTranslation(String key, Locale locale, Object... params) {
				return SpellsPage.this.getTranslation(key, locale, params);
			}
			public List<Locale> getProvidedLocales() { return new ArrayList<>(); }
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#getPageHierarchy()
	 */
	@Override
	protected PageHierarchy getPageHierarchy() {
		return PageHierarchy.SHADOWRUN6_SPELLS;
	}

	//-------------------------------------------------------------------
	public String getTitle() {
		return getTranslation(getPageHierarchy().getNavigationID());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#getUnfilteredData(java.util.Locale)
	 */
	@Override
	protected List<ASpell> getUnfilteredData(Locale loc) {
		return Shadowrun6Core.getItemList(ASpell.class);
	}

	//-------------------------------------------------------------------
	protected void initGridColumns(Locale loc) {
		logger.info("Locale: "+loc+" vs "+UI.getCurrent().getLocale());
		grid.setMinWidth("30em");
		grid.getColumns().get(0).setWidth("60%");
		grid.addColumn(ASpell::getCategory)
			.setHeader(getTranslation("navigation.lookup.shadowrun6.spells.column.category"))
			.setWidth("40%");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#initFilter(java.util.Locale)
	 */
	@Override
	protected void initFilter(Locale loc) {
		collate = Collator.getInstance(loc);
		filterSkill = new Select<Range>();
		filterSkill.setPlaceholder(getTranslation("page.spells.filter.range.placeholder"));
		List<Range> skills = Arrays.asList(ASpell.Range.values());
		Collections.sort(skills, new Comparator<Range>() {
			public int compare(Range o1, Range o2) {
				return collate.compare(o1.getName(loc), o2.getName(loc));
			}
		});
		filterSkill.setItems(skills);
		filterSkill.setItemLabelGenerator( (item) -> item!=null?item.getName(loc):getTranslation("page.spells.filter.range.empty"));
		filterSkill.setEmptySelectionAllowed(true);
		filterSkill.setEmptySelectionCaption(getTranslation("page.spells.filter.range.empty"));
		
		filterType = new Select<Category>();
		filterType.setPlaceholder(getTranslation("page.spells.filter.category.placeholder"));
		List<Category> types = Arrays.asList(Category.values());
		Collections.sort(types, new Comparator<Category>() {
			public int compare(Category o1, Category o2) {
				return collate.compare(o1.getName(loc), o2.getName(loc));
			}
		});
		filterType.setItems(types);
		filterType.setItemLabelGenerator( (item) -> item!=null?item.getName():getTranslation("page.spells.filter.category.empty"));
		filterType.setEmptySelectionAllowed(true);
		filterType.setEmptySelectionCaption(getTranslation("page.spells.filter.type.empty"));
		
		filterLayout.add(filterType, filterSkill);
	}

	//-------------------------------------------------------------------
	protected void initInteractivity() {
		super.initInteractivity();
		filterSkill.addValueChangeListener(ev -> refresh());
		filterType .addValueChangeListener(ev -> refresh());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#refresh()
	 */
	@Override
	public void refresh() {
		logger.info("refresh "+dataProvider+" with locale "+UI.getCurrent().getLocale());
		
		Locale loc = UI.getCurrent().getLocale();
		List<ASpell> filtered = new ArrayList<ASpell>();
		if (tfSearch.getValue().isBlank()) {
			filtered.addAll(Shadowrun6Core.getItemList(ASpell.class));
		} else {
			for (ASpell tmp : getUnfilteredData(loc)) {
				String name = tmp.getName(loc);
				if (name.toLowerCase().contains(tfSearch.getValue().toLowerCase())) {
					filtered.add(tmp);
				}
			}
		}
		// Skill
		if (filterSkill.getValue()!=null) {
			filtered = filtered.stream().filter(item -> item.getRange()==filterSkill.getValue()).collect(Collectors.toList());
		}
		// Type
		if (filterType.getValue()!=null) {
			filtered = filtered.stream().filter(item -> item.getCategory()==filterType.getValue()).collect(Collectors.toList());
		}
		
		Collections.sort(filtered, new Comparator<ASpell>() {
			@Override
			public int compare(ASpell c1, ASpell c2) {
				String name1 = c1.getName(UI.getCurrent().getLocale()).toLowerCase();
				String name2 = c2.getName(UI.getCurrent().getLocale()).toLowerCase();
				return Collator.getInstance(UI.getCurrent().getLocale()).compare(name1, name2);
			}
		});

		dataProvider.getItems().clear();
		logger.debug(" Setting "+filtered.size()+" items");
		dataProvider.getItems().addAll(filtered);
		dataProvider.refreshAll();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#navigateTo(java.lang.String)
	 */
	@Override
	protected void navigateTo(String id) {
    	UI.getCurrent().navigate(SpellPage.class, id);
	}
	
}
