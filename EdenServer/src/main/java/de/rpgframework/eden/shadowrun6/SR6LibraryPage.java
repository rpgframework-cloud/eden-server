package de.rpgframework.eden.shadowrun6;

import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.componentfactory.Breadcrumbs;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.FlexLayout.ContentAlignment;
import com.vaadin.flow.component.orderedlayout.FlexLayout.FlexWrap;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinService;

import de.rpgframework.eden.PageHierarchy;
import de.rpgframework.eden.gui.IHasAppHeader;
import de.rpgframework.eden.intern.lookup.LookupLayout;
import de.rpgframework.eden.splittermond.ButtonDefinitions;

/**
 * @author prelle
 *
 */
@Route(value="intern/lookup/shadowrun6", layout = LookupLayout.class)
public class SR6LibraryPage extends VerticalLayout implements IHasAppHeader {

	private static Logger logger = LogManager.getLogger(SR6LibraryPage.class);

	private PageHierarchy hierarchy = PageHierarchy.SHADOWRUN6;
	private Breadcrumbs breadcrumbs;
	private FlexLayout buttons;
	private Button btnSpells;
	
	//-------------------------------------------------------------------
	public SR6LibraryPage() {
		initComponents();
		initLayout();
		initInteractivity();
		
		UI.getCurrent().getPage().setTitle(getTranslation(hierarchy.getNavigationID()));
		getClassNames().add("shadowrun6");
	}

	//-------------------------------------------------------------------
	public String getTitle() {
		return getTranslation(hierarchy.getNavigationID());
	}

	//-------------------------------------------------------------------
	public Button createButton(String imagePath, PageHierarchy page) {
		String key = page.getNavigationID();
		if (imagePath==null) {
			Button ret = new Button(getTranslation(key));
			ret.setHeight(ButtonDefinitions.HEIGHT_BUTTON);
			return ret;
		}
		Image image = new Image(imagePath, "");
		image.setWidth(ButtonDefinitions.HEIGHT_IMAGE);

		Button ret = new Button(getTranslation(key), image);
		ret.setHeight(ButtonDefinitions.HEIGHT_BUTTON);
		return ret;
	}

	//-------------------------------------------------------------------
	private void initComponents() {
//		btnBeastiary   = createButton(ButtonDefinitions.PATH_BEASTIARY, PageHierarchy.SPLITTERMOND_BEASTIARY);
//		btnNPC         = createButton(ButtonDefinitions.PATH_NPCS     , ButtonDefinitions.KEY_NPCS);
//		btnNPC.setEnabled(false);
//		btnCreatFeat   = createButton(ButtonDefinitions.PATH_CREATFEAT, ButtonDefinitions.KEY_CREATFEAT);
//		btnCreatFeat.setEnabled(false);
//		btnGear  = createButton(ButtonDefinitions.PATH_GEAR, ButtonDefinitions.KEY_GEAR);
//		btnGear.setEnabled(false);
//		btnFeatures  = createButton(ButtonDefinitions.PATH_FEATURE, ButtonDefinitions.KEY_FEATURES);
//		btnFeatures.setEnabled(false);
//		btnMasters  = createButton(ButtonDefinitions.PATH_MASTERSHIPS, PageHierarchy.SPLITTERMOND_MASTERSHIPS);
//		btnPowers  = createButton(ButtonDefinitions.PATH_POWERS, ButtonDefinitions.KEY_POWERS);
//		btnPowers.setEnabled(false);
		btnSpells = createButton(ButtonDefinitions.PATH_SPELLS, PageHierarchy.SPLITTERMOND_SPELLS);
//		btnStatuses = createButton(ButtonDefinitions.PATH_STATUSES, ButtonDefinitions.KEY_STATUS);
//		btnStatuses.setEnabled(false);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Locale loc = UI.getCurrent().getLocale();
		breadcrumbs = hierarchy.createBreadcrumbs(VaadinService.getCurrent().getInstantiator().getI18NProvider(), loc);
//		Breadcrumbs breadcrumbs = new Breadcrumbs();
//		breadcrumbs.add(
//		    new Breadcrumb(RoleplayingSystem.SPLITTERMOND.getName(loc),"splittermond"),
//		    new Breadcrumb(getTranslation("navigation.lookup.splittermond"))
//		    );
		add(breadcrumbs);

		/* Intro graphic with centered image/text */
		Label heading = new Label(getTranslation(hierarchy.getNavigationID()));
		Div intro = new Div(heading);
		intro.setWidthFull();
		intro.setClassName("landingpage-intro");
		intro.setClassName("intro-shadowrun6", true);
		add(intro);

//		add(new H2(getTranslation("navigation.splittermond.library")));
		
		buttons = new FlexLayout(btnSpells);
		buttons.setAlignContent(ContentAlignment.SPACE_BETWEEN);
		buttons.setJustifyContentMode(JustifyContentMode.START);
		buttons.setFlexWrap(FlexWrap.WRAP);
		buttons.getChildren().forEach(comp -> ((Button)comp).getStyle().set("margin", "0.5em"));
		buttons.setWidth("80%");
		buttons.getStyle().set("margin-left", "10%");
//		buttons.setWidthFull();
		buttons.setFlexGrow(1, btnSpells);
		add(buttons);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
//		btnBeastiary.addClickListener(
//		        event -> {
//		        	UI.getCurrent().navigate(BestiariumPage.class);
//		        });
//		btnMasters.addClickListener( ev -> UI.getCurrent().navigate(MastershipsPage.class));
		btnSpells .addClickListener( ev -> UI.getCurrent().navigate(SpellsPage.class));
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see com.vaadin.flow.component.Component#onAttach(com.vaadin.flow.component.AttachEvent)
	 */
	@Override
    protected void onAttach(AttachEvent attachEvent) {
		UI.getCurrent().getElement().getClassList().clear();
		UI.getCurrent().getElement().getClassList().add("shadowrun6");
   }

}
