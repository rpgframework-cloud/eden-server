package de.rpgframework.eden.shadowrun6;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SplitterMondCore;

import com.vaadin.componentfactory.Breadcrumb;
import com.vaadin.componentfactory.Breadcrumbs;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.intern.lookup.LookupLayout;
import de.rpgframework.eden.renderer.SMSpellRenderer;

/**
 * @author prelle
 *
 */
@Route(value="lookup/shadowrun6/spells/show", layout = LookupLayout.class)
@StyleSheet("frontend://styles/splittermond.css")
public class SpellPage extends VerticalLayout implements HasUrlParameter<String> {

	private static final long serialVersionUID = -3551663637016191770L;
	private static Logger logger = LogManager.getLogger(SpellPage.class);
//	private ExtendedClientDetails details; 
	
	private Breadcrumbs breadcrumbs;
	private SMSpellRenderer renderer;
	
	//-------------------------------------------------------------------
	@SuppressWarnings("serial")
	public SpellPage() {
		Locale loc = UI.getCurrent().getLocale();
		breadcrumbs = new Breadcrumbs();
		breadcrumbs.add(
		    new Breadcrumb(RoleplayingSystem.SPLITTERMOND.getName(loc),"splittermond", true),
		    new Breadcrumb(getTranslation("navigation.splittermond.library"), "splittermond/library", true),
		    new Breadcrumb(getTranslation("navigation.splittermond.library.spells"), "/splittermond/library/spells")
		    );
		add(breadcrumbs);
		
		renderer = new SMSpellRenderer(UI.getCurrent().getLocale(), new I18NProvider() {
			public String getTranslation(String key, Locale locale, Object... params) {
				return SpellPage.this.getTranslation(key, locale, params);
			}
			public List<Locale> getProvidedLocales() { return new ArrayList<>(); }
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see com.vaadin.flow.router.HasUrlParameter#setParameter(com.vaadin.flow.router.BeforeEvent, java.lang.Object)
	 */
	@Override
	public void setParameter(BeforeEvent event, String id) {
		logger.info("spell is "+id);
		Locale loc = UI.getCurrent().getLocale();
		
		Spell data = SplitterMondCore.getItem(Spell.class, id);
		if (data==null) 
			return;
		
		UI.getCurrent().getPage().setTitle(data.getName(loc));
		breadcrumbs.add(new Breadcrumb(data.getName(loc)));

		FormLayout layout = new FormLayout();
		layout.setResponsiveSteps(
		        new ResponsiveStep("20em", 1),
		        new ResponsiveStep("40em", 2),
		        new ResponsiveStep("60em", 3));
			
		add(renderer.createComponent(data));
	}

}
