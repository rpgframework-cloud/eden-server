package de.rpgframework.eden.splittermond;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureFeature;
import org.prelle.splimo.creature.CreatureType;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.router.Route;

import de.rpgframework.eden.PageHierarchy;
import de.rpgframework.eden.gui.DataItemLibraryLayout;
import de.rpgframework.eden.gui.IHasAppHeader;
import de.rpgframework.eden.intern.lookup.LookupLayout;
import de.rpgframework.eden.renderer.CreatureRenderer;

/**
 * @author prelle
 *
 */
@Route(value=PageHierarchy.PATH_LOOKUP_SPLITTERMOND_CREATURES, layout = LookupLayout.class)
@StyleSheet("frontend://styles/splittermond.css")
@SuppressWarnings("serial")
public class BestiariumPage extends DataItemLibraryLayout<Creature> implements IHasAppHeader {

	private static Logger logger = LogManager.getLogger(BestiariumPage.class);

	private Select<CreatureType> filterType;
	private Select<Integer> filterLevel;
	private Select<CreatureFeature> filterFeat;

	private Collator collate ;

	//-------------------------------------------------------------------
	public BestiariumPage() {
		System.err.println("BestiariumPage: "+UI.getCurrent().getPage());
		UI.getCurrent().getPage().setTitle(getTranslation("splittermond.page.beastiary"));

		super.renderer = new CreatureRenderer(UI.getCurrent().getLocale(), new I18NProvider() {
			public String getTranslation(String key, Locale locale, Object... params) {
				return BestiariumPage.this.getTranslation(key, locale, params);
			}
			public List<Locale> getProvidedLocales() { return new ArrayList<>(); }
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#getPageHierarchy()
	 */
	@Override
	protected PageHierarchy getPageHierarchy() {
		return PageHierarchy.SPLITTERMOND_BEASTIARY;
	}

	//-------------------------------------------------------------------
	public String getTitle() {
		return getTranslation("splittermond.page.beastiary");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#getUnfilteredData(java.util.Locale)
	 */
	@Override
	protected List<Creature> getUnfilteredData(Locale loc) {
		return SplitterMondCore.getItemList(Creature.class);
	}

	//-------------------------------------------------------------------
	protected void initGridColumns(Locale loc) {
		grid.addColumn(new ValueProvider<Creature,String>(){
			public String apply(Creature source) {
				int min = source.getLevelGroup();
				int max = source.getLevelAlone();
				if (min==max)
					return String.valueOf(min);
				return min+"/"+max;
			}}).setHeader(getTranslation("column.level")).setWidth("4em");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#initFilter(java.util.Locale)
	 */
	@Override
	protected void initFilter(Locale loc) {
		collate = Collator.getInstance(loc);
		filterLevel = new Select<Integer>(0,1,2,3,4,5);
		filterLevel.setPlaceholder(getTranslation("page.creatures.filter.level.placeholder"));
		filterLevel.setItemLabelGenerator( (item) -> item!=null?String.valueOf(item):getTranslation("page.creatures.filter.level.empty"));
		filterLevel.setEmptySelectionAllowed(true);
		filterLevel.setEmptySelectionCaption(getTranslation("page.creatures.filter.level.empty"));

		// Creature Type
		filterType = new Select<CreatureType>();
		filterType.setPlaceholder(getTranslation("page.creatures.filter.ctype.placeholder"));
		List<CreatureType> skills = SplitterMondCore.getItemList(CreatureType.class);
		Collections.sort(skills, new Comparator<CreatureType>() {
			public int compare(CreatureType o1, CreatureType o2) {
				return collate.compare(o1.getName(loc), o2.getName(loc));
			}
		});
		filterType.setItems(skills);
		filterType.setItemLabelGenerator( (item) -> item!=null?item.getName(loc):getTranslation("page.creatures.filter.ctype.empty"));
		filterType.setEmptySelectionAllowed(true);
		filterType.setEmptySelectionCaption(getTranslation("page.creatures.filter.ctype.empty"));

		filterFeat = new Select<CreatureFeature>();
		filterFeat.setPlaceholder(getTranslation("page.creatures.filter.feat.placeholder"));
		List<CreatureFeature> types = SplitterMondCore.getItemList(CreatureFeature.class);
		Collections.sort(types, new Comparator<CreatureFeature>() {
			public int compare(CreatureFeature o1, CreatureFeature o2) {
				return collate.compare(o1.getName(), o2.getName());
			}
		});
		filterFeat.setItems(types);
		filterFeat.setItemLabelGenerator( (item) -> item!=null?item.getName():getTranslation("page.creatures.filter.feat.empty"));
		filterFeat.setEmptySelectionAllowed(true);
		filterFeat.setEmptySelectionCaption(getTranslation("page.creatures.filter.feat.empty"));

		filterLayout.add(filterType, filterLevel, filterFeat);
	}

	//-------------------------------------------------------------------
	protected void initInteractivity() {
		super.initInteractivity();
		filterLevel.addValueChangeListener(ev -> refresh());
		filterFeat .addValueChangeListener(ev -> refresh());
		filterType .addValueChangeListener(ev -> refresh());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#refresh()
	 */
	@Override
	public void refresh() {
		logger.info("refresh");
		Locale loc = UI.getCurrent().getLocale();

		List<Creature> filtered = new ArrayList<Creature>();
		if (tfSearch.getValue().isBlank()) {
			filtered.addAll(SplitterMondCore.getItemList(Creature.class));
		} else {
			for (Creature tmp : getUnfilteredData(loc)) {
				String name = tmp.getName(loc);
				if (name.toLowerCase().contains(tfSearch.getValue().toLowerCase())) {
					filtered.add(tmp);
				}
			}
		}
		// Skill
		if (filterFeat.getValue()!=null) {
			filtered = filtered.stream().filter(item -> item.getFeatures().stream().anyMatch(ct -> ct.getResolved()==filterFeat.getValue())
					).collect(Collectors.toList());
		}
		// Level
		if (filterLevel.getValue()!=null) {
			filtered = filtered.stream().filter(item -> item.getLevelAlone()==filterLevel.getValue()||item.getLevelGroup()==filterLevel.getValue()).collect(Collectors.toList());
		}
		// Type
		if (filterType.getValue()!=null) {
			filtered = filtered.stream().filter(item -> item.getCreatureTypes().stream().anyMatch(ct -> ct.getType()==filterType.getValue())
					).collect(Collectors.toList());
		}

		Collections.sort(filtered, new Comparator<Creature>() {
			@Override
			public int compare(Creature c1, Creature c2) {
				String name1 = c1.getName(UI.getCurrent().getLocale()).toLowerCase();
				String name2 = c2.getName(UI.getCurrent().getLocale()).toLowerCase();
				return Collator.getInstance(UI.getCurrent().getLocale()).compare(name1, name2);
			}
		});

		dataProvider.getItems().clear();
		logger.debug(" Setting "+filtered.size()+" items");
		dataProvider.getItems().addAll(filtered);
		dataProvider.refreshAll();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#navigateTo(java.lang.String)
	 */
	@Override
	protected void navigateTo(String id) {
    	UI.getCurrent().navigate(BestiariumCreaturePage.class, id);
	}

}
