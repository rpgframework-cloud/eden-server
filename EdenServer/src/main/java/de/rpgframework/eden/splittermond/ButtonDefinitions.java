package de.rpgframework.eden.splittermond;

/**
 * @author prelle
 *
 */
public class ButtonDefinitions {
	
	public final static String HEIGHT_IMAGE  = "4em"; 
	public final static String HEIGHT_BUTTON = "5em"; 

//	public final static String KEY_BEASTIARY   = "navigation.splittermond.library.beastiary";
//	public final static String KEY_CREATFEAT   = "navigation.splittermond.library.creature_feature";
//	public final static String KEY_FEATURES    = "navigation.splittermond.library.feature";
//	public final static String KEY_GEAR        = "navigation.splittermond.library.gear";
//	public final static String KEY_MASTERSHIPS = "navigation.splittermond.library.masterships";
//	public final static String KEY_NPCS        = "navigation.splittermond.library.npcs";
//	public final static String KEY_POWERS      = "navigation.splittermond.library.powers";
//	public final static String KEY_SPELLS      = "navigation.splittermond.library.spells";
//	public final static String KEY_STATUS      = "navigation.splittermond.library.statuses";
	
	public final static String PATH_BEASTIARY   = "frontend/images/splittermond/Bestiarium.png";
	public final static String PATH_CREATFEAT   = null;
	public final static String PATH_FEATURE     = null;
	public final static String PATH_GEAR        = null;
	public final static String PATH_MASTERSHIPS = null;
	public final static String PATH_NPCS        = "frontend/images/splittermond/NPC.png";
	public final static String PATH_POWERS      = null;
	public final static String PATH_SPELLS      = "frontend/images/splittermond/Zauber.png";
	public final static String PATH_STATUSES    = null;
	
	
}
