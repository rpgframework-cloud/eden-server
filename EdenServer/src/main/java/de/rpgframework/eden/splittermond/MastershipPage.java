package de.rpgframework.eden.splittermond;

import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.splimo.Mastership;
import org.prelle.splimo.SplitterMondCore;

import com.vaadin.componentfactory.Breadcrumb;
import com.vaadin.componentfactory.Breadcrumbs;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.PageHierarchy;
import de.rpgframework.eden.intern.lookup.LookupLayout;

/**
 * @author prelle
 *
 */
@Route(value=PageHierarchy.PATH_LOOKUP_SPLITTERMOND_MASTERHSIPS+"show", layout = LookupLayout.class)
@StyleSheet("frontend://styles/splittermond.css")
public class MastershipPage extends VerticalLayout implements HasUrlParameter<String> {

	private static final long serialVersionUID = -3551663637016191770L;
	private static Logger logger = LogManager.getLogger(MastershipPage.class);
//	private ExtendedClientDetails details; 
	
	private Breadcrumbs breadcrumbs;
	
	//-------------------------------------------------------------------
	public MastershipPage() {
		Locale loc = UI.getCurrent().getLocale();
		breadcrumbs = new Breadcrumbs();
		breadcrumbs.add(
		    new Breadcrumb(RoleplayingSystem.SPLITTERMOND.getName(loc),"splittermond", true),
		    new Breadcrumb(getTranslation("navigation.splittermond.library"), "splittermond/library", true),
		    new Breadcrumb(getTranslation("navigation.splittermond.library.masterships"), "/splittermond/library/masterships")
		    );
		add(breadcrumbs);
	}

	//-------------------------------------------------------------------
	public String getTitle() {
		return getTranslation("splittermond.page.masterships");
	}

	//-------------------------------------------------------------------
	/**
	 * @see com.vaadin.flow.router.HasUrlParameter#setParameter(com.vaadin.flow.router.BeforeEvent, java.lang.Object)
	 */
	@Override
	public void setParameter(BeforeEvent event, String id) {
		logger.info("mastership is "+id);
		Locale loc = UI.getCurrent().getLocale();
		
		Mastership data = SplitterMondCore.getItem(Mastership.class, id);
		if (data!=null) {
		UI.getCurrent().getPage().setTitle(data.getName(loc));
		breadcrumbs.add(new Breadcrumb(data.getName(loc)));
		}

		FormLayout layout = new FormLayout();
		layout.setResponsiveSteps(
		        new ResponsiveStep("20em", 1),
		        new ResponsiveStep("40em", 2),
		        new ResponsiveStep("60em", 3));
			
//		add(new H2(crea.getName(loc)));
		add(layout);
	}

}
