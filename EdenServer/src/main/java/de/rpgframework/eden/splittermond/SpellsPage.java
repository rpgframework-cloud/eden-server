package de.rpgframework.eden.splittermond;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellSchoolEntry;
import org.prelle.splimo.SpellType;
import org.prelle.splimo.SplitterMondCore;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.router.Route;

import de.rpgframework.eden.PageHierarchy;
import de.rpgframework.eden.gui.DataItemLibraryLayout;
import de.rpgframework.eden.intern.lookup.LookupLayout;
import de.rpgframework.eden.renderer.SMSpellRenderer;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Route(value=PageHierarchy.PATH_LOOKUP_SPLITTERMOND_SPELLS, layout = LookupLayout.class)
@StyleSheet("frontend://styles/splittermond.css")
public class SpellsPage extends DataItemLibraryLayout<Spell> {

	private static Logger logger = LogManager.getLogger(SpellsPage.class);

	private Select<SMSkill> filterSkill;
	private Select<Integer> filterLevel;
	private Select<SpellType> filterType;
	
	private Collator collate ;
	
	//-------------------------------------------------------------------
	public SpellsPage() {
		UI.getCurrent().getPage().setTitle(getTranslation("splittermond.page.spells"));
		
		super.renderer = new SMSpellRenderer(UI.getCurrent().getLocale(), new I18NProvider() {
			public String getTranslation(String key, Locale locale, Object... params) {
				return SpellsPage.this.getTranslation(key, locale, params);
			}
			public List<Locale> getProvidedLocales() { return new ArrayList<>(); }
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#getPageHierarchy()
	 */
	@Override
	protected PageHierarchy getPageHierarchy() {
		return PageHierarchy.SPLITTERMOND_SPELLS;
	}

	//-------------------------------------------------------------------
	public String getTitle() {
		return getTranslation(getPageHierarchy().getNavigationID());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#getUnfilteredData(java.util.Locale)
	 */
	@Override
	protected List<Spell> getUnfilteredData(Locale loc) {
		return SplitterMondCore.getItemList(Spell.class);
	}

	//-------------------------------------------------------------------
	protected void initGridColumns(Locale loc) {
//		grid.addColumn(Spell::getLevel).setHeader(getTranslation("column.level")).setWidth("15%");
		grid.addColumn(new ValueProvider<Spell,String>(){
			public String apply(Spell source) {
				int min = 99;
				int max = -1;
				for (SpellSchoolEntry entry : source.getSchools()) {
					if (filterSkill.getValue()==null || filterSkill.getValue()==entry.getSchool()) {
						min = Math.min(min, entry.getLevel());
						max = Math.max(min, entry.getLevel());
					}
				}
				if (min==max)
					return String.valueOf(min);
				return min+"-"+max;
			}}).setHeader(getTranslation("column.level")).setWidth("4em");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#initFilter(java.util.Locale)
	 */
	@Override
	protected void initFilter(Locale loc) {
		collate = Collator.getInstance(loc);
		filterLevel = new Select<Integer>(0,1,2,3,4,5);
		filterLevel.setPlaceholder(getTranslation("page.spells.filter.level.placeholder"));
		filterLevel.setItemLabelGenerator( (item) -> item!=null?String.valueOf(item):getTranslation("page.spells.filter.level.empty"));
		filterLevel.setEmptySelectionAllowed(true);
		filterLevel.setEmptySelectionCaption(getTranslation("page.spells.filter.level.empty"));
		
		filterSkill = new Select<SMSkill>();
		filterSkill.setPlaceholder(getTranslation("page.spells.filter.skill.placeholder"));
		List<SMSkill> skills = SplitterMondCore.getItemList(SMSkill.class).stream().filter(dat -> dat.getType()==SkillType.MAGIC).collect(Collectors.toList());
		Collections.sort(skills, new Comparator<SMSkill>() {
			public int compare(SMSkill o1, SMSkill o2) {
				return collate.compare(o1.getName(loc), o2.getName(loc));
			}
		});
		filterSkill.setItems(skills);
		filterSkill.setItemLabelGenerator( (item) -> item!=null?item.getName(loc):getTranslation("page.spells.filter.skill.empty"));
		filterSkill.setEmptySelectionAllowed(true);
		filterSkill.setEmptySelectionCaption(getTranslation("page.spells.filter.skill.empty"));
		
		filterType = new Select<SpellType>();
		filterType.setPlaceholder(getTranslation("page.spells.filter.type.placeholder"));
		List<SpellType> types = Arrays.asList(SpellType.values());
		Collections.sort(types, new Comparator<SpellType>() {
			public int compare(SpellType o1, SpellType o2) {
				return collate.compare(o1.getName(), o2.getName());
			}
		});
		filterType.setItems(types);
		filterType.setItemLabelGenerator( (item) -> item!=null?item.getName():getTranslation("page.spells.filter.type.empty"));
		filterType.setEmptySelectionAllowed(true);
		filterType.setEmptySelectionCaption(getTranslation("page.spells.filter.type.empty"));
		
		filterLayout.add(filterSkill, filterLevel, filterType);
	}

	//-------------------------------------------------------------------
	protected void initInteractivity() {
		super.initInteractivity();
		filterLevel.addValueChangeListener(ev -> refresh());
		filterSkill.addValueChangeListener(ev -> refresh());
		filterType .addValueChangeListener(ev -> refresh());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#refresh()
	 */
	@Override
	public void refresh() {
		logger.info("refresh "+dataProvider);
		
		Locale loc = UI.getCurrent().getLocale();
		List<Spell> filtered = new ArrayList<Spell>();
		if (tfSearch.getValue().isBlank()) {
			filtered.addAll(SplitterMondCore.getItemList(Spell.class));
		} else {
			for (Spell tmp : getUnfilteredData(loc)) {
				String name = tmp.getName(loc);
				if (name.toLowerCase().contains(tfSearch.getValue().toLowerCase())) {
					filtered.add(tmp);
				}
			}
		}
		// Skill
		if (filterSkill.getValue()!=null) {
			filtered = filtered.stream().filter(item -> item.getLevelInSchool(filterSkill.getValue())>-1).collect(Collectors.toList());
		}
		// Level
		if (filterLevel.getValue()!=null) {
			filtered = filtered.stream().filter(item -> item.getSchools().stream().anyMatch(sc -> sc.getLevel()==filterLevel.getValue())).collect(Collectors.toList());
		}
		// Type
		if (filterType.getValue()!=null) {
			filtered = filtered.stream().filter(item -> item.getTypes().contains(filterType.getValue())).collect(Collectors.toList());
		}
		
		Collections.sort(filtered, new Comparator<Spell>() {
			@Override
			public int compare(Spell c1, Spell c2) {
				String name1 = c1.getName(UI.getCurrent().getLocale()).toLowerCase();
				String name2 = c2.getName(UI.getCurrent().getLocale()).toLowerCase();
				return Collator.getInstance(UI.getCurrent().getLocale()).compare(name1, name2);
			}
		});

		dataProvider.getItems().clear();
		logger.debug(" Setting "+filtered.size()+" items");
		dataProvider.getItems().addAll(filtered);
		dataProvider.refreshAll();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#navigateTo(java.lang.String)
	 */
	@Override
	protected void navigateTo(String id) {
    	UI.getCurrent().navigate(SpellPage.class, id);
	}
	
}
