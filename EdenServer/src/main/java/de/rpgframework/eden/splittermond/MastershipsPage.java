package de.rpgframework.eden.splittermond;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.splimo.Mastership;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SplitterMondCore;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.router.Route;

import de.rpgframework.eden.PageHierarchy;
import de.rpgframework.eden.gui.DataItemLibraryLayout;
import de.rpgframework.eden.intern.lookup.LookupLayout;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Route(value=PageHierarchy.PATH_LOOKUP_SPLITTERMOND_MASTERHSIPS, layout = LookupLayout.class)
@StyleSheet("frontend://styles/splittermond.css")
public class MastershipsPage extends DataItemLibraryLayout<Mastership> {

	private static Logger logger = LogManager.getLogger(MastershipsPage.class);

	private Select<SMSkill> filterSkill;
	private Select<Integer> filterLevel;
	
	//-------------------------------------------------------------------
	public MastershipsPage() {
		UI.getCurrent().getPage().setTitle(getTranslation("splittermond.page.masterships"));
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#getPageHierarchy()
	 */
	@Override
	protected PageHierarchy getPageHierarchy() {
		return PageHierarchy.SPLITTERMOND_MASTERSHIPS;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#getUnfilteredData(java.util.Locale)
	 */
	@Override
	protected List<Mastership> getUnfilteredData(Locale loc) {
		return SplitterMondCore.getMasterships();
	}

	//-------------------------------------------------------------------
	protected void initGridColumns(Locale loc) {
		grid.addColumn(Mastership::getLevel).setHeader(getTranslation("column.level")).setWidth("15%");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#initFilter(java.util.Locale)
	 */
	@Override
	protected void initFilter(Locale loc) {
		filterLevel = new Select<Integer>(1,2,3,4);
		filterLevel.setPlaceholder(getTranslation("page.masterships.filter.level.placeholder"));
		filterLevel.setItemLabelGenerator( (item) -> item!=null?String.valueOf(item):getTranslation("page.masterships.filter.level.empty"));
		filterLevel.setEmptySelectionAllowed(true);
		filterSkill = new Select<SMSkill>();
		filterSkill.setPlaceholder(getTranslation("page.masterships.filter.skill.placeholder"));
		filterSkill.setItems(SplitterMondCore.getItemList(SMSkill.class));
		filterSkill.setItemLabelGenerator( (item) -> item!=null?item.getName(loc):getTranslation("page.masterships.filter.skill.empty"));
		filterSkill.setEmptySelectionAllowed(true);
		
		filterLayout.add(filterSkill, filterLevel);
	}

	//-------------------------------------------------------------------
	protected void initInteractivity() {
		super.initInteractivity();
		filterLevel.addValueChangeListener(ev -> refresh());
		filterSkill.addValueChangeListener(ev -> refresh());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#refresh()
	 */
	@Override
	public void refresh() {
		logger.info("refresh "+dataProvider);
		
		Locale loc = UI.getCurrent().getLocale();
		List<Mastership> filtered = new ArrayList<Mastership>();
		if (tfSearch.getValue().isBlank()) {
			filtered.addAll(SplitterMondCore.getMasterships());
		} else {
			for (Mastership tmp : getUnfilteredData(loc)) {
				String name = tmp.getName(loc);
				if (name.toLowerCase().contains(tfSearch.getValue().toLowerCase())) {
					filtered.add(tmp);
				}
			}
		}
		// Skill
		if (filterSkill.getValue()!=null) {
			filtered = filtered.stream().filter(item -> item.getSkill()==filterSkill.getValue()).collect(Collectors.toList());
		}
		// Level
		if (filterLevel.getValue()!=null) {
			filtered = filtered.stream().filter(item -> item.getLevel()==filterLevel.getValue()).collect(Collectors.toList());
		}
		
		Collections.sort(filtered, new Comparator<Mastership>() {
			@Override
			public int compare(Mastership c1, Mastership c2) {
				String name1 = c1.getName(UI.getCurrent().getLocale()).toLowerCase();
				String name2 = c2.getName(UI.getCurrent().getLocale()).toLowerCase();
				return Collator.getInstance(UI.getCurrent().getLocale()).compare(name1, name2);
			}
		});

		dataProvider.getItems().clear();
		logger.info(" Setting "+filtered.size()+" items");
		dataProvider.getItems().addAll(filtered);
		dataProvider.refreshAll();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.gui.DataItemLibraryLayout#navigateTo(java.lang.String)
	 */
	@Override
	protected void navigateTo(String id) {
    	UI.getCurrent().navigate(MastershipPage.class, id);
	}
	
}
