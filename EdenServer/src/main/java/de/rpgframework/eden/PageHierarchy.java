package de.rpgframework.eden;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;

import com.vaadin.componentfactory.Breadcrumb;
import com.vaadin.componentfactory.Breadcrumbs;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.router.Route;

import de.rpgframework.eden.intern.lookup.LookupPage;
import de.rpgframework.eden.shadowrun6.SR6LibraryPage;
import de.rpgframework.eden.splittermond.BestiariumPage;
import de.rpgframework.eden.splittermond.SMLibraryPage;
import de.rpgframework.eden.splittermond.MastershipsPage;
import de.rpgframework.eden.splittermond.SpellsPage;

/**
 * @author prelle
 *
 */
public class PageHierarchy {
	
	public final static String PREFIX_PAGES = ""; 
	public final static String PREFIX_API_INTERN = "api/"; 
	public final static String PREFIX_API_EXTERN = "anon/";
	
	public final static String PATH_EXTERN  = PREFIX_PAGES+"extern/";
	public final static String PATH_EXTERN_VERIFY  = PATH_EXTERN+"verify";

	public final static String PATH_LOOKUP  = PREFIX_PAGES+"lookup/";
	public final static String PATH_LOOKUP_SPLITTERMOND  = PATH_LOOKUP+"splittermond/";
	public final static String PATH_LOOKUP_SPLITTERMOND_CREATURES  = PATH_LOOKUP_SPLITTERMOND+"beastiary/";
	public final static String PATH_LOOKUP_SPLITTERMOND_MASTERHSIPS= PATH_LOOKUP_SPLITTERMOND+"masterships/";
	public final static String PATH_LOOKUP_SPLITTERMOND_SPELLS     = PATH_LOOKUP_SPLITTERMOND+"spells/";
	public final static String PATH_LOOKUP_SHADOWRUN6    = PATH_LOOKUP+"shadowrun6/";
	public final static String PATH_LOOKUP_SHADOWRUN6_SPELLS = PATH_LOOKUP_SHADOWRUN6+"spells/";
	
	public final static String PATH_ACCOUNT         = PREFIX_PAGES+"account/";
	public final static String PATH_ACCOUNT_PROFILE = PATH_ACCOUNT+"profile/";
	public final static String PATH_ACCOUNT_CONTENTS= PATH_ACCOUNT+"contents/";
	
	public final static PageHierarchy SPLITTERMOND_BEASTIARY   = new PageHierarchy(  "beastiary", BestiariumPage.class);
	public final static PageHierarchy SPLITTERMOND_MASTERSHIPS = new PageHierarchy("masterships", MastershipsPage.class);
	public final static PageHierarchy SPLITTERMOND_SPELLS      = new PageHierarchy(     "spells", SpellsPage.class);
	
	public final static PageHierarchy SPLITTERMOND = new PageHierarchy("splittermond", SMLibraryPage.class, 
			SPLITTERMOND_BEASTIARY,
			SPLITTERMOND_MASTERSHIPS,
			SPLITTERMOND_SPELLS
	);
	
	public final static PageHierarchy SHADOWRUN6_SPELLS      = new PageHierarchy(     "spells", de.rpgframework.eden.shadowrun6.SpellsPage.class);

	public final static PageHierarchy SHADOWRUN6 = new PageHierarchy("shadowrun6", SR6LibraryPage.class,
			SHADOWRUN6_SPELLS
	);
	
	public final static PageHierarchy LOOKUP = new PageHierarchy("lookup", LookupPage.class, 
			SPLITTERMOND,
			SHADOWRUN6
	);
	
	public final static PageHierarchy UPMOST = new PageHierarchy(null, LookupPage.class, LOOKUP);
	
	
	private String id;
	Class<? extends Component> page;
	PageHierarchy[] children;
	PageHierarchy parent;
	
	//-------------------------------------------------------------------
	PageHierarchy(String id, Class<? extends Component> page, PageHierarchy...children) {
		this.id   = id;
		this.page = page;
		this.children = children;
		for (PageHierarchy child : children) {
			child.parent = this;
		}
	}
	
	//-------------------------------------------------------------------
	public String getNavigationID() {
		if (id==null) {
			return "navigation";
		}
		if (parent==null) {
			return "navigation."+id;
		}
		return parent.getNavigationID()+"."+id;
	}
	
	//-------------------------------------------------------------------
	public int getDepth() {
		if (parent==null)
			return 1;
		return parent.getDepth()+1;
	}
	
	//-------------------------------------------------------------------
	public PageHierarchy getChild(String id) {
		for (PageHierarchy child : children) {
			if (child.id.equals(id))
				return child;
		}
		throw new NoSuchElementException();
	}
	
	//-------------------------------------------------------------------
	public List<PageHierarchy> getPath() {
		List<PageHierarchy> ret = new ArrayList<>();
		if (parent!=null)
			ret.addAll(parent.getPath());
		ret.add(this);
		return ret;
	}
	
	//-------------------------------------------------------------------
	public String getHRef() {
		Route route = page.getAnnotation(Route.class);
		if (route!=null)
			return route.value();
		
		if (parent==null)
			return "/";
		return parent.getHRef()+id+"/";
	}
	
	//-------------------------------------------------------------------
	public Breadcrumbs createBreadcrumbs(I18NProvider i18n, Locale locale) {
		Breadcrumbs breadcrumbs = new Breadcrumbs();
		for (PageHierarchy page : getPath()) {
			if (page==UPMOST)
				continue;
			String id = page.getNavigationID();
			String name = i18n.getTranslation(id, locale);
			breadcrumbs.add(new Breadcrumb(name,page.getHRef(), page.getDepth()<3));
		}
		return breadcrumbs;
	}
}
