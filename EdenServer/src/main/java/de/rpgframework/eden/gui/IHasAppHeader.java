package de.rpgframework.eden.gui;

import java.util.Optional;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;

import de.rpgframework.eden.MainLayout;

/**
 * @author prelle
 *
 */
public interface IHasAppHeader {

	//-------------------------------------------------------------------
	public default AppHeader getAppHeader(Component component) {
        Optional<Component> parent = component.getParent();
        AppHeader menu = null;
        while (parent.isPresent()) { 
            Component p = parent.get();
            if (p instanceof MainLayout) {
                MainLayout main = (MainLayout) p;
                menu = main.getAppHeader();
            }           
            parent = p.getParent();
        }
        return menu;
    }

	//-------------------------------------------------------------------
	public default AppLayout getLayout(Component component) {
        Optional<Component> parent = component.getParent();
        AppLayout menu = null;
        while (parent.isPresent()) { 
            Component p = parent.get();
            if (p instanceof AppLayout) {
            	AppLayout main = (AppLayout) p;
                menu = main;
            }           
            parent = p.getParent();
        }
        return menu;
    }
}
