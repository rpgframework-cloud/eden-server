package de.rpgframework.eden.gui;

import java.io.InputStream;

import org.apache.logging.log4j.LogManager;

import com.github.appreciated.card.RippleClickableCard;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.StreamResource;

import de.rpgframework.eden.MainLayout;
import de.rpgframework.reality.CatalogItem;

/**
 * @author prelle
 *
 */
public class ProductCard extends RippleClickableCard {
	
	private CatalogItem data;
	
	private Image image;
	private Label title;
	private Label description;
	private Label price;
	
	private Div imageDiv;

	//-------------------------------------------------------------------
	public ProductCard(CatalogItem prod) {
		this.data = prod;
		setClassName("card");
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		title = new Label(data.getName());
		description = new Label(data.getDescription());
		price = new Label("€ "+String.format("%.2f", data.getPrice()/100.0f));
		price.setClassName("text-tertiary");
		imageDiv = new Div();
		imageDiv.setHeight(null);;
		
		// Try to load image
		String fname = "products/"+data.getId()+".jpg";
		InputStream ins = MainLayout.class.getClassLoader().getResourceAsStream(fname);
		LogManager.getLogger("product").info("ins for "+fname+" = "+ins);
		if (ins!=null) {
			StreamResource res = new StreamResource(data.getId()+".jpg", () -> ins);
			image = new Image( res,"Product image");
			image.setWidth("5em");
			image.setHeight("7.3em");
		}
		
		if (image!=null) {
			imageDiv.add(image);
		}
		
	}
	//-------------------------------------------------------------------
	private void initLayout() {
		imageDiv.setWidth("5em");
		
		VerticalLayout content = new VerticalLayout();
		content.getThemeList().remove("padding");
		content.add(title);
		title.setSizeFull();
		title.getStyle().set("padding-left", "0.5em");
		Hr hr = new Hr();
		hr.setMinHeight("1px");
		content.add(hr);
		content.add(price);
		price.getStyle().set("margin-top", "0px");
		price.getStyle().set("text-align", "right");
		price.getStyle().set("padding-bottom", "5px");
		price.setWidth("97%");
		HorizontalLayout layout = new HorizontalLayout(imageDiv, content);
		layout.getThemeList().remove("spacing");
		layout.setWidthFull();
		getContent().add(layout);
//		setSize(Size.SIZE320);
		setSizeFull();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the data
	 */
	public CatalogItem getProduct() {
		return data;
	}
	
}
