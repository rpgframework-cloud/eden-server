package de.rpgframework.eden.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.componentfactory.Breadcrumbs;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.FlexLayout.ContentAlignment;
import com.vaadin.flow.component.orderedlayout.FlexLayout.FlexWrap;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.server.VaadinService;

import de.rpgframework.eden.PageHierarchy;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.PageReference;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public abstract class DataItemLibraryLayout<D extends DataItem> extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(DataItemLibraryLayout.class);
	
	protected Breadcrumbs breadcrumbs;
	protected TextField tfSearch;
	protected FlexLayout filterLayout;
	protected Grid<D> grid;
	protected HorizontalLayout bxSideBySide;
	
	protected ListDataProvider<D> dataProvider;
	protected ComponentRenderer<Component, D> renderer;

	//-------------------------------------------------------------------
	public DataItemLibraryLayout() {
		initComponents();
		initLayout();
		initInteractivity();
		
		refresh();
	}

	//-------------------------------------------------------------------
	protected abstract PageHierarchy getPageHierarchy();

	//-------------------------------------------------------------------
	protected abstract List<D> getUnfilteredData(Locale loc);

	//-------------------------------------------------------------------
	private void initComponents() {
		Locale loc = UI.getCurrent().getLocale();
		breadcrumbs = getPageHierarchy().createBreadcrumbs(VaadinService.getCurrent().getInstantiator().getI18NProvider(), loc);
		
		tfSearch = new TextField();
		tfSearch.setPlaceholder(getTranslation("page.library.search.placeholder"));
		tfSearch.setClearButtonVisible(true);
		Icon icon = VaadinIcon.SEARCH.create();
		tfSearch.setPrefixComponent(icon);
		
		grid = new Grid<D>();
		grid.setItems(getUnfilteredData(loc));
		dataProvider = (ListDataProvider<D>) grid.getDataProvider();
		grid.addColumn(new ValueProvider<D, String>() {
			public String apply(D source) {
				return source.getName(loc);
			}
		}).setHeader(getTranslation("column.name")).setWidth("80%");
//		grid.addColumn(D::getName/l).setHeader(getTranslation("column.name")).setWidth("80%");
		initGridColumns(loc);
	}

	//-------------------------------------------------------------------
	protected abstract void initGridColumns(Locale loc);

	//-------------------------------------------------------------------
	protected abstract void initFilter(Locale loc);

	//-------------------------------------------------------------------
	private void initLayout() {
		filterLayout = new FlexLayout(tfSearch);
		filterLayout.setId("filter-layout");
		filterLayout.setFlexWrap(FlexWrap.WRAP);
		filterLayout.getStyle().set("gap", "0.5em");
		filterLayout.setAlignContent(ContentAlignment.SPACE_BETWEEN);
		
		Locale loc = UI.getCurrent().getLocale();
		initFilter(loc);

 		grid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);
		grid.setMinWidth("20em");
 		UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
 			int width = details.getWindowInnerWidth();
 			if (width>900) {
 				grid.setWidth("30em");
 			} else if (width>700) {
 				grid.setWidth("20em");
 			} else {
 				grid.setWidth("90%");
 			}
             });
 		grid.setWidth("90%");
 		grid.setHeightByRows(false);
 		grid.setHeightFull();
 		
 		bxSideBySide = new HorizontalLayout(grid);
 		bxSideBySide.setHeightFull();

 		add(breadcrumbs);
		add(filterLayout);
		add(bxSideBySide);
		setSizeFull();
	}

	//-------------------------------------------------------------------
	protected void initInteractivity() {
		tfSearch.addValueChangeListener(ev -> refresh());
		grid.addItemClickListener(
		        event -> {
		     		UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
		     			int width = details.getWindowInnerWidth();
		     			if (width>700) {
				        	logger.debug("Display at side "+event.getItem().getId());
				        	Optional<Component> removeMe = bxSideBySide.getChildren().filter(ch -> ch!=grid).findAny();
				        	if (removeMe.isPresent())
				        		bxSideBySide.remove(removeMe.get());
				        	bxSideBySide.add(grid);
				        	bxSideBySide.add(renderSideComponent(event.getItem()));
		     			} else {
				        	logger.debug("Navigate to "+event.getItem().getId());
				        	navigateTo(event.getItem().getId());
		     			}
		                 });
		        });
		
	}

	//-------------------------------------------------------------------
	protected abstract void navigateTo(String id);

	//-------------------------------------------------------------------
	protected Component renderSideComponent(D data) {
		Locale loc = UI.getCurrent().getLocale();
		if (renderer!=null)
			return renderer.createComponent(data);
		
		H2 name = new H2(data.getName());
		List<String> refs = new ArrayList<>();
		for (PageReference ref : data.getPageReferences()) {
			if (ref.getLanguage().equals(loc.getLanguage())) {
				refs.add(ref.getProduct().getName(loc)+" "+ref.getPage());
			}
		}
		H4 references = new H4(String.join(", ", refs));
		
		Paragraph p = new Paragraph(data.getDescription(loc));
		p.setClassName("description");
		
		Div ret = new Div(name, references, p);
		return ret;
	}

	//-------------------------------------------------------------------
	protected abstract void refresh();
	
}
