package de.rpgframework.eden.gui;

import java.util.Arrays;

import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;

import de.rpgframework.eden.DialogTool;
import de.rpgframework.eden.EdenApplication;
import de.rpgframework.eden.account.ContentsView;
import de.rpgframework.eden.account.CreateAccountView;
import de.rpgframework.reality.Player;

/**
 * @author prelle
 *
 */
public class AppHeader extends HorizontalLayout {
	
	private final static Logger logger = EdenApplication.logger;

	private H3 viewTitle;
	private Tabs navigation;
	private MenuBar accountMenu;
	private MenuItem menuProfile;
	private MenuItem menuCreate;
	private MenuItem menuLogin;
	private MenuItem menuAccount;
	private MenuItem menuContent;
	private MenuItem menuLogout;

	//-------------------------------------------------------------------
	public AppHeader() {
		initComponents();
		initLayout();
		initInteractivty();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		viewTitle = new H3("AppHeader");
		navigation  = new Tabs();
		navigation.setAutoselect(true);
		accountMenu = new MenuBar();
		menuProfile = accountMenu.addItem(new Icon(VaadinIcon.USER));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setId("header");
		getThemeList().set("dark", true);
		setWidthFull();
		setSpacing(false);
		setAlignItems(FlexComponent.Alignment.CENTER);
		add(new DrawerToggle());
		viewTitle.addClassName("navigation-header");
		VerticalLayout vertical = new VerticalLayout(viewTitle, navigation);
		vertical.setSpacing(false);
		add(vertical);

		/* Profile menu */
		Player account = UI.getCurrent().getSession().getAttribute(Player.class);
		if (account==null) {
			menuCreate  = menuProfile.getSubMenu().addItem(getTranslation("menuitem.create"));
			menuLogin   = menuProfile.getSubMenu().addItem(getTranslation("menuitem.login"));
		} else {
			menuAccount = menuProfile.getSubMenu().addItem(getTranslation("menuitem.profile"));
			menuContent = menuProfile.getSubMenu().addItem(getTranslation("menuitem.content"));
			menuProfile.getSubMenu().addItem(new Hr());
			menuLogout = menuProfile.getSubMenu().addItem(getTranslation("menuitem.logout"));
		}
		add(accountMenu);
	}

	//-------------------------------------------------------------------
	private void initInteractivty() {
		if (menuLogin!=null)
			menuLogin.addClickListener(ev -> {
				logger.info("menuLogin clicked");
				DialogTool.showLoginDialog();
			});
		if (menuCreate!=null)
			menuCreate.addClickListener(ev -> {
				logger.info("menuCreate clicked");
				Dialog dialog = new Dialog();
				CreateAccountView view = new CreateAccountView(dialog);
				dialog.add(view);
				dialog.open();
			});
		if (menuContent!=null) {
			menuContent.addClickListener(ev -> {
				logger.info("menuContent clicked");
				UI.getCurrent().navigate(ContentsView.class);
				UI.getCurrent().getPage().reload();
			});
		}
	}

	//-------------------------------------------------------------------
	public void setTitle(String value) {
		logger.info("setTitle("+value+")");
		viewTitle.setText(value);
	}

	//-------------------------------------------------------------------
	public void setNavigation(Tab... tabs) {
		navigation.add(tabs);
	}

	//-------------------------------------------------------------------
	public Tabs getNavigationTabs() {
		return navigation;
	}
}
