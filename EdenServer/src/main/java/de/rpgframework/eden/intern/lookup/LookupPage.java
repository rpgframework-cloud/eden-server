package de.rpgframework.eden.intern.lookup;

import java.io.InputStream;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

/**
 * @author prelle
 *
 */
@Route(value="intern/lookup", layout = LookupLayout.class)
@PageTitle("page.lookup")
public class LookupPage extends VerticalLayout {

	//-------------------------------------------------------------------
	/**
	 */
	public LookupPage() {
		InputStream ins = ClassLoader.getSystemResourceAsStream("static/lookup.html");
		if (ins!=null) {
		Html html = new Html(ins);
		add(html);
		} else {
			Html html = new Html("<p>Nichts gefunden</p>");
			add(html);
		}
	}

}
