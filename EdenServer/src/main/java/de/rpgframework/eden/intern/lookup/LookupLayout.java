package de.rpgframework.eden.intern.lookup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.tabs.Tab;

import de.rpgframework.eden.EdenAppLayout;
import de.rpgframework.eden.shadowrun6.SR6LibraryPage;
import de.rpgframework.eden.splittermond.SMLibraryPage;

/**
 * @author prelle
 *
 */
public class LookupLayout extends EdenAppLayout {

	private static final long serialVersionUID = -4866755313157339405L;

	protected static Logger logger = LogManager.getLogger(LookupLayout.class);
	
	private Tab tabSplittermond;
	private Tab tabShadowrun6;
	
	//-------------------------------------------------------------------
	public LookupLayout() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.EdenAppLayout#createHeaderContent()
	 */
	@Override
	protected Tab[] createHeaderContent() {
		tabSplittermond = new Tab(getTranslation("roleplayingsystem.splittermond"));
		tabShadowrun6   = new Tab(getTranslation("roleplayingsystem.shadowrun6"));
		return new Tab[] {tabSplittermond, tabShadowrun6};
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.EdenAppLayout#getComponentForTab(com.vaadin.flow.component.tabs.Tab)
	 */
	@Override
	protected Component getComponentForTab(Tab tab) {
		if (tab==tabSplittermond) {
			return new SMLibraryPage();
		}
		if (tab==tabShadowrun6) {
			return new SR6LibraryPage();
		}
		return new Label("LookupLayout.getComponentForTab("+tab+")");
	}

}
