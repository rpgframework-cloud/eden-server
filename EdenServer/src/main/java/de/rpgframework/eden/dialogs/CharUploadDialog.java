package de.rpgframework.eden.dialogs;

import java.io.IOException;
import java.io.InputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;

import de.rpgframework.character.Attachment;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;
import de.rpgframework.reality.server.ServerCharacterHandle;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class CharUploadDialog extends Dialog {

	private static Logger logger = LogManager.getLogger(CharUploadDialog.class);
	
	private Button btnOK;
	private Button btnCancel ;
	private Select<RoleplayingSystem> cbRules;
	private TextField tfName;
	private Upload upload;
	private MemoryBuffer buffer;

	
//	private String upMimeType;
	private String upFilename;
	private InputStream upINS;

	//-------------------------------------------------------------------
	/**
	 */
	public CharUploadDialog() {
		setCloseOnOutsideClick(false);
		initComponents();
		initLayout();
		initInteractivity();
		checkButton();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		cbRules = new Select<RoleplayingSystem>(RoleplayingSystem.SHADOWRUN6, RoleplayingSystem.SPLITTERMOND);
		cbRules.setItemLabelGenerator(RoleplayingSystem::getName);
		cbRules.setLabel(getTranslation("label.roleplayingsystem"));
		
		tfName = new TextField(getTranslation("label.charname"));
		
		buffer = new MemoryBuffer();
		upload = new Upload(buffer);
		upload.setAcceptedFileTypes("application/xml", "text/xml");

		btnOK = new Button(getTranslation("button.ok"));
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		H1 titleHolder = new H1(getTranslation("page.chars.dialog.upload"));
//      titleHolder.addClassName("mt-0");
		titleHolder.setWidthFull();
		
		btnCancel  = new Button(getTranslation("button.cancel"));
		HorizontalLayout buttons = new HorizontalLayout(btnOK, btnCancel);
		
		VerticalLayout container = new VerticalLayout(titleHolder, cbRules, tfName, upload, buttons);
		
		this.add(container);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		upload.addSucceededListener(event -> {
//			logger.info("Uploaded "+event.getMIMEType()+" "+event.getFileName()+" with INS "+buffer.getInputStream());
//			upMimeType = event.getMIMEType();
			upINS      = buffer.getInputStream();
			upFilename = event.getFileName();
			checkButton();
		});
		
		tfName.addValueChangeListener(ev -> checkButton());
		
		btnCancel.addClickListener(ev -> this.setOpened(false));
		btnOK.addClickListener(ev -> {
			PlayerImpl player = (PlayerImpl) UI.getCurrent().getSession().getAttribute(Player.class);
			logger.info("User "+player.getLogin()+" wants to import '"+upFilename+"' as a "+cbRules.getValue()+" character");
			 this.setOpened(false);
			try {
				ServerCharacterHandle handle = BackendAccess.getInstance().getCharacterDatabase().addCharacter(player, cbRules.getValue(), tfName.getValue());
				byte[] data = upINS.readAllBytes();
				BackendAccess.getInstance().getAttachmentDatabase().addAttachment(handle, Attachment.Type.CHARACTER, Attachment.Format.RULESPECIFIC, upFilename, data);
			} catch (Exception e) {
				logger.warn("Failed uploading character to database",e);
				Notification.show(getTranslation("page.chars.error.upload", e.toString()), 3000, Position.MIDDLE);
			} finally {
				try {
					upINS.close();
				} catch (IOException e) {
				}
			}
		});
	}
	
	//-------------------------------------------------------------------
	private void checkButton() {
		btnOK.setEnabled(upINS!=null && !tfName.getValue().isBlank());
	}

}
