package de.rpgframework.eden;

import java.util.Arrays;
import java.util.MissingFormatArgumentException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author prelle
 *
 */
public class Log4JJettyLog implements org.eclipse.jetty.util.log.Logger {
	
	private Logger logger;

	//------------------------------------------------------
	/**
	 */
	public Log4JJettyLog() {
		logger = LogManager.getLogger("jetty");
	}

	//------------------------------------------------------
	/**
	 */
	public Log4JJettyLog(String name) {
		logger = LogManager.getLogger(name);
	}

	//------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.mortbay.log.Logger#debug(java.lang.String, java.lang.Throwable)
	 */
	public void debug(String arg0, Throwable arg1) {
//		logger.debug(arg0, arg1);
	}

	//------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.mortbay.log.Logger#debug(java.lang.String, java.lang.Object, java.lang.Object)
	 */
	public void debug(String arg0, Object arg1, Object arg2) {
//		logger.debug(arg0+" : "+arg1+" : "+arg2);
	}

	//------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.mortbay.log.Logger#getLogger(java.lang.String)
	 */
	public org.eclipse.jetty.util.log.Logger getLogger(String name) {
		return new Log4JJettyLog(name);
	}

	//------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.mortbay.log.Logger#info(java.lang.String, java.lang.Object, java.lang.Object)
	 */
	public void info(String arg0, Object arg1, Object arg2) {
		logger.info(arg0+" : "+arg1+" : "+arg2);
	}

	//------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.mortbay.log.Logger#isDebugEnabled()
	 */
	public boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}

	//------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.mortbay.log.Logger#setDebugEnabled(boolean)
	 */
	public void setDebugEnabled(boolean arg0) {
	}

	//------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.mortbay.log.Logger#warn(java.lang.String, java.lang.Throwable)
	 */
	public void warn(String mess, Throwable arg1) {
		logger.warn(mess, arg1);
	}

	//------------------------------------------------------
	/* (non-Javadoc)
	 * @see org.mortbay.log.Logger#warn(java.lang.String, java.lang.Object, java.lang.Object)
	 */
	public void warn(String mess, Object arg1, Object arg2) {
		logger.warn(mess);
	}

	@Override
	public void debug(Throwable arg0) {
		// TODO Auto-generated method stub
		
		logger.debug(arg0);
	}

	@Override
	public void debug(String msg, Object... param) {
//		try {
//			logger.debug(String.format(msg.replace("{}", "%s "), param));
//		} catch (MissingFormatArgumentException e) {
//			logger.debug(msg+"    ||| "+Arrays.toString(param));
//		}
	}

	@Override
	public void debug(String arg0, long arg1) {
//		logger.debug(arg0);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "foo";
	}

	@Override
	public void ignore(Throwable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void info(Throwable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void info(String msg, Object... param) {
		try {
			logger.info(String.format(msg.replace("{}", "%s "), param));
		} catch (MissingFormatArgumentException e) {
			logger.debug(msg+"    ||| "+Arrays.toString(param));
		}
	}

	@Override
	public void info(String arg0, Throwable arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void warn(Throwable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void warn(String msg, Object... param) {
		try {
			logger.warn(String.format(msg.replace("{}", "%s "), param));
		} catch (MissingFormatArgumentException e) {
			logger.debug(msg+"    ||| "+Arrays.toString(param));
		}
	}

}
