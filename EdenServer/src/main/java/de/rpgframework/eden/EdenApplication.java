package de.rpgframework.eden;

import static org.eclipse.jetty.security.authentication.FormAuthenticator.__J_METHOD;
import static org.eclipse.jetty.security.authentication.FormAuthenticator.__J_POST;
import static org.eclipse.jetty.security.authentication.FormAuthenticator.__J_URI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Properties;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.plus.webapp.EnvConfiguration;
import org.eclipse.jetty.plus.webapp.PlusConfiguration;
import org.eclipse.jetty.rewrite.handler.RewriteHandler;
import org.eclipse.jetty.security.Authenticator;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.security.ServerAuthException;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.security.authentication.DeferredAuthentication;
import org.eclipse.jetty.security.authentication.SessionAuthentication;
import org.eclipse.jetty.server.Authentication;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.MultiMap;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.FragmentConfiguration;
import org.eclipse.jetty.webapp.JettyWebXmlConfiguration;
import org.eclipse.jetty.webapp.MetaInfConfiguration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.WebInfConfiguration;
import org.eclipse.jetty.webapp.WebXmlConfiguration;
import org.prelle.rpgframework.splittermond.data.SplittermondDataPlugin;

import com.vaadin.flow.server.startup.ServletContextListeners;

import de.rpgframework.eden.api.InfoPingServlet;
import de.rpgframework.eden.api.Roles;
import de.rpgframework.eden.backend.EdenBackend;
import de.rpgframework.eden.base.JavaMailMailer;
import de.rpgframework.eden.base.MailerLoader;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;

/**
 * @author prelle
 *
 */
public class EdenApplication {

	public final static String PROP_CONFIG_FILE = "eden.cfgfile";

	private final static int DEFAULT_FRONTEND_PORT = 8000;
	private final static int DEFAULT_BACKEND_PORT = 5000;

	public final static String CFG_JDBC_URL   = "jdbc.url";
	public final static String CFG_JDBC_USER  = "jdbc.user";
	public final static String CFG_JDBC_PASS  = "jdbc.pass";
	public final static String CFG_SERVER_PORT= "server.port";
	public final static String CFG_SERVER_HOST= "server.hostname";

	public final static Logger logger = LogManager.getLogger("eden");

	public static String REMOTE_BACKEND_HOSTPORT = null;
	public static String REMOTE_FRONTEND_HOSTPORT = null;

	private int port;
	private Server server;
	private BackendAccess backend;
	private SQLBasedLoginService loginService;

	//-------------------------------------------------------------------
	public static void main(String[] args) throws Exception {
		Locale.setDefault(Locale.GERMAN);
		System.setProperty("vaadin.i18n.provider", TranslationProvider.class.getName());
		System.setProperty("org.eclipse.jetty.util.log.class", Log4JJettyLog.class.getName());

		String confPath = System.getProperty(PROP_CONFIG_FILE, System.getProperty("user.home")+File.separatorChar+"eden.cfg");
		logger.info("Load configuration from "+confPath);
		Properties config = new Properties();
		try {
			config.load(new FileReader(confPath));
		} catch (FileNotFoundException e) {
			logger.fatal("Missing configuration file: "+confPath+" or system property '"+PROP_CONFIG_FILE+"'");
			System.err.println("Missing configuration file: "+confPath+" or system property '"+PROP_CONFIG_FILE+"'");
			System.exit(1);
		}

		// Setup  Java Mail
		MailerLoader.setInstance(new JavaMailMailer(config));
		logger.info("Set up Splittermond");
		(new SplittermondDataPlugin()).init();
		logger.info("Set up Shadowrun 6");
		(new Shadowrun6DataPlugin()).init();

		EdenApplication app = new EdenApplication(config);
		app.run();
	}

	//-------------------------------------------------------------------
	public EdenApplication(Properties config) throws SQLException {
		port = Integer.parseInt(config.getProperty(CFG_SERVER_PORT, String.valueOf(DEFAULT_FRONTEND_PORT)));
		REMOTE_FRONTEND_HOSTPORT = null;
		try {
			REMOTE_FRONTEND_HOSTPORT = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			REMOTE_FRONTEND_HOSTPORT = "localhost";
		}
		REMOTE_FRONTEND_HOSTPORT = config.getProperty(CFG_SERVER_HOST, REMOTE_FRONTEND_HOSTPORT);
		REMOTE_FRONTEND_HOSTPORT+=":"+port;

		// Backend
		REMOTE_BACKEND_HOSTPORT = null;
		try {
			REMOTE_BACKEND_HOSTPORT = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			REMOTE_BACKEND_HOSTPORT = "localhost";
		}
		REMOTE_BACKEND_HOSTPORT = config.getProperty(CFG_SERVER_HOST, REMOTE_BACKEND_HOSTPORT);
		REMOTE_BACKEND_HOSTPORT+=":"+DEFAULT_BACKEND_PORT;


		backend = new BackendAccess(config);
		BackendAccess.setHostPortFrontend(REMOTE_FRONTEND_HOSTPORT);
		BackendAccess.setHostPortBackend(REMOTE_BACKEND_HOSTPORT);
		loginService = new SQLBasedLoginService(config);
	}

	//-------------------------------------------------------------------
	private void run() throws Exception {
		System.err.println("EdenApplication.run: Default locale is "+Locale.getDefault());
		server = new Server();

		ServerConnector portWebApp=new ServerConnector(server);
		portWebApp.setName("Frontend");
		portWebApp.setPort(port);

		ServerConnector portBackend=new ServerConnector(server);
		portBackend.setName("Backend");
		portBackend.setPort(DEFAULT_BACKEND_PORT);

		server.setConnectors(new Connector[]{portWebApp, portBackend});
//		server.setConnectors(new Connector[]{portWebApp});

		RewriteHandler rewrite = new RewriteHandler();
//		RedirectPatternRule redirect = new RedirectPatternRule();
//		redirect.setPattern("");
//		redirect.setLocation("/extern");
//		rewrite.addRule(redirect);

		HandlerCollection contexts = new HandlerCollection();
		contexts.addHandler(rewrite);
		contexts.addHandler(getAuthorizedAPIContext());
		contexts.addHandler(getAnonymousAPIContext());
		contexts.addHandler(createWebApp("/"+PageHierarchy.PREFIX_PAGES));
		server.setHandler(contexts);


		server.start();
		server.join();
		System.err.println("EdenApplication.run: Default locale is "+Locale.getDefault());

	}

	//-----------------------------------------------------------------
	private ServletContextHandler getAnonymousAPIContext() {
		ServletContextHandler servContext = new ServletContextHandler(ServletContextHandler.SESSIONS | ServletContextHandler.SECURITY);
		servContext.setInitParameter("cacheControl", "max-age=120, public");
		servContext.setContextPath("/"+PageHierarchy.PREFIX_API_EXTERN);
		servContext.getServletContext().setAttribute(Constants.ATTRIB_BACKEND, backend);
		servContext.addServlet(InfoPingServlet.class, "/ping");
		servContext.setVirtualHosts(new String[] {"@Backend"});
		return servContext;
	}

	//-----------------------------------------------------------------
	private ServletContextHandler getAuthorizedAPIContext() {
		ServletContextHandler servContext = new ServletContextHandler(ServletContextHandler.SESSIONS | ServletContextHandler.SECURITY);
		servContext.setInitParameter("cacheControl", "max-age=120, public");
		servContext.setContextPath("/"+PageHierarchy.PREFIX_API_INTERN);
		servContext.getServletContext().setAttribute(Constants.ATTRIB_BACKEND, backend);

		for (EdenBackend.ServletWithMapping mapping : EdenBackend.getServlets()) {
			servContext.addServlet(mapping.servlet, mapping.path);
		}

		servContext.setVirtualHosts(new String[] {"@Backend"});
		servContext.setSecurityHandler(
				createSecurityHandler(
						"ConstraintName",
						new String[] {Roles.VALID_USER.name()},
						getAuthenticator(),
						"Realmchen",
						loginService));
		return servContext;
	}
	//-----------------------------------------------------------------
	private WebAppContext createWebApp(String contextPath) throws URISyntaxException, MalformedURLException {
		URL webRootLocation = this.getClass().getResource("/META-INF/resources/");
		URI webRootUri = webRootLocation.toURI();

		WebAppContext context = new WebAppContext();
		context.setBaseResource(Resource.newResource(webRootUri));
		context.setContextPath(contextPath);
		context.setAttribute("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern", ".*");
		context.setAttribute("eden.backend", backend);
		context.setConfigurationDiscovered(true);
		context.setConfigurations(new Configuration[]{
				new AnnotationConfiguration(),
				new WebInfConfiguration(),
				new WebXmlConfiguration(),
				new MetaInfConfiguration(),
				new FragmentConfiguration(),
				new EnvConfiguration(),
				new PlusConfiguration(),
				new JettyWebXmlConfiguration()
		});
		context.getServletContext().setExtendedListenerTypes(true);
		context.getServletContext().setAttribute(BackendAccess.class.getSimpleName(), backend);
		context.addEventListener(new ServletContextListeners());
		context.setVirtualHosts(new String[] {"@Frontend"});
		logger.debug("Attributes = "+context.getAttributes());

		String[] allowedRoles = new String[] {"Hallo","ADMIN","SUPPORT"};
		//          LoginService fileLoginService = new FileBasedLoginService(config);
		//          RadiusLoginService loginService = new RadiusLoginService(config);
//		SQLBasedLoginService loginService = new SQLBasedLoginService(config);
		//          Authenticator authenticator = getAuthenticator();
		//          context.setSecurityHandler(createSecurityHandler("FOO", allowedRoles, authenticator, "Realmchen", loginService));
		return context;
	}

	//-------------------------------------------------------------------
	private Authenticator getAuthenticator() {
		return new BasicAuthenticator() {
			public Authentication validateRequest(ServletRequest req, ServletResponse res, boolean mandatory) throws ServerAuthException {
				HttpServletRequest request = (HttpServletRequest)req;
				if (!mandatory)
					return new DeferredAuthentication(this);

				HttpSession session = request.getSession(true);

				// Look for cached authentication
				Authentication authentication = (Authentication) session.getAttribute(SessionAuthentication.__J_AUTHENTICATED);
				if (authentication != null) {
					// Has authentication been revoked?
					if (authentication instanceof Authentication.User &&
							_loginService!=null &&
							!_loginService.validate(((Authentication.User)authentication).getUserIdentity()))
					{
						logger.debug("auth revoked {}",authentication);
						session.removeAttribute(SessionAuthentication.__J_AUTHENTICATED);
					}
					else
					{
						synchronized (session)
						{
							String j_uri=(String)session.getAttribute(__J_URI);
							if (j_uri!=null)
							{
								//check if the request is for the same url as the original and restore
								//params if it was a post
								logger.debug("auth retry {}->{}",authentication,j_uri);
								StringBuffer buf = request.getRequestURL();
								if (request.getQueryString() != null)
									buf.append("?").append(request.getQueryString());

								if (j_uri.equals(buf.toString()))
								{
									MultiMap<String> j_post = (MultiMap<String>)session.getAttribute(__J_POST);
									if (j_post!=null)
									{
										logger.debug("auth rePOST {}->{}",authentication,j_uri);
										Request base_request = Request.getBaseRequest(request);
										base_request.setContentParameters(j_post);
									}
									session.removeAttribute(__J_URI);
									session.removeAttribute(__J_METHOD);
									session.removeAttribute(__J_POST);
								}
							}
						}
						logger.trace("auth {}",authentication);
						return authentication;
					}
				}
				return super.validateRequest(req, res, mandatory);
			}
		};
	}

	//-------------------------------------------------------------------
	protected ConstraintSecurityHandler createSecurityHandler(String constraintName,
			String[] allowedRoles, Authenticator authenticator, String realm,
			LoginService loginService) {

		Constraint constraint = new Constraint();
		constraint.setName(constraintName);
		constraint.setRoles(allowedRoles);
		// This is telling Jetty to not allow unauthenticated requests through (very important!)
		constraint.setAuthenticate(true);

		ConstraintMapping cm = new ConstraintMapping();
		cm.setConstraint(constraint);
		cm.setPathSpec("/*");

		ConstraintSecurityHandler sh = new ConstraintSecurityHandler();
		sh.setAuthenticator(authenticator);
		sh.setLoginService(loginService);
		sh.setConstraintMappings(new ConstraintMapping[]{cm});
		sh.setRealmName(realm);

		return sh;
	}

	//-------------------------------------------------------------------
	/**
	 */
	public void stop() {
		try {
			server.stop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
