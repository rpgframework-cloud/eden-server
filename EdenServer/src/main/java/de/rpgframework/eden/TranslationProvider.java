package de.rpgframework.eden;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.i18n.I18NProvider;

/**
 * @author Stefan Prelle
 *
 */
@SuppressWarnings("serial")
public class TranslationProvider implements I18NProvider {
	
	public static final String BUNDLE_PREFIX = "translation";
	
	private final static Logger logger = LogManager.getLogger(TranslationProvider.class);
	
	private static Map<String, ResourceBundle> RESOURCES = new HashMap<String, ResourceBundle>();
	
	//-------------------------------------------------------------------
	static {
		for (Locale loc : Arrays.asList(Locale.ENGLISH, Locale.GERMAN)) {
			ResourceBundle res = ResourceBundle.getBundle(BUNDLE_PREFIX, loc);
			
		}
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see com.vaadin.flow.i18n.I18NProvider#getProvidedLocales()
	 */
	@Override
	public List<Locale> getProvidedLocales() {
		return Arrays.asList(Locale.ENGLISH, Locale.GERMAN);
	}

	//-------------------------------------------------------------------
	/**
	 * @see com.vaadin.flow.i18n.I18NProvider#getTranslation(java.lang.String, java.util.Locale, java.lang.Object[])
	 */
	@Override
	public String getTranslation(String key, Locale locale, Object... params) {		
		ResourceBundle res = RESOURCES.get(locale.getLanguage());
		if (res==null) {
			try {
				res = ResourceBundle.getBundle(BUNDLE_PREFIX, locale);
				logger.info("Loaded "+res.getBaseBundleName()+"_"+res.getLocale()+".properties for "+locale);
				RESOURCES.put(locale.getLanguage(), res);
				logger.info("Loaded translation for "+locale+"   now "+RESOURCES.keySet());
			} catch (MissingResourceException e) {
				logger.warn("Did not find properties for "+e.getClassName());
				return "!" + locale.getLanguage() + ": " + key;
			}
		}
		
		try {
//			logger.info("...getTranslation("+key+", locale="+locale+") ("+RESOURCES.keySet()+")= "+String.format(res.getString(key), params)+" from "+res.getBaseBundleName()+"_"+res.getLocale());
			return String.format(res.getString(key), params);
		} catch (MissingResourceException e) {
			logger.error("Missing key '"+key+"' in "+res.getBaseBundleName()+" for "+locale.getLanguage());
		}
		return key;
	}

}
