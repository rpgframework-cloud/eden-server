package de.rpgframework.eden.extern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

import de.rpgframework.eden.DialogTool;
import de.rpgframework.eden.PageHierarchy;
import de.rpgframework.eden.account.CreateAccountView;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Route(value=PageHierarchy.PATH_EXTERN, layout = PublicLayout.class)
@StyleSheet(value = "styles/info-page.css")
public class InfoPage extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(InfoPage.class);
	
	private H1 lbGreeting1;
	private H3 lbGreeting2;
	private Paragraph lbExplain1;
	private Paragraph lbExplain2;
	
	private Button btnLogin;
	private Button btnCreate;
	
	//-------------------------------------------------------------------
	public InfoPage() {
		initComponents();
		initLayout();
		initInteractivity();
		logger.info("Called");
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		lbGreeting1 = new H1(getTranslation("page.info.greeting1"));
		lbGreeting1.addClassName("pageheader");
		lbGreeting2 = new H3(getTranslation("page.info.greeting2"));
		lbGreeting2.addClassName("header");
		lbExplain1  = new Paragraph(getTranslation("page.info.explain1"));
		lbExplain2  = new Paragraph(getTranslation("page.info.explain2"));
		btnLogin   = new Button(getTranslation("page.info.button.login"));
		btnCreate  = new Button(getTranslation("page.info.button.create"));
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		this.setWidthFull();
		this.setSpacing(true);
		this.setAlignItems(Alignment.CENTER);
		
		Image img1 = new Image("frontend/images/Lookup.jpg", "Screenshot");
		img1.getStyle().set("width", "50%");
		img1.getStyle().set("float", "left");
		img1.getStyle().set("margin-right", "1em");
		Div div1 = new Div(img1, lbExplain1);
		div1.setMaxWidth("800px");

		Image img2 = new Image("frontend/images/Lookup.jpg", "Screenshot");
		img2.getStyle().set("width", "50%");
		img2.getStyle().set("float", "right");
		img2.getStyle().set("margin-left", "1em");
		Div div2 = new Div(img2, lbExplain2);
		div2.setMaxWidth("800px");
		add(lbGreeting1);
		add(lbGreeting2);
		add(div1);
		add(div2);
		
		FormLayout flex = new FormLayout(btnCreate,btnLogin);
		Div div3 = new Div(flex);
		div3.setMaxWidth("900px");
		add(div3);
		
		this.getStyle().set("spacing", "2em");
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnCreate.addClickListener(ev -> {
			Dialog dialog = new Dialog();
			CreateAccountView view = new CreateAccountView(dialog);
			dialog.add(view);
			dialog.open();
		});
		
		btnLogin.addClickListener(ev -> DialogTool.showLoginDialog());
	}

}
