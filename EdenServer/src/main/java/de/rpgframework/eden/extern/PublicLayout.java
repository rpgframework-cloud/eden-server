package de.rpgframework.eden.extern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.server.PWA;

import de.rpgframework.eden.DialogTool;
import de.rpgframework.eden.account.CreateAccountView;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Push
@PWA(name = "RPGFramework Eden", shortName = "Eden", enableInstallPrompt = true)
@JsModule("frontend://styles/shared-styles.js")
@StyleSheet("frontend://styles/eden.css")
@PageTitle("Eden")
@Viewport("width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes, viewport-fit=cover")
public class PublicLayout extends AppLayout {

	private static Logger logger = LogManager.getLogger(PublicLayout.class);
	

	//-------------------------------------------------------------------
	/**
	 */
	public PublicLayout() {
		setPrimarySection(Section.NAVBAR);
		PublicHeader header = new PublicHeader();
		header.setTitle(getTranslation("app.name"));
		addToNavbar(header);
		// TODO Auto-generated constructor stub
		
//		setContent(new InfoPage());
		logger.debug("PublicLayout");
	}
	
	//-------------------------------------------------------------------
//	private void showStartPage() {
//		Player player = UI.getCurrent().getSession().getAttribute(Player.class);
//		logger.info("Default locale is "+Locale.getDefault());
//		logger.info("player = "+player+" and locale "+UI.getCurrent().getLocale());
//		if (player==null) {
////			UI.getCurrent().navigate(InfoPage.class);
//			UI.getCurrent().navigate("extern");
////			setContent(content);
//		} else {
//			UI.getCurrent().navigate(LookupPage.class);
////			setContent(select);
//		}
//	}

}


@SuppressWarnings("serial")
class PublicHeader extends HorizontalLayout {

	private static Logger logger = LogManager.getLogger(PublicLayout.class);

	private H3 viewTitle;
	private MenuBar accountMenu;
	private MenuItem menuCreate;
	private MenuItem menuLogin;

	//-------------------------------------------------------------------
	public PublicHeader() {
		initComponents();
		initLayout();
		initInteractivty();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		viewTitle = new H3("AppHeader");
		accountMenu = new MenuBar();
		menuCreate = accountMenu.addItem(getTranslation("menuitem.create"));
		menuLogin  = accountMenu.addItem(getTranslation("menuitem.login"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setId("header");
		getThemeList().set("dark", true);
		setWidthFull();
		setSpacing(false);
		setAlignItems(FlexComponent.Alignment.CENTER);
		viewTitle.addClassName("navigation-header");
		VerticalLayout vertical = new VerticalLayout(viewTitle, accountMenu);
		vertical.setSpacing(false);
		vertical.setWidth("100%");
		vertical.setHorizontalComponentAlignment(Alignment.CENTER, accountMenu);
		add(vertical);

	}

	//-------------------------------------------------------------------
	private void initInteractivty() {
		if (menuLogin!=null)
			menuLogin.addClickListener(ev -> {
				logger.info("menuLogin clicked");
				DialogTool.showLoginDialog();
			});
		if (menuCreate!=null)
			menuCreate.addClickListener(ev -> {
				logger.info("menuCreate clicked");
				Dialog dialog = new Dialog();
				CreateAccountView view = new CreateAccountView(dialog);
				dialog.add(view);
				dialog.open();
			});
	}

	//-------------------------------------------------------------------
	public void setTitle(String value) {
		logger.info("setTitle("+value+")");
		viewTitle.setText(value);
	}

}
