package de.rpgframework.eden;

import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.notification.Notification;

import de.rpgframework.eden.intern.lookup.LookupPage;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.reality.Player;

/**
 * @author prelle
 *
 */
public class DialogTool {

	private static Logger logger = LogManager.getLogger(DialogTool.class);

	//-------------------------------------------------------------------
	public static void showLoginDialog() {
		LoginForm component = new LoginForm();
//		component.setTitle(getTranslation("dialog.login.title"));
//		component.setDescription(getTranslation("dialog.login.desc"));
		component.setAction("");
		component.setForgotPasswordButtonVisible(true);
		Dialog dia = new Dialog(component);
		component.addLoginListener(e -> {
			logger.info("login attempt: "+e.getUsername());

			BackendAccess backend = BackendAccess.getInstance();

			Player account = null;
			try {
				if (e.getUsername().contains("@")) {
					// Login with email
					account = backend.getPlayerDatabase().getPlayerByEmail(e.getUsername());
				} else {
					account = backend.getPlayerDatabase().getPlayerByLogin(e.getUsername());
				}
			} catch (SQLException e1) {
				logger.error("SQL Error accessing player database: "+e1,e1);
				Notification.show(UI.getCurrent().getTranslation("error.general.database",e1));
				dia.close();
			}

			if (account==null) {
				logger.info("Log in attempt for non-existing account: "+e.getUsername());
				component.setError(true);
				return;
			}

			// Player known - compare password
			if (account.getPassword().equals(e.getPassword())) {
				// Login successful
				component.setError(false);
				UI.getCurrent().getSession().setAttribute(Player.class, account);
				logger.debug("Logged in: "+e.getUsername());
				dia.close();
				UI.getCurrent().navigate(LookupPage.class);
				UI.getCurrent().getPage().reload();
//				UI.getCurrent().getPage().setLocation("/");
			} else {
				// Login failed
				logger.info("Invalid password for user "+e.getUsername()+" / "+account.getPassword());
				component.setForgotPasswordButtonVisible(true);
				component.setError(true);
			}
		});
		component.addForgotPasswordListener(ev -> {
			logger.info("Forgot password for "+ev);
		});
		LoginI18n i18n = LoginI18n.createDefault();
		i18n.setAdditionalInformation(UI.getCurrent().getTranslation("dialog.login.additional"));
		component.setI18n(i18n);
//		component.setOpened(true);
		
		dia.setOpened(true);
	}

}
