package de.rpgframework.eden;

import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class UploadCharacterDialog extends Dialog {

	private TextField tfName;
	private Select<RoleplayingSystem> cbRPG;
	private Upload upload;
	
	//-------------------------------------------------------------------
	/**
	 */
	public UploadCharacterDialog() {
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField(getTranslation("dialog.character.upload"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		
	}
	
}
