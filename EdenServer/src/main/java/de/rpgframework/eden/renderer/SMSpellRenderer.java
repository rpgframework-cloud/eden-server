package de.rpgframework.eden.renderer;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellEnhancementType;
import org.prelle.splimo.SpellSchoolEntry;
import org.prelle.splimo.SplitterTools;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.DescriptionList;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.i18n.I18NProvider;

import de.rpgframework.genericrpg.data.PageReference;

/**
 * @author prelle
 *
 */
public class SMSpellRenderer extends ComponentRenderer<Component, Spell> {
	
	private static final long serialVersionUID = 3602192949107430615L;

	private Locale loc;
	private I18NProvider trans;
	
	//-------------------------------------------------------------------
	public SMSpellRenderer(Locale locale, I18NProvider trans) {
		this.loc = locale;
		this.trans = trans;
	}

	//-------------------------------------------------------------------
	/**
	 * @see com.vaadin.flow.data.renderer.ComponentRenderer#createComponent(java.lang.Object)
	 */
	@Override
	public Component createComponent(Spell item) {
		H2 name = new H2(item.getName(loc));
		List<String> refs = new ArrayList<>();
		for (PageReference ref : item.getPageReferences()) {
			if (ref.getLanguage().equals(loc.getLanguage())) {
				refs.add(ref.getProduct().getName(loc)+" "+ref.getPage());
			}
		}
		H4 references = new H4(String.join(", ", refs));

		List<String> schools = new ArrayList<>();
		for (SpellSchoolEntry entry : item.getSchools()) {
			schools.add(entry.getSchool().getName(loc)+" "+entry.getLevel());
		}
	    
	    DescriptionList dl = new DescriptionList();
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("splittermond.spell.schools", loc)), 
	    		new DescriptionList.Description(String.join(", ", schools))});
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("splittermond.spell.type", loc)), 
	    		new DescriptionList.Description(String.join(", ", item.getTypes().stream().map(type -> type.getName()).collect(Collectors.toList())))});
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("splittermond.spell.difficulty", loc)), 
	    		new DescriptionList.Description(item.getDifficultyString())});
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("splittermond.spell.cost", loc)), 
	    		new DescriptionList.Description(SplitterTools.getFocusString(item.getCost()))});
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("splittermond.spell.castdur", loc)), 
	    		new DescriptionList.Description(item.getCastDurationString())});
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("splittermond.spell.castrange", loc)), 
	    		new DescriptionList.Description(item.getCastRangeString())});
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("splittermond.spell.effect", loc)), 
	    		new DescriptionList.Description(item.getDescription(loc))});
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("splittermond.spell.duration", loc)), 
	    		new DescriptionList.Description(item.getSpellDurationString())});
	    if (item.getEffectRange()!=null) {
		    dl.add(new Component[] {
		    		new DescriptionList.Term(trans.getTranslation("splittermond.spell.effectrange", loc)), 
		    		new DescriptionList.Description(item.getEffectRangeString())});
	    }
	    List<String> modList = new ArrayList<>();
	    for (SpellEnhancementType type : item.getEnhancementtype()) {
	    	modList.add(type.getName());
	    }
	    UnorderedList successList = new UnorderedList();
	    successList.add(new ListItem(new Text(String.join(", ", modList))));
	    successList.add(new ListItem(new Text(item.getEnhancementString()+": "+item.getEnhancementDescription(loc))));
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("splittermond.spell.successlevel", loc)), 
	    		new DescriptionList.Description(new HtmlContainer("br"),successList)});
	    
	    Div div = new Div(name, references, dl);
	    div.setMaxWidth("50em");
	    return div;
	}
}
