package de.rpgframework.eden.renderer;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.prelle.splimo.Attribute;
import org.prelle.splimo.MastershipReference;
import org.prelle.splimo.SMSkill;
import org.prelle.splimo.SMSkill.SkillType;
import org.prelle.splimo.SMSkillValue;
import org.prelle.splimo.Spell;
import org.prelle.splimo.SpellValue;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.Creature;
import org.prelle.splimo.creature.CreatureWeapon;
import org.prelle.splimo.items.Feature;
import org.prelle.splimo.persist.WeaponDamageConverter;

import com.vaadin.componentfactory.Breadcrumb;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.data.renderer.TemplateRenderer;
import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.router.BeforeEvent;

import de.rpgframework.genericrpg.data.PageReference;

/**
 * @author prelle
 *
 */
public class CreatureRenderer extends ComponentRenderer<Component, Creature> {

	private static final long serialVersionUID = 3602192949107430615L;

	private Locale loc;
	private I18NProvider trans;

	//-------------------------------------------------------------------
	public CreatureRenderer(Locale locale, I18NProvider trans) {
		this.loc = locale;
		this.trans = trans;
	}

	//-------------------------------------------------------------------
	private static String getCreatureWeaponLines(Creature crea, Locale loc) {
		StringBuffer buf = new StringBuffer();

		for (CreatureWeapon weapon : crea.getCreatureWeapons()) {
			List<String> features = new ArrayList<String>();
			for (Feature feat : weapon.getFeatures()) {
				features.add(feat.getName(loc));
			}

			buf.append( String.format("<tr><td>%s</td><td>%d</td><td>%s</td><td>%d</td><td>%d-W6</td><td>%s</td></tr>",
	                weapon.getName(),
	                weapon.getValue(),
	                (new WeaponDamageConverter()).write(weapon.getDamage()),
	                weapon.getSpeed(),
	                weapon.getInitiative(),
	                String.join(", ", features)
	                ));
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see com.vaadin.flow.router.HasUrlParameter#setParameter(com.vaadin.flow.router.BeforeEvent, java.lang.Object)
	 */
	@Override
	public Component createComponent(Creature crea) {
		Locale loc = UI.getCurrent().getLocale();

		UI.getCurrent().getPage().setTitle(crea.getName(loc));

		String head1 = String.format("<thead><tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th></tr></thead>",
				Attribute.CHARISMA.getShortName(loc),
				Attribute.AGILITY.getShortName(loc),
				Attribute.INTUITION.getShortName(loc),
				Attribute.CONSTITUTION.getShortName(loc),
				Attribute.MYSTIC.getShortName(loc),
				Attribute.STRENGTH.getShortName(loc),
				Attribute.MIND.getShortName(loc),
				Attribute.WILLPOWER.getShortName(loc)
				);

		String value1 = String.format("<tr><td>%d</td><td>%d</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>",
		                crea.getAttribute(Attribute.CHARISMA).getModifiedValue(),
		                crea.getAttribute(Attribute.AGILITY).getModifiedValue(),
		                crea.getAttribute(Attribute.INTUITION).getModifiedValue(),
		                crea.getAttribute(Attribute.CONSTITUTION).getModifiedValue(),
		                crea.getAttribute(Attribute.MYSTIC).getModifiedValue(),
		                crea.getAttribute(Attribute.STRENGTH).getModifiedValue(),
		                crea.getAttribute(Attribute.MIND).getModifiedValue(),
		                crea.getAttribute(Attribute.WILLPOWER).getModifiedValue()
		                );

		String head2 = String.format("<thead><tr><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th><th>%s</th></tr></thead>",
				Attribute.SIZE.getShortName(loc),
				Attribute.SPEED.getShortName(loc),
				Attribute.LIFE.getShortName(loc),
				Attribute.FOCUS.getShortName(loc),
				Attribute.RESIST_DAMAGE.getShortName(loc),
				Attribute.DAMAGE_REDUCTION.getShortName(loc),
				Attribute.RESIST_BODY.getShortName(loc),
				Attribute.RESIST_MIND.getShortName(loc)
				);

		String value2 = String.format("<tbody><tr><td>%d</td><td>%d</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr><tbody>",
		                crea.getAttribute(Attribute.SIZE).getModifiedValue(),
		                crea.getAttribute(Attribute.SPEED).getModifiedValue(),
		                crea.getAttribute(Attribute.LIFE).getModifiedValue(),
		                crea.getAttribute(Attribute.FOCUS).getModifiedValue(),
		                crea.getAttribute(Attribute.RESIST_DAMAGE).getModifiedValue(),
		                crea.getAttribute(Attribute.DAMAGE_REDUCTION).getModifiedValue(),
		                crea.getAttribute(Attribute.RESIST_BODY).getModifiedValue(),
		                crea.getAttribute(Attribute.RESIST_MIND).getModifiedValue()
		                );

		String head3 = "<thead><tr><th>Waffe</th><th>Wert</th><th>Schaden</th><th>WGS</th><th>INI</th><th>Merkmale</th></tr></thead>";

		String type  = "<br/><b>Typus:</b> "+String.join(", ",crea.getCreatureTypes().stream().map(feat -> feat.getName(loc)).collect(Collectors.toList()));
		String level = "<br/><b>Mondstergrad:</b> "+crea.getLevelAlone()+" / "+crea.getLevelGroup();
		String skills= "<br/><b>Fertigkeiten:</b> "+String.join(", ",crea.getSkillValues().stream().map(feat -> feat.getName(loc)).collect(Collectors.toList()));

		List<String> masterPer = new ArrayList<String>();
		for (SMSkillValue sVal : crea.getSkillValues()) {
			if (!sVal.getMasterships().isEmpty()) {
				List<MastershipReference> ref = sVal.getMasterships();
				Collections.sort(ref, new Comparator<MastershipReference>() {
					public int compare(MastershipReference o1, MastershipReference o2) {
						int c = ((Integer)o1.getDistributed()).compareTo(o2.getDistributed());
						if (c!=0)
							return c;
						return o1.getName(loc).compareTo(o2.getName(loc));
					}});
				StringBuffer toAdd = new StringBuffer();
				if (sVal.getSkill()==null) {
					toAdd.append("ERROR("+sVal+")");
				} else {
					toAdd.append(sVal.getSkill().getName(loc)+" (");
				}
				int lastLevel = 0;
				for (MastershipReference tmp : ref) {
					if (tmp.getDistributed()!=lastLevel) {
						switch (tmp.getDistributed()) {
						case 1: toAdd.append("I: "); break;
						case 2: toAdd.append("II: "); break;
						case 3: toAdd.append("III: "); break;
						case 4: toAdd.append("IV: "); break;
						}
					} else {
						toAdd.append(", ");
					}
					toAdd.append(tmp.getName(loc));
				}

				toAdd.append(")");
				masterPer.add(toAdd.toString());
			}
		}
		String spellsS  = getSpellString(crea, loc);
		String master  = "<br/><b>Meisterschaften:</b> "+((masterPer.isEmpty())?"-":(String.join(", ", masterPer)));
		String features= "<br/><b>Merkmale:</b> "+String.join(", ",crea.getFeatures().stream().map(feat -> feat.getNameWithRating(loc)).collect(Collectors.toList()));

//		UI.getCurrent().getPage().retrieveExtendedClientDetails(receiver -> {
//            details = receiver;
//            add(new Label("Screen: "+details.getWindowInnerWidth()+"x"+details.getWindowInnerHeight()+" / "+details.getDevicePixelRatio()));
//        });



		Html html = new Html("<div style='width: 20em; border-radius: 1em; background-color: rgba(255,255,255,0.5); padding-top: 1em; padding-bottom: 1em;'>"
		                + "<table class='splimo-stats' style='border: 1px solid #9BD1F5; width:21em' cellspacing='0'>"
		                + head1
		                + value1
		                + head2
		                + value2
		                + "</table>"
		                + "<table class='splimo-stats' style='border: 1px solid #9BD1F5; width:21em' cellspacing='0'>"
		                + head3
		                + "<tbody>"
		                + getCreatureWeaponLines(crea, loc)
		                + "</tbody>"
		                + "</table>"
		                + "<div style='margin-left: 0.25em; margin-right: 0.25em'>"
		                + type
		                + level
		                + skills
		                + spellsS
		                + master
		                + features
		                + "</div>"
		                + "</div>"
		                );


		// Image
		String path = "frontend/images/splittermond/"+crea.getId()+".png";
		Image image = new Image(path, crea.getName(loc));
		image.getStyle().set("align-self","flex-start");

		// Description
		Label desc = new Label();
		desc.getStyle().set("width", "18em");
		desc.setText("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.");
		desc.getStyle().set("border-radius", "1em");
		desc.getStyle().set("padding", "0.5em");
		desc.getStyle().set("padding-bottom", "1em");
		desc.getStyle().set("align-self","flex-start");
		desc.getStyle().set("background-color", "rgba(255,255,255,0.5");

		FormLayout layout = new FormLayout();
		layout.getStyle().set("gap", "0.5em");
		layout.add(html);

		if (image!=null) {
			layout.add(image);
		}
		layout.add(desc);
		layout.setResponsiveSteps(
		        new ResponsiveStep("20em", 1),
		        new ResponsiveStep("40em", 2),
		        new ResponsiveStep("60em", 3));

		H2 name = new H2(crea.getName(loc));
		List<String> refs = new ArrayList<>();
		for (PageReference ref : crea.getPageReferences()) {
			if (ref.getLanguage().equals(loc.getLanguage())) {
				refs.add(ref.getProduct().getName(loc)+" "+ref.getPage());
			}
		}
		H4 references = new H4(String.join(", ", refs));

	    Div div = new Div(name, references, layout);
	    return div;
	}

	//-------------------------------------------------------------------
	private static String getSpellString(Creature crea, Locale loc) {
		if (crea.getSpells().isEmpty()) {
			return "";
		}
		StringBuffer ret = new StringBuffer("<br/><b>Zauber:</b> ");

		class Depth1 {
			SMSkill skill;
			Map<Integer,List<Spell>> levels = new HashMap<Integer, List<Spell>>();
		}

		List<Depth1> list1 = new ArrayList<Depth1>();
		/*
		 * Collect data
		 */
		for (SMSkillValue sVal : crea.getSkills(SkillType.MAGIC)) {
			// Ignore all skills with no points
			if (sVal.getValue()<1 && sVal.getModifier()<1)
				continue;
			Depth1 d1 = new Depth1();
			d1.skill = sVal.getSkill();
			list1.add(d1);
			for (SpellValue spVal : crea.getSpells()) {
				if (spVal.getSkill()!=d1.skill)
					continue;
				List<Spell> spells = d1.levels.get(spVal.getSpellLevel());
				if (spells==null) {
					spells = new ArrayList<Spell>();
					d1.levels.put(spVal.getSpellLevel(), spells);
				}
				spells.add(spVal.getResolved());
			}
		}

		/*
		 * Format data
		 */
		for (Depth1 d1 : list1) {
			ret.append("  <em>"+d1.skill.getName(loc)+"</em> ");
			List<Integer> levels = new ArrayList<Integer>(d1.levels.keySet());
			Collections.sort(levels);
			for (Integer lvl : levels) {
				switch (lvl) {
				case 0: ret.append("0 "); break;
				case 1: ret.append("I "); break;
				case 2: ret.append("II "); break;
				case 3: ret.append("III "); break;
				case 4: ret.append("IV "); break;
				case 5: ret.append("V "); break;
				}
				List<Spell> spellList = new ArrayList<Spell>(d1.levels.get(lvl));
				Collections.sort(spellList, new Comparator<Spell>() {
					public int compare(Spell o1, Spell o2) {
						return Collator.getInstance(loc).compare(o1.getName(loc), o2.getName(loc));
					}
				});
				ret.append(String.join(", ", spellList.stream().map(sp -> sp.getName(loc)).collect(Collectors.toList())));
				ret.append("; ");
			}
		}

		return ret.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see com.vaadin.flow.data.renderer.ComponentRenderer#createComponent(java.lang.Object)
	 */
//	@Override
//	public Component createComponent(Creature item) {
//		Div name = new Div();
//	    name.getStyle().set("font-weight", "bold");
//	    name.setText(item.getName(loc));
//
//	    Renderer<Creature> render = TemplateRenderer.<Creature>of(
//		                  "<table class='splimo-stats' style='border: 1px solid #9BD1F5;' cellspacing='0'>"
//		                + "<thead><tr><th>AUS</th><th>BEW</th><th>INT</th><th>KON</th><th>MYS</th><th>STÄ</th><th>VER</th><th>WIL</th></tr></thead>"
//		                + "<tbody><tr><td>[[item.aus]]</td><td>[[item.bew]]</td><td>[[item.int]]</td><td>[[item.kon]]</td><td>[[item.mys]]</td><td>[[item.sta]]</td><td>[[item.ver]]</td><td>[[item.wil]]</td></tr>"
//		                + "<thead><tr><th>GK</th><th>GSW</th><th>LP</th><th>FO</th><th>VTD</th><th>SR</th><th>KW</th><th>GW</th></tr></thead>"
//		                + "<tr><td>[[item.gk]]</td><td>[[item.gsw]]</td><td>[[item.lp]]</td><td>[[item.fo]]</td><td>[[item.vtd]]</td><td>[[item.sr]]</td><td>[[item.kw]]</td><td>[[item.gw]]</td></tr>"
//		                + "</table>")
//		        .withProperty("aus", crea -> crea.getAttribute(Attribute.CHARISMA).getModifiedValue())
//		        .withProperty("bew", crea -> crea.getAttribute(Attribute.AGILITY).getModifiedValue())
//		        .withProperty("int", crea -> crea.getAttribute(Attribute.INTUITION).getModifiedValue())
//		        .withProperty("kon", crea -> crea.getAttribute(Attribute.CONSTITUTION).getModifiedValue())
//		        .withProperty("mys", crea -> crea.getAttribute(Attribute.MYSTIC).getModifiedValue())
//		        .withProperty("sta", crea -> crea.getAttribute(Attribute.STRENGTH).getModifiedValue())
//		        .withProperty("ver", crea -> crea.getAttribute(Attribute.MIND).getModifiedValue())
//		        .withProperty("wil", crea -> crea.getAttribute(Attribute.WILLPOWER).getModifiedValue())
//		        .withProperty("gk" , crea -> crea.getAttribute(Attribute.SIZE).getModifiedValue())
//		        .withProperty("gsw", crea -> crea.getAttribute(Attribute.SPEED).getModifiedValue())
//		        .withProperty("lp" , crea -> crea.getAttribute(Attribute.LIFE).getModifiedValue())
//		        .withProperty("fo" , crea -> crea.getAttribute(Attribute.FOCUS).getModifiedValue())
//		        .withProperty("vtd", crea -> crea.getAttribute(Attribute.DEFENSE).getModifiedValue())
//		        .withProperty("sr" , crea -> crea.getAttribute(Attribute.DAMAGE_REDUCTION).getModifiedValue())
//		        .withProperty("kw" , crea -> crea.getAttribute(Attribute.RESIST_BODY).getModifiedValue())
//		        .withProperty("gw" , crea -> crea.getAttribute(Attribute.RESIST_MIND).getModifiedValue())
////		        .withProperty("lastname", Person::getLastName)
////		        .withProperty("address", Person::getAddress)
////		        .withProperty("image", Person::getImage)
//		        ;
//
//	    Div div = new Div(name);
////	    render.render(div, null);
//	    return div;
//	}
}
