package de.rpgframework.eden.renderer;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.prelle.splimo.SplitterTools;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.DescriptionList;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.i18n.I18NProvider;

import de.rpgframework.genericrpg.data.PageReference;
import de.rpgframework.shadowrun.ASpell;
import de.rpgframework.shadowrun.SpellFeature;
import de.rpgframework.shadowrun.SpellFeatureList;
import de.rpgframework.shadowrun.SpellFeatureReference;

/**
 * @author prelle
 *
 */
public class SR6SpellRenderer extends ComponentRenderer<Component, ASpell> {
	
	private static final long serialVersionUID = 3602192949107430615L;

	private Locale loc;
	private I18NProvider trans;
	
	//-------------------------------------------------------------------
	public SR6SpellRenderer(Locale locale, I18NProvider trans) {
		this.loc = locale;
		this.trans = trans;
	}

	//-------------------------------------------------------------------
	/**
	 * @see com.vaadin.flow.data.renderer.ComponentRenderer#createComponent(java.lang.Object)
	 */
	@Override
	public Component createComponent(ASpell item) {
		H2 name = new H2(item.getName(loc));
		List<String> refs = new ArrayList<>();
		for (PageReference ref : item.getPageReferences()) {
			if (ref.getLanguage().equals(loc.getLanguage())) {
				refs.add(ref.getProduct().getName(loc)+" "+ref.getPage());
			}
		}
		H4 references = new H4(String.join(", ", refs));
	    
	    DescriptionList dl = new DescriptionList();
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("shadowrun6.spell.drain", loc)), 
	    		new DescriptionList.Description(String.valueOf(item.getDrain()))});
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("shadowrun6.spell.category", loc)), 
	    		new DescriptionList.Description(item.getCategory().getName(loc))});
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("shadowrun6.spell.castrange", loc)), 
	    		new DescriptionList.Description(item.getRange().getName(loc))});
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("shadowrun6.spell.effect", loc)), 
	    		new DescriptionList.Description(item.getDescription(loc))});
	    List<String> modList = new ArrayList<>();
	    for (SpellFeatureReference type : item.getFeatures()) {
	    	modList.add(type.getFeature().getName(loc));
	    }
	    UnorderedList successList = new UnorderedList();
	    successList.add(new ListItem(new Text(String.join(", ", modList))));
	    dl.add(new Component[] {
	    		new DescriptionList.Term(trans.getTranslation("shadowrun6.spell.features", loc)), 
	    		new DescriptionList.Description(new HtmlContainer("br"),successList)});
	    
	    Div div = new Div(name, references, dl);
	    div.setMaxWidth("50em");
	    return div;
	}
}
