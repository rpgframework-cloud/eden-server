package de.rpgframework.eden;

import java.util.Locale;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.Tabs.Orientation;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.PWA;

import de.rpgframework.eden.account.CreateAccountView;
import de.rpgframework.eden.extern.InfoPage;
import de.rpgframework.eden.gui.AppHeader;
import de.rpgframework.eden.splittermond.SMLibraryPage;
import de.rpgframework.reality.Player;

/**
 * The main view contains a button and a click listener.
 */
@Route("/blafuu")
@Push
@JsModule("frontend://styles/shared-styles.js")
@StyleSheet("frontend://styles/eden.css")
@PageTitle("Eden - Where your adventure begins")
@Viewport("width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes, viewport-fit=cover")
public class MainLayout extends AppLayout { //implements BeforeEnterObserver {

	private static final long serialVersionUID = -228909947495980669L;

	private static Logger logger = LogManager.getLogger(MainLayout.class);

	protected Tabs menu;
//	protected Tab  tabMain;
//	protected Tab  tabManage;
//	protected Tab  tabPrepare;
	
	private AppHeader header;
	protected InfoPage content;
//	private SelectRoleplayingSystemPage select;
	
	//-------------------------------------------------------------------
	public MainLayout() {
		setPrimarySection(Section.DRAWER);
//		tabMain    = new Tab(new RouterLink(getTranslation("navigation.home"), MainLayout.class));
//		tabManage  = new Tab(new RouterLink(getTranslation("navigation.manage"), LandingPageManage.class));
//		tabPrepare = new Tab(new RouterLink(getTranslation("navigation.prepare"), LandingPagePrepare.class));
		menu       = new Tabs(true);
//		tabsDrawer = new Tabs(true, tabMain, tabManage, tabPrepare);
//		tabsDrawer.setOrientation(Orientation.VERTICAL);
//		DrawerToggle drawerToggle = new DrawerToggle();
//		addToNavbar(drawerToggle);
//		addToDrawer(tabsDrawer);

		initUpperNavBar();

		initComponents();
//		initInteractivty();
		
		showStartPage();
	}
	
	//-------------------------------------------------------------------
	protected void initComponents() {
		content = new InfoPage();
//		select  = new SelectRoleplayingSystemPage();
	}
	
	//-------------------------------------------------------------------
	protected void showStartPage() {
		Player player = UI.getCurrent().getSession().getAttribute(Player.class);
		logger.info("Default locale is "+Locale.getDefault());
		logger.info("player = "+player+" and locale "+UI.getCurrent().getLocale());
		if (player==null) {
//			UI.getCurrent().navigate(InfoPage.class);
			UI.getCurrent().navigate("extern");
//			setContent(content);
		} else {
			UI.getCurrent().navigate(SMLibraryPage.class);
//			setContent(select);
		}
	}

	//-------------------------------------------------------------------
	protected void initUpperNavBar() {
//		menu = new MenuBar();
//
//		logger.info("Locale = "+UI.getCurrent().getSession().getLocale());
//		
////		menuLibrary  = menu.addItem(new Icon(VaadinIcon.ARCHIVE));
////		menuLibrarySplittermond = menuLibrary.getSubMenu().addItem(RoleplayingSystem.SPLITTERMOND.getName());
////		menuLibrarySplittermondBeastiary = menuLibrarySplittermond.getSubMenu().addItem(getTranslation("menuitem.beastiary"));
////		menuLibrarySplittermondBeastiary.addClickListener(ev -> UI.getCurrent().navigate(BestiariumPage.class));
////		menuLibraryShadowrun6   = menuLibrary.getSubMenu().addItem(RoleplayingSystem.SHADOWRUN6.getName());
//		
//		Player account = UI.getCurrent().getSession().getAttribute(Player.class);
////		if (account!=null) {
////			addToNavbar(new RouterLink(getTranslation("menu.characters"), CharactersOverview.class));
////			addToNavbar(new RouterLink(getTranslation("menu.characters"), CharactersOverview.class));
////		}
//		
//		// right oriented menu
//		FlexLayout layout = new FlexLayout();
//		layout.setAlignItems(Alignment.END);
//		layout.setSizeFull();
//		layout.setJustifyContentMode(JustifyContentMode.END);
//        layout.setId("header");
////        layout.getThemeList().set("dark", true);
//
//		menuProfile = menu.addItem(new Icon(VaadinIcon.USER));
//		if (account==null) {
//			menuCreate  = menuProfile.getSubMenu().addItem(getTranslation("menuitem.create"));
//			menuLogin   = menuProfile.getSubMenu().addItem(getTranslation("menuitem.login"));
//		} else {
//			menuAccount = menuProfile.getSubMenu().addItem(getTranslation("menuitem.profile"));
//			menuProfile.getSubMenu().addItem(new Hr());
//			menuLogout = menuProfile.getSubMenu().addItem(getTranslation("menuitem.logout"));
//		}
//
//
//		layout.add(menu);
////		addToNavbar(layout);
		
		header = new AppHeader();
		addToNavbar(header);
	}

//	//-------------------------------------------------------------------
//	private void initInteractivty() {
//		if (menuCreate!=null)
//		menuCreate.addClickListener(ev -> {
//			Dialog dialog = new Dialog();
//			CreateAccountView view = new CreateAccountView(dialog);
//			dialog.add(view);
//			dialog.open();
//		});
//	}

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        getTabForComponent(getContent()).ifPresent(menu::setSelectedTab);
//        viewTitle.setText(getCurrentPageTitle());
    }

    private Optional<Tab> getTabForComponent(Component component) {
        return menu.getChildren()
                .filter(tab -> ComponentUtil.getData(tab, Class.class)
                        .equals(component.getClass()))
                .findFirst().map(Tab.class::cast);
    }

    private String getCurrentPageTitle() {
        return getContent().getClass().getAnnotation(PageTitle.class).value();
    }

	//-------------------------------------------------------------------
	public AppHeader getAppHeader() {
		return header;
	}

//	//-------------------------------------------------------------------
//	private void showLoginDialog() {
//		LoginForm component = new LoginForm();
////		component.setTitle(getTranslation("dialog.login.title"));
////		component.setDescription(getTranslation("dialog.login.desc"));
//		component.setAction("");
//		component.setForgotPasswordButtonVisible(true);
//		Dialog dia = new Dialog(component);
//		component.addLoginListener(e -> {
//			logger.info("login attempt: "+e.getUsername());
//
//			BackendAccess backend = BackendAccess.getInstance();
//
//			Player account = null;
//			try {
//				if (e.getUsername().contains("@")) {
//					// Login with email
//					account = backend.getPlayerDatabase().getPlayerByEmail(e.getUsername());
//				} else {
//					account = backend.getPlayerDatabase().getPlayerByLogin(e.getUsername());
//				}
//			} catch (SQLException e1) {
//				logger.error("Error accessing player database: "+e);
//				Notification.show(getTranslation("error.internal.database",e));
//				dia.close();
//			}
//
//			if (account==null) {
//				logger.info("Log in attempt for non-existing account: "+e.getUsername());
//				component.setError(true);
//				return;
//			}
//
//			// Player known - compare password
//			if (account.getPassword().equals(e.getPassword())) {
//				// Login successful
//				component.setError(false);
//				UI.getCurrent().getSession().setAttribute(Player.class, account);
//				logger.debug("Logged in: "+e.getUsername());
//				dia.close();
//				UI.getCurrent().getPage().reload();
//			} else {
//				// Login failed
//				logger.info("Invalid password for user "+e.getUsername());
//				component.setForgotPasswordButtonVisible(true);
//				component.setError(true);
//			}
//		});
//		component.addForgotPasswordListener(ev -> {
//			logger.info("Forgot password for "+ev);
//		});
//		LoginI18n i18n = LoginI18n.createDefault();
//		i18n.setAdditionalInformation(getTranslation("dialog.login.additional"));
//		component.setI18n(i18n);
////		component.setOpened(true);
//		
//		dia.setOpened(true);
//	}
//
//	//-------------------------------------------------------------------
//	private void search(String value) {
//		logger.info("Search "+value);
//		//		List<Object> searchResult = SearchRegistry.search(value);
//		//		logger.info("Result: "+searchResult);
//		//		tfSearch.setValue("");
//		//		tfSearchNav.setValue("");
//		//		Dialog dialog = new Dialog();
//		//
//		//		dialog.add(new SearchResultView(searchResult, dialog));
//		//
//		//		dialog.setWidth("800px");
//		//		dialog.setHeight("800px");
//		//
//		//		dialog.open();
//	}

	//	//-------------------------------------------------------------------
	//	/**
	//	 * @see com.vaadin.flow.router.internal.BeforeEnterHandler#beforeEnter(com.vaadin.flow.router.BeforeEnterEvent)
	//	 */
	//	@Override
	//	public void beforeEnter(BeforeEnterEvent event) {
	//		UranosUser sessionUser = UI.getCurrent().getSession().getAttribute(UranosUser.class);
	//		// if the user is already logged in, do nothing
	//		if (sessionUser!=null)
	//			return;
	//		
	//		
	//		LoginOverlay component = new LoginOverlay();
	//		component.setTitle("Uranos");
	//		component.setDescription("Unified Reliable Alarm and Network Operation Service ;-)");
	//		component.setAction("");
	//		component.addLoginListener(e -> {
	//			if (e.getUsername().contains("@")) {
	//				component.setError(true);
	//				return;
	//			}
	//			component.setError(false);			
	//			
	//			if (!SQLAuthenticator.isUserKnown(e.getUsername())) {
	//				logger.warn("Unknown user '"+e.getUsername()+"'");
	//				// Create the user
	//				UranosUser user = new UranosUser(e.getUsername());
	//				user.setPassword(e.getPassword());
	//				user.setMailAddress(e.getUsername()+"@plusnet.de");
	//				try {
	//					GraphDatabase.getInstance().add(user);
	//					UI.getCurrent().getSession().setAttribute(UranosUser.class, user);
	//					logger.info("User "+e.getUsername()+" created a new user");
	//					// Send mail, if possible
	//					if (MailerLoader.getInstance()!=null) {
	//						logger.info("Send mail");
	//						try {
	//							MailerLoader.getInstance().send(
	//									"feedback.centraflex@plusnet.de", 
	//									Arrays.asList("feedback.centraflex@plusnet.de"), 
	//									Arrays.asList(new String[][] {{"Tool","Uranos"}}), 
	//									"Uranos: Account erzeugt für "+e.getUsername(), 
	//									"Der User '"+e.getUsername()+"' wurde als Account auf Uranos angelegt.\nBitte setze doch mal jemand die passenden Rechte.", null);
	//						} catch (Exception e1) {
	//							logger.error("Failed sending mail",e1);
	//						}
	//					}
	//					logger.info("Change UI");
	//					component.close();
	//					UI.getCurrent().navigate(ChangeAccountView.class);
	//				} catch (DatabaseException e1) {
	//					logger.error("Failed creating a new account for "+e.getUsername(),e);
	//					component.setError(true);
	//					component.setForgotPasswordButtonVisible(false);
	//				}
	//				
	//			} else {
	//				UranosUser user = SQLAuthenticator.login(e.getUsername(), e.getPassword());
	//				if (user==null) {
	//					logger.warn("User '"+e.getUsername()+"' is known, but password is wrong");
	//					component.setForgotPasswordButtonVisible(false);
	//					component.setError(true);
	//				} else {
	//					component.close();
	//					logger.debug("Set user for session");
	//					UI.getCurrent().getSession().setAttribute(UranosUser.class, user);
	//					UI.getCurrent().getPage().reload();
	//				}
	//			}
	//			});
	//		LoginI18n i18n = LoginI18n.createDefault();
	//		i18n.setAdditionalInformation("Wenn Du noch keinen Account hast, wende dich an Feedback.centraflex@plusnet.de");
	//		component.setI18n(i18n);
	//		component.setOpened(true);
	//	}

}
