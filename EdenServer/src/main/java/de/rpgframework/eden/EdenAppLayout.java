package de.rpgframework.eden;

import java.lang.reflect.Method;
import java.util.Optional;

import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouterLink;

import de.rpgframework.eden.account.AccountPage;
import de.rpgframework.eden.extern.InfoPage;
import de.rpgframework.eden.gui.AppHeader;
import de.rpgframework.eden.intern.lookup.LookupPage;
import de.rpgframework.reality.Player;

/**
 * @author prelle
 *
 */
@Push
@JsModule("frontend://styles/shared-styles.js")
@StyleSheet("frontend://styles/eden.css")
@PageTitle("Eden - Where your adventure begins")
@Viewport("width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes, viewport-fit=cover")
public abstract class EdenAppLayout extends AppLayout {
	
	private static final long serialVersionUID = 2214628675446239097L;

	private final static Logger logger = EdenApplication.logger;

	protected AppHeader header;
	protected final Tabs menu;

	//-------------------------------------------------------------------
	public EdenAppLayout() {
		setPrimarySection(Section.DRAWER);
//		addToNavbar(true, createOldHeaderContent());
		header = new AppHeader();
		header.setNavigation(createHeaderContent());
		addToNavbar(header);
//		addToNavbar(true, createHeaderContent());
		menu = createMenu();
		addToDrawer(createDrawerContent(menu));
		
		ensureLoggedInUser();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	protected void ensureLoggedInUser() {
		Player player = UI.getCurrent().getSession().getAttribute(Player.class);
		logger.info("player = "+player+" and language "+UI.getCurrent().getLocale());
		if (player==null) {
			logger.info("Not logged in - redirect");
			UI.getCurrent().navigate(InfoPage.class);
			UI.getCurrent().getPage().reload();
//			setContent(content);
		}

	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		header.getNavigationTabs().addSelectedChangeListener(ev -> {
			logger.info("Selection now "+ev.getSelectedTab()+"   prev="+ev.getPreviousTab());
			Component comp = getComponentForTab(ev.getSelectedTab());
//			UI.getCurrent().getPage().addStyleSheet("frontend://styles/hello.css");
			setContent(comp);
		});
	}

	//-------------------------------------------------------------------
	protected abstract Tab[] createHeaderContent();

	//-------------------------------------------------------------------
	protected abstract Component getComponentForTab(Tab tab);
   	
	//-------------------------------------------------------------------
    protected Component createDrawerContent(Tabs menu) {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.setPadding(false);
        layout.setSpacing(false);
        layout.getThemeList().set("spacing-s", true);
        layout.setAlignItems(FlexComponent.Alignment.STRETCH);
        VerticalLayout logoLayout = new VerticalLayout();
        logoLayout.setSpacing(false);
        logoLayout.setId("logo");
        logoLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        logoLayout.add(new Image("/"+PageHierarchy.PREFIX_PAGES+"frontend/images/RPGFramework_Logo.png", "RPGFramework"));
        H2 appName = new H2("Eden");
        appName.getStyle().set("margin-top", "0em");
        logoLayout.add(appName);
        layout.add(logoLayout, menu);
        return layout;
    }

	//-------------------------------------------------------------------
    private Tabs createMenu() {
        final Tabs tabs = new Tabs();
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        tabs.addThemeVariants(TabsVariant.LUMO_MINIMAL);
        tabs.setId("tabs");
        tabs.setAutoselect(false);
        tabs.add(createMenuItems());
        return tabs;
    }

	//-------------------------------------------------------------------
    private Component[] createMenuItems() {
        return new Tab[]{
        		createTab(getTranslation("page.lookup"), LookupPage.class), 
        		createTab(getTranslation("page.account"), AccountPage.class), 
         		};
    }

	//-------------------------------------------------------------------
    private static Tab createTab(String text, Class<? extends Component> navigationTarget) {
        final Tab tab = new Tab();
        tab.add(new RouterLink(text, navigationTarget));
        ComponentUtil.setData(tab, Class.class, navigationTarget);
        if (navigationTarget==AccountPage.class) {
        	tab.setFlexGrow(2);
        }
        return tab;
    }

    //-------------------------------------------------------------------
    /**
     * @see com.vaadin.flow.component.applayout.AppLayout#afterNavigation()
     */
    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        getTabForComponent(getContent()).ifPresent(menu::setSelectedTab);
//        viewTitle.setText(getCurrentPageTitle());
        header.setTitle(getCurrentPageTitle());
		
		ensureLoggedInUser();
    }

	//-------------------------------------------------------------------
    private Optional<Tab> getTabForComponent(Component component) {
        return menu.getChildren().filter(tab -> ComponentUtil.getData(tab, Class.class).equals(component.getClass()))
                .findFirst().map(Tab.class::cast);
    }

	//-------------------------------------------------------------------
   private String getCurrentPageTitle() {
    	if (getContent()==null)
    		return "?";
    	try {
			Method getName = getContent().getClass().getMethod("getTitle");
			return (String) getName.invoke(getContent());
		} catch (NoSuchMethodException e) {
		} catch (Exception e) {
			logger.error("Failed calling "+getContent().getClass()+".getTitle");
		}
    		
    	
    	if (getContent().getClass().getAnnotation(PageTitle.class)==null)
    		return "Missing @PageTitle in "+getContent().getClass();
        return getTranslation(getContent().getClass().getAnnotation(PageTitle.class).value(), UI.getCurrent().getLocale());
    }
   
}
