package de.rpgframework.eden.account;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.tabs.Tab;

import de.rpgframework.eden.EdenAppLayout;
import de.rpgframework.eden.shadowrun6.SR6LibraryPage;
import de.rpgframework.eden.splittermond.SMLibraryPage;

/**
 * @author prelle
 *
 */
public class AccountLayout extends EdenAppLayout {

	private static final long serialVersionUID = -4866755313157339405L;

	protected static Logger logger = LogManager.getLogger(AccountLayout.class);
	
	private Tab tabProfile;
	private Tab tabContents;
	
	//-------------------------------------------------------------------
	public AccountLayout() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.EdenAppLayout#createHeaderContent()
	 */
	@Override
	protected Tab[] createHeaderContent() {
		tabProfile = new Tab(getTranslation("navigation.profile"));
		tabContents   = new Tab(getTranslation("navigation.profile.contents"));
		return new Tab[] {tabProfile, tabContents};
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.EdenAppLayout#getComponentForTab(com.vaadin.flow.component.tabs.Tab)
	 */
	@Override
	protected Component getComponentForTab(Tab tab) {
		if (tab==tabProfile) {
			return new SMLibraryPage();
		}
		if (tab==tabContents) {
			return new ContentsView();
		}
		return new Label("AccountLayout.getComponentForTab("+tab+")");
	}

}
