package de.rpgframework.eden.account;

import java.sql.SQLException;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.componentfactory.Breadcrumb;
import com.vaadin.componentfactory.Breadcrumbs;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;

import de.rpgframework.eden.PageHierarchy;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.ShopDatabase;
import de.rpgframework.reality.CatalogItem;

/**
 * List all content packs that are connected with an account.
 * 
 * @author prelle
 *
 */
@Route(value=PageHierarchy.PATH_ACCOUNT_CONTENTS, layout = AccountLayout.class)
public class ContentsView extends VerticalLayout implements AfterNavigationObserver {

	private static Logger logger = LogManager.getLogger(ContentsView.class);

	private Locale loc;
	private Breadcrumbs breadcrumbs;
	
	private Image sectionImage;
	private Paragraph introLine1;
	private Paragraph introLine2;
	
	private Grid<CatalogItem> list;
	private ContentPackDetail detail;
	
	//-------------------------------------------------------------------
	public ContentsView() {
		loc = UI.getCurrent().getSession().getLocale();
		logger.debug("in ContentsView");

		initBreadcrumbs();
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initBreadcrumbs() {
		breadcrumbs = new Breadcrumbs();
		breadcrumbs.add(
			    new Breadcrumb(getTranslation("navigation.home"),"/", false),
		    new Breadcrumb(getTranslation("navigation.profile"), "profile"),
		    new Breadcrumb(getTranslation("navigation.profile.contents"))
		    );
		add(breadcrumbs);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("serial")
	private void initComponents() {
		introLine1 = new Paragraph(new Label(getTranslation("page.profile.contents.intro1")));
		introLine2 = new Paragraph(new Label(getTranslation("page.profile.contents.intro2")));
		sectionImage = new Image("/frontend/images/Library.jpg", "Rulebooks");
		
		list = new Grid<CatalogItem>();
		list.addColumn(CatalogItem::getName).setHeader(getTranslation("page.profile.contents.column.title")).setWidth("70%");
		list.addColumn(new ValueProvider<CatalogItem, String>() {
			public String apply(CatalogItem source) {
				return source.getRules().getName(UI.getCurrent().getLocale());
			}
		}).setHeader(getTranslation("page.profile.contents.column.system")).setWidth("30%");
		detail = new ContentPackDetail();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		list.setMinWidth("20em");
		list.setMinHeight("20em");
 		UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
 			int width = details.getWindowInnerWidth();
 			logger.info("-----width = "+width+"-----------");
 			if (width>900) {
 				list.setMinWidth("30em");
 			} else if (width>700) {
 				list.setMinWidth("20em");
 			} else {
 				list.setWidth("90%");
 			}
             });
 		list.setWidth("90%");
 		list.setHeightByRows(false);
 		list.setHeightFull();

 		sectionImage.getStyle().set("max-width","32%");
		sectionImage.getStyle().set("width","32%");
//		introLine1.getStyle().set("max-width","50%");
//		introLine2.getStyle().set("max-width","50%");
		HorizontalLayout line1 = new HorizontalLayout(sectionImage, new VerticalLayout(introLine1, introLine2));
		line1.setSpacing(true);
		line1.setClassName("section-intro");

		FlexLayout line2 = new FlexLayout(list, detail);
		list.setHeightFull();
		line2.setSizeFull();
		
		add(line1);
		add(line2);
		
		setSizeFull();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		list.addItemClickListener(
		        event -> {
		     		UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
		     			int width = details.getWindowInnerWidth();
		     			if (width>700) {
				        	logger.debug("Display at side "+event.getItem().getId());
//				        	Optional<Component> removeMe = bxSideBySide.getChildren().filter(ch -> ch!=grid).findAny();
//				        	if (removeMe.isPresent())
//				        		bxSideBySide.remove(removeMe.get());
//				        	bxSideBySide.add(grid);
//				        	bxSideBySide.add(renderSideComponent(event.getItem()));
//		     			} else {
//				        	logger.debug("Navigate to "+event.getItem().getId());
//				        	navigateTo(event.getItem().getId());
		     			}
		                 });
		        });
	}

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
    	logger.debug("afterNavigation: "+event);
    	refresh();
    }


	//-------------------------------------------------------------------
	private void refresh() {
		ShopDatabase shop = BackendAccess.getInstance().getShopDatabase();
		try {
			list.setItems(shop.getCatalogItems());
		} catch (SQLException e) {
			logger.error("Error getting catalog items",e);
			Notification.show("Error accessing database");
		}
	}

}

class ContentPackDetail extends VerticalLayout implements HasUrlParameter<Integer> {
	
	private Label title;
	
	public ContentPackDetail() {
		title = new Label("Test");
		add(title);
	}

	@Override
	public void setParameter(BeforeEvent event, Integer parameter) {
		// TODO Auto-generated method stub
		
	}
	
}
