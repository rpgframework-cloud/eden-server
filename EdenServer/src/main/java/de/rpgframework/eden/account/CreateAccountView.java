package de.rpgframework.eden.account;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.QueryParameters;

import de.rpgframework.eden.PageHierarchy;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.PlayerDatabase;
import de.rpgframework.reality.Player;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class CreateAccountView extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(CreateAccountView.class);

	private Dialog dialog;
	
	private TextField tfLogin;
	private TextField tfEmail;
	private TextField tfFirst;
	private TextField tfLast;
	private PasswordField tfPasswd1;
	private PasswordField tfPasswd2;
	
	private Button btnOK;
	private Button btnCancel;
	
	//-------------------------------------------------------------------
	public CreateAccountView(Dialog dialog) {
		this.dialog = dialog;
		dialog.setCloseOnOutsideClick(false);
		initComponents();
		initLayout();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		tfLogin = new TextField(getTranslation("dialog.account.create.login"));
		tfLogin.setMinLength(4);
		tfEmail = new TextField(getTranslation("dialog.account.create.email"));
		tfEmail.setMinLength(6);
		tfEmail.setPattern(".+@.+");
		tfFirst = new TextField(getTranslation("dialog.account.create.firstname"));
		tfFirst.setMinLength(2);
		tfLast  = new TextField(getTranslation("dialog.account.create.lastname"));
		tfLast.setMinLength(2);
		tfPasswd1 = new PasswordField(getTranslation("dialog.account.create.password"));
		tfPasswd1.setMinLength(6);
		tfPasswd2 = new PasswordField(getTranslation("dialog.account.create.password"));
		
		btnOK = new Button(getTranslation("button.ok"), VaadinIcon.CHECK.create());
		btnCancel = new Button(getTranslation("button.cancel"), VaadinIcon.CLOSE.create());
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		FormLayout form1 = new FormLayout(tfLogin, tfEmail, tfFirst, tfLast, tfPasswd1, tfPasswd2);
		FormLayout form2 = new FormLayout(btnOK, btnCancel);
		
		add(form1, form2);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnCancel.addClickListener(ev ->  dialog.close());
		btnOK.addClickListener(ev -> {
			if (allDataCorrect()) {
				logger.debug("All good");
				if (!doesAccountExist(tfLogin.getValue(), tfEmail.getValue())) {
					// Neither email nor login taken
					try {
						Player account = BackendAccess.getInstance().createPlayer(
								tfFirst.getValue(), 
								tfLast.getValue(), 
								tfEmail.getValue(), 
								tfLogin.getValue(), 
								tfPasswd1.getValue(),
								UI.getCurrent().getLocale()
								);
						logger.info("Successfully created new account: "+account);
						 UI.getCurrent().getSession().setAttribute(Player.class, account);
						 dialog.close();
							Map<String, List<String>> parameters = new HashMap<>();
							parameters.put("mail", Arrays.asList(tfEmail.getValue()));
							logger.debug("QueryParameters = "+parameters);
					    	UI.getCurrent().navigate(PageHierarchy.PATH_EXTERN_VERIFY, new QueryParameters(parameters));
//				    	UI.getCurrent().navigate(VerifyAccountView.class);
						 UI.getCurrent().getPage().reload();
					} catch (SQLException e) {
						logger.error("Failed creating account: "+e,e);
						Notification.show(getTranslation("error.general.database"));
					} catch (Exception e) {
						logger.error("Cannot create account: "+e,e);
						Notification.show(getTranslation("error.general.database"));
					}
				}
			}
		});
	}
	
	//-------------------------------------------------------------------
	private boolean allDataCorrect() {
		boolean correct  = true;
		// Login
		if (tfLogin.getValue().isBlank() || tfLogin.getValue().length()<4) {
			tfLogin.setInvalid(true);
			tfLogin.setErrorMessage(getTranslation("dialog.account.create.error.login"));
			correct = false;
		} else {
			tfLogin.setInvalid(false);
			tfLogin.setErrorMessage(null);
		}
		
		// Email
		if (tfEmail.getValue().length()<4) {
			tfEmail.setInvalid(true);
			tfEmail.setErrorMessage(getTranslation("dialog.account.create.error.email"));
			correct = false;
		} else {
			tfEmail.setErrorMessage(null);
			tfEmail.setInvalid(false);
		}
		
		// First name
		if (tfFirst.getValue().length()<2) {
			tfFirst.setErrorMessage(getTranslation("dialog.account.create.error.firstname"));
			tfFirst.setInvalid(true);
			correct = false;
		} else {
			tfFirst.setErrorMessage(null);
			tfFirst.setInvalid(false);
		}

		// Last name
		if (tfLast.getValue().length()<2) {
			tfLast.setErrorMessage(getTranslation("dialog.account.create.error.lastname"));
			tfLast.setInvalid(true);
			correct = false;
		} else {
			tfLast.setErrorMessage(null);
			tfLast.setInvalid(false);
		}

		// Last name
		if (tfPasswd1.getValue().length()<6) {
			tfPasswd1.setInvalid(true);
			tfPasswd1.setErrorMessage(getTranslation("dialog.account.create.error.password1"));
			correct = false;
		} else {
			tfPasswd1.setErrorMessage(null);
			tfPasswd1.setInvalid(false);
		}

		// Last name
		if (!tfPasswd1.getValue().equals(tfPasswd2.getValue())) {
			tfPasswd2.setInvalid(true);
			tfPasswd2.setErrorMessage(getTranslation("dialog.account.create.error.password2"));
			correct = false;
		} else {
			tfPasswd2.setErrorMessage(null);
			tfPasswd2.setInvalid(false);
		}

		return correct;
	}
	
	//-------------------------------------------------------------------
	private boolean doesAccountExist(String login, String email) {
		PlayerDatabase database = BackendAccess.getInstance().getPlayerDatabase();
		try {
			if (database.getPlayerByEmail(email)!=null) {
				tfEmail.setInvalid(true);
				tfEmail.setErrorMessage(getTranslation("dialog.account.create.error.email_in_use"));
				return true;
			}
			if (database.getPlayerByLogin(login)!=null) {
				tfLogin.setInvalid(true);
				tfLogin.setErrorMessage(getTranslation("dialog.account.create.error.login_taken"));
				return true;
			}
			return false;
		} catch (Exception e) {
			logger.error("Failed accessing database",e);
			Notification.show(getTranslation("error.general.database"));
		}
		return true;
	}
}
