package de.rpgframework.eden.account;

import java.io.InputStream;
import java.net.InetAddress;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import de.rpgframework.eden.EdenApplication;
import de.rpgframework.eden.PageHierarchy;
import de.rpgframework.reality.Player;

/**
 * @author prelle
 *
 */
@Route(value=PageHierarchy.PATH_ACCOUNT, layout = AccountLayout.class)
@PageTitle("page.account")
public class AccountPage extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(AccountPage.class);

	//-------------------------------------------------------------------
	/**
	 */
	public AccountPage() {
		Locale loc = UI.getCurrent().getSession().getLocale();
		Player player = UI.getCurrent().getSession().getAttribute(Player.class);
		logger.info("Default locale is "+Locale.getDefault());

		InputStream ins = ClassLoader.getSystemResourceAsStream("static/account.html");
		if (ins!=null) {
			Html html = new Html(ins);
			add(html);
		} else {
			Html html = new Html("<div>\n"+
					"<p>Nichts gefunden</p>"+
					"<p><b>Splittermond Foundry Compendium</b>:  http://"+player.getLogin()+":"+player.getPassword()+"@"+EdenApplication.REMOTE_BACKEND_HOSTPORT+"/"+PageHierarchy.PREFIX_API_INTERN+"foundry/splittermond/compendium/module.json</p>\n"+
					"<p><b>Shadowrun 6 Foundry Compendium</b>:  http://"+player.getLogin()+":"+player.getPassword()+"@"+EdenApplication.REMOTE_BACKEND_HOSTPORT+"/"+PageHierarchy.PREFIX_API_INTERN+"foundry/shadowrun6/compendium/module.json</p>\n"+
					"</div>");
			add(html);
		}
	}

}
