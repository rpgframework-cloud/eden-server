package de.rpgframework.eden.account;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

/**
 * @author prelle
 *
 */
public class CatalogItemCard extends HorizontalLayout {
	
	private Image image;

	//-------------------------------------------------------------------
	public CatalogItemCard() {
        addClassName("card");
        setSpacing(false);
        getThemeList().add("spacing-s");
	}

	//-------------------------------------------------------------------
	private void initComponents() {
        image = new Image();
//        image.setSrc(person.getImage());
        VerticalLayout description = new VerticalLayout();
        description.addClassName("description");
        description.setSpacing(false);
        description.setPadding(false);

	}

}
