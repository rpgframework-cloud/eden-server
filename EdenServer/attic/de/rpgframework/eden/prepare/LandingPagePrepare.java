package de.rpgframework.eden.prepare;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.Route;

import de.rpgframework.eden.MainLayout;

/**
 * @author prelle
 *
 */
@Route(value="prepare", layout = MainLayout.class)
public class LandingPagePrepare extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(LandingPagePrepare.class);
	
	private Tabs tabs;
	private Map<Tab, Component> tabsToPages;
	private Div pages;

	//-------------------------------------------------------------------
	public LandingPagePrepare() {
		initComponents();
		initLayout();
		initInteractivity();
		
		setWidthFull();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		tabsToPages = new HashMap<>();
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Div divAdventures = new Div(new AdventuresTabComponent());
		Div divSessions   = new Div(new SessionsTabComponent());
		divSessions.setVisible(false);
		
		pages = new Div(divAdventures, divSessions);
		
		Tab tabAdventures = new Tab(getTranslation("page.prepare.tab.adventures"));
		Tab tabSessions = new Tab(getTranslation("page.prepare.tab.sessions"));
		tabs = new Tabs(tabAdventures, tabSessions);
		tabsToPages.put(tabAdventures, divAdventures);
		tabsToPages.put(  tabSessions, divSessions);

		add(tabs, pages);
		
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		tabs.addSelectedChangeListener(event -> {
		    tabsToPages.values().forEach(page -> page.setVisible(false));
		    Component selectedPage = tabsToPages.get(tabs.getSelectedTab());
		    selectedPage.setVisible(true);
		});
	}

}
