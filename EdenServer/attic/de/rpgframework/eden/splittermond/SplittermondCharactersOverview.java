package de.rpgframework.eden.splittermond;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

import de.rpgframework.eden.dialogs.CharUploadDialog;
import de.rpgframework.eden.extern.InfoPage;
import de.rpgframework.reality.Player;

/**
 * @author prelle
 *
 */
@Route(value="splttermond/characters", layout =SplittermondLayout.class)
public class SplittermondCharactersOverview extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(SplittermondCharactersOverview.class);

	private Button btnImport;

	//-------------------------------------------------------------------
	public SplittermondCharactersOverview() {
		
		Player account = UI.getCurrent().getSession().getAttribute(Player.class);
		// Ensure player is logged in
		if (account==null) {
			try {
				UI.getCurrent().navigate(InfoPage.class);
				return;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		initComponents();
		initLayout();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		btnImport = new Button(new Image("frontend/icons/char_upload.png", "Character Import"));
		btnImport.setHeightFull();
		btnImport.getElement().setProperty("title", getTranslation("page.chars.button.upload"));
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		add(btnImport);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnImport.addClickListener(ev -> uploadClicked());
	}
	
	//-------------------------------------------------------------------
	private void uploadClicked() {
		
		CharUploadDialog dialog = new CharUploadDialog();
		dialog.setOpened(true);
	}
	
}
