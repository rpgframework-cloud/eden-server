package de.rpgframework.eden.splittermond;

import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

/**
 * @author prelle
 *
 */
//@Route(value="info", layout = MainLayout.class)
@Route(value="splittermond", layout = SplittermondLayout.class)
@StyleSheet("frontend://styles/splittermond.css")
public class SplittermondStartPage extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(SplittermondStartPage.class);
	
	private Button btnLibrary;
	private Button btnRules;
	
	//-------------------------------------------------------------------
	public SplittermondStartPage() {
		initComponents();
		initLayout();
		initInteractivity();
		
		setWidthFull();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		Locale loc = UI.getCurrent().getSession().getLocale();
		
		btnLibrary = new Button(getTranslation("navigation.splittermond.library", loc), VaadinIcon.SEARCH.create());
		btnLibrary.setHeight("5em");
		btnLibrary.getStyle().set("margin", "1em");
		btnRules = new Button(getTranslation("navigation.splittermond.rules", loc), VaadinIcon.SEARCH.create());
		btnRules.setHeight("5em");
		btnRules.getStyle().set("margin", "1em");
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		
		FormLayout flex = new FormLayout(btnLibrary, btnRules);
		flex.setWidthFull();
		add(flex);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnLibrary.addClickListener(ev -> {
			UI.getCurrent().navigate(LibraryPage.class);
		});
		btnRules.addClickListener(ev -> {
			UI.getCurrent().navigate(RulesPage.class);
		});
	}

}
