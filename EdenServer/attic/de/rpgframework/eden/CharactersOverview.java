package de.rpgframework.eden;

import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;

import de.rpgframework.character.Attachment;
import de.rpgframework.character.Attachment.Format;
import de.rpgframework.character.Attachment.Type;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.dialogs.CharUploadDialog;
import de.rpgframework.eden.api.AttachmentDatabase;
import de.rpgframework.eden.api.BackendAccess;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;
import de.rpgframework.reality.server.ServerCharacterHandle;

/**
 * @author prelle
 *
 */
@Route(value="characters", layout =MainLayout.class)
public class CharactersOverview extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(CharactersOverview.class);

	private Button btnImport;

	//-------------------------------------------------------------------
	public CharactersOverview() {
		
		Player account = UI.getCurrent().getSession().getAttribute(Player.class);
		// Ensure player is logged in
		if (account==null) {
			Label message = new Label(getTranslation("error.not_logged_in"));
			message.getStyle().set("font-size", "300%");
			add(message);
			return;
		}
		initComponents();
		initLayout();
		initInteractivity();
		
		addPerSystem(RoleplayingSystem.SPLITTERMOND);
		addPerSystem(RoleplayingSystem.SHADOWRUN6);
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		btnImport = new Button(new Image("frontend/icons/char_upload.png", "Character Import"));
		btnImport.setHeightFull();
		btnImport.getElement().setProperty("title", getTranslation("page.chars.button.upload"));
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		add(btnImport);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnImport.addClickListener(ev -> uploadClicked());
	}

	//-------------------------------------------------------------------
	private void addPerSystem(RoleplayingSystem rules) {
		Label heading = new Label(rules.getName());
		heading.setClassName("heading");
		
		Button btnBig     = new Button();
		Button btnMedium1 = new Button();
		Button btnMedium2 = new Button();
		Button btnSmall1  = new Button();
		Button btnSmall2  = new Button();
		Button btnSmall3  = new Button();
		Button btnSmall4  = new Button();
		
		
		HorizontalLayout bxSmall = new HorizontalLayout(btnSmall1, btnSmall2, btnSmall3, btnSmall4);
		VerticalLayout bxMedium = new VerticalLayout(btnMedium1, btnMedium2);
		HorizontalLayout bxBig = new HorizontalLayout(btnBig, bxMedium);
		VerticalLayout bxAll = new VerticalLayout(heading, bxBig, bxSmall);
		
		add(bxAll);
		
		// Fill
		AttachmentDatabase attachDB = BackendAccess.getInstance().getAttachmentDatabase();
		PlayerImpl player = (PlayerImpl) UI.getCurrent().getSession().getAttribute(Player.class);
		try {
			List<CharacterHandle> list = BackendAccess.getInstance().getCharacterDatabase().listCharacters(player);
			logger.info("Player has "+list.size()+" characters");
			list = list.stream().filter(handle -> handle.getRuleIdentifier()==rules).collect(Collectors.toList());
			logger.info("Player has "+list.size()+" "+rules+" characters");
			
			for (int i=0; i<Math.min(7, list.size()); i++) {
				CharacterHandle handle = list.get(i);
				Image img = null;
				Attachment attach = attachDB.getAttachment((ServerCharacterHandle) handle, Type.CHARACTER, Format.IMAGE);
				logger.info(" bytes of "+handle.getName()+" = "+attach);
				if (attach!=null && attach.getData()!=null) {
					img = new Image(new StreamResource("img.png", () -> {
			                return new ByteArrayInputStream(attach.getData());
			        }), "Portrait of "+handle.getName());
				} else  {
					img = new Image("frontend/icons/char_upload.png","Dummy");
				}
				
				switch (i) {
				case 0: btnBig.setText(handle.getName()); btnBig.setIcon(img); img.setHeight("64px"); break;
				case 1: btnMedium1.setText(handle.getName()); btnMedium1.setIcon(img); break;
				case 2: btnMedium2.setText(handle.getName()); break;
				case 3: btnSmall1.setText(handle.getName()); break;
				case 4: btnSmall2.setText(handle.getName()); break;
				case 5: btnSmall3.setText(handle.getName()); break;
				case 6: btnSmall4.setText(handle.getName()); break;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//-------------------------------------------------------------------
	private void uploadClicked() {
		
		CharUploadDialog dialog = new CharUploadDialog();
		dialog.setOpened(true);
	}
	
}
