package de.rpgframework.eden.manage;

import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.componentfactory.Breadcrumb;
import com.vaadin.componentfactory.Breadcrumbs;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.FlexLayout.ContentAlignment;
import com.vaadin.flow.component.orderedlayout.FlexLayout.FlexWrap;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.shadowrun6.Shadowrun6Layout;
import de.rpgframework.eden.splittermond.SplittermondLayout;

/**
 * @author prelle
 *
 */
public class ContentTabComponent extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(ContentTabComponent.class);
	
	private Breadcrumbs breadcrumbs;
	private Image sectionImage;
	private Paragraph introLine1;
	private Paragraph introLine2;
	
	private Button btnSplittermond;
	private Button btnShadowrun6;

	//-------------------------------------------------------------------
	public ContentTabComponent() {
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		Locale loc = UI.getCurrent().getSession().getLocale();
		breadcrumbs = new Breadcrumbs();
		breadcrumbs.add(
			    new Breadcrumb(getTranslation("navigation.home"),"/", true),
		    new Breadcrumb(getTranslation("navigation.manage"), "manage"),
		    new Breadcrumb(getTranslation("navigation.manage"))
		    );
		add(breadcrumbs);

		introLine1 = new Paragraph(new Label(getTranslation("page.manage.tab.content.intro1")));
		introLine2 = new Paragraph(new Label(getTranslation("page.manage.tab.content.intro2")));
		sectionImage = new Image("/frontend/images/Placeholder.png", "Pile of rulebooks");
		
		Image logoSpliMo = new Image("frontend/images/splittermond/Logo.png", "Logo Splittermond");
		logoSpliMo.setWidth("4em");
		Image logoSR6    = new Image("frontend/images/shadowrun6/Schlange.png", "Logo Shadowrun 6");
		logoSR6.setWidth("4em");
		btnSplittermond = new Button(RoleplayingSystem.SPLITTERMOND.getName(loc), logoSpliMo);
		btnShadowrun6   = new Button(RoleplayingSystem.SHADOWRUN6.getName(loc), logoSR6);
		btnSplittermond.setHeight("5em");
		btnSplittermond.setWidth("300px");
		btnShadowrun6.setHeight("5em");
		btnShadowrun6.setWidth("300px");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		sectionImage.getStyle().set("max-width","32%");
		sectionImage.getStyle().set("width","32%");
		introLine1.getStyle().set("max-width","50%");
		introLine2.getStyle().set("max-width","50%");
		HorizontalLayout line1 = new HorizontalLayout(sectionImage, new VerticalLayout(introLine1, introLine2));
		line1.setSpacing(true);
		line1.setClassName("section-intro");

		add(line1);
		
		/* Available systesm */
		FlexLayout flex  = new FlexLayout(btnSplittermond, btnShadowrun6);
		flex.getStyle().set("gap", "10px");
		flex.setWidthFull();
		flex.setAlignContent(ContentAlignment.START);
		flex.setFlexWrap(FlexWrap.WRAP);
		add(flex);
		
		UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
			flex.setWidth( (details.getWindowInnerWidth()*0.95)+"px");
			if (details.getWindowInnerWidth()<800) {

			}
		});
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnSplittermond.addClickListener(ev -> {
			UI.getCurrent().navigate(ShopSplittermondPage.class);
		});
		
		btnShadowrun6.addClickListener(ev -> {
			UI.getCurrent().navigate(Shadowrun6Layout.class);
		});
	}

}
