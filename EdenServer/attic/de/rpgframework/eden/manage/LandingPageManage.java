package de.rpgframework.eden.manage;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.componentfactory.Breadcrumb;
import com.vaadin.componentfactory.Breadcrumbs;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.Route;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.MainLayout;

/**
 * @author prelle
 *
 */
@Route(value="manage", layout = MainLayout.class)
public class LandingPageManage extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(LandingPageManage.class);
	
	private Breadcrumbs breadcrumbs;
	private Div intro;
	
	private Tabs tabs;
	private Map<Tab, Component> tabsToPages;
	private Div pages;

	//-------------------------------------------------------------------
	public LandingPageManage() {
		initComponents();
		initLayout();
		initInteractivity();
		
		setWidthFull();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		Locale loc = UI.getCurrent().getSession().getLocale();
		breadcrumbs = new Breadcrumbs();
		breadcrumbs.add(
			    new Breadcrumb(getTranslation("navigation.home"),"/"),
		    new Breadcrumb(getTranslation("navigation.manage"))
		    );
		add(breadcrumbs);

		/* Intro graphic with centered image/text */
		Label heading = new Label(getTranslation("page.manage.heading"));
		intro = new Div(heading);
		intro.setWidthFull();
		intro.setClassName("landingpage-intro");

		
		tabsToPages = new HashMap<>();
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Div divPlayer = new Div();
		Div divGroups = new Div();
		Div divContent = new Div(new ContentTabComponent());
		Div divMedia = new Div();
		divPlayer.setVisible(false);
		divGroups.setVisible(false);
		divMedia.setVisible(false);
		
		pages = new Div(divPlayer, divGroups, divContent, divMedia);
		
		Tab tabPlayers  = new Tab(getTranslation("page.manage.tab.players"));
		Tab tabGroups   = new Tab(getTranslation("page.manage.tab.groups"));
		Tab tabContent  = new Tab(getTranslation("page.manage.tab.content"));
		Tab tabMedia    = new Tab(getTranslation("page.manage.tab.media"));
		tabs = new Tabs(tabPlayers, tabGroups, tabContent, tabMedia);
		tabsToPages.put(tabPlayers, divPlayer);
		tabsToPages.put( tabGroups, divGroups);
		tabsToPages.put(tabContent, divContent);
		tabsToPages.put(  tabMedia, divMedia);
		tabs.setSelectedTab(tabContent);
		tabs.getStyle().set("align", "center");

		add(intro, tabs, pages);
		
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		tabs.addSelectedChangeListener(event -> {
		    tabsToPages.values().forEach(page -> page.setVisible(false));
		    Component selectedPage = tabsToPages.get(tabs.getSelectedTab());
		    selectedPage.setVisible(true);
		});
	}

}
