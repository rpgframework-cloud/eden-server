package de.rpgframework.eden;

import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.RouterLink;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.shadowrun6.Shadowrun6Layout;
import de.rpgframework.eden.splittermond.SplittermondLayout;

/**
 * @author prelle
 *
 */
//@Route(value="info", layout = MainLayout.class)
public class SelectRoleplayingSystemPage extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(SelectRoleplayingSystemPage.class);
	
	private Button btnSplittermond;
	private Button btnShadowrun;
	
	//-------------------------------------------------------------------
	public SelectRoleplayingSystemPage() {
		initComponents();
		initLayout();
		initInteractivity();
		
		setWidthFull();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		Locale loc = UI.getCurrent().getSession().getLocale();
		
		Image logoSpliMo = new Image("frontend/images/splittermond/Logo.png", "Logo Splittermond");
		logoSpliMo.setWidth("4em");
		Image logoSR6    = new Image("frontend/images/shadowrun6/Schlange.png", "Logo Shadowrun 6");
		logoSR6.setWidth("4em");
		
		btnSplittermond = new Button(RoleplayingSystem.SPLITTERMOND.getName(loc), logoSpliMo);
		btnSplittermond.setHeight("5em");
		btnSplittermond.getStyle().set("margin", "1em");
		btnShadowrun    = new Button(RoleplayingSystem.SHADOWRUN6.getName(loc),logoSR6);
		btnShadowrun.setHeight("5em");
		btnShadowrun.getStyle().set("margin", "1em");
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		
		FormLayout flex = new FormLayout(btnSplittermond,btnShadowrun);
		flex.setWidthFull();
		add(flex);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnSplittermond.addClickListener(ev -> {
			UI.getCurrent().navigate(SplittermondLayout.class);
		});
		
		btnShadowrun.addClickListener(ev -> {
			UI.getCurrent().navigate(Shadowrun6Layout.class);
		});
	}

}
