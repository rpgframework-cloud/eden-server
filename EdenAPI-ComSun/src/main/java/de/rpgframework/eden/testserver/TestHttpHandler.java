package de.rpgframework.eden.testserver;

import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * @author prelle
 *
 */
@SuppressWarnings("exports")
public class TestHttpHandler implements HttpHandler {

	//-------------------------------------------------------------------
	/**
	 */
	public TestHttpHandler() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see com.sun.net.httpserver.HttpHandler#handle(com.sun.net.httpserver.HttpExchange)
	 */
	@Override
	public void handle(HttpExchange t) throws IOException {
		System.out.println("TestHttpHandler: "+t);
		t.getRequestBody();
        final String response = "OK";
        t.sendResponseHeaders(200, response.length());
        final OutputStream os = t.getResponseBody();
        os.write(response.getBytes());
        os.close();
	}

}
