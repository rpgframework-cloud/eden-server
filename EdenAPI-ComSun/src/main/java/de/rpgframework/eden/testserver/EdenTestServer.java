package de.rpgframework.eden.testserver;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.file.Path;
import java.util.function.Predicate;

import com.sun.net.httpserver.BasicAuthenticator;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpHandlers;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.Request;
import com.sun.net.httpserver.SimpleFileServer;
import com.sun.net.httpserver.SimpleFileServer.OutputLevel;
/**
 * @author prelle
 *
 */
public class EdenTestServer {

	private static final InetSocketAddress LOOPBACK_ADDR =  new InetSocketAddress(InetAddress.getLoopbackAddress(), 8080);
    static final Path CWD = Path.of(".").toAbsolutePath();
	//-------------------------------------------------------------------
	/**
	 */
	public EdenTestServer() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		var fileHandler = SimpleFileServer.createFileHandler(CWD);
        var deleteHandler = HttpHandlers.of(204, Headers.of(), "");
        Predicate<Request> IS_DELETE = r -> r.getRequestMethod().equals("DELETE");
        var handler = HttpHandlers.handleOrElse(IS_DELETE, deleteHandler, fileHandler);

        var outputFilter = SimpleFileServer.createOutputFilter(System.out, OutputLevel.VERBOSE);
        var server = HttpServer.create(LOOPBACK_ADDR, 10, "/", handler, outputFilter);

        createPlayerContext(server);
        server.start();
	}

	//-------------------------------------------------------------------
	private static void createPlayerContext(HttpServer server) {
		HttpContext context = server.createContext("/protected", new TestHttpHandler());
		context.setAuthenticator(new BasicAuthenticator("get") {
	        @Override
	        public boolean checkCredentials(String user, String pwd) {
	            return user.equals("admin") && pwd.equals("admin");
	        }
		});
	}

}
