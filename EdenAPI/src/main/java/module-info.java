module eden.api {
	exports de.rpgframework.eden.api;
	opens de.rpgframework.eden.api;

	requires de.rpgframework.core;
	requires com.fasterxml.jackson.databind;
}