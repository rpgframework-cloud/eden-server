package de.rpgframework.eden.api;

//-------------------------------------------------------------------
public class EdenPingInfo {
	private String mainSiteURL;
	private String accountCreationURL;
	//-------------------------------------------------------------------
	public String getMainSiteURL() {return mainSiteURL;}
	public String getAccountCreationURL() { return accountCreationURL;}
	//-------------------------------------------------------------------
	/**
	 * @param mainSiteURL the mainSiteURL to set
	 */
	public void setMainSiteURL(String mainSiteURL) {
		this.mainSiteURL = mainSiteURL;
	}
	//-------------------------------------------------------------------
	/**
	 * @param accountCreationURL the accountCreationURL to set
	 */
	public void setAccountCreationURL(String accountCreationURL) {
		this.accountCreationURL = accountCreationURL;
	}
}