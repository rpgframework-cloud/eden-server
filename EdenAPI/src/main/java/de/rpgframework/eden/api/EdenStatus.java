package de.rpgframework.eden.api;

/**
 * @author prelle
 *
 */
public enum EdenStatus {

	INTERNAL_ERROR(500),
	MAIL_ERROR(504),
	INSUFFICENT_DATA(400),
	UNAUTHORIZED(403),
	NO_SUCH_ITEM(404),
	NOT_ACCEPTABLE(406),
	ALREADY_EXISTS(409),

	OK(200),
	CREATED(202),
	;

	int code;

	EdenStatus(int code) {
		this.code = code;
	}

	public int code() {
		return code;
	}

	public static EdenStatus fromCode(int code) {
		for (EdenStatus tmp : EdenStatus.values()) {
			if (tmp.code() == code) {
				return tmp;
			}
		}
		return INTERNAL_ERROR;
	}
}
