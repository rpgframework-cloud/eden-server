package de.rpgframework.eden.api;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

import de.rpgframework.core.RoleplayingSystem;

//-------------------------------------------------------------------
public class EdenAccountInfo {
	private String login;
	private String firstName;
	private String lastName;
	private String email;
	private boolean verified;
	private String secret;
	private String lang;
	private Map<RoleplayingSystem, List<String>> modules;
	private String verificationCode;

	//-------------------------------------------------------------------
	public String dump() {
		return "login=" + login + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", verified=" + verified + ", secret=" + secret + ", lang=" + lang + ", modules=" + modules;
	}

	//-------------------------------------------------------------------
	public String getFirstName() {return firstName;}
	public String getLastName() { return lastName;}
	public String getEmail() { return email;}
	public String getLogin() { return login;}
	public String getSecret() { return secret;}
	public String getLanguage() { return lang;}
	@JsonIgnore
	public Locale getLocale() { return (lang!=null)?Locale.forLanguageTag(lang):Locale.getDefault();}
	public Map<RoleplayingSystem, List<String>> getLicensedModules() { return modules;}
	public String getVerificationCode() { return verificationCode;}
	public boolean isVerified() { return verified;}

	//-------------------------------------------------------------------
	public EdenAccountInfo setEmail(String data) { this.email = data; return this;}
	public EdenAccountInfo setLogin(String data) { this.login = data; return this;}
	public EdenAccountInfo setSecret(String data) { this.secret = data; return this;}
	public EdenAccountInfo setFirstName(String data) { this.firstName = data; return this;}
	public EdenAccountInfo setLastName(String data) { this.lastName = data; return this;}
	public EdenAccountInfo setLanguage(String data) { this.lang = data; return this;}
	@JsonIgnore
	public EdenAccountInfo setLocale(Locale data) { this.lang = data.getLanguage(); return this;}
	public EdenAccountInfo setModules(Map<RoleplayingSystem, List<String>> data) { this.modules = data; return this;}
	public EdenAccountInfo setVerificationCode(String data) { this.verificationCode = data; return this;}
	public EdenAccountInfo setVerified(boolean data) { this.verified = data; return this;}

}
