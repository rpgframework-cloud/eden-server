Startseite
  Charaktere
  

  Splittermond
  +-------------+  +------+
  |             |  |      |
  |             |  +------+
  +-------------+  |      |
                   +------+
                   
                   
Home
* Konto
  +-- Profil
  +-- Lizenzen

* Splittermond
  +-- Charaktere
  +-- Bibliothek
      +-- Bestiarium
      +-- Gegner & NPCs
      +-- Zauber
  
* Shadowrun 6
  +-- Charaktere
  +-- Matrixrecherche
      +-- Critter
      +-- Gegnerprofile
      +-- Zauber

      
* Vorbereiten
  Hier werden Abenteuer mit Szenen vorbereitet
  Liste der jüngsten Abenteuer - weitere über Suche
  
  Jedes Abenteuer ist eine Ansammlung von Szenen
  

Daten
======
Immer mit Anwendung ausliefern oder Aktualisierbar machen?



EDEN ist ein Dienst, der Dir Regelinhalte aus Rollenspielprodukten zum Nachschlagen und für andere Anwendungen zur Verfügung stellt.
So kannst Du z.B. Zauberbeschreibungen, Vor- und Nachteile, Stärken, Meisterschaften, Kräfte, u.ä. online oder in Begleit-Anwendungen nachlesen.
Inhaltspakete können im Shop bei den sich beteiligenden Verlagen erworben werden. 

Neben der reinen Nachschlagefunktion soll die Nutzungsmöglichkeit der Daten im Laufe der Zeit wachsen.
So ist z.B. eine Bereitstellung als Foundry VTT Kompendium vorgesehen, ebenso wie eine Bereitstellung in
Charaktergeneratoren.