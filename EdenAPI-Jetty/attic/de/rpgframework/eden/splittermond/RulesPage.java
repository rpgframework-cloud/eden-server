package de.rpgframework.eden.splittermond;

import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.componentfactory.Breadcrumb;
import com.vaadin.componentfactory.Breadcrumbs;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.FlexLayout.ContentAlignment;
import com.vaadin.flow.component.orderedlayout.FlexLayout.FlexWrap;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@Route(value="splittermond/rules", layout = SplittermondLayout.class)
@StyleSheet("frontend://styles/splittermond.css")
public class RulesPage extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(RulesPage.class);

	private FlexLayout buttons;
	private Button btnSpells;
	private Button btnMasterships;
	private Button btnGear;
	private Button btnStatuses;
	
	//-------------------------------------------------------------------
	public RulesPage() {
		initComponents();
		initLayout();
		initInteractivity();
		
		UI.getCurrent().getPage().setTitle(getTranslation("navigation.splittermond.rules"));
	}

	//-------------------------------------------------------------------
	public Button createButton(String imagePath, String key) {
		if (imagePath==null) {
			Button ret = new Button(getTranslation(key));
			ret.setHeight(ButtonDefinitions.HEIGHT_BUTTON);
			return ret;
		}
		Image image = new Image(imagePath, "");
		image.setWidth(ButtonDefinitions.HEIGHT_IMAGE);

		Button ret = new Button(getTranslation(key), image);
		ret.setHeight(ButtonDefinitions.HEIGHT_BUTTON);
		return ret;
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		btnGear        = createButton(ButtonDefinitions.PATH_GEAR       , ButtonDefinitions.KEY_GEAR);
		btnSpells      = createButton(ButtonDefinitions.PATH_SPELLS     , ButtonDefinitions.KEY_SPELLS);
		btnMasterships = createButton(ButtonDefinitions.PATH_MASTERSHIPS, ButtonDefinitions.KEY_MASTERSHIPS);
		btnStatuses    = createButton(ButtonDefinitions.PATH_STATUSES   , ButtonDefinitions.KEY_STATUS);
		btnGear       .setEnabled(false);
		btnSpells     .setEnabled(false);
		btnMasterships.setEnabled(false);
		btnStatuses   .setEnabled(false);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Locale loc = UI.getCurrent().getLocale();
		Breadcrumbs breadcrumbs = new Breadcrumbs();
		breadcrumbs.add(
		    new Breadcrumb(RoleplayingSystem.SPLITTERMOND.getName(loc),"splittermond"),
		    new Breadcrumb(getTranslation("navigation.splittermond.rules"))
		    );
		add(breadcrumbs);

		add(new H3(getTranslation("navigation.splittermond.rules")));
		
		buttons = new FlexLayout(btnSpells, btnMasterships, btnGear, btnStatuses);
		buttons.setAlignContent(ContentAlignment.SPACE_BETWEEN);
		buttons.setJustifyContentMode(JustifyContentMode.START);
		buttons.setFlexWrap(FlexWrap.WRAP);
		buttons.getChildren().forEach(comp -> ((Button)comp).getStyle().set("margin", "0.5em"));
		buttons.setWidth("80%");
		buttons.getStyle().set("margin-left", "10%");
//		buttons.setWidthFull();
		buttons.setFlexGrow(1, btnSpells, btnMasterships, btnGear, btnStatuses);
		add(buttons);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnSpells.addClickListener(
		        event -> {
		        	UI.getCurrent().navigate(BestiariumPage.class);
		        });
	}
}
