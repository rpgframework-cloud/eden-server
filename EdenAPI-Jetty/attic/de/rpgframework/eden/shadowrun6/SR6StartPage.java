package de.rpgframework.eden.shadowrun6;

import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.RouterLink;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.shadowrun6.Shadowrun6Layout;
import de.rpgframework.eden.splittermond.SplittermondLayout;

/**
 * @author prelle
 *
 */
//@Route(value="info", layout = MainLayout.class)
public class SR6StartPage extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(SR6StartPage.class);
	
	private Button btnMatrix;
	
	//-------------------------------------------------------------------
	public SR6StartPage() {
		initComponents();
		initLayout();
		initInteractivity();
		
		setWidthFull();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		Locale loc = UI.getCurrent().getSession().getLocale();
		
		btnMatrix = new Button(getTranslation("navigation.shadowrun6.library", loc), VaadinIcon.SEARCH.create());
		btnMatrix.setHeight("5em");
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		
		FormLayout flex = new FormLayout(btnMatrix);
		flex.setWidthFull();
		add(flex);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnMatrix.addClickListener(ev -> {
			UI.getCurrent().navigate(LibraryPage.class);
		});
	}

}
