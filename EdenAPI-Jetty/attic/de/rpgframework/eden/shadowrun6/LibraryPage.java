package de.rpgframework.eden.shadowrun6;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.creature.Creature;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;

/**
 * @author prelle
 *
 */
@Route(value="shadowrun6/library", layout = Shadowrun6Layout.class)
@StyleSheet("frontend://styles/shadowrun6.css")
public class LibraryPage extends FlexLayout {

	private static Logger logger = LogManager.getLogger(LibraryPage.class);

	private Button btnBeastiary;
	
	//-------------------------------------------------------------------
	public LibraryPage() {
		initComponents();
		initLayout();
		initInteractivity();
		
		UI.getCurrent().getPage().setTitle(getTranslation("shadowrun6.page.library"));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		btnBeastiary = new Button(getTranslation("shadowrun6.page.beastiary"));
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {		
		add(btnBeastiary);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
//		btnBeastiary.addClickListener(
//		        event -> {
//		        	UI.getCurrent().navigate(BestiariumPage.class);
//		        });
	}
}
