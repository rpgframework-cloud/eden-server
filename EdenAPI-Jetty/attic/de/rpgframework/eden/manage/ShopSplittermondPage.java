package de.rpgframework.eden.manage;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.appreciated.css.grid.GridLayoutComponent;
import com.github.appreciated.css.grid.sizes.Length;
import com.github.appreciated.css.grid.sizes.MinMax;
import com.github.appreciated.css.grid.sizes.Repeat.RepeatMode;
import com.github.appreciated.layout.FlexibleGridLayout;
import com.vaadin.componentfactory.Breadcrumb;
import com.vaadin.componentfactory.Breadcrumbs;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

import de.rpgframework.eden.gui.ProductCard;
import de.rpgframework.eden.splittermond.SplittermondLayout;
import de.rpgframework.reality.Product;
import de.rpgframework.reality.server.ProductImpl;

@Route(value="manage/content/splittermond", layout = SplittermondLayout.class)
public class ShopSplittermondPage extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(ShopSplittermondPage.class);

	private Breadcrumbs breadcrumbs;
	private Div intro;

	//-------------------------------------------------------------------
	public ShopSplittermondPage() {
		initComponents();
		initLayout();
		initInteractivity();
		
		setWidthFull();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		Locale loc = UI.getCurrent().getSession().getLocale();
		
		breadcrumbs = new Breadcrumbs();
		breadcrumbs.add(
			new Breadcrumb(getTranslation("navigation.home"),"/", true),
		    new Breadcrumb(getTranslation("navigation.manage"), "manage"),
		    new Breadcrumb(getTranslation("navigation.manage.content.splittermond"))
		    );
		add(breadcrumbs);

		/* Intro graphic with centered image/text */
		Label heading = new Label(getTranslation("page.manage.content.splittermond.heading"));
		intro = new Div(heading);
		intro.setWidthFull();
		intro.setClassName("landingpage-intro");
		intro.setClassName("intro-splittermond", true);
		add(intro);
		
		ProductImpl prod1 = new ProductImpl("GRW", "Kreaturen: Das Grundregelwerk");
		ProductImpl prod2 = new ProductImpl("BuU", "Kreaturen: Bestien und Ungeheuer");
		ProductImpl prod3 = new ProductImpl("Magie", "Kreaturen: Die Magie");
		prod1.setDescription("Die 31 verbreitesten Kreaturen von Lorakis");
		prod2.setDescription("92 Kreaturen aus ganz Lorakis");
		prod2.setPrice(399);
		prod3.setPrice( 99);
//		ProductCard card1 = new ProductCard(prod1);
//		ProductCard card2 = new ProductCard(prod2);
//		ProductCard card3 = new ProductCard(prod3);
		
//		card1.addClickListener(onClick -> cardClicked(card1));
		ProductCard[] cards = getCardsForProducts(prod1, prod2, prod3);

		
		FlexibleGridLayout layout = new FlexibleGridLayout()
				.withColumns(RepeatMode.AUTO_FILL, new MinMax(new Length("190px"), new Length("320px")))
				.withAutoRows(new Length("7.5em"))
				.withItems(cards)
				.withPadding(true)
				.withSpacing(true)
				.withAutoFlow(GridLayoutComponent.AutoFlow.ROW)
				.withOverflow(GridLayoutComponent.Overflow.AUTO);
		layout.setSizeFull();
		layout.
		setSizeFull();
		add(layout);

//		FlexLayout flex = new FlexLayout(card1, card2, card3);
//		flex.setWidthFull();
//		flex.getStyle().set("gap", "1em");
		
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		
		FormLayout flex = new FormLayout();
		flex.setWidthFull();
		add(flex);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
//		btnLibrary.addClickListener(ev -> {
//			UI.getCurrent().navigate(LibraryPage.class);
//		});
//		btnRules.addClickListener(ev -> {
//			UI.getCurrent().navigate(RulesPage.class);
//		});
	}
	
	//-------------------------------------------------------------------
	private ProductCard[] getCardsForProducts(Product...products) {
		List<ProductCard> ret = new ArrayList<ProductCard>();
		for (Product prod : products) {
			ProductCard card = new ProductCard(prod);
			card.addClickListener(onClick -> cardClicked(card));
			ret.add(card);
		}
		ProductCard[] ret2 = new ProductCard[ret.size()];
		ret2 = ret.toArray(ret2);
		return ret2;
	}

	//-------------------------------------------------------------------
	/**
	 * @param card
	 */
	private void cardClicked(ProductCard card) {
		logger.debug("Card clicked "+card.getProduct().getId());
		BuyProductDialog dialog = new BuyProductDialog(card.getProduct());
		dialog.open();
	}

}
