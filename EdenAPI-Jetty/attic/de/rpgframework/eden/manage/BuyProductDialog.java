package de.rpgframework.eden.manage;

import com.github.appreciated.card.label.PrimaryLabel;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.FlexLayout.ContentAlignment;
import com.vaadin.flow.component.orderedlayout.FlexLayout.FlexWrap;

import de.rpgframework.eden.splittermond.ButtonDefinitions;
import de.rpgframework.reality.Product;

/**
 * @author prelle
 *
 */
public class BuyProductDialog extends Dialog {
	
	private Product product;
	
	private PrimaryLabel title;
	private Label description;
	private Label price;
	private Button btnCancel;
	private Button btnBuy;

	//-------------------------------------------------------------------
	public BuyProductDialog(Product product) {
		this.product = product;
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		title = new PrimaryLabel(product.getTitle());
		description = new Label(product.getDescription());
		price = new Label("€ "+String.format("%.2f", product.getPrice()/100.0f));
		price.setClassName("text-large-price");
		price.setWidthFull();
		
		btnCancel = new Button(getTranslation("button.cancel"));
		btnBuy    = new Button(getTranslation("button.buy"));
		btnBuy.setAutofocus(true);
		btnBuy.setClassName("button-buy");
		btnBuy.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		btnCancel.setClassName("button-cancel");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		FlexLayout flex = new FlexLayout(btnCancel, btnBuy);
		flex.setAlignContent(ContentAlignment.SPACE_BETWEEN);
		flex.setFlexWrap(FlexWrap.NOWRAP);
		flex.setWidth("100%");
		flex.getStyle().set("gap", "1em");
		
		add(title, description, new Hr(), price, flex);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnCancel.addClickListener(onClick ->  close());
	}

}
