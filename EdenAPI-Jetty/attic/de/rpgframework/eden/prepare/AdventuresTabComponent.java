package de.rpgframework.eden.prepare;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

/**
 * @author prelle
 *
 */
public class AdventuresTabComponent extends VerticalLayout {

	private static Logger logger = LogManager.getLogger(AdventuresTabComponent.class);
	
	private Image sectionImage;
	private Paragraph sectionText;
	
	private ListBox listAdv;

	//-------------------------------------------------------------------
	public AdventuresTabComponent() {
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		sectionText = new Paragraph(new Label(getTranslation("page.prepare.tab.adventures.introtext")));
		sectionImage = new Image("/frontend/images/ToDo_Placeholder_Adventures.jpg", "Pile of Adventures");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		sectionImage.getStyle().set("max-width","32%");
		sectionImage.getStyle().set("width","32%");
		sectionText.getStyle().set("max-width","32%");
		HorizontalLayout line1 = new HorizontalLayout(sectionImage, sectionText);
		line1.setSpacing(true);
		line1.setClassName("section-intro");

		add(line1);
		UI.getCurrent().getPage().retrieveExtendedClientDetails(details -> {
			if (details.getWindowInnerWidth()<800) {

			}
		});
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

}
