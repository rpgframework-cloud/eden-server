package de.rpgframework.eden;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.jetty.security.AbstractLoginService;
import org.eclipse.jetty.security.RolePrincipal;
import org.eclipse.jetty.security.UserPrincipal;
import org.eclipse.jetty.server.UserIdentity;
import org.eclipse.jetty.util.security.Password;

import de.rpgframework.eden.api.servlet.Roles;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author spr
 *
 */
public class SQLBasedLoginService extends AbstractLoginService {

	private final static Logger logger = System.getLogger("login.file");

	private Map<String, UserIdentity> cache;

	//-----------------------------------------------------------------
	public SQLBasedLoginService(Properties pro) {
		super.setFullValidate(false);
		cache = new HashMap<>();
	}

	//-----------------------------------------------------------------
	/**
	 * @see org.eclipse.jetty.security.AbstractLoginService#login(java.lang.String, java.lang.Object, javax.servlet.ServletRequest)
	 */
//	@Override
//	public UserIdentity login(String username, Object credentials, ServletRequest request) {
//		logger.log(Level.INFO, "login '"+username+"'");
//		UserIdentity ident = cache.get(username);
//		if (ident==null) {
//			ident = super.login(username, credentials, request);
//			logger.log(Level.DEBUG, "result = "+ident);
//			if (ident!=null)
//				cache.put(username, ident);
//		}
//
//		if (ident!=null) {
//			HttpSession session = ((HttpServletRequest) request).getSession(true);
//			if (session != null) {
//				//	            SessionAuthentication sessionAuth = new SessionAuthentication(getAuthMethod(), user, credentials);
//				SessionAuthentication sessionAuth = new SessionAuthentication("Basic", ident, credentials);
//				session.setAttribute(SessionAuthentication.__J_AUTHENTICATED, sessionAuth);
//			}
//		}
//		return ident;
//	}


	//-------------------------------------------------------------------
	/**
	 * @see org.eclipse.jetty.security.AbstractLoginService#loadRoleInfo(org.eclipse.jetty.security.AbstractLoginService.UserPrincipal)
	 */
	@Override
	protected List<RolePrincipal> loadRoleInfo(UserPrincipal user) {
		logger.log(Level.DEBUG, "...loadRoleInfo("+user+")...");
//		try {
//			UranosUser dbUser = GraphDatabase.getInstance().get(UranosUser.class, user.getName());
//			if (dbUser==null) {
//				return new String[0];
////				return new String[] {"ADMIN","SUPPORT"};
//			} else {
//				return dbUser.getRoles().split(",");
//			}
//		} catch (DatabaseException e) {
//			logger.log(Level.ERROR, "Failed getting user from database",e);
//			return new String[0];
//		}
		return List.of (new RolePrincipal(Roles.VALID_USER.name()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.eclipse.jetty.security.AbstractLoginService#loadUserInfo(java.lang.String)
	 */
	@Override
	protected UserPrincipal loadUserInfo(String userName) {
		logger.log(Level.DEBUG, "...loadUserInfo("+userName+")...");

		try {
			PlayerImpl dbUser = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(userName);
			UserPrincipal userPrincipal = null;
			if (dbUser==null) {
				logger.log(Level.WARNING, "Unknown user: "+userName);
				return null;
//				userPrincipal = new QSCUserPrincipal(userName, "?", new String[] {"ADMIN","SUPPORT"});
			} else {
				userPrincipal = new UserPrincipal(
						userName,
						new Password(dbUser.getPassword())
						);
				logger.log(Level.INFO, "Loaded user "+userPrincipal+" with PW "+dbUser.getPassword());
			}
			return userPrincipal;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed getting user from database",e);
			return null;
		}
	}

}
