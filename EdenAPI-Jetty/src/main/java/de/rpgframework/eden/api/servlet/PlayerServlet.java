package de.rpgframework.eden.api.servlet;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.api.EdenStatus;
import de.rpgframework.eden.logic.AccountLogic;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.eden.logic.LogicResult;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class PlayerServlet extends HttpServlet {

    private final static Logger logger = System.getLogger("eden.api");

	public final static String PREFIX = "/account";

    private final static Pattern GENERIC = Pattern.compile(".*"+PREFIX);
    private final static Pattern SPECIFIC = Pattern.compile(".*"+PREFIX+"/(.*)");

    private Gson gson;
    private BackendAccess backend;

    //-------------------------------------------------------------------
    /**
     * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
     */
    public void init(ServletConfig config) throws ServletException {
    	super.init(config);
    	logger.log(Level.WARNING, "Init called");
    	gson = new GsonBuilder().setPrettyPrinting().create();

    	for (Enumeration<String> e= config.getInitParameterNames(); e.hasMoreElements(); ) {
    		logger.log(Level.DEBUG, "  init parameter name = "+e.nextElement());
    	}

    	logger.log(Level.DEBUG, "Servlet context = "+config.getServletContext());
    	for (Enumeration<String> e= config.getServletContext().getAttributeNames(); e.hasMoreElements(); ) {
    		logger.log(Level.DEBUG, "  attribute name = "+e.nextElement());
    	}

    	backend = (BackendAccess) config.getServletContext().getAttribute(Constants.ATTRIB_BACKEND);
    }

    //-------------------------------------------------------------------
    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
            logger.log(Level.DEBUG, "----------doGet "+request.getRequestURL()+"--------------------");
            logger.log(Level.DEBUG, "User = "+request.getUserPrincipal());
            response.setContentType("text/html; charset=UTF-8");

            String uri = request.getRequestURI();

            try {

			Matcher matcher1 = SPECIFIC.matcher(URLDecoder.decode(uri, "UTF-8"));
			Matcher matcher2 = GENERIC.matcher(URLDecoder.decode(uri, "UTF-8"));
			if (matcher1.matches()) {
				logger.log(Level.DEBUG, "Use SPECIFIC");
				String what = matcher1.group(1);
				logger.log(Level.DEBUG, "doGet {0}", what);
				if ("verify".equals(what)) {
					sendVerificationCode(request, response);
				}
				//serveGetAttachment(response, user, player, matcher1);
			} else if (matcher2.matches()) {
				getAccountInfo(request, response);
                return;
			} else{
            	logger.log(Level.DEBUG, "URI does not match pattern: "+uri+"   pattern="+SPECIFIC);
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
			} catch (Exception e) {
				logger.log(Level.ERROR, "Failed accessing player",e);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
			}
    }

	//-------------------------------------------------------------------
   private void getAccountInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	// Determine logged in player
        Player player = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
        if (player==null) {
           	logger.log(Level.DEBUG, "No such player {0}", request.getUserPrincipal().getName());
            response.sendError(404, "No such player");
            return;
        	}
       	logger.log(Level.DEBUG, "getAccountInfo() by {0} / {1}", player.getLogin(), player.getEmail());
       	logger.log(Level.DEBUG, "  verified = {0}", player.isVerified());

 		EdenAccountInfo pdu = new EdenAccountInfo();
 		pdu.setLogin(player.getLogin());
		pdu.setEmail(player.getEmail());
		pdu.setFirstName(player.getFirstName());
		pdu.setLastName(player.getLastName());
		pdu.setLocale( ((PlayerImpl)player).getLocale());
		pdu.setModules(new HashMap<>());
		pdu.setVerified(player.isVerified());
		Map<RoleplayingSystem, List<String>> licensed = new HashMap<RoleplayingSystem, List<String>>();
		licensed.put(RoleplayingSystem.SPLITTERMOND, Arrays.asList("CORE","JdG"));
		licensed.put(RoleplayingSystem.SHADOWRUN6, Arrays.asList("CORE"));
		pdu.setModules(licensed);
		logger.log(Level.INFO, "Write "+gson.toJson(pdu));
		response.getWriter().println(gson.toJson(pdu));
        response.setStatus(HttpServletResponse.SC_OK);
    }

	//-------------------------------------------------------------------
	private void sendVerificationCode(HttpServletRequest request, HttpServletResponse response) {
		logger.log(Level.DEBUG, "ENTER sendVerificationCode");

		PlayerImpl player = null;

		try {
			player = backend.getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
			if (player==null) {
				logger.log(Level.ERROR, "Valid player '"+request.getUserPrincipal().getName()+"' but did not find associated Player object");
				response.setHeader("Reason", "Did not find player data");
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return;
			}

			LogicResult result = AccountLogic.sendVerificationCode(player);
			if (result.status!=EdenStatus.OK) {
				response.setHeader("Reason", result.message);
	            response.setStatus(result.status.code());
	            return;
			}
		} catch (SQLException e) {
			response.setHeader("Reason", "Error searching for player data");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		} finally {
			logger.log(Level.DEBUG, "LEAVE sendVerificationCode");
		}
	}

    //-------------------------------------------------------------------
    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
            logger.log(Level.ERROR, "----------doPut "+request.getRequestURL()+"--------------------");
            logger.log(Level.DEBUG, "User = "+request.getUserPrincipal());
            response.setContentType("text/html; charset=UTF-8");

            PlayerImpl player = null;
            try {
				player = backend.getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
				if (player==null) {
					logger.log(Level.ERROR, "Valid player '"+request.getUserPrincipal().getName()+"' but did not find associated Player object");
					response.setHeader("Reason", "Did not find player data");
					response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
					return;
				}
			} catch (SQLException e) {
				response.setHeader("Reason", "Database error: "+e);
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
 			}

			String uri = request.getRequestURI();

            try {

			Matcher matcher1 = SPECIFIC.matcher(URLDecoder.decode(uri, "UTF-8"));
			Matcher matcher2 = GENERIC.matcher(URLDecoder.decode(uri, "UTF-8"));
			if (matcher1.matches()) {
				logger.log(Level.DEBUG, "Use SPECIFIC");
				String what = matcher1.group(1);
				logger.log(Level.DEBUG, "doPut {0}", what);
				if ("verify".equals(what)) {
					processVerificationCode(request, response, player);
				}
				//serveGetAttachment(response, user, player, matcher1);
			} else{
            	logger.log(Level.DEBUG, "URI does not match pattern: "+uri+"   pattern="+SPECIFIC);
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
			} catch (Exception e) {
				logger.log(Level.ERROR, "Failed accessing player",e);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
			}
    }

	//-------------------------------------------------------------------
	private void processVerificationCode(HttpServletRequest request, HttpServletResponse response, PlayerImpl player) {
		logger.log(Level.DEBUG, "ENTER processVerificationCode");

		try {
			byte[] data = request.getInputStream().readAllBytes();
			String json = new String(data, request.getCharacterEncoding());
			EdenAccountInfo info = gson.fromJson(json, EdenAccountInfo.class);

			String sentCode = info.getVerificationCode();
			LogicResult result = AccountLogic.verifyAccount(player, sentCode);
			if (result.status!=EdenStatus.OK) {
				response.setHeader("Reason", result.message);
	            response.setStatus(result.status.code());
	            return;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			logger.log(Level.DEBUG, "LEAVE processVerificationCode");
		}
	}

}
