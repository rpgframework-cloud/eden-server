package de.rpgframework.eden.api.jetty;

import static org.eclipse.jetty.security.authentication.FormAuthenticator.__J_METHOD;
import static org.eclipse.jetty.security.authentication.FormAuthenticator.__J_POST;
import static org.eclipse.jetty.security.authentication.FormAuthenticator.__J_URI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.SQLException;
import java.util.Properties;

import org.eclipse.jetty.rewrite.handler.RewriteHandler;
import org.eclipse.jetty.security.Authenticator;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.security.ServerAuthException;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.security.authentication.DeferredAuthentication;
import org.eclipse.jetty.security.authentication.SessionAuthentication;
import org.eclipse.jetty.server.Authentication;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.MultiMap;
import org.eclipse.jetty.util.component.LifeCycle;
import org.eclipse.jetty.util.security.Constraint;

import de.rpgframework.eden.SQLBasedLoginService;
import de.rpgframework.eden.api.servlet.EdenBackend;
import de.rpgframework.eden.api.servlet.InfoPingServlet;
import de.rpgframework.eden.api.servlet.Roles;
import de.rpgframework.eden.base.JavaMailMailer;
import de.rpgframework.eden.base.MailerLoader;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.reality.server.EdenAPIServer;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

/**
 * @author prelle
 *
 */
public class JettyMain implements EdenAPIServer {

	public final static String PROP_CONFIG_FILE = "eden.cfgfile";
	public final static Logger logger = System.getLogger("eden");
	private final static int DEFAULT_BACKEND_PORT = 0;

	private static BackendAccess backend;
	private int port;
	private Server server;
	private SQLBasedLoginService loginService;

    /**
     * Application main entry point.
     * @param args command line arguments.
     * @throws SQLException
     */
    public static void main(final String[] args) throws SQLException {
		String confPath = System.getProperty(PROP_CONFIG_FILE, System.getProperty("user.home")+File.separatorChar+"eden.cfg");
		logger.log(Level.INFO,"Load configuration from "+confPath);
		Properties config = new Properties();
		try {
			config.load(new FileReader(confPath));
		} catch (FileNotFoundException e) {
			logger.log(Level.ERROR,"Missing configuration file: "+confPath+" or system property '"+PROP_CONFIG_FILE+"'");
			System.err.println("Missing configuration file: "+confPath+" or system property '"+PROP_CONFIG_FILE+"'");
			System.exit(1);
		} catch (Exception e) {
			logger.log(Level.ERROR,"Error loading file: "+confPath+" or system property '"+PROP_CONFIG_FILE+"'");
			System.err.println("Error loading file: "+confPath+" or system property '"+PROP_CONFIG_FILE+"'");
			System.exit(1);
		}

		new BackendAccess(config);

		JettyMain server = new JettyMain();
		server.configure(config);

		try {
			server.start(config);
			System.out.println("Started API server on port "+server.getPort());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
    }

	//-------------------------------------------------------------------
	public JettyMain() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.server.EdenAPIServer#configure(java.util.Properties)
	 */
	@Override
	public void configure(Properties config) {
		logger.log(Level.TRACE, "ENTER configure");
		try {
			MailerLoader.setInstance(new JavaMailMailer(config));
			loginService = new SQLBasedLoginService(config);

			port = Integer.parseInt(config.getProperty("eden.api.port", "0"));
		} finally {
			logger.log(Level.TRACE, "LEAVE configure");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.server.EdenAPIServer#getPort()
	 */
	@Override
	public int getPort() {
		return port;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.server.EdenAPIServer#start(java.util.Properties)
	 */
	@Override
	public void start(Properties config) throws Exception {
    	logger.log(Level.TRACE, "ENTER start()");
    	try {
    		backend = new BackendAccess(config);
    		BackendAccess.setInstance(backend);

    		server = new Server();

    		ServerConnector portBackend=new ServerConnector(server);
    		portBackend.setName("Backend");
    		portBackend.setPort(port);

    		server.setConnectors(new Connector[]{portBackend});

    		RewriteHandler rewrite = new RewriteHandler();
//    		RedirectPatternRule redirect = new RedirectPatternRule();
//    		redirect.setPattern("");
//    		redirect.setLocation("/extern");
//    		rewrite.addRule(redirect);

    		HandlerCollection contexts = new HandlerCollection();
    		contexts.addHandler(rewrite);
    		contexts.addHandler(getAuthorizedAPIContext());
    		contexts.addHandler(getAnonymousAPIContext());
    		server.setHandler(contexts);


    		server.addEventListener(new LifeCycle.Listener() {
    	        public void lifeCycleStarted(LifeCycle event)
    	        {
    	        	logger.log(Level.INFO, "API server is running on port "+portBackend.getLocalPort());
    	        	port = portBackend.getLocalPort();
    	        }
			});
    		server.start();
    	} catch (Exception e) {
    		logger.log(Level.ERROR, "Failed starting Webserver",e);
    		throw e;
    	} finally {
        	logger.log(Level.TRACE, "LEAVE start()");
    	}

	}

	//-----------------------------------------------------------------
	private ServletContextHandler getAnonymousAPIContext() {
		ServletContextHandler servContext = new ServletContextHandler(ServletContextHandler.SESSIONS | ServletContextHandler.SECURITY);
		servContext.setInitParameter("cacheControl", "max-age=120, public");
		servContext.setContextPath("/anon");
		servContext.getServletContext().setAttribute(Constants.ATTRIB_BACKEND, backend);

		for (EdenBackend.ServletWithMapping mapping : EdenBackend.getAnonServlets()) {
			logger.log(Level.DEBUG, "Register servlet {0} for {1}", mapping.servlet.getSimpleName(), servContext.getContextPath()+mapping.path);
			servContext.addServlet(mapping.servlet, mapping.path);
		}
		servContext.setVirtualHosts(new String[] {"@Backend"});
		return servContext;
	}

	//-----------------------------------------------------------------
	private ServletContextHandler getAuthorizedAPIContext() {
		ServletContextHandler servContext = new ServletContextHandler(ServletContextHandler.SESSIONS | ServletContextHandler.SECURITY);
		servContext.setInitParameter("cacheControl", "max-age=120, public");
		servContext.setContextPath("/api");
		servContext.getServletContext().setAttribute(Constants.ATTRIB_BACKEND, backend);

		for (EdenBackend.ServletWithMapping mapping : EdenBackend.getServlets()) {
			logger.log(Level.DEBUG, "Register servlet {0} for {1}", mapping.servlet.getSimpleName(), servContext.getContextPath()+mapping.path);
			servContext.addServlet(mapping.servlet, mapping.path);
		}

		servContext.setVirtualHosts(new String[] {"@Backend"});
		servContext.setSecurityHandler(
				createSecurityHandler(
						"ConstraintName",
						new String[] {Roles.VALID_USER.name()},
						getAuthenticator(),
						"Realmchen",
						loginService));
		return servContext;
	}

	//-------------------------------------------------------------------
	private Authenticator getAuthenticator() {
		return new BasicAuthenticator() {
			public Authentication validateRequest(ServletRequest req, ServletResponse res, boolean mandatory) throws ServerAuthException {
				HttpServletRequest request = (HttpServletRequest)req;
				if (!mandatory)
					return new DeferredAuthentication(this);

				HttpSession session = request.getSession(true);

				// Look for cached authentication
				Authentication authentication = (Authentication) session.getAttribute(SessionAuthentication.__J_AUTHENTICATED);
				if (authentication != null) {
					// Has authentication been revoked?
					if (authentication instanceof Authentication.User &&
							_loginService!=null &&
							!_loginService.validate(((Authentication.User)authentication).getUserIdentity()))
					{
						logger.log(Level.DEBUG, "auth revoked {}",authentication);
						session.removeAttribute(SessionAuthentication.__J_AUTHENTICATED);
					}
					else
					{
						synchronized (session)
						{
							String j_uri=(String)session.getAttribute(__J_URI);
							if (j_uri!=null)
							{
								//check if the request is for the same url as the original and restore
								//params if it was a post
								logger.log(Level.DEBUG, "auth retry {}->{}",authentication,j_uri);
								StringBuffer buf = request.getRequestURL();
								if (request.getQueryString() != null)
									buf.append("?").append(request.getQueryString());

								if (j_uri.equals(buf.toString()))
								{
									MultiMap<String> j_post = (MultiMap<String>)session.getAttribute(__J_POST);
									if (j_post!=null)
									{
										logger.log(Level.DEBUG, "auth rePOST {}->{}",authentication,j_uri);
										Request base_request = Request.getBaseRequest(request);
										base_request.setContentParameters(j_post);
									}
									session.removeAttribute(__J_URI);
									session.removeAttribute(__J_METHOD);
									session.removeAttribute(__J_POST);
								}
							}
						}
						logger.log(Level.TRACE, "auth {}",authentication);
						return authentication;
					}
				}
				return super.validateRequest(req, res, mandatory);
			}
		};
	}

	//-------------------------------------------------------------------
	protected ConstraintSecurityHandler createSecurityHandler(String constraintName,
			String[] allowedRoles, Authenticator authenticator, String realm,
			LoginService loginService) {

		Constraint constraint = new Constraint();
		constraint.setName(constraintName);
		constraint.setRoles(allowedRoles);
		// This is telling Jetty to not allow unauthenticated requests through (very important!)
		constraint.setAuthenticate(true);

		ConstraintMapping cm = new ConstraintMapping();
		cm.setConstraint(constraint);
		cm.setPathSpec("/*");

		ConstraintSecurityHandler sh = new ConstraintSecurityHandler();
		sh.setAuthenticator(authenticator);
		sh.setLoginService(loginService);
		sh.setConstraintMappings(new ConstraintMapping[]{cm});
		sh.setRealmName(realm);

		return sh;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.server.EdenAPIServer#stop()
	 */
	@Override
	public void stop() throws Exception {
		server.stop();
	}

}
