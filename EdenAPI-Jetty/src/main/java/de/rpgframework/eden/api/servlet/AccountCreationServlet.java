package de.rpgframework.eden.api.servlet;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.charset.Charset;
import java.security.Principal;
import java.util.Enumeration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.logic.AccountLogic;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.eden.logic.LogicResult;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class AccountCreationServlet extends HttpServlet {

	private final static Logger logger = System.getLogger("eden.api");

	private Gson gson;
	private BackendAccess backend;

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		logger.log(Level.WARNING, "Init called");
		gson = new GsonBuilder().setPrettyPrinting().create();

		for (Enumeration<String> e= config.getInitParameterNames(); e.hasMoreElements(); ) {
			logger.log(Level.DEBUG, "  init parameter name = "+e.nextElement());
		}

		logger.log(Level.DEBUG, "Servlet context = "+config.getServletContext());
		for (Enumeration<String> e= config.getServletContext().getAttributeNames(); e.hasMoreElements(); ) {
			logger.log(Level.DEBUG, "  attribute name = "+e.nextElement());
		}

		backend = (BackendAccess) config.getServletContext().getAttribute(Constants.ATTRIB_BACKEND);
		logger.log(Level.INFO, "Backend is "+backend);
	}

	//-------------------------------------------------------------------
	/**
	 * @see jakarta.servlet.http.HttpServlet#doPost(jakarta.servlet.http.HttpServletRequest, jakarta.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.INFO, "----------doPost "+request.getRequestURL()+"--------------------");
		response.setContentType("text/html; charset=UTF-8");

		Principal user = request.getUserPrincipal();
		logger.log(Level.DEBUG, "user = "+user);
			byte[] raw = request.getInputStream().readAllBytes();
			String json = new String(raw, Charset.defaultCharset());

			EdenAccountInfo data = gson.fromJson(json, EdenAccountInfo.class);
			LogicResult result = AccountLogic.createAccount(data);
    		if (result.successObject!=null) {
				response.getWriter().println(gson.toJson(result.successObject));
				response.setStatus(result.status.code());
    		} else {
    			response.setHeader("Reason", result.message);
				response.setStatus(result.status.code());
    		}
	}

}
