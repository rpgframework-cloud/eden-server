package de.rpgframework.eden.api.servlet;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.URLDecoder;
import java.security.Principal;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.security.auth.Subject;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.logic.ActivationKeyDatabase;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.BoughtDatabase;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.eden.logic.ShopDatabase;
import de.rpgframework.reality.ActivationKey;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.CatalogItem;
import de.rpgframework.reality.CatalogItem.Category;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * <pre>
 * GET /api/license		- Get all bought items
 * 	                      Parameter: rules - Roleplaying System
 *                                   category - FVTT, CHARGEN, ...

 * POST /api/license	- Add a new bought item
 * 						  Parameter: activationKey = Code to consume
 * </pre>
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class ShopServlet extends HttpServlet {

	public final static String PREFIX = "/license";

    private final static Logger logger = System.getLogger("eden.api");

    private final static Pattern SPECIFIC = Pattern.compile(PREFIX+"/(.*)");
	private final static Pattern WITHOUT_UUID = Pattern.compile(PREFIX+"/*");
    private final static Pattern LICENSE_UNSPECIFIC = Pattern.compile(PREFIX+"/*");

    private Gson gson;
    private BackendAccess backend;

    //-------------------------------------------------------------------
    /**
     * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
     */
    public void init(ServletConfig config) throws ServletException {
    	super.init(config);
    	logger.log(Level.WARNING, "Init called");
    	gson = new GsonBuilder().setPrettyPrinting().create();

    	for (Enumeration<String> e= config.getInitParameterNames(); e.hasMoreElements(); ) {
    		logger.log(Level.DEBUG, "  init parameter name = "+e.nextElement());
    	}

    	logger.log(Level.DEBUG, "Servlet context = "+config.getServletContext());
    	for (Enumeration<String> e= config.getServletContext().getAttributeNames(); e.hasMoreElements(); ) {
    		logger.log(Level.DEBUG, "  attribute name = "+e.nextElement());
    	}

    	backend = (BackendAccess) config.getServletContext().getAttribute(Constants.ATTRIB_BACKEND);
    }

    //-------------------------------------------------------------------
    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
            logger.log(Level.DEBUG, "----------doGet "+request.getRequestURL()+"--------------------");
            logger.log(Level.DEBUG, "User = "+request.getUserPrincipal());
            response.setContentType("text/html; charset=UTF-8");

            Principal user = request.getUserPrincipal();
            String uri = request.getRequestURI();

    		PlayerImpl player = null;
    		try {
    			player = backend.getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
    			if (player==null) {
    				logger.log(Level.ERROR, "Valid player '"+user.getName()+"' but did not find associated Player object");
    				response.setHeader("X-Reason", "Valid player '"+user.getName()+"' but did not find associated Player object");
    				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    				return;
    			}
    		} catch (SQLException e) {
				logger.log(Level.ERROR, "Failed accessing SQL database: "+e);
				response.setHeader("X-Reason", "Failed accessing database");
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return;
    		}
    		logger.log(Level.DEBUG, "Player "+player);
    		logger.log(Level.DEBUG, "Params "+request.getParameterMap());


			Matcher licenseUnspecificMatcher = LICENSE_UNSPECIFIC.matcher(URLDecoder.decode(uri, "UTF-8"));
			if (licenseUnspecificMatcher.matches()) {
				getAllLicences(request, response, player);
				return;
			}
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
    }

	//-------------------------------------------------------------------
	private void getAllLicences(HttpServletRequest request, HttpServletResponse response, PlayerImpl player) {
		logger.log(Level.INFO, "BEGIN: getAllLicences() for User "+request.getUserPrincipal());
		try {
			Principal principal = request.getUserPrincipal();
			logger.log(Level.INFO, "prince = "+principal.getClass());
			logger.log(Level.INFO, "prince = "+principal.implies(new Subject()));

			BoughtDatabase licenseDB = backend.getLicenseDatabase();
			List<BoughtItem> list = licenseDB.getBoughtItems(player);
			logger.log(Level.INFO, "Got unfiltered: " + list);
			ShopDatabase items = backend.getShopDatabase();
			// Apply rules filter
			if (request.getParameter("rules") != null) {
				RoleplayingSystem rules = RoleplayingSystem.valueOf(request.getParameter("rules"));
				list = list.stream().filter(item -> {
					try {
						return (items.getCatalogItem(item.getItemID()) != null)
								&& items.getCatalogItem(item.getItemID()).getRules() == rules;
					} catch (SQLException e) {
						logger.log(Level.ERROR, "Error filtering by rules", e);
						return false;
					}
				}).collect(Collectors.toList());
			}
			// Apply language filter
			if (request.getParameter("lang") != null) {
				String lang = request.getParameter("lang");
				list = list.stream().filter(item -> {
					try {
						return (items.getCatalogItem(item.getItemID()) != null)
								&& items.getCatalogItem(item.getItemID()).getLanguage().equals(lang);
					} catch (SQLException e) {
						logger.log(Level.ERROR, "Error filtering by rules", e);
						return false;
					}
				}).collect(Collectors.toList());
			}
			// Apply filter
			if (request.getParameter("category") != null) {
				Category category = Category.valueOf(request.getParameter("category"));
				list = list.stream().filter(item -> {
					try {
						return (items.getCatalogItem(item.getItemID()) != null)
								&& items.getCatalogItem(item.getItemID()).getCategory() == category;
					} catch (SQLException e) {
						logger.log(Level.ERROR, "Error filtering by rules", e);
						return false;
					}
				}).collect(Collectors.toList());
			}
			logger.log(Level.INFO, "Got filtered: " + list);

			if (request.getParameter("plugins")!=null && Boolean.valueOf(request.getParameter("plugins"))) {
				List<String> plugins = new ArrayList<>();
				for (BoughtItem item : list) {
					CatalogItem cItem = items.getCatalogItem(item.getItemID());
					for (String plugin : cItem.getDatasets()) {
						if (!plugins.contains(plugin)) {
							plugins.add(plugin);
						}
					}
				}
				logger.log(Level.INFO, "Return as plugins " + plugins);
				response.getWriter().println(gson.toJson(plugins));
				response.setStatus(HttpServletResponse.SC_OK);
			} else {
				response.getWriter().println(gson.toJson(list));
				response.setStatus(HttpServletResponse.SC_OK);
			}
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed getting licenses",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} finally {
			logger.log(Level.INFO, "END  : getAllLicences() for User "+request.getUserPrincipal());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.DEBUG, "----------doPost "+request.getRequestURL()+"--------------------");
		logger.log(Level.DEBUG, "User = "+request.getUserPrincipal());
		response.setContentType("text/html; charset=UTF-8");

		Principal user = request.getUserPrincipal();
		if (user==null) {
			logger.log(Level.DEBUG, "PUT request for attachment, but no user object found");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.setHeader("Reason", "User object not found");
			return;
		}
		PlayerImpl player = null;
		try {
			player = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Failed accessing player",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}

		String uri = request.getRequestURI();
		Matcher matcher = WITHOUT_UUID.matcher(URLDecoder.decode(uri, "UTF-8"));
		if (!matcher.matches()) {
			logger.log(Level.DEBUG, "User "+user+": URI does not match pattern: "+uri+"   pattern="+WITHOUT_UUID);
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		if (logger.isLoggable(Level.TRACE))
			logger.log(Level.TRACE, "Params = "+request.getParameterMap());

		if (request.getParameter("activationKey")!=null) {
			consumeActivationKey(request, response, player, UUID.fromString(request.getParameter("activationKey")));
			return;
		}
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
	}

	//-------------------------------------------------------------------
	private void consumeActivationKey(HttpServletRequest request, HttpServletResponse response, PlayerImpl player, UUID key) {
		logger.log(Level.INFO, "BEGIN: consumeActivationKey("+key+") for User "+request.getUserPrincipal());
		try {
			Principal principal = request.getUserPrincipal();

			// Check existence of activation key
			ActivationKeyDatabase actKeys = backend.getActivationKeyDatabase();
			ActivationKey toConsume = actKeys.get(key);
			if (toConsume==null) {
				logger.log(Level.ERROR, "User "+principal.getName()+" tries to use invalid key "+key);
				response.setHeader("X-Reason", "Unknown key");
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				return;
			}

			// Make sure the key points to a valid catalog item
			ShopDatabase catalog = backend.getShopDatabase();
			CatalogItem item = catalog.getCatalogItem(toConsume.getItemID());
			if (item==null) {
				logger.log(Level.ERROR, "User "+principal.getName()+" tries to use key "+key+" that points to unknown item '"+toConsume.getItemID()+"'");
				response.setHeader("X-Reason", "Outdated key - no such item");
				response.setStatus(HttpServletResponse.SC_CONFLICT);
				return;
			}

			// Make sure that the player did not already buy that item
			BoughtDatabase licenseDB = backend.getLicenseDatabase();
			for (BoughtItem tmp : licenseDB.getBoughtItems(player)) {
				if (tmp.getItemID().equals(item.getId())) {
					logger.log(Level.WARNING, "Player "+player.getUuid()+" is trying to buy item (s)he already owns: "+tmp.getItemID());
					response.setHeader("X-Reason", "Already owned");
					response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
					return;
				}

			}


			BoughtItem bought = new BoughtItem(player.getUuid(), item.getId(), Instant.now(), 0);
			bought.setLicenseKey(key);
			// Mark item as bought
			licenseDB.add(bought);
			// Consume key
			actKeys.remove(toConsume);
			logger.log(Level.INFO, "Player "+player.getUuid()+" ("+player.getFirstName()+" "+player.getLastName()+") activated license "+key+" ("+item.getName()+")");


			response.getWriter().println(gson.toJson(bought));
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed consuming activation key",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} finally {
			logger.log(Level.INFO, "END  : consumeActivationKey() for User "+request.getUserPrincipal());
		}
	}

}
