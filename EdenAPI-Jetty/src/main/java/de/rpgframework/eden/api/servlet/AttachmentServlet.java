package de.rpgframework.eden.api.servlet;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.URLDecoder;
import java.security.Principal;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.security.auth.Subject;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.character.Attachment;
import de.rpgframework.character.Attachment.Format;
import de.rpgframework.character.Attachment.Type;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;
import de.rpgframework.reality.server.ServerCharacterHandle;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class AttachmentServlet extends HttpServlet {

    private final static Logger logger = System.getLogger("eden.api");

    private final static Pattern SPECIFIC = Pattern.compile("/attachment/(.*)");
	private final static Pattern WITHOUT_UUID = Pattern.compile("/attachment/*");

    private Gson gson;
    private BackendAccess backend;

    //-------------------------------------------------------------------
    /**
     * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
     */
    public void init(ServletConfig config) throws ServletException {
    	super.init(config);
    	logger.log(Level.WARNING, "Init called");
    	gson = new GsonBuilder().setPrettyPrinting().create();

    	for (Enumeration<String> e= config.getInitParameterNames(); e.hasMoreElements(); ) {
    		logger.log(Level.DEBUG, "  init parameter name = "+e.nextElement());
    	}

    	logger.log(Level.DEBUG, "Servlet context = "+config.getServletContext());
    	for (Enumeration<String> e= config.getServletContext().getAttributeNames(); e.hasMoreElements(); ) {
    		logger.log(Level.DEBUG, "  attribute name = "+e.nextElement());
    	}

    	backend = (BackendAccess) config.getServletContext().getAttribute(Constants.ATTRIB_BACKEND);
    }

    //-------------------------------------------------------------------
    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
            logger.log(Level.DEBUG, "----------doGet "+request.getRequestURL()+"--------------------");
            logger.log(Level.DEBUG, "User = "+request.getUserPrincipal());
            response.setContentType("text/html; charset=UTF-8");

            Principal user = request.getUserPrincipal();
            String uri = request.getRequestURI();
            Matcher matcher = SPECIFIC.matcher(URLDecoder.decode(uri, "UTF-8"));
            if (!matcher.matches()) {
            	logger.log(Level.DEBUG, "URI does not match pattern: "+uri+"   pattern="+SPECIFIC);
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                    return;
            }

            logger.log(Level.DEBUG, "URI does match: "+user.implies(new Subject()));

            try {
				Player player = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
				if (player==null) {
					logger.log(Level.ERROR, "Valid player '"+user.getName()+"' but did not find associated Player object");
	                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	                return;
				}
				BackendAccess.getInstance().getCharacterDatabase();

				response.getWriter().println(gson.toJson(player));
                response.setStatus(HttpServletResponse.SC_OK);
                return;
			} catch (SQLException e) {
				logger.log(Level.ERROR, "Failed accessing player",e);
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
			}
    }

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.DEBUG, "----------doPost "+request.getRequestURL()+"--------------------");
		logger.log(Level.DEBUG, "User = "+request.getUserPrincipal());
		response.setContentType("text/html; charset=UTF-8");

		Principal user = request.getUserPrincipal();
		if (user==null) {
			logger.log(Level.DEBUG, "PUT request for attachment, but no user object found");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.setHeader("Reason", "User object not found");
			return;
		}
		PlayerImpl player = null;
		try {
			player = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Failed accessing player",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}

		String uri = request.getRequestURI();
		Matcher matcher = WITHOUT_UUID.matcher(URLDecoder.decode(uri, "UTF-8"));
		if (!matcher.matches()) {
			logger.log(Level.DEBUG, "User "+user+": URI does not match pattern: "+uri+"   pattern="+WITHOUT_UUID);
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		if (logger.isLoggable(Level.TRACE))
			logger.log(Level.TRACE, "Params = "+request.getParameterMap());
		String name = request.getParameter("name");
		String charUUID = request.getParameter("char");
		if (charUUID==null) {
			logger.log(Level.DEBUG, "User "+user+": Parameter 'char' missing");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "char parameter missing");
			return;
		}
		ServerCharacterHandle handle = null;
		try {
			handle = (ServerCharacterHandle)BackendAccess.getInstance().getCharacterDatabase().getCharacter(player, UUID.fromString(charUUID));
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Failed accessing character",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
		if (handle==null) {
			logger.log(Level.DEBUG, "User "+user+": Parameter 'char' points to unknown character");
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			response.setHeader("Reason", "Parameter 'char' points to unknown character");
			return;
		}

		String type_p = request.getParameter("type");
		if (type_p==null) {
			logger.log(Level.DEBUG, "User "+user+": Parameter 'type' missing");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "rules parameter missing");
			return;
		}
		Type type = null;
		try {
			type = Type.valueOf(type_p);
		} catch (Exception e) {
			logger.log(Level.DEBUG, "User "+user+": Parameter 'type' invalid");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "type parameter invalid");
			return;
		}
		String format_p = request.getParameter("format");
		if (format_p==null) {
			logger.log(Level.DEBUG, "User "+user+": Parameter 'format' missing");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "format parameter missing");
			return;
		}
		Format format = null;
		try {
			format = Format.valueOf(format_p);
		} catch (Exception e) {
			logger.log(Level.DEBUG, "User "+user+": Parameter 'format' invalid");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "format parameter invalid");
			return;
		}


		logger.log(Level.INFO, String.format("User '%s' adds attachment '%s', type=%s, format=%s", user.getName(), name,  type, format));

		// Read data
		byte[] data = request.getInputStream().readAllBytes();

		try {
			// Ensure character does not exist yet
			Attachment attach = new Attachment(handle, null, type, format);
			backend.getAttachmentDatabase().addAttachment(handle, attach);
			response.getWriter().println(gson.toJson(attach));
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Failed creating attachment",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}

}
