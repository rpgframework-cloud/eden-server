package de.rpgframework.eden.api.jetty;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author prelle
 *
 */
public class PageHierarchy {

	public final static String PREFIX_PAGES = "";
	public final static String PREFIX_API_INTERN = "api/";
	public final static String PREFIX_API_EXTERN = "anon/";

	public final static String PATH_EXTERN  = PREFIX_PAGES+"extern/";
	public final static String PATH_EXTERN_VERIFY  = PATH_EXTERN+"verify";

	public final static String PATH_LOOKUP  = PREFIX_PAGES+"lookup/";
	public final static String PATH_LOOKUP_SPLITTERMOND  = PATH_LOOKUP+"splittermond/";
	public final static String PATH_LOOKUP_SPLITTERMOND_CREATURES  = PATH_LOOKUP_SPLITTERMOND+"beastiary/";
	public final static String PATH_LOOKUP_SPLITTERMOND_MASTERHSIPS= PATH_LOOKUP_SPLITTERMOND+"masterships/";
	public final static String PATH_LOOKUP_SPLITTERMOND_SPELLS     = PATH_LOOKUP_SPLITTERMOND+"spells/";
	public final static String PATH_LOOKUP_SHADOWRUN6    = PATH_LOOKUP+"shadowrun6/";
	public final static String PATH_LOOKUP_SHADOWRUN6_SPELLS = PATH_LOOKUP_SHADOWRUN6+"spells/";

	public final static String PATH_ACCOUNT         = PREFIX_PAGES+"account/";
	public final static String PATH_ACCOUNT_PROFILE = PATH_ACCOUNT+"profile/";
	public final static String PATH_ACCOUNT_CONTENTS= PATH_ACCOUNT+"contents/";


}
