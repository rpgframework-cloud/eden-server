package de.rpgframework.eden.api.servlet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.URLDecoder;
import java.security.Principal;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.character.Attachment;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.reality.server.PlayerImpl;
import de.rpgframework.reality.server.ServerCharacterHandle;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class CharacterServlet extends HttpServlet {

	private final static Logger logger = System.getLogger("eden.api");

	public final static String PREFIX = "/character";

	private final static Pattern CHAR_WITHOUT_UUID = Pattern.compile(".*"+PREFIX+"/*");
	private final static Pattern CHAR_WITH_UUID = Pattern.compile(".*"+PREFIX+"/([0-9a-fA-F-]+)");
	private final static Pattern ATTACH_WITHOUT_UUID = Pattern.compile(".*"+PREFIX+"/([0-9a-fA-F-]+)/attachment/*");
	private final static Pattern ATTACH_WITH_UUID = Pattern.compile(".*"+PREFIX+"/([0-9a-fA-F-]+)/attachment/([0-9a-fA-F-]+)");

	private Gson gson;
	private BackendAccess backend;

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		logger.log(Level.WARNING, "Init called");
		gson = new GsonBuilder().setPrettyPrinting().create();

		for (Enumeration<String> e= config.getInitParameterNames(); e.hasMoreElements(); ) {
			logger.log(Level.DEBUG, "  init parameter name = "+e.nextElement());
		}

		logger.log(Level.DEBUG, "Servlet context = "+config.getServletContext());
		for (Enumeration<String> e= config.getServletContext().getAttributeNames(); e.hasMoreElements(); ) {
			logger.log(Level.DEBUG, "  attribute name = "+e.nextElement());
		}

		backend = (BackendAccess) config.getServletContext().getAttribute(Constants.ATTRIB_BACKEND);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.DEBUG, "----------doGet "+request.getRequestURL()+"--------------------");
		logger.log(Level.DEBUG, "User = "+request.getUserPrincipal());
		response.setContentType("text/html; charset=UTF-8");

		Principal user = request.getUserPrincipal();

		// Get player object
		PlayerImpl player = null;
		try {
			player = backend.getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
			if (player==null) {
				logger.log(Level.ERROR, "Valid player '"+user.getName()+"' but did not find associated Player object");
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return;
			}

			String uri = request.getRequestURI();
			logger.log(Level.DEBUG, "Find pattern for: "+uri);
			Matcher matcher1 = ATTACH_WITH_UUID.matcher(URLDecoder.decode(uri, "UTF-8"));
			Matcher matcher2 = ATTACH_WITHOUT_UUID.matcher(URLDecoder.decode(uri, "UTF-8"));
			Matcher matcher3 = CHAR_WITH_UUID.matcher(URLDecoder.decode(uri, "UTF-8"));
			if (matcher1.matches()) {
				logger.log(Level.DEBUG, "Use ATTACH_WITH_UUID");
				serveGetAttachment(response, user, player, matcher1);
			} else if (matcher2.matches()) {
				logger.log(Level.DEBUG, "Use ATTACH_WITHOUT_UUID");
				serveListAttachments(response, user, player, UUID.fromString(matcher2.group(1)));
			} else if (matcher3.matches()) {
				logger.log(Level.DEBUG, "Use CHAR_WITH_UUID");
				serveGetCharacter(response, user, player, UUID.fromString(matcher3.group(1)));
			} else {
				logger.log(Level.DEBUG, "Use list");
				serveListCharacters(request, response, user, player);
			}
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Failed accessing character",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}

	}

	//-------------------------------------------------------------------
	private void serveGetAttachment(HttpServletResponse response, Principal user, PlayerImpl player, Matcher matcher) {
		logger.log(Level.DEBUG, "serveGetAttachment");
		UUID charUUID = UUID.fromString(matcher.group(1));
		UUID attachUUID = UUID.fromString(matcher.group(2));
		try {
			ServerCharacterHandle handle = (ServerCharacterHandle) backend.getCharacterDatabase().getCharacter(player, charUUID);
			if (handle==null) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				return;
			}
			for (Attachment attach : backend.getAttachmentDatabase().getAttachments(handle)) {
				if (attach.getID().equals(attachUUID)) {
					logger.log(Level.INFO, "Write "+attach.getData()+" with "+attach.getData().length+" bytes");
					response.setContentType("application/octet-stream");
					response.setContentLength(attach.getData().length);
					response.getOutputStream().write(attach.getData());
					return;
				}
			}
			logger.log(Level.ERROR, "No attachment "+attachUUID+" associated with character "+charUUID);
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed accessing attachment: "+e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}

	//-------------------------------------------------------------------
	private void serveListAttachments(HttpServletResponse response, Principal user, PlayerImpl player, UUID charUUID) {
		logger.log(Level.DEBUG, "serveListAttachments");
		try {
			ServerCharacterHandle handle = (ServerCharacterHandle) backend.getCharacterDatabase().getCharacter(player, charUUID);
			if (handle==null) {
				logger.log(Level.WARNING, "Player "+player+" asked for attachments of non-existing character "+charUUID);
				response.setHeader("X-Reason", "Character not found");
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				return;
			}
			logger.log(Level.DEBUG, "Character found");
			List<Attachment> list = backend.getAttachmentDatabase().getAttachments(handle);
			logger.log(Level.DEBUG, "Attachments: "+list);
			list.forEach(attach -> attach.setData(null));
			response.getWriter().println(gson.toJson(list));
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed accessing attachments: "+e);
			response.setHeader("X-Reason", e.toString());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}

	//-------------------------------------------------------------------
	private void serveGetCharacter(HttpServletResponse response, Principal user, PlayerImpl player, UUID charUUID) {
		try {
			ServerCharacterHandle handle = (ServerCharacterHandle) backend.getCharacterDatabase().getCharacter(player, charUUID);
			if (handle==null) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				return;
			}
			response.getWriter().println(gson.toJson(handle));
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed accessing attachment: "+e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}

	//-------------------------------------------------------------------
	private void serveListCharacters(HttpServletRequest request, HttpServletResponse response, Principal user, PlayerImpl player) {
		try {
			List<CharacterHandle> ret = backend.getCharacterDatabase().listCharacters(player);
			String rules_p = request.getParameter("rules");
			if (rules_p!=null) {
				RoleplayingSystem rules = RoleplayingSystem.valueOf(rules_p);
				ret = ret.stream().filter(ch -> ch.getRuleIdentifier()==rules).collect(Collectors.toList());
			}
			logger.log(Level.DEBUG, "Return list of "+ret.size()+" characters");
			logger.log(Level.DEBUG, "Send\n"+gson.toJson(ret));
			response.getWriter().println(gson.toJson(ret));
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed accessing character list: "+e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.INFO, "----------doPost "+request.getRequestURL()+"--------------------");
		logger.log(Level.DEBUG, "User = "+request.getUserPrincipal());
		response.setContentType("text/html; charset=UTF-8");

		Principal user = request.getUserPrincipal();
		if (user==null) {
			logger.log(Level.DEBUG, "PUT request for character, but no user object found");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.setHeader("Reason", "User object not found");
			return;
		}
		PlayerImpl player = null;
		try {
			player = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Failed accessing player",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}

		String uri = request.getRequestURI();
		Matcher matcher2 = ATTACH_WITHOUT_UUID.matcher(URLDecoder.decode(uri, "UTF-8"));
		Matcher matcher1 = CHAR_WITHOUT_UUID.matcher(URLDecoder.decode(uri, "UTF-8"));
		if (matcher2.matches()) {
			serveAddAttachment(request, response, user, player, UUID.fromString(matcher2.group(1)));
		} else if (matcher1.matches()) {
			serveAddCharacter(request, response, user, player);
		} else {
			logger.log(Level.WARNING, "POST request does not match any known format");
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			response.setHeader("Reason", "Unsupported URI");
			return;
		}


	}

	//-------------------------------------------------------------------
	private void serveAddCharacter(HttpServletRequest request, HttpServletResponse response, Principal user, PlayerImpl player) {
		logger.log(Level.DEBUG, "serveAddCharacter");
//		String name = request.getParameter("name");
//		String rules_p = request.getParameter("rules");
//		if (rules_p==null) {
//			logger.log(Level.DEBUG, "User "+user+": Parameter 'rules' missing");
//			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//			response.setHeader("Reason", "rules parameter missing");
//			return;
//		}
//		RoleplayingSystem rules = null;
//		try {
//			rules = RoleplayingSystem.valueOf(rules_p);
//		} catch (Exception e) {
//			logger.log(Level.DEBUG, "User "+user+": Parameter 'rules' invalid");
//			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//			response.setHeader("Reason", "rules parameter invalid");
//			return;
//		}

		try {
			ServerCharacterHandle upload = gson.fromJson(request.getReader(), ServerCharacterHandle.class);
			String name = upload.getName();
			RoleplayingSystem rules = upload.getRuleIdentifier();
			logger.log(Level.INFO, String.format("User '%s' adds character '%s' for %s", user.getName(), name,  rules));

			// Ensure character does not exist yet
			ServerCharacterHandle handle = backend.getCharacterDatabase().getCharacter(player, rules, name);
			if (handle!=null) {
				logger.log(Level.WARNING, "User "+user+" tries to add already existing character "+rules+"/"+name);
				response.setStatus(HttpServletResponse.SC_CONFLICT);
				response.setHeader("Reason", "already exists");
				return;
			}


			handle = backend.getCharacterDatabase().addCharacter(player, rules, name);
			response.getWriter().println(gson.toJson(handle));
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed creating character",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}

	//-------------------------------------------------------------------
	private void serveAddAttachment(HttpServletRequest request, HttpServletResponse response, Principal user, PlayerImpl player, UUID charUUID) {
		logger.log(Level.DEBUG, "serveAddAttachment");
		String type_p = request.getParameter("type");
		String format_p = request.getParameter("format");
		String filename = request.getParameter("filename");
		// Type
		if (type_p==null) {
			logger.log(Level.DEBUG, "User "+user+": Parameter 'type' missing");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "type parameter missing");
			return;
		}
		Attachment.Type type = null;
		try {
			type = Attachment.Type.valueOf(type_p);
		} catch (Exception e) {
			logger.log(Level.DEBUG, "User "+user+": Parameter 'rules' invalid");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "rules parameter invalid");
			return;
		}
		// Format
		if (format_p==null) {
			logger.log(Level.DEBUG, "User "+user+": Parameter 'format' missing");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "format parameter missing");
			return;
		}
		Attachment.Format format = null;
		try {
			format = Attachment.Format.valueOf(format_p);
		} catch (Exception e) {
			logger.log(Level.DEBUG, "User "+user+": Parameter 'format' invalid");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "format parameter invalid");
			return;
		}
		// Filename
		if (filename==null) {
			logger.log(Level.DEBUG, "User "+user+": Parameter 'filename' missing");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.setHeader("Reason", "filename parameter missing");
			return;
		}


		logger.log(Level.INFO, String.format("User '%s' adds attachment of type '%s' format %s to character %s", user.getName(), type, format, charUUID));

		try {
			// Ensure character exists
			ServerCharacterHandle handle = (ServerCharacterHandle) backend.getCharacterDatabase().getCharacter(player, charUUID);
			if (handle==null) {
				logger.log(Level.WARNING, "User "+user+" tries to add attachment non-existing character "+charUUID);
				response.setStatus(HttpServletResponse.SC_CONFLICT);
				response.setHeader("Reason", "already exists");
				return;
			}

			// Read data
			byte[] data = request.getInputStream().readAllBytes();

			// Ensure character does not exist yet
//			Attachment attach = backend.getAttachmentDatabase().addAttachment(handle, type, format, filename, data);
//			response.getWriter().println(gson.toJson(attach));
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed creating character",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Change the character
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.DEBUG, "----------doPut "+request.getRequestURL()+"--------------------");
//		logger.log(Level.DEBUG, "User = "+request.getUserPrincipal());
		response.setContentType("text/html; charset=UTF-8");

		Principal user = request.getUserPrincipal();
		if (user==null) {
			logger.log(Level.DEBUG, "POST request for character, but no user object found");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.setHeader("Reason", "User object not found");
			return;
		}

		PlayerImpl player = null;
		try {
			player = BackendAccess.getInstance().getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Failed accessing player",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}

		String uri = request.getRequestURI();
		Matcher matcher1 = ATTACH_WITH_UUID.matcher(URLDecoder.decode(uri, "UTF-8"));
		Matcher matcher2 = CHAR_WITH_UUID.matcher(URLDecoder.decode(uri, "UTF-8"));
		if (matcher1.matches()) {
			serveModifyAttachment(request, response, user, player, UUID.fromString(matcher1.group(1)), UUID.fromString(matcher1.group(2)));
		} else if (matcher2.matches()) {
			serveModifyCharacter(request, response, user, player, UUID.fromString(matcher2.group(1)));
		}
	}

	//-------------------------------------------------------------------
	private void serveModifyCharacter(HttpServletRequest request, HttpServletResponse response, Principal user, PlayerImpl player, UUID uuid) {
		logger.log(Level.DEBUG, "Overwrite character "+uuid);

		try {
			CharacterHandle newHandle = gson.fromJson(new InputStreamReader(request.getInputStream()), CharacterHandle.class);
			CharacterHandle oldHandle = backend.getCharacterDatabase().getCharacter(player, uuid);
			if (!(oldHandle.getUUID().equals(newHandle.getUUID()))) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.setHeader("Reason","UUID mismatch");
				return;
			}

			backend.getCharacterDatabase().update(newHandle);
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed updating character",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}

	private void serveModifyAttachment(HttpServletRequest request, HttpServletResponse response, Principal user, PlayerImpl player, UUID charUUID, UUID uuid) {
		logger.log(Level.DEBUG, "serveModifyAttachment");

		try {
			CharacterHandle handle = backend.getCharacterDatabase().getCharacter(player, charUUID);
			if (handle==null) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				response.setHeader("Reason","Unknown character");
				return;
			}

//			logger.log(Level.INFO, "New attach "+new String(request.getInputStream().readAllBytes()));
			Attachment newHandle = gson.fromJson(new InputStreamReader(request.getInputStream()), Attachment.class);
			if (!(uuid.equals(newHandle.getID()))) {
				logger.log(Level.WARNING, "Attachment UUID mismatch on PUT");
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.setHeader("Reason","Attachment UUID mismatch");
				return;
			}

			backend.getAttachmentDatabase().update(newHandle);
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed updating character",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Change the character
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.DEBUG, "----------doDelete "+request.getRequestURL()+"--------------------");
		logger.log(Level.DEBUG, "User = "+request.getUserPrincipal());
		response.setContentType("text/html; charset=UTF-8");

		Principal user = request.getUserPrincipal();
		if (user==null) {
			logger.log(Level.DEBUG, "POST request for character, but no user object found");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.setHeader("Reason", "User object not found");
			return;
		}

		PlayerImpl player = null;
		try {
			player = backend.getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Failed accessing player",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}

		String uri = request.getRequestURI();
		Matcher matcher1 = ATTACH_WITH_UUID.matcher(URLDecoder.decode(uri, "UTF-8"));
		Matcher matcher2 = CHAR_WITH_UUID.matcher(URLDecoder.decode(uri, "UTF-8"));
		if (matcher1.matches()) {
			serveDeleteAttachment(request, response, user, player, UUID.fromString(matcher1.group(1)), UUID.fromString(matcher1.group(2)));
		} else if (matcher2.matches()) {
			serveDeleteCharacter(request, response, user, player, UUID.fromString(matcher2.group(1)));
		}
	}

	//-------------------------------------------------------------------
	private void serveDeleteCharacter(HttpServletRequest request, HttpServletResponse response, Principal user, PlayerImpl player, UUID uuid) {
		logger.log(Level.DEBUG, "serviceDeleteCharacter");

		try {
			CharacterHandle oldHandle = backend.getCharacterDatabase().getCharacter(player, uuid);
			if (oldHandle==null) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				return;
			}
			logger.log(Level.INFO, "User "+user+" / Player "+player.getEmail()+" deletes character "+uuid+" / "+oldHandle);
			backend.getCharacterDatabase().delete(oldHandle);
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Failed deleting character",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}

	//-------------------------------------------------------------------
	private void serveDeleteAttachment(HttpServletRequest request, HttpServletResponse response, Principal user, PlayerImpl player, UUID uuid, UUID attachUUID) {
		logger.log(Level.DEBUG, "serviceDeleteAttachment");

		try {
			ServerCharacterHandle oldHandle = backend.getCharacterDatabase().getCharacter(player, uuid);
			if (oldHandle==null) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				return;
			}
			Attachment attach = backend.getAttachmentDatabase().getAttachment(oldHandle, attachUUID);
			if (attach==null) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				return;
			}
			logger.log(Level.INFO, "User "+user+" / Player "+player.getEmail()+" deletes attachment "+uuid+" / "+attachUUID+" = "+attach.getFilename());
			backend.getAttachmentDatabase().delete(attach);
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Failed deleting character",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
	}

}
