package de.rpgframework.eden.api.servlet;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.security.Principal;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class AccountServlet extends HttpServlet {

	public final static String PREFIX = "/ping";

	private final static Logger logger = System.getLogger("eden.api");

	private final static Pattern SPECIFIC = Pattern.compile("/ping");

	private Gson gson;
	private BackendAccess backend;

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		logger.log(Level.WARNING, "Init called");
		gson = new GsonBuilder().setPrettyPrinting().create();

		for (Enumeration<String> e= config.getInitParameterNames(); e.hasMoreElements(); ) {
			logger.log(Level.DEBUG, "  init parameter name = "+e.nextElement());
		}

		logger.log(Level.DEBUG, "Servlet context = "+config.getServletContext());
		for (Enumeration<String> e= config.getServletContext().getAttributeNames(); e.hasMoreElements(); ) {
			logger.log(Level.DEBUG, "  attribute name = "+e.nextElement());
		}

		backend = (BackendAccess) config.getServletContext().getAttribute(Constants.ATTRIB_BACKEND);
		logger.log(Level.INFO, "Backend is "+backend);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.INFO, "----------doGet "+request.getRequestURL()+"--------------------");
		response.setContentType("text/html; charset=UTF-8");

		Principal user = request.getUserPrincipal();
		logger.log(Level.DEBUG, "user = "+user);
		try {
			PlayerImpl player = backend.getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
			if (player==null) {
				logger.log(Level.ERROR, "Valid player '"+user.getName()+"' but did not find associated Player object");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}
			logger.log(Level.INFO, "Create EdenAccountInfo");
			EdenAccountInfo pdu = new EdenAccountInfo();
			pdu.setFirstName( player.getFirstName() );
			pdu.setLastName ( player.getLastName() );
			pdu.setEmail    (  player.getEmail() );
			// Licenses
			Map<RoleplayingSystem, List<String>> licensed = new HashMap<RoleplayingSystem, List<String>>();
			licensed.put(RoleplayingSystem.SPLITTERMOND, Arrays.asList("CORE","JdG"));
			licensed.put(RoleplayingSystem.SHADOWRUN6, Arrays.asList("CORE"));
			pdu.setModules(licensed);

			response.getWriter().println(gson.toJson(pdu));
			logger.log(Level.INFO, "Write "+gson.toJson(pdu));
			response.setStatus(HttpServletResponse.SC_OK);
			return;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		return;
	}

}
