package de.rpgframework.eden.foundry;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.URLDecoder;
import java.security.Principal;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.Constants;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class CompendiumServlet extends HttpServlet {

	private final static Logger logger = System.getLogger("eden.api");

	private final static Pattern MODULE_JSON = Pattern.compile("/api/foundry/([0-9a-zA-Z-]+)/([0-9a-zA-Z-]+)/module.json");
	private final static Pattern DOWNLOAD_ZIP = Pattern.compile("/api/foundry/([0-9a-zA-Z-]+)/([0-9a-zA-Z-]+).zip");

	private Gson gson;
	private BackendAccess backend;

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		logger.log(Level.WARNING, "Init called");
		gson = new GsonBuilder().setPrettyPrinting().create();

		for (Enumeration<String> e= config.getInitParameterNames(); e.hasMoreElements(); ) {
			logger.log(Level.DEBUG, "  init parameter name = "+e.nextElement());
		}

		logger.log(Level.DEBUG, "Servlet context = "+config.getServletContext());
		for (Enumeration<String> e= config.getServletContext().getAttributeNames(); e.hasMoreElements(); ) {
			logger.log(Level.DEBUG, "  attribute name = "+e.nextElement());
		}

		backend = (BackendAccess) config.getServletContext().getAttribute(Constants.ATTRIB_BACKEND);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.log(Level.INFO, "----------doGet "+request.getRequestURL()+"--------------------");
		logger.log(Level.DEBUG, "User = "+request.getUserPrincipal());

		Principal user = request.getUserPrincipal();

		// Get player object
		PlayerImpl player = null;
		try {
			player = backend.getPlayerDatabase().getPlayerByLogin(request.getUserPrincipal().getName());
			if (player==null) {
				logger.log(Level.ERROR, "Valid player '"+user.getName()+"' but did not find associated Player object");
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return;
			}

			String uri = request.getRequestURI();
			Matcher matcher1 = MODULE_JSON.matcher(URLDecoder.decode(uri, "UTF-8"));
			Matcher matcher2 = DOWNLOAD_ZIP.matcher(URLDecoder.decode(uri, "UTF-8"));
			if (matcher1.matches()) {
				serveModule(response, user, player, matcher1, request.getRequestURL().toString());
			} else if (matcher2.matches()) {
				serveDownload(response, user, player, matcher2, request.getRequestURL().toString());
//			} else if (matcher3.matches()) {
//				serveGetCharacter(response, user, player, UUID.fromString(matcher3.group(1)));
			} else {
//				serveListCharacters(request, response, user, player);
				logger.log(Level.ERROR, "Valid player '"+user.getName()+"' but did not find associated compendium object");
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Failed accessing compendium",e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}

	}

	//-------------------------------------------------------------------
	private void serveModule(HttpServletResponse response, Principal user, PlayerImpl player, Matcher matcher, String reqURL) {
		logger.log(Level.DEBUG, "serveModule");
		String systemID = matcher.group(1);
		String moduleID = matcher.group(2);
		logger.log(Level.INFO, "serve module.json for "+systemID+"/"+moduleID);
		if (!"compendium".equals(moduleID)) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		try {
		Module module = null;
//		if ("splittermond".equals(systemID))
//			module = SplittermondCompendiumFactory.create(player, player.getLocale(), true);
//		else if ("shadowrun6".equals(systemID))
//			module = Shadowrun6CompendiumFactory.create(player, player.getLocale(), true);
		if (module==null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		String json = gson.toJson(module);
		response.setContentType("application/json; charset=UTF-8");
		response.setContentLength(json.getBytes().length);
			response.getOutputStream().write(json.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

	//-------------------------------------------------------------------
	private void serveDownload(HttpServletResponse response, Principal user, PlayerImpl player, Matcher matcher, String reqURL) {
		logger.log(Level.DEBUG, "serveDownload");
		String systemID = matcher.group(1);
		String moduleID = matcher.group(2);
		logger.log(Level.INFO, "serve download for "+systemID+"/"+moduleID);
		if (!"compendium".equals(moduleID)) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		try {
			Module module = null;
//			if ("splittermond".equals(systemID))
//				module = SplittermondCompendiumFactory.create(player, player.getLocale(), false);
//			else if ("shadowrun6".equals(systemID))
//				module = Shadowrun6CompendiumFactory.create(player, player.getLocale(), false);
			if (module==null) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				return;
			}

		response.setContentType("application/data");
		response.setContentLength(module.fos.toByteArray().length);
			response.getOutputStream().write(module.fos.toByteArray());
			response.getOutputStream().flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}

}
