package de.rpgframework.eden.foundry;

import java.util.ArrayList;
import java.util.List;

import de.rpgframework.foundry.ItemData;

/**
 * @author prelle
 *
 */
public class CompendiumEntry {

	public String _id;
	public String name;
	public String type;
	public String img;
	public Object data;
	public TokenEntry token;
	public List<ItemData> items;

	//-------------------------------------------------------------------
	/**
	 */
	public CompendiumEntry() {
		items = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public void addItems(ItemData value) {
		items.add(value);
	}

}
