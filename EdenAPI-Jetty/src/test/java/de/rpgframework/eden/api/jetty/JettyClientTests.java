package de.rpgframework.eden.api.jetty;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.sql.Connection;
import java.util.Properties;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;

import de.rpgframework.eden.logic.BackendAccess;

/**
 * @author prelle
 *
 */
@Suite
@SelectPackages("de.rpgframework.eden.client.test")
@ExtendWith({StartJettyExtension.class})
@ConfigurationParameter(key = "server", value="helidon")
public class JettyClientTests {

	private final static Logger logger = System.getLogger(JettyClientTests.class.getPackageName());

	protected static Connection c;
	protected static BackendAccess backend;
	private static JettyMain server;

	//-------------------------------------------------------------------
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.err.println("JettyClientTests.setUpBeforeClass");
		logger.log(Level.INFO, "setUpBeforeClass");
    	System.setProperty("io.helidon.logging.config.disabled","false");
	}

	//-------------------------------------------------------------------
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.err.println("JettyClientTests.tearDownAfterClass");
		logger.log(Level.INFO, "tearDownAfterClass");
	}

	//-------------------------------------------------------------------
	@BeforeEach
	void setUp() throws Exception {
		System.err.println("JettyClientTests.setUp");
		logger.log(Level.INFO, "setUp");
		Properties config = new Properties();
		server = new JettyMain();
		server.configure(config);
	}

	//-------------------------------------------------------------------
	@AfterEach
	void tearDown() throws Exception {
		System.err.println("JettyClientTests.tearDown");
		server.stop();
	}

	//-------------------------------------------------------------------
	/**
	 */
	public JettyClientTests() {
		System.err.println("JettyClientTests.<init>");
		logger.log(Level.INFO, "Construct");
		// TODO Auto-generated constructor stub
	}
	static {
		System.out.println("Stat");
		logger.log(Level.INFO, "Static");
		// TODO Auto-generated constructor stub
	}

}
