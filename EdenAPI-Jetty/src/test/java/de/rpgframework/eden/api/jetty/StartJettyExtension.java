package de.rpgframework.eden.api.jetty;

import static org.junit.jupiter.api.extension.ExtensionContext.Namespace.GLOBAL;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.Properties;

import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Store.CloseableResource;

import de.rpgframework.eden.client.test.ServerArgumentsProvider;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.reality.server.EdenAPIServer;
import de.rpgframework.eden.client.test.CommonClientTest;

/**
 * @author prelle
 *
 */
public class StartJettyExtension implements BeforeAllCallback, CloseableResource, AfterAllCallback {

	private final static Logger logger = System.getLogger(JettyClientTests.class.getPackageName());

	private EdenAPIServer server;

	public final static String user = CommonClientTest.TESTUSER;
	public final static String pass = CommonClientTest.TESTPASS;

	//-------------------------------------------------------------------
	/**
	 */
	public StartJettyExtension() {
		System.err.println("StartJettyExtension.<init>");
		logger.log(Level.INFO, "StartJettyExtension.<init>");
    	Authenticator authenticator = new java.net.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				logger.log(Level.WARNING, "getPasswordAuthentication   "+getRequestingHost());
				logger.log(Level.WARNING, "..."+user+" : "+pass);
				return new PasswordAuthentication (user, pass.toCharArray());
			}
		};
		Authenticator.setDefault(authenticator);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.junit.jupiter.api.extension.ExtensionContext.Store.CloseableResource#close()
	 */
	@Override
	public void close() throws Throwable {
		// TODO Auto-generated method stub
		System.err.println("StartJettyExtension.close");

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.junit.jupiter.api.extension.BeforeAllCallback#beforeAll(org.junit.jupiter.api.extension.ExtensionContext)
	 */
	@Override
	public void beforeAll(ExtensionContext context) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("StartJettyExtension.beforeAll");
		logger.log(Level.INFO, "beforeAll");
		Properties config = new Properties();
		config.setProperty(BackendAccess.CFG_JDBC_URL, "jdbc:hsqldb:mem:testdb");
		config.setProperty(BackendAccess.CFG_JDBC_USER, "SA");
		config.setProperty(BackendAccess.CFG_JDBC_PASS, "");
		server = new JettyMain();
		server.configure(config);
		logger.log(Level.INFO, "start Helidon");
		server.start(config);

		context.getStore(GLOBAL).put("port", server.getPort());
		ServerArgumentsProvider.setInstance(server);


//		// We need to use a unique key here, across all usages of this particular extension.
//	    String uniqueKey = this.getClass().getName();
//	    Object value = context.getRoot().getStore(GLOBAL).get(uniqueKey);
//	    if (value == null) {
//	      // First test container invocation.
//	      context.getRoot().getStore(GLOBAL).put(uniqueKey, this);
//	      //setup();
//	    }
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.junit.jupiter.api.extension.AfterAllCallback#afterAll(org.junit.jupiter.api.extension.ExtensionContext)
	 */
	@Override
	public void afterAll(ExtensionContext context) throws Exception {
		logger.log(Level.INFO, "afterAll");
		server.stop();
	}

}
