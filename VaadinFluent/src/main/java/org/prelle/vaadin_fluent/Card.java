package org.prelle.vaadin_fluent;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.PropertyDescriptor;
import com.vaadin.flow.component.PropertyDescriptors;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;

/**
 * @author prelle
 *
 */
public class Card extends Composite<Div> {

	private static final String DEFAULT_STYLE_CLASS = "card";

	public enum Size {
		SIZE160(160),
		SIZE240(240),
		SIZE320(320),
		;
		int size;
		Size(int val) {
			this.size = val;
		}
		public int getValue() { return size; }
	}

	private Size size;

	//-------------------------------------------------------------------
	public Card() {
		getContent().setClassName(DEFAULT_STYLE_CLASS);
		setSize(Size.SIZE320);
	}

	//-------------------------------------------------------------------
	public Card setSize(Size value) {
		this.size = value;
		getContent().getStyle().set("width", value.getValue()+"px");
		return this;
	}

}
