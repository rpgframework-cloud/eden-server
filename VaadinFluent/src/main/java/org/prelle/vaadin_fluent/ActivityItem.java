package org.prelle.vaadin_fluent;

import java.time.Instant;

import com.vaadin.flow.component.html.Div;

/**
 * @author prelle
 *
 */
public class ActivityItem extends Div {

	private Instant timestamp;
	private String activityDescriptionProperty = new String();

	//-------------------------------------------------------------------
	/**
	 */
	public ActivityItem() {
	}

	/******************************************
	 * Property ACTIVITY DESCRIPTION
	 ******************************************/
	public String getActivityDescriptionProperty() { return activityDescriptionProperty; }
	public void setActivityDescriptionProperty(String value) { activityDescriptionProperty = value; }

}
