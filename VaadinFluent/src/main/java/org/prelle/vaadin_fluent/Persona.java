package org.prelle.vaadin_fluent;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

/**
 * @author prelle
 *
 */
public class Persona extends HorizontalLayout {
	
	private static final long serialVersionUID = -7746510805847761157L;

	public enum Size {
		SIZE8(8),
		SIZE24(24),
		SIZE32(32),
		SIZE40(40),
		SIZE48(48),
		SIZE56(56),
		SIZE72(72),
		SIZE100(100),
		SIZE120(120)
		;
		int size;
		Size(int val) {
			this.size = val;
		}
		public int getValue() { return size; }
	}
	
	private Size   size = Size.SIZE48;
	private Image  image = null;
	private String imageInitials ;
	private Label text;
	private Label secondaryText;
	private Label tertiaryText;
	private Label optionalText;

	//-------------------------------------------------------------------
	public Persona() {
		setSpacing(true);
		text = new Label();
		secondaryText = new Label();
		tertiaryText  = new Label();
		optionalText  = new Label();
		refresh();
	}

	/******************************************
	 *  SIZE
	 ******************************************/
	public Size getSize() { return size; }
	public void setSize(Size value) { size=value; refresh();}

	/******************************************
	 *  IMAGE
	 ******************************************/
	public Image getImage() { return image; }
	public void setImage(Image value) { image=value; refresh();}

	/******************************************
	 *  IMAGE INITIALS
	 ******************************************/
	public String getImageInitials() { return imageInitials; }
	public void setImageInitials(String value) { imageInitials=value; refresh();}

	/******************************************
	 *  TEXT
	 ******************************************/
	public String getText() { return text.getText(); }
	public void setText(String value) { text.setText(value); refresh();}

	/******************************************
	 *  SECONDARY TEXT
	 ******************************************/
	public String getSecondaryText() { return secondaryText.getText(); }
	public void setSecondaryText(String value) { secondaryText.setText(value); refresh();}

	/******************************************
	 *  TERTIARY TEXT
	 ******************************************/
	public String getTertiaryText() { return tertiaryText.getText(); }
	public void setTertiaryText(String value) { tertiaryText.setText(value); refresh();}

	/******************************************
	 *  OPTIONAL TEXT
	 ******************************************/
	public String getOptionalText() { return optionalText.getText(); }
	public void setOptionalText(String value) { optionalText.setText(value); refresh();}

	//-------------------------------------------------------------------
	private void refresh() {
		/*
		 * Prepare the coin component
		 */
		Component coin = null;
		if (size==Size.SIZE8) {
			coin = VaadinIcon.USER.create();
			((Icon)coin).setSize("8px");
		} else if (image!=null) {
			coin = image;
			image.setWidth(size.getValue()+"px");
			image.setHeight(size.getValue()+"px");
			image.getStyle().set("clip-path", "circle(50% at 50% 50%)");
		} else {
			// TODO: render initials to coin
			System.err.println("TODO: Persona: Render initials to coin");
			Label initialCoin = new Label(imageInitials);
			initialCoin.getStyle().set("font-size", (size.getValue()/2.3)+"pt");
			initialCoin.getStyle().set("background-color", "lime");
			initialCoin.getStyle().set("width", size.getValue()+"px");
			initialCoin.getStyle().set("height", size.getValue()+"px");
			initialCoin.setWidth(size.getValue()+"px");
			initialCoin.setHeight(size.getValue()+"px");
			initialCoin.getStyle().set("clip-path", "circle(50% at 50% 50%)");
			coin = initialCoin;
		}
		
		/*
		 * Prepare the info component
		 */
		VerticalLayout info = new VerticalLayout(text);
		if (size.ordinal()>=Size.SIZE40.ordinal()) {
			info.add(secondaryText);
		}
		if (size.ordinal()>=Size.SIZE72.ordinal()) {
			info.add(tertiaryText);
		}
		if (size.ordinal()>=Size.SIZE100.ordinal()) {
			info.add(optionalText);
		}
		
		/* Layout */
		super.removeAll();
		super.add(coin, info);
	}
}
