
FROM openjdk:17-jdk-slim
WORKDIR /eden

ADD http://www.rpgframework.de/eden/eden-backend-develop.jar eden-backend-develop.jar

# Copy the binary built in the 1st stage
#COPY --from=build /eden/eden-backend-develop.jar ./eden
RUN ls

CMD ["java", "-jar", "eden-backend-develop.jar"]

EXPOSE 5000
