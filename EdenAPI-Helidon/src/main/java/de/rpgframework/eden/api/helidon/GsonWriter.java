package de.rpgframework.eden.api.helidon;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.charset.Charset;
import java.util.concurrent.Flow.Publisher;

import com.google.gson.Gson;

import io.helidon.common.GenericType;
import io.helidon.common.http.DataChunk;
import io.helidon.common.http.MediaType;
import io.helidon.common.reactive.Single;
import io.helidon.media.common.CharBuffer;
import io.helidon.media.common.ContentWriters;
import io.helidon.media.common.MessageBodyWriter;
import io.helidon.media.common.MessageBodyWriterContext;

/**
 * @author prelle
 *
 */
public class GsonWriter implements MessageBodyWriter<MessageBodyWriterContext> {

	private final static Logger logger = System.getLogger(GsonWriter.class.getPackageName());

	private Gson gson;

	//-------------------------------------------------------------------
	/**
	 */
	public GsonWriter(Gson gson) {
		this.gson = gson;
	}

	@Override
	public PredicateResult accept(GenericType<?> type, MessageBodyWriterContext context) {
		return CharSequence.class.isAssignableFrom(type.rawType())?PredicateResult.NOT_SUPPORTED:PredicateResult.COMPATIBLE;
	}

	@Override
	public Publisher<DataChunk> write(Single<? extends MessageBodyWriterContext> content,
			GenericType<? extends MessageBodyWriterContext> type, MessageBodyWriterContext context) {
		logger.log(Level.DEBUG, "serialize "+type);
        MediaType contentType = context.findAccepted(MediaType.JSON_PREDICATE, MediaType.APPLICATION_JSON);
        context.contentType(contentType);

        CharBuffer buffer = new CharBuffer();
        try {
        	Object obj = content.await();
//    		System.out.println("-----> obj "+obj);
    		buffer.append(gson.toJson(obj));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return ContentWriters.writeCharBuffer(buffer, Charset.forName("UTF-8"));
	}

}
