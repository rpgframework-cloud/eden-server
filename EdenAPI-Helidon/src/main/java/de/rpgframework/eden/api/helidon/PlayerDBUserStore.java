package de.rpgframework.eden.api.helidon;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import de.rpgframework.eden.logic.PlayerDatabase;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;
import io.helidon.security.providers.httpauth.SecureUserStore;

/**
 * @author prelle
 *
 */
public class PlayerDBUserStore implements SecureUserStore {

	private final static Logger logger = System.getLogger("eden.api");

	private static PlayerDBUserStore instance;


	public static void setInstance(PlayerDBUserStore store) {
		instance = store;;
	}

	public static PlayerDBUserStore getInstance() {
		return instance;
	}



	private PlayerDatabase database;
	private Map<String, EdenUser> cache;

	//-------------------------------------------------------------------
	public PlayerDBUserStore(PlayerDatabase database) {
		this.database = database;
		cache = new HashMap<>();
	}

	//-------------------------------------------------------------------
	public void clearCache() {
		cache.clear();
	}

	//-------------------------------------------------------------------
	/**
	 * @see io.helidon.security.providers.httpauth.SecureUserStore#user(java.lang.String)
	 */
	@Override
	public Optional<User> user(String login) {
		logger.log(Level.TRACE, "user({0})", login);
		EdenUser result = cache.get(login);
		try {
			if (result==null) {
				try {
					Player player = database.getPlayerByLogin(login);
					if (player!=null) {
						result = new EdenUser(player);
						cache.put(login, result);
					} else
						logger.log(Level.WARNING, "No such user: {0}", login);
				} catch (SQLException e) {
					logger.log(Level.ERROR, "SQL error: "+e,e);
				}
			}
			logger.log(Level.TRACE, "result = {0}", result);
			return Optional.ofNullable(result);
		} finally {
			if (result!=null && result.getPlayer()!=null) {
				logger.log(Level.TRACE, "user({0}) returns {1}", login,((PlayerImpl)result.getPlayer()).getUuid());
			} else
				logger.log(Level.TRACE, "user({0}) returns {1}", login,result);
		}
	}

}
