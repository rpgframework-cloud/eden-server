package de.rpgframework.eden.api.helidon;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Flow.Publisher;

import com.google.gson.Gson;

import io.helidon.common.GenericType;
import io.helidon.common.http.DataChunk;
import io.helidon.common.mapper.Mapper;
import io.helidon.common.reactive.Single;
import io.helidon.media.common.ContentReaders;
import io.helidon.media.common.MessageBodyReader;
import io.helidon.media.common.MessageBodyReaderContext;

/**
 * @author prelle
 *
 */
public class GsonReader implements MessageBodyReader<MessageBodyReaderContext> {

	private final static Logger logger = System.getLogger(GsonWriter.class.getPackageName());

	private Gson gson;

	//-------------------------------------------------------------------
	public GsonReader(Gson gson) {
		this.gson = gson;
	}

	//-------------------------------------------------------------------
	/**
	 * @see io.helidon.media.common.MessageBodyOperator#accept(io.helidon.common.GenericType, io.helidon.media.common.MessageBodyContext)
	 */
	@Override
	public PredicateResult accept(GenericType<?> type, MessageBodyReaderContext context) {
		return CharSequence.class.isAssignableFrom(type.rawType())?PredicateResult.NOT_SUPPORTED:PredicateResult.COMPATIBLE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see io.helidon.media.common.MessageBodyReader#read(java.util.concurrent.Flow.Publisher, io.helidon.common.GenericType, io.helidon.media.common.MessageBodyReaderContext)
	 */
	@Override
	public <U extends MessageBodyReaderContext> Single<U> read(Publisher<DataChunk> publisher, GenericType<U> type,
			MessageBodyReaderContext context) {
//		logger.log(Level.WARNING, "Class        = "+type);
//		logger.log(Level.WARNING, "Content Type = "+ context.contentType());

        return ContentReaders.readBytes(publisher).map(new BytesToObject<>(type, gson));
	}

    private static final class BytesToObject<T> implements Mapper<byte[], T> {

        private final GenericType<? super T> type;
        private final Gson gson;

        BytesToObject(GenericType<T> type, Gson gson) {

            this.type = type;
            this.gson = gson;
        }

        @Override
        @SuppressWarnings("unchecked")
        public T map(byte[] bytes) {
//        	if (bytes.length<2000)
//        		logger.log(Level.WARNING, "Received "+new String(bytes, StandardCharsets.UTF_8));
           try {
                Type t = this.type.type();
//                if (t instanceof ParameterizedType) {
//                    TypeFactory typeFactory = objectMapper.getTypeFactory();
//                    ParameterizedType pt = (ParameterizedType) t;
//                    JavaType javaType = typeFactory.constructType(pt);
//        			return gson.fromJson(new String(bytes, StandardCharsets.UTF_8), javaType);
////                   return objectMapper.readValue(bytes, javaType);
//                } else {
                T ret = (T) gson.fromJson(new String(bytes, StandardCharsets.UTF_8), type.rawType());
    			logger.log(Level.DEBUG, "Return {0}", ret);
        			return ret;
//                 }
            } catch (final Exception wrapMe) {
    			wrapMe.printStackTrace();
                throw new RuntimeException(wrapMe.getMessage(), wrapMe);
            }
        }
    }

}
