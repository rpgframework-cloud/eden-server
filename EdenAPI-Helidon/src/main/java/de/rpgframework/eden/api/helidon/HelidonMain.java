package de.rpgframework.eden.api.helidon;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;

import de.rpgframework.eden.base.JavaMailMailer;
import de.rpgframework.eden.base.MailerLoader;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.MailCenter;
import de.rpgframework.reality.server.EdenAPIServer;
import io.helidon.common.configurable.Resource;
import io.helidon.common.pki.KeyConfig;
import io.helidon.common.reactive.Single;
import io.helidon.config.Config;
import io.helidon.config.FileConfigSource;
import io.helidon.config.spi.ConfigSource;
import io.helidon.security.Security;
import io.helidon.security.SubjectType;
import io.helidon.security.integration.webserver.WebSecurity;
import io.helidon.security.providers.httpauth.HttpBasicAuthProvider;
import io.helidon.security.providers.httpauth.HttpDigestAuthProvider;
import io.helidon.webserver.Routing;
import io.helidon.webserver.WebServer;
import io.helidon.webserver.WebServerTls;



/**
 * The application main class.
 */
public final class HelidonMain implements EdenAPIServer {

	public final static String PROP_CONFIG_FILE = "eden.cfgfile";
	public final static Logger logger = System.getLogger("eden");

	private static BackendAccess backend;
	private int port;
	private Single<WebServer> webserver;


    /**
     * Application main entry point.
     * @param args command line arguments.
     */
    public static void main(final String[] args) {
    	System.setProperty("io.helidon.logging.config.disabled","true");

		String confPath = System.getProperty(PROP_CONFIG_FILE, System.getProperty("user.home")+File.separatorChar+"eden.cfg");
		logger.log(Level.INFO,"Load configuration from "+confPath);
		Properties config = new Properties();
		try {
			config.load(new FileReader(confPath));
		} catch (FileNotFoundException e) {
			logger.log(Level.ERROR,"Missing configuration file: "+confPath+" or system property '"+PROP_CONFIG_FILE+"'");
			System.err.println("Missing configuration file: "+confPath+" or system property '"+PROP_CONFIG_FILE+"'");
			System.exit(1);
		} catch (Exception e) {
			logger.log(Level.ERROR,"Error loading file: "+confPath+" or system property '"+PROP_CONFIG_FILE+"'");
			System.err.println("Error loading file: "+confPath+" or system property '"+PROP_CONFIG_FILE+"'");
			System.exit(1);
		}

		HelidonMain server = new HelidonMain();
		server.configure(config);

		try {
			server.start(config);
			System.out.println("Started API server on port "+server.getPort());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
    }

    //-------------------------------------------------------------------
    /**
     * @see de.rpgframework.reality.server.EdenAPIServer#configure(java.util.Properties)
     */
    @Override
    public void configure(Properties config) {
		// Setup  Java Mail
    	if (MailerLoader.getInstance() == null)
    		MailerLoader.setInstance(new JavaMailMailer(config));

	    // Send test email
    	//MailerLoader.getInstance().send("eden@rpgframework.de", List.of("stefan@prelle.org"), List.of(), "Eden started", "Test Email beim Serverstart", null);
    	MailCenter.send("stefan@prelle.org", "Eden restart ", "<h1>Server Running</h1><p>The server just started</p>");
   }

    //-------------------------------------------------------------------
    /**
     * @see de.rpgframework.reality.server.EdenAPIServer#start(java.util.Properties)
     */
    public void start(Properties pro) throws Exception {
    	logger.log(Level.DEBUG, "ENTER start()");
    	try {
		backend = new BackendAccess(pro);
		BackendAccess.setInstance(backend);

        // load logging configuration
        //LogConfig.configureRuntime();

        // By default this will pick up application.yaml from the classpath
        Config config = Config.create();
        config = Config.builder()
        		.disableEnvironmentVariablesSource()
        		.build();

        WebServerTls tls = WebServerTls.builder()
        		.enabled(true)
        		.privateKey(
        				KeyConfig.keystoreBuilder()
        					.keystorePassphrase("helidon")
        					.keystore(Resource.create("",ClassLoader.getSystemResourceAsStream("cert.p12")))
        					.build()
        				)
        		.build()
        		;
        WebServer server = WebServer.builder(createRouting(config))
                //.config(config.get("server"))
        		.port(Integer.parseInt(pro.getProperty("eden.api.port","0")))
        		.tls(tls)
//                .addMediaSupport(JsonpSupport.create())
                .addMediaSupport(GsonMediaSupport.create())
                //.addMediaSupport(RawBytesMediaSupport.create())
                .build();

        webserver = server.start();

        // Try to start the server. If successful, print some info and arrange to
        // print a message at shutdown. If unsuccessful, print the exception.
        webserver.thenAccept(ws -> {
            System.out.println("WEB server is up! http://localhost:" + ws.port());
            logger.log(Level.INFO, "Server started on port {0}", ws.port());
            port = ws.port();
            ws.whenShutdown().thenRun(() -> System.out.println("WEB server is DOWN. Good bye!"));
        })
        .exceptionallyAccept(t -> {
            System.err.println("Startup failed: " + t.getMessage());
            t.printStackTrace(System.err);
        });
    	} catch (Exception e) {
    		logger.log(Level.ERROR, "Failed starting Webserver",e);
    		throw e;
    	} finally {
        	logger.log(Level.DEBUG, "LEAVE start()");
    	}
    }

    public void stop() throws Exception {
    	webserver.get().shutdown();
    }

    /**
     * Creates new {@link Routing}.
     *
     * @return routing configured with JSON support, a health check, and a service
     * @param config configuration of this server
     */
    private static Routing createRouting(Config config) {
        AccountService accountService = new AccountService(config);
        CharacterService charService = new CharacterService(config);
        LicenseService licenseService = new LicenseService(config);
        ShopService shopService = new ShopService(config);

        PlayerDBUserStore store = new PlayerDBUserStore(backend.getPlayerDatabase());
        PlayerDBUserStore.setInstance(store);

//        Map<String, ConfigUserStore.User> users = new HashMap<>();
//        users.put("jack", null);
////        UserStore store = user -> Optional.ofNullable(users.get(user));
//        ConfigUserStore store = new ConfigUserStore() {};

        SubjectType type = SubjectType.USER;
        HttpBasicAuthProvider httpBasicAuthProvider = HttpBasicAuthProvider.builder()
        		  .realm("myRealm")
        		  .subjectType(SubjectType.USER)
        		  .userStore(store)
      		  .build();
        HttpDigestAuthProvider httpDigestAuthProvider = HttpDigestAuthProvider.builder()
      		  .realm("myRealm")
      		  .subjectType(SubjectType.USER)
      		  .userStore(store)
    		  .build();
//        Security security = Security.create(config.get("security"));
        Security security = Security.builder()
        		.addAuthenticationProvider(httpBasicAuthProvider)
        		.addAuthenticationProvider(httpDigestAuthProvider)
        		.build();

        WebSecurity webSec = WebSecurity.create(security).securityDefaults(WebSecurity.authenticate());
        Routing.Builder builder = Routing.builder()
        		.register(webSec)
        		.register("/anon", new PingService(config))
                .register("/api/account", webSec, accountService)
                .register("/api/character", webSec, charService)
                .register("/api/license", webSec, licenseService)
                .register("/api/shop", webSec, shopService)
                ;


        return builder.build();
    }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.reality.server.EdenAPIServer#getPort()
	 */
	@Override
	public int getPort() {
		return port;
	}
}
