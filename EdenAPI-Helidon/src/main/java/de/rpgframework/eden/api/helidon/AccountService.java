package de.rpgframework.eden.api.helidon;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.http.HttpRequest.BodyPublishers;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.Flow.Publisher;

import com.google.gson.Gson;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.ResourceI18N;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.api.EdenStatus;
import de.rpgframework.eden.base.MailerLoader;
import de.rpgframework.eden.logic.AccountLogic;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.BoughtDatabase;
import de.rpgframework.eden.logic.LogicResult;
import de.rpgframework.eden.logic.PlayerDatabase;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;
import io.helidon.common.http.Http.ResponseStatus;
import io.helidon.common.reactive.Single;
import io.helidon.config.Config;
import io.helidon.security.SecurityContext;
import io.helidon.security.integration.webserver.WebSecurity;
import io.helidon.security.providers.httpauth.SecureUserStore.User;
import io.helidon.webserver.Routing.Rules;
import io.helidon.webserver.ServerRequest;
import io.helidon.webserver.ServerResponse;
import io.helidon.webserver.Service;

/**
 * @author prelle
 *
 */
public class AccountService implements Service {

	private final static Logger logger = System.getLogger("eden.api");

	private final static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(AccountService.class.getName(), Locale.ENGLISH, Locale.GERMAN);

	Config config;

	//-------------------------------------------------------------------
	public AccountService(Config config) {
		this.config = config;
	}

	//-------------------------------------------------------------------
	private static Player getPlayer(ServerRequest request) {
		Player ret = null;
	   	EdenUser user = null;
	   	try {
	   		Optional<SecurityContext> optSec = request.context().get(SecurityContext.class);
	   		if (optSec.isEmpty()) {
	   			return null;
	   		} else {
	   			String userName = optSec.get().userName();
	   			Optional<User> optUser = PlayerDBUserStore.getInstance().user(userName);
	   			if (optUser.isPresent()) {
	   				user = (EdenUser) optUser.get();
	   			}
	   			logger.log(Level.TRACE, "User {0} = {1}", userName, user);
	   		}
	   		if (user!=null)
	   			ret = user.getPlayer();
	   		return ret;
	   	} finally {
	   		logger.log(Level.DEBUG, "getPlayer(ServerRequest) returns {0}", ret);
	   	}
	}

	//-------------------------------------------------------------------
	/**
	 * @see io.helidon.reactive.webserver.Service#update(io.helidon.reactive.webserver.Routing.Rules)
	 */
	@Override
	public void update(Rules rules) {
        rules
//        .get("/", WebSecurity.rolesAllowed(EdenRoles.USER), this::getDefaultMessageHandler)
          .get("/", WebSecurity.authorize(), this::getAccountInfo)
          .post("/", this::createAccount)
          .put("/", WebSecurity.authorize(), this::updateAccount)
          .delete("/", WebSecurity.authorize(), this::deleteAccount)
          .get ("/verify", WebSecurity.authorize(), this::reverifyAccount)
          .put ("/verify", WebSecurity.authorize(), this::verifyAccount)
          .get ("/recover",this::recoverAccount)
          .put ("/recover",this::recoverAccountConfirm)
        ;
	}

	//-------------------------------------------------------------------
	private static EdenAccountInfo convertToAccountInfo(Player player) {
 		EdenAccountInfo pdu = new EdenAccountInfo();
		pdu.setEmail(player.getEmail());
		pdu.setFirstName(player.getFirstName());
		pdu.setLastName(player.getLastName());
		pdu.setLogin(player.getLogin());
		pdu.setLocale( ((PlayerImpl)player).getLocale());
		pdu.setModules(new HashMap<>());
		pdu.setVerified(player.isVerified());
		return pdu;
	}

	//-------------------------------------------------------------------
   private void getAccountInfo(ServerRequest request, ServerResponse response) {
    	logger.log(Level.WARNING, "serve request for context "+request.context());
    	// Determine logged in player
    	Player player = getPlayer(request);
    	sendResponse(response, convertToAccountInfo(player));
    }

    //-------------------------------------------------------------------
    private void createAccount(ServerRequest request, ServerResponse response) {
    	logger.log(Level.WARNING, "create account");
    	Single<EdenAccountInfo> content = request.content().as(EdenAccountInfo.class);
    	logger.log(Level.INFO, "Found "+content);

    	content.acceptEither(content, data -> {
    		LogicResult result = AccountLogic.createAccount(data);
    		if (result.successObject!=null) {
    			logger.log(Level.INFO, "Successfully created account for {0} / {1}", data.getLogin(), data.getEmail());
				response
				.status(result.status.code())
				.send(result.successObject);
    		} else {
    			sendError(response, result.status, result.message);
    		}
    	});
    }

    //-------------------------------------------------------------------
    private void updateAccount(ServerRequest request, ServerResponse response) {
    	logger.log(Level.WARNING, "update account");
    	Single<EdenAccountInfo> content = request.content().as(EdenAccountInfo.class);
    	logger.log(Level.INFO, "Found "+content);

    	PlayerImpl player = (PlayerImpl) getPlayer(request);

    	content.acceptEither(content, data -> {
    		LogicResult result = AccountLogic.updateAccount(player, data);
    		if (result.successObject!=null) {
				response
				.status(result.status.code())
				.send(result.successObject);
    		} else {
    			sendError(response, result.status, result.message);
    		}
    	});
    }

    //-------------------------------------------------------------------
    private static String createNewCode() {
    	Random rand = new Random();
    	int n = rand.nextInt(10000);
    	if (n<10) return "000"+n;
    	if (n<100) return "00"+n;
    	if (n<1000) return "0"+n;
    	return String.valueOf(n);
    }

    //-------------------------------------------------------------------
    private static String getRecoverHTML(Locale loc, String code, String login, String ip) {
    	StringBuffer html = new StringBuffer();
    	html.append("<h2>"+RES.getString("recovermail.body1", loc)+"</h2>");
    	html.append("<p>"+RES.format("recovermail.body2", loc, login, ip)+"</p>");
    	html.append("<p text-align=\"center\"; style=\"font-size: 200%\">"+code+"</p>");
    	html.append("<p>"+RES.getString("recovermail.body3", loc)+"</p>");
    	return html.toString();
    }

    //-------------------------------------------------------------------
    private void reverifyAccount(ServerRequest request, ServerResponse response) {
		logger.log(Level.WARNING, "reverifyAccount");
	   	PlayerImpl player = (PlayerImpl) getPlayer(request);
	   	if (player.isVerified()) {
	   	   	logger.log(Level.WARNING, "Player {0} asks for a new verification code, but is already verified", player.getUuid()+"/"+player.getEmail());
	   	   	sendError(response, EdenStatus.ALREADY_EXISTS, "Already verified");
	   	} else {
	   		logger.log(Level.INFO, "Player {0} wants to reverify", player.getUuid()+"/"+player.getEmail());
	   		AccountLogic.sendVerificationCode(player);
	   	}
   }

    //-------------------------------------------------------------------
    private void verifyAccount(ServerRequest request, ServerResponse response) {
    	logger.log(Level.WARNING, "verify account");
    	Single<EdenAccountInfo> content = request.content().as(EdenAccountInfo.class);

    	content.acceptEither(content, data -> {
    		logger.log(Level.INFO, "Uploaded: "+data);
    		logger.log(Level.INFO, "- code  : "+data.getVerificationCode());
        	try {
				PlayerImpl player = (PlayerImpl)getPlayer(request);
				logger.log(Level.INFO, "- player: "+player);
				logger.log(Level.INFO, "- vCode : "+player.getVerificationCode());
				LogicResult result = AccountLogic.verifyAccount(player, data.getVerificationCode());
				if (result.status!=EdenStatus.OK) {
					sendError(response, result.status, result.message);
				} else {
					response.status(200).send();
				}
				return;
//				if (!player.getVerificationCode().equals(data.getVerificationCode())) {
//					sendError(response, EdenStatus.UNAUTHORIZED, "Verification code does not match");
//				} else {
//					logger.log(Level.INFO, "Player {0} verification code does match - mark as verified", player.getUuid()+"/"+player.getEmail());
//					PlayerDatabase db = BackendAccess.getInstance().getPlayerDatabase();
//					player.setVerified(true);
//					player.setVerificationCode(null);
//					try {
//						db.updatePlayer(player);
//						logger.log(Level.INFO, "Player {0} verified", player.getUuid()+"/"+player.getEmail());
//						response.status(200).send();
//					} catch (Exception e) {
//						sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
//						logger.log(Level.WARNING, "Error writing verified player", e);
//					}
//					AccountLogic.sendVerificationCode(player);
//				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
			}
     	});
    }

    //-------------------------------------------------------------------
    private void recoverAccount(ServerRequest request, ServerResponse response) {
    	logger.log(Level.WARNING, "recover account");
    	logger.log(Level.INFO, "Params ="+request.queryParams());

    	// Check that all parameters exist
    	Optional<String> optPlayer = request.queryParams().first("player");
    	if (optPlayer.isEmpty()) {
    		sendError(response, EdenStatus.INSUFFICENT_DATA, "Player missing");
    		return;
    	}

    	// Obtain player from databasee
    	PlayerDatabase db = BackendAccess.getInstance().getPlayerDatabase();
    	PlayerImpl player = null;
    	try {
			player = db.getPlayerByLogin(optPlayer.get());
			if (player==null) {
				logger.log(Level.WARNING, "A password recovery for an unknown player {0} was requested from {1}", optPlayer.get(), request.remoteAddress());
				response
					.status(ResponseStatus.create(202))
					.send(BodyPublishers.ofString(RES.getString("recover.message")));
				return;
			}
		} catch (SQLException e) {
			logger.log(Level.ERROR, "Failed getting player info from DB",e);
    		sendError(response, EdenStatus.INTERNAL_ERROR, "Database error");
    		return;
		}

    	String code = createNewCode();
		player.setVerificationCode(code);
		try {
			db.updatePlayer(player);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String error = MailerLoader.getInstance().send(
				"eden@rpgframework.de",
				List.of(player.getEmail()),
				List.of(),
				RES.getString("recovermail.subject", player.getLocale()),
				"Read the HTML content, not this",
				getRecoverHTML(player.getLocale(), code, optPlayer.get(), request.remoteAddress()));
		logger.log(Level.WARNING, "Mail sending returns: {0}", error);
		if (error!=null) {
			response.status(ResponseStatus.create(501)).send(BodyPublishers.ofString(error));
		} else {
			String mess = RES.getString("recover.message");
			response
				.status(ResponseStatus.create(200))
				.send(mess);
		}
   }

    //-------------------------------------------------------------------
    private void recoverAccountConfirm(ServerRequest request, ServerResponse response) {
    	logger.log(Level.WARNING, "confirm recovered account");
    	Single<EdenAccountInfo> content = request.content().as(EdenAccountInfo.class);
    	logger.log(Level.INFO, "Found "+content);

    	content.acceptEither(content, data -> {
        	logger.log(Level.INFO, "Account recovery confirmation "+data);
    		// Validate data
        	if (data==null) {
        		sendError(response, EdenStatus.INSUFFICENT_DATA, "Missing data");
        		return;
        	}
        	if (data.getVerificationCode()==null || data.getSecret()==null || data.getLogin()==null) {
        		sendError(response, EdenStatus.INSUFFICENT_DATA, "Missing data");
        		return;
        	}
        	// Check password rules
        	if (data.getSecret().length()<8) {
        		sendError(response, EdenStatus.INSUFFICENT_DATA, "Password does not match rules");
        		return;
        	}

    		// Obtain player from databasee
    		PlayerDatabase db = BackendAccess.getInstance().getPlayerDatabase();
    		PlayerImpl player = null;
    		try {
    			player = db.getPlayerByLogin(data.getLogin());
    		} catch (SQLException e) {
    			logger.log(Level.ERROR, "Failed getting player info from DB",e);
    			sendError(response, EdenStatus.INTERNAL_ERROR, "Database error");
    			return;
    		}
    		logger.log(Level.WARNING, "Accepted "+player);
    		if (player==null) {
    			sendError(response, EdenStatus.NO_SUCH_ITEM, "You are kidding, aren't you");
    			return;
    		}

    		// Check if verification code matches
    		if (!player.getVerificationCode().equals(data.getVerificationCode())) {
    			sendError(response, EdenStatus.UNAUTHORIZED, null);
    			return;
    		}
    		// Update password
    		player.setPassword(data.getSecret());
    		try {
				db.updatePlayer(player);
				logger.log(Level.WARNING, "Password updated for ''{0}'' from ''{1}''", data.getLogin(), request.remoteAddress());
				Optional<User> user = PlayerDBUserStore.getInstance().user(data.getLogin());
				if (user.isPresent()) {
					((EdenUser)user.get()).setPlayer(player);
				}
				response.status(ResponseStatus.create(200, "Updated")).send();
			} catch (SQLException e) {
    			logger.log(Level.ERROR, "Error updating player in DB",e);
    			sendError(response, EdenStatus.INTERNAL_ERROR, "Database error");
    			return;
			}
    	}
    	);
   }

    //-------------------------------------------------------------------
    private void deleteAccount(ServerRequest request, ServerResponse response) {
    	logger.log(Level.WARNING, "deleteAccount");
    	PlayerImpl player = (PlayerImpl) getPlayer(request);

    	LogicResult result = AccountLogic.deleteAccount(player);
    	if (result.successObject!=null) {
    		logger.log(Level.INFO, "Successfully deleted account {0} and all its characters", player.getEmail());
			response
				.status(result.status.code())
				.send(result.successObject);
    	} else {
    		sendError(response, result.status, result.message);
    	}
    }

    //-------------------------------------------------------------------
    private void sendError(ServerResponse response, EdenStatus code, String mess) {
    	response
    		.addHeader("Reason", (mess!=null)?mess:code.name())
    		.status(ResponseStatus.create(code.code(), code.name()))
    		.send()
    		;
    }

    //-------------------------------------------------------------------
    private void sendResponse(ServerResponse response, Object name) {
// //       String msg = String.format("%s %s!", "Moin", name);
//
//        Gson gson = new Gson();
//        String returnObject = gson.toJson(name);
////        JsonObject returnObject = JSON.createObjectBuilder()
////                .add("message", msg)
////                .build();
        response.send(name);
    }

}
