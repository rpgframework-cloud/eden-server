package de.rpgframework.eden.api.helidon;

/**
 * @author prelle
 *
 */
public interface EdenRoles {

	public final static String ADMIN = "ADMIN";
	public final static String USER = "USER";
	
}
