package de.rpgframework.eden.api.helidon;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import io.helidon.config.Config;
import io.helidon.webserver.Routing.Rules;
import io.helidon.webserver.ServerRequest;
import io.helidon.webserver.ServerResponse;
import io.helidon.webserver.Service;

/**
 * @author prelle
 *
 */
public class PingService implements Service {

	private final static Logger logger = System.getLogger("de.rpgframework.eden.");

	//-------------------------------------------------------------------
	private class EdenPingInfo {
		private String mainSiteURL;
		private String accountCreationURL;
	}

	//-------------------------------------------------------------------
	public PingService(Config config) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see io.helidon.reactive.webserver.Service#update(io.helidon.reactive.webserver.Routing.Rules)
	 */
	@Override
	public void update(Rules rules) {
        rules
        .get("/ping", this::getDefaultMessageHandler)
        ;
	}

	//-------------------------------------------------------------------
    /**
     * Return information about this host
     * @param request the server request
     * @param response the server response
     */
    private void getDefaultMessageHandler(ServerRequest request, ServerResponse response) {
    	logger.log(Level.INFO, "Ping from "+request.headers().value("User-Agent"));
		EdenPingInfo pdu = new EdenPingInfo();
		pdu.accountCreationURL = "http://localhost:8000/create";
		pdu.mainSiteURL = "http://localhost:8000";

		response.send(pdu);
    }

}
