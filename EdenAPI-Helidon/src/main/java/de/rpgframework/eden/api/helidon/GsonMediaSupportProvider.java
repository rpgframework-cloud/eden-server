package de.rpgframework.eden.api.helidon;

import io.helidon.config.Config;
import io.helidon.media.common.MediaSupport;
import io.helidon.media.common.spi.MediaSupportProvider;

/**
 * @author prelle
 *
 */
public class GsonMediaSupportProvider implements MediaSupportProvider, MediaSupport {

    private static final String JACKSON = "gson";

	//-------------------------------------------------------------------
	/**
	 */
	public GsonMediaSupportProvider() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see io.helidon.media.common.spi.MediaSupportProvider#create(io.helidon.config.Config)
	 */
	@Override
	public MediaSupport create(Config config) {
		System.err.println("GsonMediaSupportProvider.create");
		return GsonMediaSupport.create();
	}

    private String configName(String enumName) {
        return enumName.toLowerCase()
                .replace('_', '-');
    }

    @Override
    public String configKey() {
        return JACKSON;
    }

}
