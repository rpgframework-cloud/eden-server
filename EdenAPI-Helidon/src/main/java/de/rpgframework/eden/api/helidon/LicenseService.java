package de.rpgframework.eden.api.helidon;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.ResourceI18N;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.api.EdenStatus;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.BoughtDatabase;
import de.rpgframework.eden.logic.LogicResult;
import de.rpgframework.eden.logic.ShopDatabase;
import de.rpgframework.reality.ActivationKey;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.CatalogItem;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;
import io.helidon.common.http.Http.ResponseStatus;
import io.helidon.common.reactive.Single;
import io.helidon.config.Config;
import io.helidon.security.SecurityContext;
import io.helidon.security.integration.webserver.WebSecurity;
import io.helidon.security.providers.httpauth.SecureUserStore.User;
import io.helidon.webserver.Routing.Rules;
import io.helidon.webserver.ServerRequest;
import io.helidon.webserver.ServerResponse;
import io.helidon.webserver.Service;

/**
 * @author prelle
 *
 */
public class LicenseService implements Service {

	private final static Logger logger = System.getLogger("eden.api");

	private final static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(AccountService.class.getName(), Locale.ENGLISH, Locale.GERMAN);

	Config config;

	//-------------------------------------------------------------------
	public LicenseService(Config config) {
		this.config = config;
	}

	//-------------------------------------------------------------------
	private static Player getPlayer(ServerRequest request) {
		Player ret = null;
	   	EdenUser user = null;
	   	try {
	   		Optional<SecurityContext> optSec = request.context().get(SecurityContext.class);
	   		if (optSec.isEmpty()) {
	   			return null;
	   		} else {
	   			String userName = optSec.get().userName();
	   			Optional<User> optUser = PlayerDBUserStore.getInstance().user(userName);
	   			if (optUser.isPresent()) {
	   				user = (EdenUser) optUser.get();
	   			}
	   		}
	   		if (user!=null)
	   			ret = user.getPlayer();
	   		return ret;
	   	} finally {
	   		logger.log(Level.DEBUG, "getPlayer(ServerRequest) returns {0}", ret);
	   	}
	}

	//-------------------------------------------------------------------
	/**
	 * @see io.helidon.reactive.webserver.Service#update(io.helidon.reactive.webserver.Routing.Rules)
	 */
	@Override
	public void update(Rules rules) {
        rules
    		.get("/datasets", WebSecurity.authorize(), this::getDatasets)
         	.get("/", WebSecurity.authorize(), this::getBoughtItems)
       		.post("/activate", WebSecurity.authorize(), this::activate)
        ;
	}

	//-------------------------------------------------------------------
   private void getBoughtItems(ServerRequest request, ServerResponse response) {
    	logger.log(Level.INFO, "serve request for context "+request.context());
    	// Determine logged in player
    	PlayerImpl player = (PlayerImpl) getPlayer(request);

    	logger.log(Level.INFO, "parameters="+request.queryParams());
    	Optional<String> rulesS = request.queryParams().first("rules");
    	RoleplayingSystem rules = null;
    	if (rulesS.isPresent()) {
	    	rules = RoleplayingSystem.valueOf(rulesS.get());
    	}

    	ShopDatabase shop = BackendAccess.getInstance().getShopDatabase();
    	BoughtDatabase licenses = BackendAccess.getInstance().getLicenseDatabase();

    	try {
			List<BoughtItem> list = licenses.getBoughtItems(player);
			for (BoughtItem item : list) {
				item.setName( shop.getCatalogItem(item.getItemID()).getName() );
			}
	        sendResponse(response, list);
		} catch (SQLException e) {
			logger.log(Level.ERROR, "SQL error", e);
			sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
		}
    }

	//-------------------------------------------------------------------
   /**
    * Returns a full list of all datasets assigned to the player
    */
   private void getDatasets(ServerRequest request, ServerResponse response) {
    	logger.log(Level.INFO, "serve request for context "+request.context());
    	// Determine logged in player
    	PlayerImpl player = (PlayerImpl) getPlayer(request);
    	if (player==null) {
    		sendError(response, EdenStatus.UNAUTHORIZED, "Unknown player");
    		return;
    	}

		//
    	ShopDatabase shop = BackendAccess.getInstance().getShopDatabase();
    	BoughtDatabase licenses = BackendAccess.getInstance().getLicenseDatabase();
    	LogicResult result = BackendAccess.getInstance().getLicenseLogic().getLicensedDatasets(player);
    	List<String> datasets = (List<String>) result.successObject;

    	try {
//			List<BoughtItem> list = licenses.getBoughtItems(player);
//			for (BoughtItem bought : list) {
//				CatalogItem item = shop.getCatalogItem(bought.getItemID());
//				if (item==null) {
//					logger.log(Level.ERROR, "Bought item {0} references unknown catalog item {1}", bought.getTransactionID(), bought.getItemID());
//				} else {
//					item.getDatasets().forEach( ds -> {
//						String key = item.getRules().name()+"/"+ds+"/"+;
//						if (!datasets.contains(key)) {
//							datasets.add(key);
//						}
//					});
//				}
//			}

    		logger.log(Level.INFO, "Send datasets "+datasets);
			String plain = String.join("\n", datasets);
			String encoded = BackendAccess.getInstance().getLicenseLogic().encode(plain);
	        sendResponse(response, encoded);
		} catch (Exception e) {
			logger.log(Level.ERROR, "SQL error", e);
			sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
		}
    }

   //-------------------------------------------------------------------
   private void activate(ServerRequest request, ServerResponse response) {
	   logger.log(Level.INFO, "serve request for context "+request.context());
   		// Determine logged in player
   		PlayerImpl player = (PlayerImpl) getPlayer(request);
    	if (player==null) {
    		sendError(response, EdenStatus.UNAUTHORIZED, "Unknown player");
    		return;
    	}

    	Single<UUID> content = request.content().as(UUID.class);
       	content.acceptEither(content, data -> {

          	logger.log(Level.DEBUG, "Player {0}/{1} wants to activate {2}", player.getUuid(), player.getLogin(), data);

          	try {
				ActivationKey key = BackendAccess.getInstance().getActivationKeyDatabase().get(data);
				if (key==null) {
		          	logger.log(Level.DEBUG, "Player {0}/{1} wants to activate non-existing key {2}", player.getUuid(), player.getLogin(), data);
		          	String message = RES.format("error.no_such_activation_key", player.getLocale(),data);
					sendError(response, EdenStatus.NOT_ACCEPTABLE, message);
					return;
				}
				// New perform activation
				LogicResult result = BackendAccess.getInstance().getLicenseLogic().useActivationKey(player.getUuid(), data);
				if (result.status==EdenStatus.OK) {
		          	logger.log(Level.DEBUG, "Player {0}/{1} successfully activated {2}", player.getUuid(), player.getLogin(), data);
					response
						.status(ResponseStatus.create(202, "Created"))
						.send(result.successObject)
						;
				} else {
					sendError(response, result.status, result.message);
				}
				return;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
       	}).exceptionallyAccept(e -> {
			sendError(response, EdenStatus.INTERNAL_ERROR, "Internal error: "+e.toString());
       	});
   }

    //-------------------------------------------------------------------
    private void sendError(ServerResponse response, EdenStatus code, String mess) {
    	response
    		.addHeader("Reason", (mess!=null)?mess:code.name())
    		.status(ResponseStatus.create(code.code(), code.name()))
    		.send()
    		;
    }

    //-------------------------------------------------------------------
    private void sendResponse(ServerResponse response, Object name) {
// //       String msg = String.format("%s %s!", "Moin", name);
//
//        Gson gson = new Gson();
//        String returnObject = gson.toJson(name);
////        JsonObject returnObject = JSON.createObjectBuilder()
////                .add("message", msg)
////                .build();
        response.send(name);
    }

}
