package de.rpgframework.eden.api.helidon;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import de.rpgframework.reality.Player;
import io.helidon.security.providers.httpauth.HttpDigest;
import io.helidon.security.providers.httpauth.SecureUserStore.User;

/**
 * @author prelle
 *
 */
public class EdenUser implements User {

	private final static Logger logger = System.getLogger("eden.api");

	private Player player;

	//-------------------------------------------------------------------
	public EdenUser(Player player) {
		this.player = player;
	}

	//-------------------------------------------------------------------
	/**
	 * @see io.helidon.security.providers.httpauth.SecureUserStore.User#login()
	 */
	@Override
	public String login() {
		return player.getLogin();
	}

	//-------------------------------------------------------------------
	/**
	 * @see io.helidon.security.providers.httpauth.SecureUserStore.User#isPasswordValid(char[])
	 */
	@Override
	public boolean isPasswordValid(char[] password) {
		String check = new String(password);
		logger.log(Level.TRACE, "isPasswordValid({0})={1}  {2}", check, player.getPassword().equals(check), player.getPassword());
		return player.getPassword().equals(check);
	}

	//-------------------------------------------------------------------
	public Collection<String> roles() {
		//logger.log(Level.INFO, "roles");
        return Set.of(EdenRoles.USER);
    }

	//-------------------------------------------------------------------
	/**
         * Digest authentication requires a hash of username, realm and password.
         * As password should not be revealed by systems, this is to provide the HA1 (from Digest Auth terminology)
         * based on the known (public) information combined with the secret information available to user store only (password).
         * <p>
         * ha1 algorithm ({@code unq} stands for "unquoted value")
         * <pre>
         *    ha1 = md5(a1);
         *    a1 = unq(username-value) ":" unq(realm-value) ":" passwd
         * </pre>
         *
         * @param realm configured realm
         * @param algorithm algorithm of the hash (current only MD5 supported by Helidon)
         * @return a digest to use for validation of incoming request
	 */
    public Optional<String> digestHa1(String realm, HttpDigest.Algorithm algorithm) {
		logger.log(Level.TRACE, "digestHa1({0}  {1})", realm, algorithm);

		String a1 = player.getLogin()+":"+realm+":"+player.getPassword();
		try {
			MessageDigest md = MessageDigest.getInstance(algorithm.name());
			byte[] digestB = md.digest(a1.getBytes(StandardCharsets.UTF_8));
			// Convert to String
			String digest = toHex(digestB);
			return Optional.of(digest);
		} catch (NoSuchAlgorithmException e) {
			logger.log(Level.WARNING, "Digest algorithm not supported: {0}",algorithm,e);
		}

       return Optional.empty();
    }

    //-------------------------------------------------------------------
    private static String toHex(byte[] data) {
    	StringBuffer buf = new StringBuffer();
    	for (int i=0; i<data.length; i++) {
    		byte num = data[i];
            char[] hexDigits = new char[2];
            hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
            hexDigits[1] = Character.forDigit((num & 0xF), 16);
            buf.append( new String(hexDigits));
    	}
    	return buf.toString();
    }

	//-------------------------------------------------------------------
	public Player getPlayer() {
		return player;
	}

	//-------------------------------------------------------------------
	public void setPlayer(Player data) {
		this.player = data;
	}
}
