package de.rpgframework.eden.api.helidon;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.helidon.common.LazyValue;
import io.helidon.media.common.MediaSupport;
import io.helidon.media.common.MessageBodyReader;
import io.helidon.media.common.MessageBodyWriter;

/**
 * @author prelle
 *
 */
public class GsonMediaSupport implements MediaSupport {

	private static final LazyValue<GsonMediaSupport> DEFAULT = LazyValue.create(() -> new GsonMediaSupport());

	private static Gson gson;

    //-------------------------------------------------------------------
    public static GsonMediaSupport create() {
		gson = (new GsonBuilder()).setPrettyPrinting().create();
        return DEFAULT.get();
    }


	//-------------------------------------------------------------------
	private GsonMediaSupport() {
	}
	
    //-------------------------------------------------------------------
    /**
     * @see io.helidon.media.common.MediaSupport#writers()
     */
	@Override
    public Collection<MessageBodyWriter<?>> writers() {
        return List.of(new GsonWriter(gson));
    }
    
	//-------------------------------------------------------------------
	/**
	 * @see io.helidon.media.common.MediaSupport#readers()
	 */
	@Override
    public Collection<MessageBodyReader<?>> readers() {
        return List.of(new GsonReader(gson));
    }

}
