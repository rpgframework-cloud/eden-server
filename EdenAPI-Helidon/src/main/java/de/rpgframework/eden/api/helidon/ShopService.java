package de.rpgframework.eden.api.helidon;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.api.EdenStatus;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.BoughtDatabase;
import de.rpgframework.eden.logic.ShopDatabase;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.CatalogItem;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;
import io.helidon.common.http.Http.ResponseStatus;
import io.helidon.config.Config;
import io.helidon.security.SecurityContext;
import io.helidon.security.integration.webserver.WebSecurity;
import io.helidon.security.providers.httpauth.SecureUserStore.User;
import io.helidon.webserver.Routing.Rules;
import io.helidon.webserver.ServerRequest;
import io.helidon.webserver.ServerResponse;
import io.helidon.webserver.Service;

/**
 * @author prelle
 *
 */
public class ShopService implements Service {

	private final static Logger logger = System.getLogger("eden.api");

	private final static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(AccountService.class.getName(), Locale.ENGLISH, Locale.GERMAN);

	Config config;

	//-------------------------------------------------------------------
	public ShopService(Config config) {
		this.config = config;
	}

	//-------------------------------------------------------------------
	private static Player getPlayer(ServerRequest request) {
		Player ret = null;
	   	EdenUser user = null;
	   	try {
	   		Optional<SecurityContext> optSec = request.context().get(SecurityContext.class);
	   		if (optSec.isEmpty()) {
	   			return null;
	   		} else {
	   			String userName = optSec.get().userName();
	   			Optional<User> optUser = PlayerDBUserStore.getInstance().user(userName);
	   			if (optUser.isPresent()) {
	   				user = (EdenUser) optUser.get();
	   			}
	   		}
	   		if (user!=null)
	   			ret = user.getPlayer();
	   		return ret;
	   	} finally {
	   		logger.log(Level.DEBUG, "getPlayer(ServerRequest) returns {0}", ret);
	   	}
	}

	//-------------------------------------------------------------------
	/**
	 * @see io.helidon.reactive.webserver.Service#update(io.helidon.reactive.webserver.Routing.Rules)
	 */
	@Override
	public void update(Rules rules) {
        rules
    		.get("/{rules}/{id}", WebSecurity.authorize(), this::getCatalogItem)
        ;
	}

	//-------------------------------------------------------------------
   private void getCatalogItem(ServerRequest request, ServerResponse response) {
    	logger.log(Level.INFO, "serve request for context "+request.context());
    	// Determine logged in player
    	PlayerImpl player = (PlayerImpl) getPlayer(request);

    	logger.log(Level.INFO, "parameters="+request.queryParams());
    	Optional<String> rulesS = request.queryParams().first("rules");
    	RoleplayingSystem rules = null;
    	if (rulesS.isPresent()) {
	    	rules = RoleplayingSystem.valueOf(rulesS.get());
    	}
    	logger.log(Level.INFO, "rules="+request.path().param("rules"));
    	logger.log(Level.INFO, "id="+request.path().param("id"));

    	ShopDatabase shop = BackendAccess.getInstance().getShopDatabase();

    	try {
    		CatalogItem item = shop.getCatalogItem(request.path().param("id"));
			if (item==null) {
				sendError(response, EdenStatus.NO_SUCH_ITEM, "Item "+request.path().param("id")+" not found");
			}
	        sendResponse(response, item);
		} catch (SQLException e) {
			logger.log(Level.ERROR, "SQL error", e);
			sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
		}
    }

    //-------------------------------------------------------------------
    private void sendError(ServerResponse response, EdenStatus code, String mess) {
    	response
    		.addHeader("Reason", (mess!=null)?mess:code.name())
    		.status(ResponseStatus.create(code.code(), code.name()))
    		.send()
    		;
    }

    //-------------------------------------------------------------------
    private void sendResponse(ServerResponse response, Object name) {
// //       String msg = String.format("%s %s!", "Moin", name);
//
//        Gson gson = new Gson();
//        String returnObject = gson.toJson(name);
////        JsonObject returnObject = JSON.createObjectBuilder()
////                .add("message", msg)
////                .build();
        response.send(name);
    }

}
