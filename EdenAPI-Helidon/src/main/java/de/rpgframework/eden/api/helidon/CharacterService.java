package de.rpgframework.eden.api.helidon;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.character.Attachment;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.eden.api.EdenStatus;
import de.rpgframework.eden.logic.AttachmentDatabase;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.CharacterDatabase;
import de.rpgframework.reality.Player;
import de.rpgframework.reality.server.PlayerImpl;
import de.rpgframework.reality.server.ServerCharacterHandle;
import io.helidon.common.http.Http.ResponseStatus;
import io.helidon.common.reactive.Single;
import io.helidon.config.Config;
import io.helidon.security.SecurityContext;
import io.helidon.security.integration.webserver.WebSecurity;
import io.helidon.security.providers.httpauth.SecureUserStore.User;
import io.helidon.webserver.Routing.Rules;
import io.helidon.webserver.ServerRequest;
import io.helidon.webserver.ServerResponse;
import io.helidon.webserver.Service;

/**
 * @author prelle
 *
 */
public class CharacterService implements Service {

	private final static Logger logger = System.getLogger("de.rpgframework.eden");

	private final static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(CharacterService.class.getName(), Locale.ENGLISH, Locale.GERMAN);

	Config config;
	private Gson gson;

	//-------------------------------------------------------------------
	public CharacterService(Config config) {
		this.config = config;
		gson = (new GsonBuilder()).setPrettyPrinting().create();
	}

	//-------------------------------------------------------------------
	private static PlayerImpl getPlayer(ServerRequest request) {
		Player ret = null;
	   	EdenUser user = null;
	   	try {
	   		Optional<SecurityContext> optSec = request.context().get(SecurityContext.class);
	   		if (optSec.isEmpty()) {
	   			return null;
	   		} else {
	   			String userName = optSec.get().userName();
	   			Optional<User> optUser = PlayerDBUserStore.getInstance().user(userName);
	   			if (optUser.isPresent()) {
	   				user = (EdenUser) optUser.get();
	   			}
	   		}
	   		if (user!=null)
	   			ret = user.getPlayer();
	   		return (PlayerImpl) ret;
	   	} finally {
	   		logger.log(Level.TRACE, "getPlayer(ServerRequest) returns {0}", ret);
	   	}
	}

	//-------------------------------------------------------------------
	/**
	 * @see io.helidon.reactive.webserver.Service#update(io.helidon.reactive.webserver.Routing.Rules)
	 */
	@Override
	public void update(Rules rules) {
        rules
//        .get("/", WebSecurity.rolesAllowed(EdenRoles.USER), this::getDefaultMessageHandler)
          .get ("/{uuid}/attachment", WebSecurity.authorize(), this::getAllAttachments)
          .get ("/{cuuid}/attachment/{auuid}", WebSecurity.authorize(), this::getAttachment)
          .post("/{cuuid}/attachment", WebSecurity.authorize(), this::createAttachment)
          .put ("/{cuuid}/attachment/{auuid}", WebSecurity.authorize(), this::modifyAttachment)
          .delete("/{cuuid}/attachment/{auuid}", WebSecurity.authorize(), this::deleteAttachment)
          .get("/", WebSecurity.authorize(), this::getAllCharacters)
          .post("/", WebSecurity.authorize(), this::createCharacter)
          .delete("/{uuid}", WebSecurity.authorize(), this::deleteCharacter)
         ;
	}

	//-------------------------------------------------------------------
   private void getAllCharacters(ServerRequest request, ServerResponse response) {
    	logger.log(Level.INFO, "ENTER getAllCharacters()-------------------------- ");
    	try {
        	// Determine logged in player
        	PlayerImpl player = getPlayer(request);

        	CharacterDatabase charDB = BackendAccess.getInstance().getCharacterDatabase();
			List<CharacterHandle> list = charDB.listCharacters(player);
	        sendResponse(response, list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
		} finally {
	    	logger.log(Level.INFO, "LEAVE getAllCharacters()-------------------------- ");
		}
    }

	//-------------------------------------------------------------------
   private void deleteCharacter(ServerRequest request, ServerResponse response) {
  		logger.log(Level.DEBUG, "ENTER deleteCharacter()");
  		// Determine logged in player
  		PlayerImpl player = getPlayer(request);
  		String uuidParam = request.path().param("uuid");
  		logger.log(Level.DEBUG, "Request deletion of character {0}", uuidParam);
  		if (uuidParam==null) {
  			sendError(response, EdenStatus.INTERNAL_ERROR, "Unexpected URL format");
  			return;
  		}
  		UUID charUUID = UUID.fromString(uuidParam);

  		CharacterDatabase charDB = BackendAccess.getInstance().getCharacterDatabase();
  		ServerCharacterHandle handle = null;
  		try {
			handle = charDB.getCharacter(player, charUUID);
			if (handle==null) {
	  			sendError(response, EdenStatus.NO_SUCH_ITEM, "No such character");
	  			return;
			}
			logger.log(Level.INFO,"User {0} / Player {1} deletes character {2} / {3}", player.getUuid(), player.getEmail(), charUUID, handle.getName());
			for (Attachment attach : BackendAccess.getInstance().getAttachmentDatabase().getAttachments(handle)) {
				logger.log(Level.INFO,"  User {0} / Player {1} deleted attachment {2} of {3}", player.getUuid(), player.getEmail(), attach.getFilename(), handle.getName());
				BackendAccess.getInstance().getAttachmentDatabase().delete(attach);
				handle.removeAttachment(attach);
			}
			logger.log(Level.INFO,"  Now mark character {0} as deleted ", handle.getName());
			handle.setDeleted(true);
	  		charDB.update(handle);
	  		response.send();
		} catch (SQLException e) {
			logger.log(Level.ERROR, "SQL error while trying to access character {0} of player {1} = {2}:\n{3}", charUUID, player.getLogin(), player.getUuid(), e.getMessage());
 			sendError(response, EdenStatus.INTERNAL_ERROR, "Error accessing character");
  			return;
		}
   }

	//-------------------------------------------------------------------
  private void getAllAttachments(ServerRequest request, ServerResponse response) {
   		logger.log(Level.TRACE, "ENTER getAllAttachments()");
   		try {
   	   		// Determine logged in player
   	   		PlayerImpl player = getPlayer(request);
   	   		String uuidParam = request.path().param("uuid");
   	   		if (uuidParam==null) {
   	   			sendError(response, EdenStatus.INTERNAL_ERROR, "Unexpected URL format");
   	   			return;
   	   		}
   	    	UUID charUUID = UUID.fromString(uuidParam);

   	    	CharacterDatabase charDB = BackendAccess.getInstance().getCharacterDatabase();
   	    	ServerCharacterHandle handle = null;
   	    	try {
   				handle = charDB.getCharacter(player, charUUID);
   			} catch (SQLException e) {
   				logger.log(Level.ERROR, "SQL error while trying to access character {0} of player {1} = {2}", charUUID, player.getLogin(), player.getUuid());
   	   			sendError(response, EdenStatus.INTERNAL_ERROR, "Error accessing character");
   	   			return;
   			}

   	   		logger.log(Level.INFO, "Request all attachments for character {1}/{0}", handle.getName(), player.getLogin());
   	   		AttachmentDatabase attachDB = BackendAccess.getInstance().getAttachmentDatabase();
   	    	try {
   				List<Attachment> list = attachDB.getAttachments(handle);
   				// Don't transfer content here
   				list.forEach(attach -> attach.setData(null));
   		        sendResponse(response, list);
   			} catch (Exception e) {
   				// TODO Auto-generated catch block
   				e.printStackTrace();
   				sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
   			}
   		} finally {
   	   		logger.log(Level.TRACE, "LEAVE getAllAttachments()");
   		}
   }

	//-------------------------------------------------------------------
  	private void getAttachment(ServerRequest request, ServerResponse response) {
 		logger.log(Level.INFO, "getAttachment() "+request);
 		// Determine logged in player
 		PlayerImpl player = getPlayer(request);
 		String cUUID_s = request.path().param("cuuid");
 		String aUUID_s = request.path().param("auuid");
 		logger.log(Level.DEBUG, "Request attachment {1} for character {0}", cUUID_s, aUUID_s);
 		if (cUUID_s==null || aUUID_s==null) {
 			sendError(response, EdenStatus.INTERNAL_ERROR, "Unexpected URL format");
 			return;
 		}
 		UUID charUUID = UUID.fromString(cUUID_s);
 		UUID attachUUID = UUID.fromString(aUUID_s);

 		CharacterDatabase charDB = BackendAccess.getInstance().getCharacterDatabase();
 		ServerCharacterHandle handle = null;
 		try {
			handle = charDB.getCharacter(player, charUUID);
		} catch (SQLException e) {
			logger.log(Level.ERROR, "SQL error while trying to access character {0} of player {1} = {2}", charUUID, player.getLogin(), player.getUuid());
 			sendError(response, EdenStatus.INTERNAL_ERROR, "Error accessing character");
 			return;
		}

 		AttachmentDatabase attachDB = BackendAccess.getInstance().getAttachmentDatabase();
 		try {
 			Attachment data = attachDB.getAttachment(handle, attachUUID);
 	 		if (cUUID_s==null || aUUID_s==null) {
 	 			sendError(response, EdenStatus.NO_SUCH_ITEM, "Unknown attachment");
 	 			return;
 	 		}
 	 		logger.log(Level.DEBUG, "Have {0} bytes to return", data.getData().length);
 	    	response
 	    		.addHeader("Content-Type", "application/data")
 	    		.send(data.getData())
 	    		;
    		;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
		}
  	}

	//-------------------------------------------------------------------
  	private void deleteAttachment(ServerRequest request, ServerResponse response) {
 		logger.log(Level.INFO, "deleteAttachment() "+request);
 		// Determine logged in player
 		PlayerImpl player = getPlayer(request);
 		String cUUID_s = request.path().param("cuuid");
 		String aUUID_s = request.path().param("auuid");
 		logger.log(Level.DEBUG, "Delete attachment {1} for character {0}", cUUID_s, aUUID_s);
 		if (cUUID_s==null || aUUID_s==null) {
 			sendError(response, EdenStatus.INTERNAL_ERROR, "Unexpected URL format");
 			return;
 		}
 		UUID charUUID = UUID.fromString(cUUID_s);
 		UUID attachUUID = UUID.fromString(aUUID_s);

 		CharacterDatabase charDB = BackendAccess.getInstance().getCharacterDatabase();
 		ServerCharacterHandle handle = null;
 		try {
			handle = charDB.getCharacter(player, charUUID);
		} catch (SQLException e) {
			logger.log(Level.ERROR, "SQL error while trying to access character {0} of player {1} = {2}", charUUID, player.getLogin(), player.getUuid());
 			sendError(response, EdenStatus.INTERNAL_ERROR, "Error accessing character");
 			return;
		}

 		AttachmentDatabase attachDB = BackendAccess.getInstance().getAttachmentDatabase();
 		try {
 			Attachment data = attachDB.getAttachment(handle, attachUUID);
 	 		if (data==null) {
 	 			sendError(response, EdenStatus.NO_SUCH_ITEM, "Unknown attachment");
 	 			return;
 	 		}
 	 		attachDB.delete(data);

 	 		logger.log(Level.WARNING, "Deleted attachment {0} for character {1}", data.getFilename(), charUUID);
 	    	response.send()
 	    		;
    		;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
		}
  	}

    //-------------------------------------------------------------------
    private void sendError(ServerResponse response, EdenStatus code, String mess) {
    	response
    		.addHeader("Reason", (mess!=null)?mess:code.name())
    		.status(ResponseStatus.create(code.code(), code.name()))
    		.send()
    		;
    }

    //-------------------------------------------------------------------
    private void sendResponse(ServerResponse response, Object name) {
 //       String msg = String.format("%s %s!", "Moin", name);

        String returnObject = gson.toJson(name);
//        JsonObject returnObject = JSON.createObjectBuilder()
//                .add("message", msg)
//                .build();
        response.send(returnObject);
    }

    //-------------------------------------------------------------------
    private void createCharacter(ServerRequest request, ServerResponse response) {
    	logger.log(Level.WARNING, "create character");
    	Single<ServerCharacterHandle> content = request.content().as(ServerCharacterHandle.class);
    	logger.log(Level.INFO, "Found "+content);
  		PlayerImpl player = getPlayer(request);
    	logger.log(Level.INFO, "Player "+player);

    	content.acceptEither(content, data -> {
    		logger.log(Level.INFO, "Uploaded: "+data);
    		logger.log(Level.INFO, "- name : "+data.getName());
       		logger.log(Level.INFO, "- name : "+data.getUUID());
    		if (data.getName()==null) {
        		logger.log(Level.WARNING, "Uploaded a character without name!");
    			sendError(response, EdenStatus.INSUFFICENT_DATA, "Name missing");
    			return;
    		}
    		if (data.getLastModified()==null) {
    			data.setLastModified(Date.from(Instant.now()));
    		}

    		CharacterDatabase db = BackendAccess.getInstance().getCharacterDatabase();
    		try {
        		logger.log(Level.INFO, "Now add character to database");
				ServerCharacterHandle handle = db.addCharacter(player, data);
				logger.log(Level.INFO, "Created character {0} for player {1}", handle.getUUID(), player.getUuid());
				sendResponse(response, handle.getUUID());
			} catch (SQLException e) {
				logger.log(Level.ERROR, "Failed writing to database: "+e.getErrorCode()+"/"+e.getSQLState()+": "+e.getMessage());
				if (e.getSQLState().equals("23000")) {
					sendError(response, EdenStatus.ALREADY_EXISTS, "Character "+data.getUUID()+" already exists on server");
				} else
					sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
			}
    	});
    }

    //-------------------------------------------------------------------
    private void createAttachment(ServerRequest request, ServerResponse response) {
    	logger.log(Level.INFO, "createAttachment");
    	Single<Attachment> content = request.content().as(Attachment.class);
//    	logger.log(Level.INFO, "Found "+content);
  		PlayerImpl player = getPlayer(request);
    	logger.log(Level.DEBUG, "Player "+player);

    	logger.log(Level.TRACE , "--> Character UUID : {0}",request.path().param("cuuid"));
    	logger.log(Level.TRACE, "--> Request Params : {0}",request.queryParams());
       	UUID charUUID = UUID.fromString(request.path().param("cuuid"));
		if (charUUID==null) {
			sendError(response, EdenStatus.INSUFFICENT_DATA, "Missing character UUID");
			return;
		}

    	content.acceptEither(content, attach -> {
           	String name = attach.getFilename();
           	Attachment.Format format = attach.getFormat();
           	Attachment.Type   type   = attach.getType();
           	logger.log(Level.TRACE , "--> Name           : {0}",name);
           	logger.log(Level.TRACE , "--> Format         : {0}",format);
           	logger.log(Level.TRACE , "--> Type           : {0}",type);
    		if (name==null) {
    			sendError(response, EdenStatus.INSUFFICENT_DATA, "Name missing");
    			return;
    		}
    		if (format==null) {
    			sendError(response, EdenStatus.INSUFFICENT_DATA, "Format missing");
    			return;
    		}
    		if (attach.getData()==null)
    			attach.setData(new byte[0]);
          	logger.log(Level.INFO, "User {0} uploaded {1} bytes for {2} as {3} {4}", player.getLogin(), attach.getData().length, name, format, type);

    		CharacterDatabase db = BackendAccess.getInstance().getCharacterDatabase();
    		try {
				ServerCharacterHandle handle = db.getCharacter(player, charUUID);
				if (handle==null) {
					sendError(response, EdenStatus.NO_SUCH_ITEM, "No such character");
					return;
				}
				BackendAccess.getInstance().getAttachmentDatabase().addAttachment(handle, attach);
				logger.log(Level.INFO, "Created attachment {0} of {1} bytes for player {2}", attach.getID(), attach.getData().length, player.getUuid());

				sendResponse(response, attach.getID());
				return;
			} catch (SQLException e) {
				e.printStackTrace();
				sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
			}
    		response.send();
		}).exceptionallyAccept(e -> {
			logger.log(Level.ERROR,"Error decoding",e);
			sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
		});
    }

    //-------------------------------------------------------------------
    private void modifyAttachment(ServerRequest request, ServerResponse response) {
    	logger.log(Level.INFO, "ENTER modifyAttachment");
    	try {

     	Single<byte[]> content = request.content().as(byte[].class);
//    	logger.log(Level.INFO, "Found "+content);
  		PlayerImpl player = getPlayer(request);
 		String cUUID_s = request.path().param("cuuid");
 		String aUUID_s = request.path().param("auuid");
 		logger.log(Level.DEBUG, "  Modify attachment {1} for character {0}", cUUID_s, aUUID_s);
 		if (cUUID_s==null || aUUID_s==null) {
 			sendError(response, EdenStatus.INTERNAL_ERROR, "Unexpected URL format");
 			return;
 		}
 		if (cUUID_s.equals("null") || aUUID_s.equals("null")) {
 			sendError(response, EdenStatus.INTERNAL_ERROR, "Invalid UUIDs in URL");
 			return;
 		}

    	content.acceptEither(content, data -> {
          	logger.log(Level.DEBUG , "  Attachment {0} bytes",data.length);

    		try {
    	       	UUID charUUID = UUID.fromString(cUUID_s);
       	       	UUID attachUUID = UUID.fromString(aUUID_s);
    			CharacterDatabase db = BackendAccess.getInstance().getCharacterDatabase();
    			ServerCharacterHandle handle = db.getCharacter(player, charUUID);
				if (handle==null) {
					sendError(response, EdenStatus.NO_SUCH_ITEM, "No such character");
					return;
				}
				Attachment previous = BackendAccess.getInstance().getAttachmentDatabase().getAttachment(handle, attachUUID);
				if (previous==null) {
					sendError(response, EdenStatus.NO_SUCH_ITEM, "Character has no such attachment");
					return;
				} else {
					previous.setData(data);
					BackendAccess.getInstance().getAttachmentDatabase().update(previous);
					logger.log(Level.INFO, "Updated attachment {0} of {1} bytes for player {2}", previous.getID(), data.length, player.getUuid());
				}
	        	response.send();
			} catch (SQLException e) {
				e.printStackTrace();
				sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
			}
    		}).exceptionallyAccept(e -> {
    			logger.log(Level.ERROR,"Error decoding",e);
				sendError(response, EdenStatus.INTERNAL_ERROR, e.toString());
    		});
    		response.send();
       	} finally {
        	logger.log(Level.INFO, "LEAVE modifyAttachment");
       	}
   }
}
