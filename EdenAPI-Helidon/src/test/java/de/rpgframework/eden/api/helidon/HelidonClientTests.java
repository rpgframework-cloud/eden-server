package de.rpgframework.eden.api.helidon;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.Connection;
import java.util.Properties;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SuiteDisplayName;

import de.rpgframework.eden.logic.BackendAccess;

/**
 * @author prelle
 *
 */
@Suite
@SelectPackages("de.rpgframework.eden.client.test")
@ExtendWith({StartHelidonExtension.class})
@ConfigurationParameter(key = "server", value="helidon")
public class HelidonClientTests {

	private final static Logger logger = System.getLogger(HelidonClientTests.class.getPackageName());

	protected static Connection c;
	protected static BackendAccess backend;
	private static HelidonMain server;

	//-------------------------------------------------------------------
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.err.println("HelidonClientTests.setUpBeforeClass");
		logger.log(Level.INFO, "setUpBeforeClass");
    	System.setProperty("io.helidon.logging.config.disabled","false");
	}

	//-------------------------------------------------------------------
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.err.println("HelidonClientTests.tearDownAfterClass");
		logger.log(Level.INFO, "tearDownAfterClass");
	}

	//-------------------------------------------------------------------
	@BeforeEach
	void setUp() throws Exception {
		System.err.println("HelidonClientTests.setUp");
		logger.log(Level.INFO, "setUp");
		Properties config = new Properties();
		server = new HelidonMain();
		server.configure(config);
	}

	//-------------------------------------------------------------------
	@AfterEach
	void tearDown() throws Exception {
		System.err.println("HelidonClientTests.tearDown");
		server.stop();
	}

	//-------------------------------------------------------------------
	/**
	 */
	public HelidonClientTests() {
		System.err.println("HelidonClientTests.<init>");
		logger.log(Level.INFO, "Construct");
		// TODO Auto-generated constructor stub
	}
	static {
		System.out.println("Stat");
		logger.log(Level.INFO, "Static");
		// TODO Auto-generated constructor stub
	}

}
