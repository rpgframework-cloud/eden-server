package de.rpgframework.eden.api.helidon;

import static org.junit.jupiter.api.extension.ExtensionContext.Namespace.GLOBAL;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Properties;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Store.CloseableResource;

import de.rpgframework.eden.client.test.ServerArgumentsProvider;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.reality.server.EdenAPIServer;

/**
 * @author prelle
 *
 */
public class StartHelidonExtension implements BeforeAllCallback, CloseableResource, AfterEachCallback {

	private final static Logger logger = System.getLogger(HelidonClientTests.class.getPackageName());

	private EdenAPIServer server;

	//-------------------------------------------------------------------
	/**
	 */
	public StartHelidonExtension() {
		System.err.println("StartHelidonExtension.<init>");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.junit.jupiter.api.extension.ExtensionContext.Store.CloseableResource#close()
	 */
	@Override
	public void close() throws Throwable {
		// TODO Auto-generated method stub
		System.err.println("StartHelidonExtension.close");

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.junit.jupiter.api.extension.BeforeAllCallback#beforeAll(org.junit.jupiter.api.extension.ExtensionContext)
	 */
	@Override
	public void beforeAll(ExtensionContext context) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("StartHelidonExtension.beforeAll");
		logger.log(Level.INFO, "beforeAll");
		Properties config = new Properties();
		config.setProperty(BackendAccess.CFG_JDBC_URL, "jdbc:hsqldb:mem:testdb");
		config.setProperty(BackendAccess.CFG_JDBC_USER, "SA");
		config.setProperty(BackendAccess.CFG_JDBC_PASS, "");
		server = new HelidonMain();
		server.configure(config);
		logger.log(Level.INFO, "start Helidon");
		server.start(config);

		context.getStore(GLOBAL).put("port", server.getPort());
		ServerArgumentsProvider.setInstance(server);


//		// We need to use a unique key here, across all usages of this particular extension.
//	    String uniqueKey = this.getClass().getName();
//	    Object value = context.getRoot().getStore(GLOBAL).get(uniqueKey);
//	    if (value == null) {
//	      // First test container invocation.
//	      context.getRoot().getStore(GLOBAL).put(uniqueKey, this);
//	      //setup();
//	    }
	}

	@Override
	public void afterEach(ExtensionContext context) throws Exception {
		logger.log(Level.INFO, "afterEach");
		PlayerDBUserStore.getInstance().clearCache();
	}

}
