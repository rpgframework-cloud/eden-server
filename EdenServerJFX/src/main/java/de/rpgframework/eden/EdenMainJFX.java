package de.rpgframework.eden;

import java.io.IOException;

import com.jpro.webapi.JProApplication;
import com.jpro.webapi.WebAPI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author prelle
 *
 */
public class EdenMainJFX extends Application {

    private WebAPI webAPI = null;

	public static void main(String[] args) {
        launch(args);
    }

	//-------------------------------------------------------------------
	/**
	 */
	public EdenMainJFX() {
		// TODO Auto-generated constructor stub
	}

    /**
     * Sets the value of the application's WebAPI to <code>webAPI</code>.
     * This function is used from JPro internally.
     * @param webAPI WebAPI
     */
    public void setWebAPI(WebAPI webAPI) {
        this.webAPI = webAPI;
    }


    /**
     * Gets the value of the application's WebAPI.
     * @return WebAPI if not set null is returned.
     */
    public WebAPI getWebAPI() {
        return webAPI;
    }

	//-------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
        //load user interface as FXML file
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/de/rpgframework/eden/fxml/EdenMainJFX.fxml"));
        Scene scene = null;
        try
        {
            Parent root = loader.load();
            HelloJProFXMLController controller = loader.getController();

            //create JavaFX scene
            scene = new Scene(root);
            stage.setScene(scene);
           controller.init(this, null);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        stage.setTitle("Hello Eden!");

        //open JavaFX window
        stage.show();
	}

}
