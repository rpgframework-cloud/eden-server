package de.rpgframework.eden;

import java.util.Locale;

public class EdenMainJFXStarter {

	public static void main(String[] args) {
		Locale.setDefault(Locale.ENGLISH);
		EdenMainJFX.main(args);
	}

}
