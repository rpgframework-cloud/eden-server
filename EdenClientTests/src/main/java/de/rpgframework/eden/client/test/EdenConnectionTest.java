package de.rpgframework.eden.client.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.api.EdenPingInfo;
import de.rpgframework.eden.api.EdenStatus;
import de.rpgframework.eden.base.MailerLoader;
import de.rpgframework.eden.client.EdenAPIException;
import de.rpgframework.eden.client.EdenCodes;
import de.rpgframework.eden.client.EdenConnection;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.reality.server.EdenAPIServer;
import de.rpgframework.reality.server.PlayerImpl;

public class EdenConnectionTest {

	protected final static Logger logger = System.getLogger("ClientTest");

	protected static EdenAPIServer server;
	private BackendAccess backend;
	private Connection c;
	private static DummyMailSender mailSender;

	//-------------------------------------------------------------------
	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
		logger.log(Level.INFO, "setUpBeforeClass");
		server = ServerArgumentsProvider.getServer();
		mailSender = new DummyMailSender();
		MailerLoader.setInstance(mailSender);
	}

	//-------------------------------------------------------------------
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		logger.log(Level.INFO, "tearDownAfterClass");
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		backend = new BackendAccess("jdbc:hsqldb:mem:testdb", "SA","");
		c = backend.getInternalConnection();
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
//		System.out.println("tearDown--------------------------------");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS Attachments");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS Characters");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS BoughtItems");
		c.createStatement().executeUpdate("DROP TABLE IF EXiSTS Player");
		c.close();
	}

	//-------------------------------------------------------------------
	@Test
	void testConnect() throws SQLException {
		logger.log(Level.INFO, "testConnect");
		UUID uuid = UUID.randomUUID();
		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password, lang) VALUES('"+uuid+"','Manfred','Müstermann','test@invalid.de','manni','XYlRXHvA4QbSwRVG5szjdg==','de')");
		stat.close();

		EdenConnection con = new EdenConnection("http","localhost", server.getPort(), "UnitTest", "1.0");
		con.addListener( l -> logger.log(Level.INFO, "State changes to {0}",l));
		con.start();
		try {Thread.sleep(1000); } catch (InterruptedException e) {}
		System.out.println("\nNun mit falschem Login");
		con.login("manni", "falsch", null);
		try {Thread.sleep(1000); } catch (InterruptedException e) {}
		System.out.println("\nNun mit richtigem Login");
		con.login("manni", "muster", null);
		try {Thread.sleep(1000); } catch (InterruptedException e) {}

		con.stop();
	}

	//-------------------------------------------------------------------
	@Test
	void testConnectVerified() throws SQLException {
		logger.log(Level.INFO, "testConnect");
		UUID uuid = UUID.randomUUID();
		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password, lang, verified) VALUES('"+uuid+"','Manfred','Müstermann','test@invalid.de','manni','XYlRXHvA4QbSwRVG5szjdg==','de','1')");
		stat.close();

		EdenConnection con = new EdenConnection("http","localhost", server.getPort(), "UnitTest", "1.0");
		con.addListener( l -> logger.log(Level.INFO, "State changes to {0}",l));
		con.start();
		con.login("manni", "muster", null);
		try {Thread.sleep(1000); } catch (InterruptedException e) {}

		con.stop();
	}

}
