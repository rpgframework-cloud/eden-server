package de.rpgframework.eden.client.test;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import de.rpgframework.character.IUserDatabase;
import de.rpgframework.eden.client.EdenConnection;
import de.rpgframework.eden.client.RemoteUserDatabase;
import de.rpgframework.eden.logic.ActivationKeyDatabase;
import de.rpgframework.eden.logic.AttachmentDatabase;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.BoughtDatabase;
import de.rpgframework.eden.logic.CharacterDatabase;
import de.rpgframework.eden.logic.PlayerDatabase;
import de.rpgframework.eden.logic.ShopDatabase;
import de.rpgframework.reality.server.EdenAPIServer;

/**
 * @author spr
 *
 */
public class CommonClientTest {

	protected final static Logger logger = System.getLogger("ClientTest");

	public final static String TESTUSER = "manni";
	public final static String TESTPASS = "muster";

	protected static Connection c;
	protected static EdenAPIServer server;
	protected static BackendAccess backend;

	protected EdenConnection eden;
	protected IUserDatabase charProv;

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setupBeforeClass() throws Exception {
		logger.log(Level.INFO, "setupBeforeClass");
		server = ServerArgumentsProvider.getServer();

		backend = BackendAccess.getInstance();
	}

//	//-------------------------------------------------------------------
//	private static Authenticator getAuthenticator() {
//		return new BasicAuthenticator() {
//			@SuppressWarnings("unchecked")
//			public Authentication validateRequest(ServletRequest req, ServletResponse res, boolean mandatory) throws ServerAuthException {
//				logger.log(Level.DEBUG, "validateRequest");
//				HttpServletRequest request = (HttpServletRequest)req;
//				if (!mandatory)
//					return new DeferredAuthentication(this);
//
//				HttpSession session = request.getSession(true);
//
//				// Look for cached authentication
//				Authentication authentication = (Authentication) session.getAttribute(SessionAuthentication.__J_AUTHENTICATED);
//				if (authentication != null) {
//					// Has authentication been revoked?
//					if (authentication instanceof Authentication.User &&
//							_loginService!=null &&
//							!_loginService.validate(((Authentication.User)authentication).getUserIdentity()))
//					{
//						logger.log(Level.DEBUG, "auth revoked {}",authentication);
//						session.removeAttribute(SessionAuthentication.__J_AUTHENTICATED);
//					} else {
//						synchronized (session) {
//							String j_uri=(String)session.getAttribute(FormAuthenticator.__J_URI);
//							if (j_uri!=null) {
//								//check if the request is for the same url as the original and restore
//								//params if it was a post
//								logger.log(Level.DEBUG, "auth retry {0}->{1}",authentication,j_uri);
//								StringBuffer buf = request.getRequestURL();
//								if (request.getQueryString() != null)
//									buf.append("?").append(request.getQueryString());
//
//								if (j_uri.equals(buf.toString()))
//								{
//									MultiMap<String> j_post = (MultiMap<String>)session.getAttribute(FormAuthenticator.__J_POST);
//									if (j_post!=null)
//									{
//										logger.log(Level.DEBUG, "auth rePOST {0}->{1}",authentication,j_uri);
//										Request base_request = Request.getBaseRequest(request);
//										base_request.setContentParameters(j_post);
//									}
//									session.removeAttribute(FormAuthenticator.__J_URI);
//									session.removeAttribute(FormAuthenticator.__J_METHOD);
//									session.removeAttribute(FormAuthenticator.__J_POST);
//								}
//							}
//						}
//						logger.log(Level.TRACE, "auth {0}",authentication);
//						return authentication;
//					}
//				}
//				return super.validateRequest(req, res, mandatory);
//			}
//		};
//	}
//
//	//-------------------------------------------------------------------
//	protected static ConstraintSecurityHandler createSecurityHandler(String constraintName,
//			String[] allowedRoles, Authenticator authenticator, String realm,
//			LoginService loginService) {
//
//		Constraint constraint = new Constraint();
//		constraint.setName(constraintName);
//		constraint.setRoles(new String[] {Roles.VALID_USER.name()});
//		// This is telling Jetty to not allow unauthenticated requests through (very important!)
//		constraint.setAuthenticate(true);
//
//		ConstraintMapping cm = new ConstraintMapping();
//		cm.setConstraint(constraint);
//		cm.setPathSpec("/*");
//
//		ConstraintSecurityHandler sh = new ConstraintSecurityHandler();
//		sh.setAuthenticator(authenticator);
//		sh.setLoginService(loginService);
//		sh.setConstraintMappings(new ConstraintMapping[]{cm});
//		sh.setRealmName(realm);
//
//		return sh;
//	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS "+AttachmentDatabase.TABLE);
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS "+CharacterDatabase.TABLE);
		c.createStatement().executeUpdate("DROP TABLE IF EXiSTS "+ActivationKeyDatabase.TABLE);
		c.createStatement().executeUpdate("DROP TABLE IF EXiSTS "+BoughtDatabase.TABLE);
		c.createStatement().executeUpdate("DROP TABLE IF EXiSTS "+ShopDatabase.TABLE);
		c.createStatement().executeUpdate("DROP TABLE IF EXiSTS "+PlayerDatabase.TABLE);
		c.createStatement().execute("SHUTDOWN");
		c.close();
		server.stop();
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		//		backend = new BackendAccess("jdbc:hsqldb:mem:testdb", "SA","");
		c = BackendAccess.getInstance().getInternalConnection();
		eden = new EdenConnection("http","localhost",server.getPort(), "Test","");
		logger.log(Level.DEBUG, "Log in now");
		eden.login(TESTUSER, TESTPASS, null);

		charProv = new RemoteUserDatabase(eden);
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
		logger.log(Level.DEBUG, "tearDown--------------------------\n\n");
		//PlayerDBUserStore.getInstance().clearCache();
		try {
			c.createStatement().executeUpdate("DELETE FROM "+AttachmentDatabase.TABLE);
			c.createStatement().executeUpdate("DELETE FROM "+CharacterDatabase.TABLE);
			c.createStatement().executeUpdate("DELETE FROM "+ActivationKeyDatabase.TABLE);
			c.createStatement().executeUpdate("DELETE FROM "+BoughtDatabase.TABLE);
			c.createStatement().executeUpdate("DELETE FROM "+ShopDatabase.TABLE);
			c.createStatement().executeUpdate("DELETE FROM "+PlayerDatabase.TABLE);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
