package de.rpgframework.eden.client.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.hsqldb.lib.StringInputStream;
import org.junit.jupiter.api.Test;

import de.rpgframework.character.Attachment;
import de.rpgframework.character.Attachment.Format;
import de.rpgframework.character.Attachment.Type;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.FileBasedCharacterHandle;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.logic.AttachmentDatabase;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.eden.logic.CharacterDatabase;
import de.rpgframework.eden.logic.PlayerDatabase;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.CatalogItem;
import de.rpgframework.reality.CatalogItem.Category;
import de.rpgframework.reality.CatalogItem.State;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author spr
 *
 */
class CharacterProviderTest extends CommonClientTest {

	//-----------------------------------------------------------------
	@Test
	void testList() throws Exception {
		System.out.println("---testList------------------------------------------");
		// Write data to read
		UUID uuid = UUID.randomUUID();

		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password,lang) VALUES('"+uuid+"','Manfred','Mustermann','test@invalid.de','"+TESTUSER+"','"+PlayerDatabase.encodePassword(TESTPASS)+"'),'de'");
		stat.execute("INSERT INTO Characters (id, player, rules, name) VALUES('"+UUID.randomUUID()+"','"+uuid+"','SPLITTERMOND','Carimea')");
		stat.close();

		List<CharacterHandle> handles = charProv.getCharacters();
		assertNotNull(handles);
		assertEquals(1, handles.size());
		// Login again - should use session cookie
		charProv.getCharacters();
	}

	//-----------------------------------------------------------------
	@Test
	void testDelete() throws Exception {
		System.out.println("---testDelete------------------------------------------");
		// Write data to read
		UUID uuid = UUID.randomUUID();

		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password,lang) VALUES('"+uuid+"','Manfred','Mustermann','test@invalid.de','"+TESTUSER+"','"+PlayerDatabase.encodePassword(TESTPASS)+"'),'de'");
		stat.execute("INSERT INTO Characters (id, player, rules, name) VALUES('"+UUID.randomUUID()+"','"+uuid+"','SPLITTERMOND','Carimea')");
		stat.close();

		List<CharacterHandle> handles = charProv.getCharacters();
		logger.log(Level.DEBUG, "Returned from server: "+handles);
		assertNotNull(handles);
		assertEquals(1, handles.size());
		logger.log(Level.DEBUG, "Returned from server[0]: "+handles.get(0));
		logger.log(Level.DEBUG, "Returned from server[0]: class="+handles.get(0).getClass());

		charProv.deleteCharacter(handles.get(0));

		// Ensure character is delected
		stat = c.createStatement();
		ResultSet set = stat.executeQuery("SELECT * FROM "+CharacterDatabase.TABLE);
		assertFalse(set.next());
	}

	//-----------------------------------------------------------------
	@Test
	void testCreate() throws Exception {
		System.out.println("---testCreate------------------------------------------");
		// Write data to read
		UUID uuid = UUID.randomUUID();

		Statement stat = c.createStatement();
		stat.execute("INSERT INTO "+PlayerDatabase.TABLE+" (id, firstName, lastName, email, login, password,lang) VALUES('"+uuid+"','Manfred','Mustermann','test@invalid.de','"+TESTUSER+"','"+PlayerDatabase.encodePassword(TESTPASS)+"'),'de'");
//		stat.execute("INSERT INTO Characters (id, player, rules, name) VALUES('"+UUID.randomUUID()+"','"+uuid+"','SPLITTERMOND','Carimea')");
		stat.close();
		logger.log(Level.DEBUG, "Added player {0}", uuid);

		CharacterHandle handle = new CharacterHandle() {
			@Override
			public Path getPath() {return null;}
		};
		handle.setName("Carimea");
		handle.setRuleIdentifier(RoleplayingSystem.SPLITTERMOND);
		logger.log(Level.DEBUG, "Returned from server: "+handle);
		assertNotNull(handle);
		assertNotNull(handle.getUUID());

		// Ensure character is deleted
		stat = c.createStatement();
		ResultSet set = stat.executeQuery("SELECT * FROM "+CharacterDatabase.TABLE);
		assertTrue(set.next());
		assertEquals(handle.getUUID().toString(), set.getString("id"));
		assertEquals(handle.getName(), set.getString("name"));
		assertEquals("SPLITTERMOND", set.getString("rules"));
	}

	//-----------------------------------------------------------------
	@Test
	void testAddAttachment() throws Exception {
		System.out.println("---testAddAttachment------------------------------------------");
		// Write data to read
		UUID uuid = UUID.randomUUID();

		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password,lang) VALUES('"+uuid+"','Manfred','Mustermann','test@invalid.de','"+TESTUSER+"','"+PlayerDatabase.encodePassword(TESTPASS)+"'),'de'");
		stat.execute("INSERT INTO Characters (id, player, rules, name) VALUES('"+UUID.randomUUID()+"','"+uuid+"','SPLITTERMOND','Carimea')");
		stat.close();

		List<CharacterHandle> handles = charProv.getCharacters();
		assertNotNull(handles);
		assertEquals(1, handles.size());
		CharacterHandle handle = handles.get(0);
		assertNotNull(handle.getUUID());
		logger.log(Level.ERROR, "characterhandle = "+handle.getUUID());

		Attachment attach = new Attachment(handle, UUID.randomUUID(), Type.CHARACTER, Format.RULESPECIFIC);
		attach.setFilename("foo.xml");
		attach.setData("<xml>\n<hello world=\"true\"/>\n</xml>".getBytes(StandardCharsets.UTF_8));
		assertNotNull(attach.getParent(), "attachment not linked with character");
		charProv.createAttachment(handle, attach);
//		Attachment attach = charProv.addAttachment(handle, Type.CHARACTER, Format.RULESPECIFIC, "foo.xml", "<xml>\n<hello world=\"true\"/>\n</xml>".getBytes(StandardCharsets.UTF_8));
		assertNotNull(attach, "No attachment returned");

		// Ensure attachment is created
		stat = c.createStatement();
		ResultSet set = stat.executeQuery("SELECT * FROM "+AttachmentDatabase.TABLE);
		assertTrue(set.next());
		logger.log(Level.DEBUG, "       ID according to DB: "+set.getString("id"));
		logger.log(Level.DEBUG, "Character according to DB: "+set.getString("charac"));
		assertNotNull(set.getString("charac"), "Linked character is NULL");
		assertEquals(attach.getID().toString(), set.getString("id"));

		assertEquals(attach.getParent().getUUID().toString(), set.getString("charac"), "Not linked to correct character");
		assertEquals(attach.getFilename(), set.getString("name"));
		assertEquals("<xml>\n<hello world=\"true\"/>\n</xml>", new String(set.getBlob("data").getBinaryStream().readAllBytes()));
	}

	//-----------------------------------------------------------------
	@Test
	void testListAttachments() throws Exception {
		System.out.println("---testListAttachments------------------------------------------");
		// Write data to read
		UUID uuid = UUID.randomUUID();
		UUID cuuid = UUID.randomUUID();
		UUID auuid = UUID.randomUUID();
		logger.log(Level.DEBUG, "Fill database with player {0}", uuid);
		logger.log(Level.DEBUG, "Fill database with character {0}", cuuid);
		logger.log(Level.DEBUG, "Fill database with attachment {0}", auuid);
		long millis = System.currentTimeMillis();

		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password,lang) VALUES('"+uuid+"','Manfred','Mustermann','test@invalid.de','"+TESTUSER+"','"+PlayerDatabase.encodePassword(TESTPASS)+"'),'de'");
		stat.execute("INSERT INTO Characters (id, player, rules, name) VALUES('"+cuuid+"','"+uuid+"','SPLITTERMOND','Carimea')");
		PreparedStatement pStat = c.prepareStatement("INSERT INTO Attachments (id, charac, type, format, name, data, modified) VALUES (?,?,'CHARACTER','RULESPECIFIC',?,?,?)");
		pStat.setString(1, auuid.toString());
		pStat.setString(2, cuuid.toString());
		pStat.setString(3, "test.xml");
		pStat.setBlob(4, new StringInputStream("<xml>hello</hello>"));
		pStat.setTimestamp(5, new Timestamp(millis));
		pStat.execute();
		pStat.close();
		stat.close();

		List<CharacterHandle> handles = charProv.getCharacters();
		assertNotNull(handles);
		assertEquals(1, handles.size());
		CharacterHandle handle = handles.get(0);
		assertNotNull(handle.getUUID());
		logger.log(Level.DEBUG, "characterhandle = "+handle.getUUID());

		List<Attachment> list = charProv.getAttachments(handle);
		assertNotNull(list, "No attachments returned");
		assertFalse(list.isEmpty());
		assertEquals(1, list.size(), "Should b1 exactly 1 attachment");
		assertEquals(auuid, list.get(0).getID());
		assertNull(list.get(0).getData());
		assertEquals(millis/1000*1000, list.get(0).getLastModified().getTime());
	}

	//-----------------------------------------------------------------
	@Test
	void testGetAttachment() throws Exception {
		System.out.println("---testGetAttachment------------------------------------------");
		// Write data to read
		UUID uuid = UUID.randomUUID();
		UUID cuuid = UUID.randomUUID();
		UUID auuid = UUID.randomUUID();
		long millis = System.currentTimeMillis();
		byte[] data = "<xml>hello</xml>".getBytes("UTF-8");

		Statement stat = c.createStatement();
		logger.log(Level.DEBUG, "Fill database with player {0}", uuid);
		logger.log(Level.DEBUG, "Fill database with character {0}", cuuid);
		logger.log(Level.DEBUG, "Fill database with attachment {0}", auuid);
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password,lang) VALUES('"+uuid+"','Manfred','Mustermann','test@invalid.de','"+TESTUSER+"','"+PlayerDatabase.encodePassword(TESTPASS)+"'),'de'");
		stat.execute("INSERT INTO Characters (id, player, rules, name) VALUES('"+cuuid+"','"+uuid+"','SPLITTERMOND','Carimea')");
		PreparedStatement pStat = c.prepareStatement("INSERT INTO Attachments (id, charac, type, format, name, data, modified) VALUES (?,?,'CHARACTER','RULESPECIFIC',?,?,?)");
		pStat.setString(1, auuid.toString());
		pStat.setString(2, cuuid.toString());
		pStat.setString(3, "test.xml");
		pStat.setBlob(4, new ByteArrayInputStream(data));
		pStat.setTimestamp(5, new Timestamp(millis));
		pStat.execute();
		pStat.close();

		ResultSet set = stat.executeQuery("SELECT * FROM Characters");
		logger.log(Level.DEBUG, "set {0}", set);
		while (set.next()) {
			logger.log(Level.DEBUG, "...player {0} = {1}", set.getString("player"), set.getString("id"));
		}

		stat.close();

		List<CharacterHandle> handles = charProv.getCharacters();
		assertNotNull(handles);
		assertEquals(1, handles.size(), "Did not find attachment");
		CharacterHandle handle = handles.get(0);
		assertNotNull(handle.getUUID());
		assertEquals(cuuid, handle.getUUID(), "Returned character UUID does not match added");

		byte[] ret = charProv.getAttachmentData(handle, new Attachment(handle, auuid, null, null));
		assertNotNull(ret, "No attachment returned");
//		assertNotNull(ret.getData());
		assertEquals(data.length, ret.length, "Data lengths differ");
		assertTrue(Arrays.equals(data, ret));
	}

	//-----------------------------------------------------------------
	@Test
	void testModifyAttachment() throws Exception {
		System.out.println("---testModifyAttachment------------------------------------------");
		// Write data to read
		UUID uuid = UUID.randomUUID();
		UUID cuuid = UUID.randomUUID();
		UUID auuid = UUID.randomUUID();
		long millis = System.currentTimeMillis();
		byte[] data = "<xml>hello</xml>".getBytes("UTF-8");
		byte[] data2 = "<xml>Höllo</xml>".getBytes("UTF-8");

		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password,lang) VALUES('"+uuid+"','Manfred','Mustermann','test@invalid.de','"+TESTUSER+"','"+PlayerDatabase.encodePassword(TESTPASS)+"'),'de'");
		stat.execute("INSERT INTO Characters (id, player, rules, name) VALUES('"+cuuid+"','"+uuid+"','SPLITTERMOND','Carimea')");
		PreparedStatement pStat = c.prepareStatement("INSERT INTO Attachments (id, charac, type, format, name, data, modified) VALUES (?,?,'CHARACTER','RULESPECIFIC',?,?,?)");
		pStat.setString(1, auuid.toString());
		pStat.setString(2, cuuid.toString());
		pStat.setString(3, "test.xml");
		pStat.setBlob(4, new ByteArrayInputStream(data));
		pStat.setTimestamp(5, new Timestamp(millis));
		pStat.execute();
		pStat.close();
		stat.close();

		List<CharacterHandle> handles = charProv.getCharacters();
		assertNotNull(handles);
		assertEquals(1, handles.size());
		CharacterHandle handle = handles.get(0);
		assertNotNull(handle.getUUID());

		logger.log(Level.INFO, "auuid = "+auuid);
		Attachment at = new Attachment(handle, auuid, null, null);
		byte[] bytes = charProv.getAttachmentData(handle, at);
		assertNotNull(at, "No attachment returned");
//		assertNotNull(at.getData());
//		assertEquals(at.getID(), auuid);
		assertTrue(Arrays.equals(data, bytes));

		// Now modify
		at.setData(data2);
		at.setFilename("neu.xml");
		logger.log(Level.INFO, "Send = "+at);
		charProv.modifyAttachment(handle, at);

		stat = c.createStatement();
		ResultSet set = stat.executeQuery("SELECT * FROM "+AttachmentDatabase.TABLE);
		assertTrue(set.next());
		assertEquals("neu.xml", set.getString("name"));
		String stored = new String( set.getBytes("data"), StandardCharsets.UTF_8);
		assertEquals("<xml>Höllo</xml>", stored);

		// Get again
		bytes = charProv.getAttachmentData(handle, at);
		assertNotNull(at, "No attachment returned");
//		assertNotNull(at.getData());
//		assertFalse(Arrays.equals(data, at.getData()), "Data not updated");
		assertTrue(Arrays.equals(data2, at.getData()));

	}

	//-----------------------------------------------------------------
	@Test
	void testDeleteAttachment() throws Exception {
		System.out.println("---testDeleteAttachment------------------------------------------");
		// Write data to read
		UUID uuid = UUID.randomUUID();
		UUID cuuid = UUID.randomUUID();
		UUID auuid = UUID.randomUUID();
		long millis = System.currentTimeMillis();
		byte[] data = "<xml>hello</xml>".getBytes("UTF-8");

		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password,lang) VALUES('"+uuid+"','Manfred','Mustermann','test@invalid.de','"+TESTUSER+"','"+PlayerDatabase.encodePassword(TESTPASS)+"'),'de'");
		stat.execute("INSERT INTO Characters (id, player, rules, name) VALUES('"+cuuid+"','"+uuid+"','SPLITTERMOND','Carimea')");
		PreparedStatement pStat = c.prepareStatement("INSERT INTO Attachments (id, charac, type, format, name, data, modified) VALUES (?,?,'CHARACTER','RULESPECIFIC',?,?,?)");
		pStat.setString(1, auuid.toString());
		pStat.setString(2, cuuid.toString());
		pStat.setString(3, "test.xml");
		pStat.setBlob(4, new ByteArrayInputStream(data));
		pStat.setTimestamp(5, new Timestamp(millis));
		pStat.execute();
		pStat.close();
		stat.close();

		List<CharacterHandle> handles = charProv.getCharacters();
		CharacterHandle handle = handles.get(0);

		List<Attachment> list = charProv.getAttachments(handle);
		assertEquals(1, list.size());

		logger.log(Level.DEBUG, "now delete");
		charProv.deleteAttachment(handle, list.get(0));
		list = charProv.getAttachments(handle);
		assertEquals(0, list.size());

		// Ensure attachment is deleted
		stat = c.createStatement();
		ResultSet set = stat.executeQuery("SELECT * FROM "+AttachmentDatabase.TABLE);
		assertFalse(set.next());
	}

	//-----------------------------------------------------------------
	UUID prepareDatabase() throws SQLException {
		BackendAccess backend = BackendAccess.getInstance();
		CatalogItem catalog1 = backend.getShopDatabase().add(new CatalogItem("SpliMo_Bundle1", Category.CHARGEN_FVTT, RoleplayingSystem.SPLITTERMOND, "de", "Einsteigerbundle", 1995, State.AVAILABLE, "CORE","MSK"));
		CatalogItem catalog2 = backend.getShopDatabase().add(new CatalogItem("Die Magie"     , Category.CHARGEN     , RoleplayingSystem.SPLITTERMOND, "de", "Die Magie"       ,  995, State.AVAILABLE, "MAGIE"));
		CatalogItem catalog3 = backend.getShopDatabase().add(new CatalogItem("Grundregelwerk", Category.CHARGEN     , RoleplayingSystem.SHADOWRUN6  , "de", "Grundregelwerk"  , 2000, State.AVAILABLE, "CORE"));
		CatalogItem catalog4 = backend.getShopDatabase().add(new CatalogItem("Arkane Kräfte" , Category.CHARGEN     , RoleplayingSystem.SHADOWRUN6  , "de", "Arkane Kräfte"   , 2000, State.AVAILABLE, "SWYRD"));

		PlayerImpl player = backend.getPlayerDatabase().createPlayer("Manfred", "Mustermann", "test@invalid.de", TESTUSER, TESTPASS, Locale.GERMAN);

		BoughtItem bought1 = new BoughtItem(player.getUuid(), catalog1.getId(), Instant.now(), 2000);
		BoughtItem bought2 = new BoughtItem(player.getUuid(), catalog2.getId(), Instant.now(), 2000);
		BoughtItem bought3 = new BoughtItem(player.getUuid(), catalog3.getId(), Instant.now(), 2000);

		backend.getLicenseDatabase().add(bought1);
		backend.getLicenseDatabase().add(bought2);
		backend.getLicenseDatabase().add(bought3);

		return player.getUuid();
	}

}
