package de.rpgframework.eden.client.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.api.EdenPingInfo;
import de.rpgframework.eden.api.EdenStatus;
import de.rpgframework.eden.base.MailerLoader;
import de.rpgframework.eden.client.EdenAPIException;
import de.rpgframework.eden.client.EdenCodes;
import de.rpgframework.eden.client.EdenConnection;
import de.rpgframework.eden.logic.BackendAccess;
import de.rpgframework.reality.server.EdenAPIServer;
import de.rpgframework.reality.server.PlayerImpl;

public class LoginTest {

	protected final static Logger logger = System.getLogger("ClientTest");

	protected static EdenAPIServer server;
	protected static EdenConnection con;
	private BackendAccess backend;
	private Connection c;
	private static DummyMailSender mailSender;

	//-------------------------------------------------------------------
	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
		logger.log(Level.INFO, "setUpBeforeClass");
		server = ServerArgumentsProvider.getServer();
		mailSender = new DummyMailSender();
		MailerLoader.setInstance(mailSender);
		con = new EdenConnection("http","localhost", server.getPort(), "UnitTest", "1.0");

	}

	//-------------------------------------------------------------------
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		logger.log(Level.INFO, "tearDownAfterClass");
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		backend = new BackendAccess("jdbc:hsqldb:mem:testdb", "SA","");
		c = backend.getInternalConnection();
	}

	//-----------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
//		System.out.println("tearDown--------------------------------");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS Attachments");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS Characters");
		c.createStatement().executeUpdate("DROP TABLE IF EXISTS BoughtItems");
		c.createStatement().executeUpdate("DROP TABLE IF EXiSTS Player");
		c.close();
	}

	//-------------------------------------------------------------------
	@Test
	void testConnect() {
		logger.log(Level.INFO, "testConnect");
		EdenPingInfo info = con.getInfo();
		assertNotNull(info, "Connection failed");
		System.out.println("EdenPingInfo = "+info);
	}

	//-------------------------------------------------------------------
	@Test
	void testLogin() throws SQLException {
		logger.log(Level.INFO, "testLogin");
		UUID uuid = UUID.randomUUID();
		Statement stat = c.createStatement();
		stat.execute("INSERT INTO Player (id, firstName, lastName, email, login, password, lang) VALUES('"+uuid+"','Manfred','Müstermann','test@invalid.de','manni','XYlRXHvA4QbSwRVG5szjdg==','de')");
		stat.close();

		// Try getting info without logging in
		try {
			con.getAccountInfo();
			fail("Missing credentials not detected");
		} catch (EdenAPIException e) {
			assertEquals(EdenCodes.UNAUTHORIZED, e.getCode(), "Expected UNAUTHORIZED");
		} catch (Exception e) {
			fail("Unexpected error: "+e);
		}

		con.login("manni", "falsch", null);
		try {
			con.getAccountInfo();
			fail("Invalid credentials not detected");
		} catch (EdenAPIException e) {
			assertEquals(EdenCodes.UNAUTHORIZED, e.getCode(), "Expected UNAUTHORIZED");
		} catch (Exception e) {
			fail("Unexpected error: "+e);
		}

		con.login("manni", "muster", null);
		try {
			EdenAccountInfo info = con.getAccountInfo();
			assertNotNull(info);
			assertEquals("Müstermann", info.getLastName());
			assertFalse(info.isVerified());
		} catch (Exception e) {
			fail("Unexpected error: "+e);
		}
	}

	//-------------------------------------------------------------------
	@Test
	void testCreatePlayer() throws SQLException {
		logger.log(Level.INFO, "testCreatePlayer");

		// Create with invalid email address
		try {
			DummyMailSender.addResult("blume@localhost", "Kaputt");
			con.createAccount("ghibli", "blume@localhost", "myPass", "Studio", "Ghibli", Locale.JAPAN);
		} catch (EdenAPIException e) {
			assertEquals(EdenStatus.MAIL_ERROR.code(), e.getCode(), "Expected MAIL_ERROR");
		} catch (Exception e) {
			fail("Unexpected error: "+e);
			e.printStackTrace();
		}

		// Create with invalid email address
		try {
			DummyMailSender.clear();
			EdenAccountInfo info =con.createAccount("ghibli", "blume@valid.de", "myPass", "Studio", "Ghibli", Locale.JAPAN);
			assertNotNull(DummyMailSender.getText("blume@valid.de"));
			assertNotNull(DummyMailSender.getHeader("blume@valid.de", "X-Dump"), "Missing verification code");
			assertNotNull(info);

			// Verify database
			Statement stat = c.createStatement();
			ResultSet set = stat.executeQuery("SELECT * FROM Player");
			assertTrue(set.next());
			assertEquals("Studio", set.getString("firstName"));
			assertEquals(false, set.getBoolean("verified"));
			assertFalse(set.next());
			set.close();
			stat.close();
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unexpected error: "+e);
		}

		// Get verification code sent via fake email
		String code = DummyMailSender.getHeader("blume@valid.de", "X-Dump");

		try {
			con.login("ghibli", "myPass", null);
			con.verifyAccount(code);

			// Verify database
			Statement stat = c.createStatement();
			ResultSet set = stat.executeQuery("SELECT * FROM Player");
			assertTrue(set.next());
			assertEquals("Studio", set.getString("firstName"));
			assertEquals(true, set.getBoolean("verified"));
			assertFalse(set.next());
			set.close();
			stat.close();
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unexpected error: "+e);
		}
	}

}
