package de.rpgframework.eden.client.test;

import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import de.rpgframework.reality.server.EdenAPIServer;

/**
 * @author prelle
 *
 */
public class ServerArgumentsProvider implements ArgumentsProvider {

	private static EdenAPIServer server;

	//-------------------------------------------------------------------
	public static void setInstance(EdenAPIServer server) {
		ServerArgumentsProvider.server = server;
	}
	public static EdenAPIServer getServer() { return server; }

	//-------------------------------------------------------------------
	/**
	 * @see org.junit.jupiter.params.provider.ArgumentsProvider#provideArguments(org.junit.jupiter.api.extension.ExtensionContext)
	 */
	@Override
	public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
		System.out.println("ServerArgumentsProvider: "+context);
		return Stream.of(Arguments.of(server.getPort()));
	}

}
