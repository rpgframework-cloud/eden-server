package de.rpgframework.eden.client.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.charset.Charset;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.rpgframework.character.Attachment;
import de.rpgframework.character.Attachment.Format;
import de.rpgframework.character.Attachment.Type;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterIOException;
import de.rpgframework.character.CharacterIOException.ErrorCode;
import de.rpgframework.character.CharacterProvider;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.client.SyncingCharacterProvider;

/**
 * @author prelle
 *
 */
public class LocalCharacterProviderTest {

	private static Logger logger = System.getLogger("ClientTest");

	private static Path playerDir;
	private static CharacterProvider charProv;

	//-------------------------------------------------------------------
	@BeforeEach
	void setup() throws Exception {
		playerDir = Files.createTempDirectory("unittest");
		charProv = new SyncingCharacterProvider<>(playerDir, null, RoleplayingSystem.CORIOLIS);
	}

	//-------------------------------------------------------------------
	@AfterEach
	void tearDown() throws Exception {
		logger.log(Level.DEBUG, "Delete {0}", playerDir.toAbsolutePath().toString());

		Files.walkFileTree(playerDir, new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Set<PosixFilePermission> ownerWritable = PosixFilePermissions.fromString("rw-rw-rw-");
				Files.setPosixFilePermissions(file, ownerWritable);

				Files.delete(file);
//				logger.log(Level.DEBUG, "Deleted {}",file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				Set<PosixFilePermission> WRITEALL = PosixFilePermissions.fromString("rwxrwxrwx");
				Files.setPosixFilePermissions(dir, WRITEALL);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				Set<PosixFilePermission> WRITEALL = PosixFilePermissions.fromString("rwxrwxrwx");
				Files.setPosixFilePermissions(dir, WRITEALL);
				Files.delete(dir);
//				logger.log(Level.DEBUG, "Deleted dir {}",dir);
				return FileVisitResult.CONTINUE;
			}
		});
//		try (Stream<Path> walk = Files.walk(playerDir)) {
//		    walk.sorted(Comparator.reverseOrder())
//		        .map(Path::toFile)
//		        .peek(c -> logger.log(Level.DEBUG, "Delete {}",c))
//		        .forEach(File::delete);
//		}
//		Files.deleteIfExists(playerDir);
	}

	//-----------------------------------------------------------------
	@Test
	void testCreateWithoutAttachments() throws Exception {
		logger.log(Level.DEBUG, "testCreateWithoutAttachments--------------------------\n\n");
		CharacterHandle handle = charProv.createCharacter("Carimea Nachname", RoleplayingSystem.SPLITTERMOND);
		logger.log(Level.DEBUG, "Returned from server: "+handle);
		assertNotNull(handle);
		assertNotNull(handle.getUUID());
		assertEquals("Carimea Nachname", handle.getName());
		assertEquals(RoleplayingSystem.SPLITTERMOND, handle.getRuleIdentifier());

		assertNotNull(playerDir);
		assertNotNull(playerDir.resolve("myself"));
		assertNotNull(playerDir.resolve("myself").resolve("splittermond"));
		assertNotNull(playerDir.resolve("myself").resolve("splittermond").resolve(handle.getUUID().toString()));
		Path tmp = playerDir.resolve("myself").resolve("splittermond").resolve(handle.getUUID().toString());
		logger.log(Level.DEBUG, "Expect char dir at "+tmp);
		assertTrue(Files.exists(tmp), "No directory for char created");

		Path meta = tmp.resolve("metadata.properties");
		assertTrue(Files.exists(meta), "No metadata created");

		Properties pro = new Properties();
		pro.load(Files.newInputStream(meta));
		String uuid = pro.getProperty("uuid");
		String sync = pro.getProperty("sync");
		assertNotNull(uuid, "No UUID in metadata");
		assertNotNull(sync, "No Sync in metadata");
		// Ensure valid UUID
		UUID.fromString(uuid);
		Boolean.parseBoolean(sync);
	}

//	//-----------------------------------------------------------------
//	@Test
//	void testDoubleCreate() throws Exception {
//		charProv.createCharacter("Carimea Nachname", RoleplayingSystem.SPLITTERMOND);
//		try {
//			// The character already exists - this should not work
//			charProv.createCharacter("Carimea Nachname", RoleplayingSystem.SPLITTERMOND);
//			fail("Double creation not detected");
//		} catch (CharacterIOException e) {
//			assertEquals(ErrorCode.CHARACTER_WITH_THAT_NAME_EXISTS, e.getCode());
//		}
//	}

	//-----------------------------------------------------------------
	@Test
	void testDetectReadOnly() throws Exception {
		Set<PosixFilePermission> ownerWritable = PosixFilePermissions.fromString("---------");
		Files.setPosixFilePermissions(playerDir, ownerWritable);
		try {
			charProv.createCharacter("Carimea Nachname", RoleplayingSystem.SPLITTERMOND);
			fail("Missing permissions not detected");
		} catch (CharacterIOException e) {
			ownerWritable = PosixFilePermissions.fromString("r-xr-xr-x");
			Files.setPosixFilePermissions(playerDir, ownerWritable);
			assertEquals(ErrorCode.FILESYSTEM_WRITE, e.getCode());
		} finally {
			ownerWritable = PosixFilePermissions.fromString("r-xr-xr-x");
			Files.setPosixFilePermissions(playerDir, ownerWritable);
		}
	}

	//-----------------------------------------------------------------
	@Test
	void testCreateWithAttachments() throws Exception {
		CharacterHandle handle = charProv.createCharacter("Carimea Nachname", RoleplayingSystem.SPLITTERMOND);
		logger.log(Level.DEBUG, "Returned from server: "+handle);
		assertNotNull(handle);
		assertNotNull(handle.getUUID());
		assertEquals("Carimea Nachname", handle.getName());
		assertEquals(RoleplayingSystem.SPLITTERMOND, handle.getRuleIdentifier());
//		CharacterHandle handle = testCreateWithoutAttachments();

		byte[] content = ("Some content äöüß").getBytes(Charset.defaultCharset());
		Attachment attach = charProv.addAttachment(handle, Type.CHARACTER, Format.TEXT, "suggested" , content);
		assertNotNull(attach);
		assertNotNull(attach.getID());

		// Ensure attachment is present and not 0 bytes
		Path file = playerDir.resolve("myself").resolve("splittermond").resolve(handle.getUUID().toString()).resolve(handle.getName()+".txt");
		assertTrue(Files.exists(file), "No attachment created");
		assertEquals(content.length, Files.size(file), "File length mismatch");

		// Ensure metadata exists
		Path meta = file.getParent().resolve("metadata.properties");
		assertTrue(Files.exists(meta), "No metadata created");

		// .. and are parseable
		Properties pro = new Properties();
		pro.load(Files.newInputStream(meta));
		// .. contain the expected values
		String type   = pro.getProperty("attachment."+attach.getID()+".type");
		String format = pro.getProperty("attachment."+attach.getID()+".format");
		String fileS  = pro.getProperty("attachment."+attach.getID()+".file");
		assertNotNull(type);
		assertNotNull(format);
		assertNotNull(fileS);
		assertEquals("CHARACTER", type);
		assertEquals("TEXT", format);
		assertEquals(handle.getName()+".txt", fileS);
	}

}
