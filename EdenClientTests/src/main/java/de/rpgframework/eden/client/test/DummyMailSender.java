package de.rpgframework.eden.client.test;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.rpgframework.eden.base.Mailer;

/**
 *
 */
public class DummyMailSender implements Mailer {

	private final static Logger logger = System.getLogger(DummyMailSender.class.getPackageName());

	private static Map<String, String> errorsByToAddr = new HashMap<>();
	private static Map<String, String> textByToAddr = new HashMap<>();
	private static Map<String, Map<String,String>> headersByToAddr = new HashMap<>();

	//-------------------------------------------------------------------
	/**
	 */
	public DummyMailSender() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public static void addResult(String toAddr, String error) {
		errorsByToAddr.put(toAddr, error);
		logger.log(Level.DEBUG, "Configured {0} to return error {1}", new Object[] {toAddr, error});
	}

	//-------------------------------------------------------------------
	public static void clear() {
		errorsByToAddr.clear();
		textByToAddr.clear();
		headersByToAddr.clear();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.eden.base.Mailer#send(java.lang.String, java.util.List, java.util.List, java.lang.String, java.lang.String, java.lang.String, de.rpgframework.eden.base.Mailer.MimeBody[])
	 */
	@Override
	public String send(String fromAddr, List<String> toAddr, List<String[]> header, String subject, String text,
			String html, MimeBody... attachments) {
		for (String to : toAddr) {
			logger.log(Level.INFO, "Send mail to: "+to+"\n"+html);
			if (errorsByToAddr.containsKey(to))
				return errorsByToAddr.get(to);
			textByToAddr.put(to, text);
			Map<String,String> map = new HashMap<>();
			for (String[] head : header) {
				map.put(head[0], head[1]);
			}
			headersByToAddr.put(to, map);
			logger.log(Level.DEBUG, "  headers: "+map);
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static String getText(String toAddr) {
		return textByToAddr.get(toAddr);
	}

	//-------------------------------------------------------------------
	public static String getHeader(String toAddr, String head) {
		if (!headersByToAddr.containsKey(toAddr))
			return null;
		Map<String,String> map = headersByToAddr.get(toAddr);
		if (map==null)
			throw new NullPointerException("No results for "+toAddr);
		return map.get(head);
	}

}
