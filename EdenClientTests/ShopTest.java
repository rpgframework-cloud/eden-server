package de.rpgframework.eden.client.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.lang.System.Logger.Level;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.client.EdenAPIException;
import de.rpgframework.eden.logic.ActivationKeyDatabase;
import de.rpgframework.reality.ActivationKey;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.CatalogItem;
import de.rpgframework.reality.CatalogItem.Category;
import de.rpgframework.reality.CatalogItem.State;
import de.rpgframework.reality.server.PlayerImpl;

/**
 * @author prelle
 *
 */
public class ShopTest extends CommonClientTest {

	//-----------------------------------------------------------------
	UUID prepareDatabase() throws SQLException {
		CatalogItem catalog1 = backend.getShopDatabase().add(new CatalogItem("SpliMo_Bundle1", Category.CHARGEN_FVTT, RoleplayingSystem.SPLITTERMOND, "de", "Einsteigerbundle", 1995, State.AVAILABLE, "CORE","MSK"));
		CatalogItem catalog2 = backend.getShopDatabase().add(new CatalogItem("Die Magie"     , Category.CHARGEN     , RoleplayingSystem.SPLITTERMOND, "de", "Die Magie"       ,  995, State.AVAILABLE, "MAGIE"));
		CatalogItem catalog3 = backend.getShopDatabase().add(new CatalogItem("Grundregelwerk", Category.CHARGEN     , RoleplayingSystem.SHADOWRUN6  , "de", "Grundregelwerk"  , 2000, State.AVAILABLE, "CORE"));
		CatalogItem catalog4 = backend.getShopDatabase().add(new CatalogItem("Arkane Kräfte" , Category.CHARGEN     , RoleplayingSystem.SHADOWRUN6  , "de", "Arkane Kräfte"   , 2000, State.AVAILABLE, "SWYRD"));

		PlayerImpl player = backend.getPlayerDatabase().createPlayer("Manfred", "Mustermann", "test@invalid.de", TESTUSER, TESTPASS, Locale.GERMAN);

		BoughtItem bought1 = new BoughtItem(player.getUuid(), catalog1.getId(), Instant.now(), 2000);
		BoughtItem bought2 = new BoughtItem(player.getUuid(), catalog2.getId(), Instant.now(), 2000);
		BoughtItem bought3 = new BoughtItem(player.getUuid(), catalog3.getId(), Instant.now(), 2000);

		backend.getLicenseDatabase().add(bought1);
		backend.getLicenseDatabase().add(bought2);
		backend.getLicenseDatabase().add(bought3);

		backend.getActivationKeyDatabase().add(new ActivationKey(UUID.randomUUID(), catalog4.getId(), Instant.now().minusSeconds(300)));

		return player.getUuid();
	}

	//-----------------------------------------------------------------
	@Test
	void testList() throws Exception {
		prepareDatabase();


		List<BoughtItem> items = eden.getShopClient().listBought(null, "de");
		logger.log(Level.INFO, "Got "+items);
		assertNotNull(items);
		assertEquals(3, items.size(), "Expected non-empty list");


		items = eden.getShopClient().listBought(RoleplayingSystem.SPLITTERMOND, "de");
		logger.log(Level.INFO, "Got "+items);
		assertNotNull(items);
		assertEquals(2, items.size(), "Expected non-empty list");
	}

	//-----------------------------------------------------------------
	@Test
	void testListPlugins() throws Exception {
		prepareDatabase();


		List<String> items = eden.getShopClient().listPlugins(null, "de");
		logger.log(Level.INFO, "Got "+items);
		assertNotNull(items);
		assertEquals(3, items.size(), "Expected non-empty list");
		assertTrue(items.contains("CORE"));
		assertTrue(items.contains("MSK"));
		assertTrue(items.contains("MAGIE"));

		items = eden.getShopClient().listPlugins(RoleplayingSystem.SPLITTERMOND, "de");
		logger.log(Level.INFO, "Got "+items);
		assertNotNull(items);
		assertEquals(3, items.size(), "Expected non-empty list");


		items = eden.getShopClient().listPlugins(RoleplayingSystem.SHADOWRUN6, "de");
		logger.log(Level.INFO, "Got "+items);
		assertNotNull(items);
		assertTrue(items.contains("CORE"));
		assertFalse(items.contains("MSK"));
		assertFalse(items.contains("MAGIE"));
		assertEquals(1, items.size(), "Expected non-empty list");
	}

	//-----------------------------------------------------------------
	@Test
	void testUseActivationKey() throws Exception {
		prepareDatabase();

		Statement stat = c.createStatement();
		ResultSet set = stat.executeQuery("SELECT id FROM "+ActivationKeyDatabase.TABLE+" LIMIT 1");
		assertTrue(set.next());
		UUID actKey = UUID.fromString(set.getString(1));
		set.close();

		// Ensure plugin has not been bought
		List<BoughtItem> items = eden.getShopClient().listBought(RoleplayingSystem.SHADOWRUN6, "de");
		assertNotNull(items);
		assertEquals(1, items.size(), "Expected non-empty list");

		BoughtItem bought = eden.getShopClient().useActivationKey(actKey);
		assertNotNull(bought);
		assertEquals(actKey, bought.getLicenseKey());

		// Ensure activation key has been used
		set = stat.executeQuery("SELECT * FROM "+ActivationKeyDatabase.TABLE+" WHERE id='"+actKey+"'");
		assertFalse(set.next(), "Used activation key is still in database");

		// Ensure plugin has been bought
		items = eden.getShopClient().listBought(RoleplayingSystem.SHADOWRUN6, "de");
		logger.log(Level.INFO, "Got "+items);
		assertNotNull(items);
		assertEquals(2, items.size(), "Expected non-empty list");

		// Ensure that re-using the key fails
		try {
			eden.getShopClient().useActivationKey(actKey);
			fail("Rebuying shouldn't be possible");
		} catch (EdenAPIException e) {
			assertEquals(403, e.getCode(), e.toString());
		}
	}


}
