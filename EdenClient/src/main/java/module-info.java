module de.rpgframework.eden.client {
	exports de.rpgframework.eden.client;
	opens   de.rpgframework.eden.client;

	requires java.sql;
	requires com.google.gson;
	requires de.rpgframework.core;
	requires transitive de.rpgframework.rules;
	requires simple.persist;
	requires transitive java.net.http;
	requires jdk.httpserver;
	requires transitive eden.api;

}