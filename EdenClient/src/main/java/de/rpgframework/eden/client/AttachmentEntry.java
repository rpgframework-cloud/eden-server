package de.rpgframework.eden.client;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.character.Attachment.Format;
import de.rpgframework.character.Attachment.Type;

public class AttachmentEntry {
	@Attribute(required=true)
	public String file;
	@Attribute(required=false)
	public Type type;
	@Attribute(required=false)
	public Format format;

	//-------------------------------------------------------------------
	public AttachmentEntry() {}

	//-------------------------------------------------------------------
	public AttachmentEntry(String fname, Type type, Format format) {
		this.file = fname;
		this.type = type;
		this.format= format;
	}
}