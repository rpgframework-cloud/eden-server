package de.rpgframework.eden.client;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterProviderLoader;
import de.rpgframework.eden.client.EdenConnection.StateFlag;

/**
 * @author prelle
 *
 */
public class SynchronizeCharactersStep implements OnlineStartupStep, EdenClientConstants {

	protected static Logger logger = System	.getLogger(SynchronizeCharactersStep.class.getPackageName());

	private EdenConnection eden;

	//-------------------------------------------------------------------
	public SynchronizeCharactersStep() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.StartupStep#updateConnection(de.rpgframework.eden.client.EdenConnection)
	 */
	@Override
	public void updateConnection(EdenConnection con) {
		this.eden = con;
		logger.log(Level.INFO, "Set EdenConnection({0})",eden.getStateFlags());
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		logger.log(Level.TRACE, "ENTER {0}",getClass().getSimpleName());
		try {
			if (eden.isOffline()) {
				logger.log(Level.WARNING, "Offline - don't synchronize");
				return;
			}
			if (!eden.hasStateFlag(StateFlag.ACCOUNT_VERIFIED)) {
				logger.log(Level.WARNING, "Account not verified yet - don't synchronize");
				return;
			}
			SyncingCharacterProvider<CharacterHandle> prov = (SyncingCharacterProvider<CharacterHandle>) CharacterProviderLoader.getCharacterProvider();
			if (prov!=null) {
				logger.log(Level.DEBUG, "Start synchronization");
				prov.start();
			}
		} finally {
			logger.log(Level.TRACE, "LEAVE {0}",getClass().getSimpleName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.StartupStep#canRun()
	 */
	@Override
	public boolean canRun() {
		return eden!=null && !eden.isOffline() && eden.hasStateFlag(StateFlag.ACCOUNT_VERIFIED);
	}

}
