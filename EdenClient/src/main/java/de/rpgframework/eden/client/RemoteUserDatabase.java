package de.rpgframework.eden.client;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.URL;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.rpgframework.character.Attachment;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterIOException;
import de.rpgframework.character.DatasetDefinition;
import de.rpgframework.character.FileBasedCharacterHandle;
import de.rpgframework.character.IUserDatabase;
import de.rpgframework.character.CharacterIOException.ErrorCode;

/**
 *
 */
public class RemoteUserDatabase implements IUserDatabase {

	private final static Logger logger = System.getLogger(RemoteUserDatabase.class.getPackageName());

	private final static String SCHEME = "https://";
	private final static String API_PATH = "api/";

	private EdenConnection eden;

	//-------------------------------------------------------------------
	/**
	 */
	public RemoteUserDatabase(EdenConnection con) {
		this.eden  = con;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#getCharacters()
	 */
	@Override
	public List<CharacterHandle> getCharacters() throws IOException {
		URL url = new URL(String.format(SCHEME+"%s/"+API_PATH+"character/", eden.getHostPort()));
		logger.log(Level.INFO, "open GET "+url);
		HttpResponse<String> response = eden.sendGET(url);
		int code = response.statusCode(); //con.getResponseCode();

		if (code!=200)
			throw new EdenAPIException(code, "Failed listing character: code "+code);

		//logger.log(Level.DEBUG, "Cookie: "+con.getHeaderField("Set-Cookie"));
		java.lang.reflect.Type typeOfObjectsList = new TypeToken<ArrayList<FileBasedCharacterHandle>>() {}.getType();
		List<CharacterHandle> handles = new ArrayList<CharacterHandle>();
		handles = (new Gson()).fromJson(response.body(), typeOfObjectsList);
		return handles;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#storeCharacter(de.rpgframework.character.CharacterHandle)
	 */
	@Override
	public void createCharacter(CharacterHandle handle) throws IOException {
		logger.log(Level.DEBUG, "ENTER createCharacter({0})", handle.getName());
		try {
			URL url = new URL(String.format(SCHEME+"%s/"+API_PATH+"character", eden.getHostPort()));
			logger.log(Level.INFO, "storeCharacter: "+url);
			logger.log(Level.INFO, "  Name "+handle.getName());
			logger.log(Level.INFO, "  UUID "+handle.getUUID());

			Gson gson = (new Gson());
			String json = gson.toJson(handle);
			logger.log(Level.INFO, "  JSON "+json);

			HttpResponse<String> response = eden.sendPOST(url, json);
			json = response.body();
			UUID uuid = (new Gson()).fromJson(json, UUID.class);
			if (handle.getUUID()==null) {
				handle.setUUID(uuid);
			} else if (!handle.getUUID().equals(uuid)) {
				throw new IllegalArgumentException("Remote and local UUIDs differ");
			}
		} finally {
			logger.log(Level.DEBUG, "LEAVE createCharacter({0})", handle.getName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#storeCharacter(de.rpgframework.character.CharacterHandle)
	 */
	@Override
	public void modifyCharacter(CharacterHandle handle) throws IOException {
		logger.log(Level.DEBUG, "ENTER modifyCharacter({0})", handle.getName());
		try {
			URL url = new URL(String.format(SCHEME+"%s/"+API_PATH+"character", eden.getHostPort()));
			logger.log(Level.INFO, "storeCharacter: "+url);

			Gson gson = (new Gson());
			String json = gson.toJson(handle);
			logger.log(Level.INFO, "  JSON "+json);

			HttpResponse<String> response = eden.sendPUT(url, json);
			json = response.body();
			UUID uuid = (new Gson()).fromJson(json, UUID.class);
			if (handle.getUUID()==null) {
				handle.setUUID(uuid);
			} else if (!handle.getUUID().equals(uuid)) {
				throw new IllegalArgumentException("Remote and local UUIDs differ");
			}
		} finally {
			logger.log(Level.DEBUG, "LEAVE modifyCharacter({0})", handle.getName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#deleteCharacter(de.rpgframework.character.CharacterHandle)
	 */
	@Override
	public void deleteCharacter(CharacterHandle handle) throws IOException {
		logger.log(Level.DEBUG, "ENTER deleteCharacter({0})", handle.getName());
		try {
			URL url = new URL(String.format(SCHEME+"%s/"+API_PATH+"character/%s", eden.getHostPort(), handle.getUUID().toString()));

			eden.sendDELETE(url);
		} finally {
			logger.log(Level.DEBUG, "LEAVE deleteCharacter({0})", handle.getName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#retrieveCharacter(java.util.UUID)
	 */
	@Override
	public CharacterHandle retrieveCharacter(UUID key) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#createAttachment(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment.Type, de.rpgframework.character.Attachment.Format, java.lang.String)
	 */
	@Override
	public void createAttachment(CharacterHandle handle, Attachment attach) throws IOException {
		URL url = new URL(String.format(SCHEME+"%s/"+API_PATH+"character/%s/attachment", eden.getHostPort(), handle.getUUID().toString()));
		logger.log(Level.INFO, "addAttachment POST "+url);

		try {
			Gson gson = (new Gson());
			String json = gson.toJson(attach);
			logger.log(Level.INFO, "  JSON "+json);

			UUID uuid = eden.sendPOST(url, json, UUID.class);
			attach.setID(uuid);
			attach.setParent(handle);
		} catch (IOException e) {
			logger.log(Level.ERROR, "Error adding attachment: "+e);
			throw e;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error adding attachment: "+url,e);
			throw new IOException(e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#getAttachments(de.rpgframework.character.CharacterHandle)
	 */
	@Override
	public List<Attachment> getAttachments(CharacterHandle handle) throws IOException {
		List<Attachment> handles = new ArrayList<Attachment>();
		logger.log(Level.DEBUG, "ENTER: getAttachments({0})", handle.getName());
		try {
			URL url = new URL(String.format(SCHEME+"%s/"+API_PATH+"character/%s/attachment", eden.getHostPort(),  handle.getUUID().toString()));
			HttpResponse<String> response = eden.sendGET(url);
			int code = response.statusCode(); //con.getResponseCode();

			if (code!=200)
				throw new CharacterIOException(ErrorCode.SERVER_ERROR, "Failed listing attachments: "+code);

			java.lang.reflect.Type typeOfObjectsList = new TypeToken<ArrayList<Attachment>>() {}.getType();
			handles = (new Gson()).fromJson(response.body(), typeOfObjectsList);
			return handles;
		} finally {
			logger.log(Level.DEBUG, "LEAVE: getAttachments({0})", handle.getName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#modifyAttachment(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment)
	 */
	@Override
	public void modifyAttachment(CharacterHandle handle, Attachment attach) throws IOException {
		URL url = new URL(String.format(SCHEME+"%s/"+API_PATH+"character/%s/attachment/%s", eden.getHostPort(), handle.getUUID().toString(), attach.getID()));
		logger.log(Level.DEBUG, "ENTER: modifyAttachment({0}, {1})", handle.getName(), attach.getFilename());

		try {
			Gson gson = (new Gson());
			String json = gson.toJson(attach);
			logger.log(Level.INFO, "  JSON "+json);

			eden.sendPUT(url, json);
		} catch (IOException e) {
			logger.log(Level.ERROR, "Error adding attachment: "+e);
			throw e;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error adding attachment: "+url,e);
			throw new IOException(e);
		} finally {
			logger.log(Level.DEBUG, "LEAVE: modifyAttachment({0}, {1})", handle.getName(), attach.getFilename());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#modifyAttachmentData(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment)
	 */
	@Override
	public void modifyAttachmentData(CharacterHandle handle, Attachment attach) throws IOException {
		URL url = new URL(String.format(SCHEME+"%s/"+API_PATH+"character/%s/attachment/%s", eden.getHostPort(), handle.getUUID().toString(), attach.getID()));
		logger.log(Level.DEBUG, "ENTER: modifyAttachmentData({0}, {1})", handle.getName(), attach.getFilename());

		try {
			eden.sendPUT(url, attach.getData());
		} catch (IOException e) {
			logger.log(Level.ERROR, "Error adding attachment data: "+e);
			throw e;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error adding attachment data: "+url,e);
			throw new IOException(e);
		} finally {
			logger.log(Level.DEBUG, "LEAVE: modifyAttachmentData({0}, {1})", handle.getName(), attach.getFilename());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#getAttachmentData(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment)
	 */
	@Override
	public byte[] getAttachmentData(CharacterHandle rawHandle, Attachment attach) throws IOException {
		if (attach.getData()!=null)
			return attach.getData();

		if (attach.getLocalFile()==null)
			throw new NullPointerException("No local file set");
		byte[] data = Files.readAllBytes(attach.getLocalFile());
		attach.setData(data);
		return data;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#deleteAttachment(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment)
	 */
	@Override
	public void deleteAttachment(CharacterHandle handle, Attachment attach) throws IOException {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#retrieveAttachment(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment)
	 */
	@Override
	public byte[] retrieveAttachment(CharacterHandle handle, Attachment attach) throws IOException {
		logger.log(Level.DEBUG, "ENTER: getAttachment {0}",attach.getID());
		URL url = new URL(String.format(SCHEME+"%s/"+API_PATH+"character/%s/attachment/%s", eden.getHostPort(), handle.getUUID().toString(), attach.getID()));
		try {
			byte[] data = eden.sendGETRaw(url);
			logger.log(Level.INFO, "Parse " + data.length + " bytes");
			attach.setData(data);
			return data;
		} catch (IOException e) {
			logger.log(Level.ERROR, "Error getting attachment: "+e);
			throw e;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error getting attachment: "+url,e);
			throw new IOException(e);
		} finally {
			logger.log(Level.DEBUG, "LEAVE: getAttachment");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#getDatasets()
	 */
	@Override
	public List<DatasetDefinition> getDatasets() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#storeDataset(de.rpgframework.character.DatasetDefinition)
	 */
	@Override
	public void storeDataset(DatasetDefinition value) throws IOException {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#deleteDataset(de.rpgframework.character.DatasetDefinition)
	 */
	@Override
	public void deleteDataset(DatasetDefinition value) throws IOException {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#getDatasetLocalization(de.rpgframework.character.DatasetDefinition, java.lang.String)
	 */
	@Override
	public byte[] getDatasetLocalization(DatasetDefinition value, String lang) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#storeDatasetLocalization(de.rpgframework.character.DatasetDefinition, java.lang.String, byte[])
	 */
	@Override
	public void storeDatasetLocalization(DatasetDefinition value, String lang, byte[] data) throws IOException {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#deleteDatasetLocalization(de.rpgframework.character.DatasetDefinition, java.lang.String)
	 */
	@Override
	public void deleteDatasetLocalization(DatasetDefinition value, String lang) throws IOException {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#getDatasetFile(de.rpgframework.character.DatasetDefinition, java.lang.String)
	 */
	@Override
	public byte[] getDatasetFile(DatasetDefinition value, String name) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#storeDatasetFile(de.rpgframework.character.DatasetDefinition, java.lang.String, byte[])
	 */
	@Override
	public void storeDatasetFile(DatasetDefinition value, String name, byte[] data) throws IOException {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.IUserDatabase#deleteDatasetFile(de.rpgframework.character.DatasetDefinition, java.lang.String)
	 */
	@Override
	public void deleteDatasetFile(DatasetDefinition value, String name) throws IOException {
		// TODO Auto-generated method stub

	}

}
