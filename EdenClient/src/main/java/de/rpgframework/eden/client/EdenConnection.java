package de.rpgframework.eden.client;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Type;
import java.net.ConnectException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpConnectTimeoutException;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.function.Consumer;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.api.EdenAccountInfo;
import de.rpgframework.eden.api.EdenPingInfo;
import de.rpgframework.eden.api.EdenStatus;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.CatalogItem;
import de.rpgframework.reality.EdenConnectionState;

/**
 * @author prelle
 *
 */
public class EdenConnection {

	public static enum StateFlag {
		SERVER_ONLINE,
		CREDENTIALS_EXIST,
		CREDENTIALS_CORRECT,
		ACCOUNT_VERIFIED
	}

	private final static String API_PATH = "api";

	private final static Logger logger = System.getLogger(EdenConnection.class.getPackageName());

	private static CookieManager cookieManager = new CookieManager();

	private String hostPort;
	private String user;
	private String pass;
	private String SCHEME;
	private boolean credentialsSuccessful;
	private int loginAttempts = 0;
	private NonInteractiveAuthenticator authenticator;
	private HttpClient client;

	private Gson gson;
	private String userAgent;

	private int state = 0;
	private EdenAccountInfo cachedAccountInfo;

	private Thread thread;
	private boolean stopped;

	private List<Consumer<List<StateFlag>>> listeners = new ArrayList<>();

	//-------------------------------------------------------------------
	public EdenConnection(String scheme, String host, int port, String appName, String appVersion) {
		cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
		this.SCHEME = scheme;
		if (port==0) throw new IllegalArgumentException("Port must be > 0");
		hostPort = host+":"+port;
		gson = new Gson();
		authenticator = new NonInteractiveAuthenticator(null, this);
		userAgent = appName+"/"+appVersion+"/"+System.getProperty("os.name")+"/"+System.getProperty("os.arch")+"/"+Locale.getDefault().getLanguage();

		SSLContext sslContext = null;
		try {
			sslContext = SSLContext.getInstance("TLS");
			var trustManager = new X509TrustManager() {
			    @Override
			    public X509Certificate[] getAcceptedIssuers() {
			        return new X509Certificate[]{};
			    }
			    @Override
			    public void checkClientTrusted(X509Certificate[] certs, String authType) { }
			    @Override
			    public void checkServerTrusted(X509Certificate[] certs, String authType) { }
			};
			sslContext.init(null, new TrustManager[]{trustManager}, new SecureRandom());
		} catch (Exception e) {
			logger.log(Level.WARNING, "Problem configuring SSL Context",e);
		}


		// PREVENTS HOST VALIDATION
		System.setProperty("jdk.internal.httpclient.disableHostnameVerification", Boolean.TRUE.toString());
		client = HttpClient.newBuilder()
				.authenticator(authenticator)
				.sslContext(sslContext)
				.cookieHandler(cookieManager)
				.build();

		  HttpCookie cookie = new HttpCookie("session", "Hello");
		  cookieManager.getCookieStore().add(URI.create("http://127.0.0.1"), cookie);

		  thread = new Thread(new Runnable() {
			public void run() {
				while (!stopped) {
					try {
						update();
						logger.log(Level.DEBUG, "Waiting for change");
						synchronized (thread) {
							thread.wait(60000);
						}
					} catch (Throwable e) {
						logger.log(Level.ERROR,"Error checking EdenConnection", e);
					}
				}
			}
		  }, "EdenConnection");
	}

	//-------------------------------------------------------------------
	public void addListener(Consumer<List<StateFlag>> callback) {
		if (!listeners.contains(callback)) {
			logger.log(Level.DEBUG, "Add listener {0}", callback.getClass().getSimpleName());
			listeners.add(callback);
			callback.accept(getStateFlags());
		}
	}

	//-------------------------------------------------------------------
	public void start() {
		  thread.setDaemon(false);
		  thread.start();
	}

	//-------------------------------------------------------------------
	public void stop() {
		stopped = true;
	}

	//-------------------------------------------------------------------
	public String toString() { return getBaseURL(); }

	//-------------------------------------------------------------------
	public String getHostPort() { return hostPort; }
	//-------------------------------------------------------------------
	public String getBaseURL() {
		return String.format( SCHEME+"://%s/"+API_PATH , hostPort);
	}
	//-------------------------------------------------------------------
	public NonInteractiveAuthenticator getAuthenticator() { return authenticator; }
	public HttpClient getClient() { return client; }

	//-------------------------------------------------------------------
	public List<StateFlag> getStateFlags() {
		List<StateFlag> flags = new ArrayList<>();
		for (StateFlag flag : StateFlag.values()) {
			if (hasStateFlag(flag)) {
				flags.add(flag);
			}
		}
		return flags;
	}

	//-------------------------------------------------------------------
	public boolean hasStateFlag(StateFlag flag) {
		int bit = 1 << flag.ordinal();
		return (state & bit) != 0;
	}

	//-------------------------------------------------------------------
	public void setStateFlag(StateFlag flag) {
		int bit = 1 << flag.ordinal();
		boolean oldState = (state & bit) != 0;
		state = state | bit;
		if (oldState==false) {
			logger.log(Level.INFO, "set state {0}", flag);
			informListeners();
		}
	}

	//-------------------------------------------------------------------
	void clearStateFlag(StateFlag flag) {
		int bit = 1 << flag.ordinal();
		boolean oldState = (state & bit) != 0;
		state = state & ~bit;
		if (oldState) {
			logger.log(Level.INFO, "clear state {0}", flag);
			informListeners();
		}
	}

	//-------------------------------------------------------------------
	private void informListeners() {
		List<StateFlag> flags = getStateFlags();
		for (Consumer<List<StateFlag>> listener : listeners) {
			try {
				logger.log(Level.INFO, "Tell {0} of state change to {1}", listener.getClass(), flags);
				listener.accept(flags);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	//-------------------------------------------------------------------
	public void progressState() {
	}

	//-------------------------------------------------------------------
	public void login(String user, String pass, InteractiveAuthenticator fallback) {
		logger.log(Level.INFO, "Set login data for {0}", user);
		this.user = user;
		this.pass = pass;
		authenticator.setInteractiveFallback(fallback);
		credentialsSuccessful = true;
		loginAttempts = 0;

		if (user==null || pass==null) {
			clearStateFlag(StateFlag.CREDENTIALS_EXIST);
		} else {
			setStateFlag(StateFlag.CREDENTIALS_EXIST);
			this.credentialsSuccessful = true;
			synchronized (thread) { thread.notify(); }
		}
	}

	//-------------------------------------------------------------------
	public void logout() {
		logger.log(Level.INFO, "Log out {0}", user);
		this.user = null;
		this.pass = null;
		this.credentialsSuccessful = false;
//		authenticator.setCredentials(null, null);
		clearStateFlag(StateFlag.CREDENTIALS_EXIST);
		clearStateFlag(StateFlag.CREDENTIALS_CORRECT);
	}

	//-------------------------------------------------------------------
	public void deleteAccount() throws EdenAPIException  {
		logger.log(Level.INFO, "deleteAccount({0})", user);

		String url_s = SCHEME+"://"+hostPort+"/api/account";
		try {
			HttpResponse<String> response = sendDELETE(new URL(url_s));
			int code = response.statusCode(); //con.getResponseCode();
			if (code>=400) {
				//String reason = con.getHeaderField("Reason");
				String reason = response.headers().firstValue("Reason").isPresent()?response.headers().firstValue("Reason").get():null;
				logger.log(Level.ERROR, "Failed deleting account: {0}  / reason={1}", code, reason);
				throw new EdenAPIException(code, reason);
//				return null;
			} else {
				EdenAccountInfo pdu = gson.fromJson(response.body(), EdenAccountInfo.class);
				logger.log(Level.INFO, "Server confirmed account deletion");

				logout();
			}
		} catch (Exception e) {
			if (e instanceof EdenAPIException)
				throw (EdenAPIException)e;
			logger.log(Level.ERROR, "Failed contacting server: "+e);
			throw new EdenAPIException(e);
		}

	}

//	//-------------------------------------------------------------------
//	public EdenCharacterProvider getCharacterProvider() {
//		return new EdenCharacterProvider(this);
//	}

	//-------------------------------------------------------------------
	public ShopClient getShopClient() {
		return new ShopClient(this);
	}

	//-------------------------------------------------------------------
	public HttpResponse<String> sendGET(URL url, Duration timeout) throws IOException {
		try {
			HttpRequest request = HttpRequest.newBuilder(url.toURI())
				.header("User-Agent", userAgent)
				.timeout(timeout)
				.GET()
				.build();

			HttpResponse<String> response =  client.send(request, BodyHandlers.ofString());
			logger.log(Level.DEBUG, "Response {0}",response);
			credentialsSuccessful = response.statusCode()!=401;
			if (response.statusCode()>=400) {
				throw new EdenAPIException(EdenStatus.INTERNAL_ERROR.code(),
						response.headers().firstValue("Reason").orElse("Received HTTP code "+response.statusCode()));
			}
			setStateFlag(StateFlag.SERVER_ONLINE);
			return response;
		} catch (HttpConnectTimeoutException e) {
			logger.log(Level.ERROR, "Timeout contacting {0}",url);
			clearStateFlag(StateFlag.SERVER_ONLINE);
			throw e;
		} catch (ConnectException e) {
			logger.log(Level.WARNING, "Error contacting {0}: {1}",url, e.toString());
			clearStateFlag(StateFlag.SERVER_ONLINE);
			throw e;
		} catch (IOException e) {
			if (e.getMessage()!=null && e.getMessage().contains("No credentials provided")) {
				clearStateFlag(StateFlag.CREDENTIALS_CORRECT);
				throw new EdenAPIException(EdenStatus.UNAUTHORIZED, "Invalid credentials");
			}
			logger.log(Level.ERROR, "Error contacting "+url,e);
			throw e;
		} catch (URISyntaxException | InterruptedException e) {
			logger.log(Level.ERROR, "Internal error talking to server "+url,e);
			clearStateFlag(StateFlag.SERVER_ONLINE);
			throw new IOException(e);
		}
	}

	//-------------------------------------------------------------------
	public HttpResponse<String> sendGET(URL url) throws IOException {
		return sendGET(url, Duration.ofSeconds(3));
	}

	//-------------------------------------------------------------------
	public <T> T sendGETFromJson(URL url, Class<T> type) throws IOException {
		try {
			HttpRequest request = HttpRequest.newBuilder(url.toURI())
				.header("User-Agent", userAgent)
				.timeout(Duration.ofSeconds(3))
				.GET()
				.build();

			HttpResponse<String> response =  client.send(request, BodyHandlers.ofString());
			credentialsSuccessful = response.statusCode()!=401;

			if (response.statusCode()>=400) {
				throw new EdenAPIException(EdenStatus.INTERNAL_ERROR.code(),
						response.headers().firstValue("Reason").orElse("Received HTTP code "+response.statusCode()));
			}

			logger.log(Level.DEBUG, "Convert from JSON: "+response.body());
			T pdu = gson.fromJson(response.body(), type);

			return pdu;
		} catch (URISyntaxException | InterruptedException e) {
			e.printStackTrace();
			throw new IOException(e);
		}
	}

	//-------------------------------------------------------------------
	public byte[] sendGETRaw(URL url) throws IOException {
		try {
			HttpRequest request = HttpRequest.newBuilder(url.toURI())
				.header("User-Agent", userAgent)
				.timeout(Duration.ofSeconds(3))
				.GET()
				.build();

			HttpResponse<byte[]> response =  client.send(request, BodyHandlers.ofByteArray());
			credentialsSuccessful = response.statusCode()!=401;

			if (response.statusCode()>=400) {
				throw new EdenAPIException(EdenStatus.INTERNAL_ERROR.code(),
						response.headers().firstValue("Reason").orElse("Received HTTP code "+response.statusCode()));
			}

			return response.body();
		} catch (URISyntaxException | InterruptedException e) {
			logger.log(Level.ERROR, "Failed sending API command",e);
			throw new EdenAPIException(e);
		}
	}

	//-------------------------------------------------------------------
	public HttpResponse<String> sendPOST(URL url, String json) throws IOException {
		try {
			HttpRequest request = HttpRequest.newBuilder(url.toURI())
				.header("User-Agent", userAgent)
				.header("Content-Type", "application/json")
				.timeout(Duration.ofSeconds(3))
				.POST(BodyPublishers.ofString(json))
				.build();

			logger.log(Level.DEBUG, "sendPOST: {0}", url);
			HttpResponse<String> response =  client.send(request, BodyHandlers.ofString());
			credentialsSuccessful = response.statusCode()!=401;
			if (response.statusCode()>=400) {
				throw new EdenAPIException(EdenStatus.fromCode(response.statusCode()),
						response.headers().firstValue("Reason").orElse("Received HTTP code "+response.statusCode()));
			}
			return response;
		} catch (URISyntaxException | InterruptedException e) {
			logger.log(Level.ERROR, "Failed sending API command",e);
			throw new EdenAPIException(e);
		}
	}

	//-------------------------------------------------------------------
	public <T> T  sendPOST(URL url, String json, Class<T> type) throws IOException {
		try {
			HttpRequest request = HttpRequest.newBuilder(url.toURI())
				.header("User-Agent", userAgent)
				.header("Content-Type", "application/json")
				.timeout(Duration.ofSeconds(3))
				.POST(BodyPublishers.ofString(json))
				.build();

			logger.log(Level.DEBUG, "sendPOST: {0}", url);
			HttpResponse<String> response =  client.send(request, BodyHandlers.ofString());
			credentialsSuccessful = response.statusCode()!=401;

			logger.log(Level.DEBUG, "Convert from JSON: "+response.body());
			T pdu = gson.fromJson(response.body(), type);

			return pdu;
		} catch (URISyntaxException | InterruptedException e) {
			logger.log(Level.ERROR, "Failed sending API command",e);
			throw new EdenAPIException(e);
		}
	}

	//-------------------------------------------------------------------
	public HttpResponse<String> sendPUT(URL url, String json) throws IOException {
		try {
			HttpRequest request = HttpRequest.newBuilder(url.toURI())
				.header("User-Agent", userAgent)
				.header("Content-Type", "application/json")
				.timeout(Duration.ofSeconds(3))
				.PUT(BodyPublishers.ofString(json))
				.build();

			logger.log(Level.DEBUG, "sendPOST: {0}", url);
			HttpResponse<String> response =  client.send(request, BodyHandlers.ofString());
			credentialsSuccessful = response.statusCode()!=401;
			if (response.statusCode()>=400) {
				throw new EdenAPIException(EdenStatus.INTERNAL_ERROR.code(),
						response.headers().firstValue("Reason").orElse("Received HTTP code "+response.statusCode()));
			}
			return response;
		} catch (URISyntaxException | InterruptedException e) {
			logger.log(Level.ERROR, "Failed sending API command",e);
			throw new EdenAPIException(e);
		}
	}

	//-------------------------------------------------------------------
	public HttpResponse<String> sendPUT(URL url, byte[] data) throws IOException {
		try {
			HttpRequest request = HttpRequest.newBuilder(url.toURI())
				.header("User-Agent", userAgent)
				.header("Content-Type", "application/data")
				.timeout(Duration.ofSeconds(3))
				.PUT(BodyPublishers.ofByteArray(data))
				.build();

			logger.log(Level.DEBUG, "sendPUT: {0}", url);
			HttpResponse<String> response =  client.send(request, BodyHandlers.ofString());
			credentialsSuccessful = response.statusCode()!=401;
			if (response.statusCode()>=400) {
				throw new EdenAPIException(EdenStatus.INTERNAL_ERROR.code(),
						response.headers().firstValue("Reason").orElse("Received HTTP code "+response.statusCode()));
			}
			return response;
		} catch (URISyntaxException | InterruptedException e) {
			logger.log(Level.ERROR, "Failed sending API command",e);
			throw new EdenAPIException(e);
		}
	}

	//-------------------------------------------------------------------
	public HttpResponse<String> sendDELETE(URL url) throws IOException {
		try {
			HttpRequest request = HttpRequest.newBuilder(url.toURI())
				.header("User-Agent", userAgent)
				.header("Content-Type", "application/json")
				.timeout(Duration.ofSeconds(3))
				.DELETE()
				.build();

			logger.log(Level.DEBUG, "sendDELETE: {0}", url);
			HttpResponse<String> response =  client.send(request, BodyHandlers.ofString());
			credentialsSuccessful = response.statusCode()!=401;
			if (response.statusCode()>=400) {
				throw new EdenAPIException(EdenStatus.INTERNAL_ERROR.code(),
						response.headers().firstValue("Reason").orElse("Received HTTP code "+response.statusCode()));
			}
			return response;
		} catch (URISyntaxException | InterruptedException e) {
			logger.log(Level.ERROR, "Failed sending API command",e);
			throw new IOException(e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Try to contact the server and obtain URLs relevant to the user
	 */
	public EdenPingInfo getInfo() {
		String url_s = SCHEME+"://"+hostPort+"/anon/ping";
		try {
			logger.log(Level.INFO, "Try contacting server at {0}", hostPort);
			logger.log(Level.DEBUG, "User Agent is {0}", userAgent);
			HttpResponse<String> response = sendGET(new URL(url_s), Duration.ofSeconds(3));
			// Errors are already converted to exceptions in sendGET
			EdenPingInfo pdu = gson.fromJson(response.body(), EdenPingInfo.class);
			setStateFlag(StateFlag.SERVER_ONLINE);
			return pdu;
		} catch (IOException e) {
			logger.log(Level.DEBUG, "Failed contacting server {0}: {1}",hostPort,e);
		}
		return null;
	}

	//-------------------------------------------------------------------
    public final boolean isOffline() {
    	return cachedAccountInfo==null;
    }

	//-------------------------------------------------------------------
	public EdenAccountInfo getCachedAccountInfo() {
		return cachedAccountInfo;
	}

	//-------------------------------------------------------------------
	/**
	 * Try to contact the server and obtain URLs relevant to the user
	 */
	public EdenAccountInfo getAccountInfo() throws EdenAPIException {
		try {
			String url_s = SCHEME+"://"+hostPort+"/api/account";
			logger.log(Level.INFO, "Contact "+url_s);
			// Long timeout, because it may result in a login dialog and wait for the user
			HttpResponse<String> response = sendGET(new URL(url_s), Duration.ofMinutes(5));
			// Errors are already converted to exceptions in sendGET

			EdenAccountInfo pdu = gson.fromJson(response.body(), EdenAccountInfo.class);
			logger.log(Level.INFO, "AccountInfo received successfully: "+pdu.dump());
			logger.log(Level.INFO, "Name: "+pdu.getFirstName()+ " "+pdu.getLastName());
			logger.log(Level.INFO, "Login: "+pdu.getLogin()+ " verified="+pdu.isVerified());

			setStateFlag(StateFlag.CREDENTIALS_CORRECT);
			this.cachedAccountInfo = pdu;
			return pdu;
		} catch (EdenAPIException e) {
			logger.log(Level.ERROR, "EdenAPI: {0}  \t {1}",e.getStatus(), e.getMessage());
			throw e;
		} catch (IOException e) {
			if (e.getMessage().contains("too many authentication attempts"))
				throw new EdenAPIException(EdenCodes.UNAUTHORIZED, e.getMessage());
			if (e.getMessage().contains("No credentials provided"))
				throw new EdenAPIException(EdenCodes.UNAUTHORIZED, e.getMessage());
			logger.log(Level.WARNING, "Failed contacting server: ",e);
			return null;
//		} catch (URISyntaxException e) {
//			logger.log(Level.ERROR, "Failed contacting server",e);
//			throw new RuntimeException(e);
//		} catch (InterruptedException e) {
//			logger.log(Level.ERROR, "Failed contacting server",e);
		}
//		return null;
	}

	//-------------------------------------------------------------------
	public EdenAccountInfo createAccount(String login, String email, String pass, String first, String last, Locale lang) throws EdenAPIException {
		EdenAccountInfo info = new EdenAccountInfo()
				.setLogin(login)
				.setEmail(email)
				.setSecret(pass)
				.setFirstName(first)
				.setLastName(last)
				.setLocale(lang)
				;
		String url_s = SCHEME+"://"+hostPort+"/api/account";
		try {
			String json = gson.toJson(info);
			HttpRequest request = HttpRequest.newBuilder(new URI(url_s))
				.header("Content-Type", "application/json;charset=UTF-8")
				.header("User-Agent", userAgent)
				.POST(HttpRequest.BodyPublishers.ofString(json, StandardCharsets.UTF_8))
				.timeout(Duration.ofSeconds(3))
				.build();

			logger.log(Level.DEBUG, "POST "+url_s);
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());

			int code = response.statusCode(); //con.getResponseCode();
			credentialsSuccessful = response.statusCode()!=401;
			if (code>=400) {
				//String reason = con.getHeaderField("Reason");
				String reason = response.headers().firstValue("Reason").isPresent()?response.headers().firstValue("Reason").get():null;
				logger.log(Level.ERROR, "Failed creating account: {0}  / reason={1}", code, reason);
				throw new EdenAPIException(code, reason);
//				return null;
			} else {
				EdenAccountInfo pdu = gson.fromJson(response.body(), EdenAccountInfo.class);
				logger.log(Level.INFO, "Server confirmed account creation");
				return pdu;
			}
//
////			EdenPingInfo pdu = gson.fromJson(new InputStreamReader(con.getInputStream()), EdenPingInfo.class);
//			EdenPingInfo pdu = gson.fromJson(response.body(), EdenPingInfo.class);
//			return pdu;
		} catch (IOException | InterruptedException | URISyntaxException e) {
			if (e instanceof EdenAPIException)
				throw (EdenAPIException)e;
			logger.log(Level.ERROR, "Failed contacting server: "+e);
			throw new EdenAPIException(e);
		}
	}

	//-------------------------------------------------------------------
	public EdenAccountInfo updateAccount(EdenAccountInfo info) throws EdenAPIException {
		logger.log(Level.INFO, "updateAccount");
		String url_s = SCHEME+"://"+hostPort+"/api/account";
		try {
			String json = gson.toJson(info);
			HttpResponse<String> response = sendPUT(new URL(url_s), json);

			int code = response.statusCode(); //con.getResponseCode();
			credentialsSuccessful = response.statusCode()!=401;
			if (code>=400) {
				//String reason = con.getHeaderField("Reason");
				String reason = response.headers().firstValue("Reason").isPresent()?response.headers().firstValue("Reason").get():null;
				logger.log(Level.ERROR, "Failed creating account: {0}  / reason={1}", code, reason);
				throw new EdenAPIException(code, reason);
//				return null;
			} else {
				EdenAccountInfo pdu = gson.fromJson(response.body(), EdenAccountInfo.class);
				logger.log(Level.ERROR, "Received "+pdu.getFirstName());
				this.cachedAccountInfo = pdu;
				return pdu;
			}
//
////			EdenPingInfo pdu = gson.fromJson(new InputStreamReader(con.getInputStream()), EdenPingInfo.class);
//			EdenPingInfo pdu = gson.fromJson(response.body(), EdenPingInfo.class);
//			return pdu;
		} catch (IOException  e) {
			logger.log(Level.ERROR, "Failed contacting 2server: ",e);
			if (e instanceof EdenAPIException)
				throw (EdenAPIException)e;
			logger.log(Level.ERROR, "Failed contacting server: "+e);
			throw new EdenAPIException(e);
		}
	}

	//-------------------------------------------------------------------
	public void requestAccountVerification() throws EdenAPIException {
		try {
			throw new RuntimeException("requestAccountVerification");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String url_s = SCHEME+"://"+hostPort+"/api/account/verify";
		try {
			logger.log(Level.INFO, "initiated verification with  "+client.authenticator());
			HttpResponse<String> response = sendGET(new URL(url_s));

			int code = response.statusCode();
			credentialsSuccessful = response.statusCode()!=401;
			if (code>=400) {
				logger.log(Level.WARNING, "Failed contacting server at {0}: {1} - {2}",url_s,code,response);
				throw new EdenAPIException(code, "Received HTTP code "+code+" when accessing contact URL");
			}

//			logger.log(Level.DEBUG, "Convert from JSON: "+response.body());
//			EdenAccountInfo pdu = gson.fromJson(response.body(), EdenAccountInfo.class);
//			logger.log(Level.INFO, "AccountInfo received successfully: "+pdu.getFirstName());
		} catch (EdenAPIException e) {
			throw e;
		} catch (IOException e) {
			logger.log(Level.WARNING, "Failed contacting server: ",e);
		}
	}

	//-------------------------------------------------------------------
	public void verifyAccount(String vcode) throws EdenAPIException {
		logger.log(Level.DEBUG, "ENTER verifyAccount");
		EdenAccountInfo info = new EdenAccountInfo()
				.setLogin(user)
				.setVerificationCode(vcode)
				;
		String url_s = SCHEME+"://"+hostPort+"/api/account/verify";
		try {
			String json = gson.toJson(info);
			HttpRequest request = HttpRequest.newBuilder(new URI(url_s))
					.header("Content-Type", "application/json;charset=UTF-8")
					.header("User-Agent", userAgent)
				.PUT(HttpRequest.BodyPublishers.ofString(json, StandardCharsets.UTF_8))
				.timeout(Duration.ofSeconds(3))
				.build();


			logger.log(Level.INFO, "verify with  "+client.authenticator());
//			client.sendAsync(request, BodyHandlers.ofString());
			HttpResponse<String> response = client.send(request, BodyHandlers.ofString());

			int code = response.statusCode(); //con.getResponseCode();
			credentialsSuccessful = response.statusCode()!=401;
			if (code>=400) {
				//String reason = con.getHeaderField("Reason");
				String reason = response.headers().firstValue("Reason").isPresent()?response.headers().firstValue("Reason").get():null;
				logger.log(Level.ERROR, "Failed verifying account: {0}  / reason={1}", code, reason);
				if (reason==null)
					reason = "Verification failed. Server replied with "+code;
				throw new EdenAPIException(code, reason);
			} else {
				logger.log(Level.INFO, "Successfully verified");
				setStateFlag(StateFlag.ACCOUNT_VERIFIED);
				return;
			}
		} catch (IOException | InterruptedException | URISyntaxException e) {
			if (e instanceof EdenAPIException)
				throw (EdenAPIException)e;
			logger.log(Level.ERROR, "Failed contacting server: "+e);
			throw new EdenAPIException(e);
		} finally {
			logger.log(Level.DEBUG, "LEAVE verifyAccount");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Initiate account recovery
	 * @param user
	 * @return Server message
	 */
	public String accountRecover(String user) throws EdenAPIException {
		String url_s = SCHEME+"://"+hostPort+"/api/account/recover?player="+user;
		try {
			HttpRequest request = HttpRequest.newBuilder(new URI(url_s))
					.header("Content-Type", "application/json;charset=UTF-8")
					.header("User-Agent", userAgent)
				.GET()
				.timeout(Duration.ofSeconds(5))
				.build();

			logger.log(Level.INFO, "recover account for {0}",user);
			HttpResponse<String> response = HttpClient.newHttpClient().send(request, BodyHandlers.ofString());

			int code = response.statusCode(); //con.getResponseCode();
			String reason = null;
			if (response.headers().firstValue("Reason").isPresent())
				reason = response.headers().firstValue("Reason").get();
			logger.log(Level.INFO, "Account recovery returned {0} and {1}",code, reason);
			logger.log(Level.INFO, "Account recovery body: "+response.body());
			if (code<300) {
				return response.body();
			}

			throw new EdenAPIException(EdenStatus.INTERNAL_ERROR, reason);
		} catch (IOException | InterruptedException | URISyntaxException e) {
			if (e instanceof EdenAPIException)
				throw (EdenAPIException)e;
			logger.log(Level.ERROR, "Failed contacting server: "+e);
			throw new EdenAPIException(e);
		}
	}

	//-------------------------------------------------------------------
	public boolean accountRecoverConfirm(String user, String vcode, String password) throws EdenAPIException {
		EdenAccountInfo info = new EdenAccountInfo()
				.setLogin(user)
				.setVerificationCode(vcode)
				.setSecret(password)
				;

		String url_s = SCHEME+"://"+hostPort+"/api/account/recover";
		try {
			String json = gson.toJson(info);
			HttpRequest request = HttpRequest.newBuilder(new URI(url_s))
					.header("Content-Type", "application/json;charset=UTF-8")
					.header("User-Agent", userAgent)
				.PUT(HttpRequest.BodyPublishers.ofString(json, StandardCharsets.UTF_8))
				.timeout(Duration.ofSeconds(5))
				.build();

			logger.log(Level.INFO, "recover account confirm for {0}",user);
			HttpResponse<String> response = HttpClient.newHttpClient().send(request, BodyHandlers.ofString());

			int code = response.statusCode(); //con.getResponseCode();
			credentialsSuccessful = response.statusCode()!=401;
			if (code==200) {
				return true;
			}

			return false;
		} catch (IOException | InterruptedException | URISyntaxException e) {
			if (e instanceof EdenAPIException)
				throw (EdenAPIException)e;
			logger.log(Level.ERROR, "Failed contacting server: "+e);
			throw new EdenAPIException(e);
		}
	}

	//-------------------------------------------------------------------
	public CatalogItem getCatalogItem(RoleplayingSystem rules, String id) {
		try {
			URL url = new URL(SCHEME+"://"+hostPort+"/api/shop/"+rules+"/"+id);
			logger.log(Level.DEBUG, "Contact "+url);

			HttpResponse<String> response = sendGET(url);
			int code = response.statusCode(); //con.getResponseCode();
			if (code>=400) {
				String reason = response.headers().firstValue("Reason").isPresent()?response.headers().firstValue("Reason").get():null;
				logger.log(Level.ERROR, "Failed getting content packs: {0}  / reason={1}", code, reason);
				if (reason==null)
					reason = "Getting bought content failed. Server replied with "+code;
				throw new EdenAPIException(code, reason);
			}

			return gson.fromJson(response.body(), CatalogItem.class);
		} catch (IOException e) {
			logger.log(Level.WARNING, "Failed contacting server: "+e);
			return null;
		}
	}

	//-------------------------------------------------------------------
	public List<BoughtItem> getContentPacks(RoleplayingSystem rules) {
		if (!hasStateFlag(StateFlag.CREDENTIALS_CORRECT))
			return List.of();
		try {
			URL url = new URL(SCHEME+"://"+hostPort+"/api/license?rules="+rules);
			logger.log(Level.DEBUG, "Contact "+url);

			HttpResponse<String> response = sendGET(url);
			int code = response.statusCode(); //con.getResponseCode();
			if (code>=400) {
				String reason = response.headers().firstValue("Reason").isPresent()?response.headers().firstValue("Reason").get():null;
				logger.log(Level.ERROR, "Failed getting content packs: {0}  / reason={1}", code, reason);
				if (reason==null)
					reason = "Getting bought content failed. Server replied with "+code;
				throw new EdenAPIException(code, reason);
			}

			Type type = new TypeToken<ArrayList<BoughtItem>>(){}.getType();
			ArrayList<BoughtItem> pdu = new ArrayList<BoughtItem>();
			pdu = gson.fromJson(response.body(), type);
			return pdu;
		} catch (IOException e) {
			logger.log(Level.WARNING, "Failed contacting server: "+e);
			return new ArrayList<>();
		}
	}

//	//-------------------------------------------------------------------
//	public List<String> getLicensedDatasets() {
//		logger.log(Level.INFO, "ENTER: getLicensedDatasets()");
//		ArrayList<String> pdu = new ArrayList<String>();
//		try {
//			URL url = new URL(SCHEME+"://"+hostPort+"/api/license");
//			logger.log(Level.DEBUG, "Contact "+url);
//
//			HttpResponse<String> response = sendGET(url);
//			int code = response.statusCode(); //con.getResponseCode();
//			if (code>=400) {
//				String reason = response.headers().firstValue("Reason").isPresent()?response.headers().firstValue("Reason").get():null;
//				logger.log(Level.ERROR, "Failed getting datasets: {0}  / reason={1}", code, reason);
//				if (reason==null)
//					reason = "Getting bought content failed. Server replied with "+code;
//				throw new EdenAPIException(code, reason);
//			}
//
//			Type type = new TypeToken<ArrayList<BoughtItem>>(){}.getType();
//			pdu = gson.fromJson(response.body(), type);
//		} catch (IOException e) {
//			logger.log(Level.WARNING, "Failed contacting server: "+e);
//		} finally {
//			logger.log(Level.INFO, "LEAVE: getLicensedDatasets() returns {0} datasets", pdu.size());
//		}
//		return pdu;
//	}

	//-------------------------------------------------------------------
	/**
	 * Try to convert the given activation key into a license.
	 * @param uuid
	 * @throws EdenAPIException
	 */
	public void activateLicense(UUID uuid) throws EdenAPIException {
		try {
			String url_s = SCHEME+"://"+hostPort+"/api/license/activate";
			logger.log(Level.DEBUG, "Contact "+url_s);
			HttpResponse<String> response = sendPOST(new URL(url_s), gson.toJson(uuid));
			int code = response.statusCode();
			if (code>=400) {
				logger.log(Level.WARNING, "Failed contacting server at {0}: {1} - {2}",url_s,code,response);
				String problem = "Received HTTP code "+code+" when accessing contact URL";
				if (response.headers().firstValue("Reason").isPresent())
					problem = response.headers().firstValue("Reason").get();
				throw new EdenAPIException(code, problem);
			}

			logger.log(Level.DEBUG, "Convert from JSON: "+response.body());
			BoughtItem pdu = gson.fromJson(response.body(), BoughtItem.class);
			logger.log(Level.DEBUG, "BoughtItem received successfully: "+pdu);
			return ;
		} catch (EdenAPIException e) {
			throw e;
		} catch (IOException e) {
			if (e.getMessage().contains("too many authentication attempts"))
				throw new EdenAPIException(EdenCodes.UNAUTHORIZED, e.getMessage());
			if (e.getMessage().contains("No credentials provided"))
				throw new EdenAPIException(EdenCodes.UNAUTHORIZED, e.getMessage());
			logger.log(Level.WARNING, "Failed contacting server: ",e);
		}
	}

	//-------------------------------------------------------------------
	private void update() {
		logger.log(Level.DEBUG, "ENTER update()");
		if (!hasStateFlag(StateFlag.SERVER_ONLINE)) {
			if (!checkIfServerIsOnline()) {
				return;
			}
		}
		logger.log(Level.DEBUG, "{0} found", StateFlag.SERVER_ONLINE);
		// Server is online


		if (!hasStateFlag(StateFlag.CREDENTIALS_EXIST)) {
			return;
		}
		logger.log(Level.DEBUG, "{0} found", StateFlag.CREDENTIALS_EXIST);
		// Credentials are entered


		if (!hasStateFlag(StateFlag.CREDENTIALS_CORRECT)) {
			if (!checkIfCredentialsAreCorrect()) {
				return;
			}
		}
		logger.log(Level.DEBUG, "{0} found", StateFlag.CREDENTIALS_CORRECT);
		// Credentials are correct

		if (!hasStateFlag(StateFlag.ACCOUNT_VERIFIED)) {
			logger.log(Level.DEBUG, "Account email adress not verified yet");
			return;
		}
		logger.log(Level.DEBUG, "{0} found", StateFlag.ACCOUNT_VERIFIED);
	}

	//-------------------------------------------------------------------
	/**
	 * Called from @link{#update()}
	 */
	private boolean checkIfServerIsOnline() {
		logger.log(Level.DEBUG, "ENTER checkIfServerIsOnline");
		String url_s = SCHEME+"://"+hostPort+"/anon/ping";
		try {
			logger.log(Level.INFO, "Try contacting server at {0}", hostPort);
			logger.log(Level.DEBUG, "User Agent is {0}", userAgent);
			HttpResponse<String> response = sendGET(new URL(url_s), Duration.ofSeconds(3));
			// Errors are already converted to exceptions in sendGET
			gson.fromJson(response.body(), EdenPingInfo.class);
			setStateFlag(StateFlag.SERVER_ONLINE);
			logger.log(Level.INFO, "Server {0} is online",hostPort);
			synchronized (thread) {
				thread.notify();
			}
			return true;
		} catch (IOException e) {
			logger.log(Level.ERROR, "Failed contacting server {0}: {1}",hostPort,e);
		} finally {
			logger.log(Level.DEBUG, "LEAVE checkIfServerIsOnline");
		}
		return false;
	}

	//-------------------------------------------------------------------
	private boolean checkIfCredentialsAreCorrect() {
		try {
			String url_s = SCHEME+"://"+hostPort+"/api/account";
			logger.log(Level.INFO, "Contact "+url_s);
			// Long timeout, because it may result in a login dialog and wait for the user
			HttpResponse<String> response = sendGET(new URL(url_s), Duration.ofMinutes(5));
			// Errors are already converted to exceptions in sendGET

			logger.log(Level.INFO, "Convert from JSON: "+response.body());
			EdenAccountInfo pdu = gson.fromJson(response.body(), EdenAccountInfo.class);
			logger.log(Level.INFO, "AccountInfo received successfully: "+pdu.dump());
			logger.log(Level.INFO, "Name: "+pdu.getFirstName()+ " "+pdu.getLastName());
			logger.log(Level.INFO, "Login: "+pdu.getLogin()+ " verified="+pdu.isVerified());

			setStateFlag(StateFlag.CREDENTIALS_CORRECT);
			logger.log(Level.INFO, "Credentials are correct");
			this.cachedAccountInfo = pdu;

			if (pdu.isVerified()) {
				setStateFlag(StateFlag.ACCOUNT_VERIFIED);
				BabylonEventBus.fireEvent(BabylonEventType.EDEN_STATE_CHANGED, EdenConnectionState.CONNECTED);
			} else {
				clearStateFlag(StateFlag.ACCOUNT_VERIFIED);
				BabylonEventBus.fireEvent(BabylonEventType.EDEN_STATE_CHANGED, EdenConnectionState.NOT_CONNECTED);
			}
			return true;
		} catch (Exception e) {
			if (e.getMessage().contains("too many authentication attempts")) {
				clearStateFlag(StateFlag.CREDENTIALS_EXIST);
			} else
			if (e.getMessage().contains("No credentials provided")) {
				// Also throwns when credentials are incorrect, so ignore here
				//clearStateFlag(StateFlag.CREDENTIALS_EXIST);
			} else
			logger.log(Level.WARNING, "Failed contacting server: "+e);
			BabylonEventBus.fireEvent(BabylonEventType.EDEN_STATE_CHANGED, EdenConnectionState.NOT_CONNECTED);
		}
		return false;
	}

	public String getLogin() { return user; }
	public String getPassword() { return pass; }
	public boolean getCredentialsSuccessful() { return credentialsSuccessful; }
	public void setCredentialsSuccessful(boolean value) { credentialsSuccessful=value; loginAttempts=0;
	logger.log(Level.DEBUG, "setCredentialsSuccessful({0})", credentialsSuccessful);
	}
	public void increaseLoginAttempts() { loginAttempts++; }
	public int getLoginAttempts() { return loginAttempts; }
}
