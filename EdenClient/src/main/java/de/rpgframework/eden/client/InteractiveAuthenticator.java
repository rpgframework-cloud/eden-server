package de.rpgframework.eden.client;

/**
 * @author prelle
 *
 */
public interface InteractiveAuthenticator {

	public String[] requestCredentials();

}
