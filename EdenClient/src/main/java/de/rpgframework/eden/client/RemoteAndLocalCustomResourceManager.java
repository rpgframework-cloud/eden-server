package de.rpgframework.eden.client;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import de.rpgframework.character.CharacterIOException;
import de.rpgframework.character.CharacterIOException.ErrorCode;
import de.rpgframework.core.CustomDescriptionHandle;
import de.rpgframework.core.CustomResourceManager;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class RemoteAndLocalCustomResourceManager implements CustomResourceManager {

	//private CustomResourceManagerListener callback;
	private RoleplayingSystem rules;

	protected Logger logger = System.getLogger(RemoteAndLocalCustomResourceManager.class.getPackageName());

	private EdenConnection eden;
	//private EdenCustomResourceManager onlineCharProv;

	private Path localBaseDir;

	private Map<String,CustomDescriptionHandle> bundleNames;
	private Instant hasBeenCheckedLast;

	//-------------------------------------------------------------------
	/**
	 */
	public RemoteAndLocalCustomResourceManager(Path customDir, EdenConnection eden, RoleplayingSystem rules) throws CharacterIOException {
		this.eden = eden;
		this.rules = rules;
		bundleNames = new LinkedHashMap<>();

		localBaseDir = customDir;
		logger.log(Level.INFO, "Expect custom data at "+localBaseDir);
		System.setProperty("customDir", localBaseDir.toString());

		// Ensure that directory exists
		try {
			Files.createDirectories(localBaseDir);
		} catch (IOException e) {
			logger.log(Level.ERROR, "Could not create custom data directory: "+e);
			throw new CharacterIOException(ErrorCode.FILESYSTEM_WRITE, null, "Could not create directories", localBaseDir.toString(), e);
		}
	}

	//-------------------------------------------------------------------
	public void start() {
		// TODO Auto-generated method stub
//		onlineCharProv = new EdenCustomResourceManager(eden);

		logger.log(Level.WARNING, "TODO: synchronize with cloud\n\n\n");
		synchronize();
	}

	//-------------------------------------------------------------------
	private void synchronize() {
//		try {
//			List<CharacterHandle> charsOnline = onlineCharProv.getMyCharacters(rules);
//			List<CharacterHandle> charsLocal  = getMyCharacters(rules);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			System.exit(1);
//		}
	}

	//-------------------------------------------------------------------
	private Collection<CustomDescriptionHandle> getPropertiesFiles(RoleplayingSystem rules) {
		if (!bundleNames.isEmpty() || (hasBeenCheckedLast!=null && Instant.now().minusSeconds(60).isBefore(hasBeenCheckedLast)))
			return bundleNames.values();

		// Collect names of all resource bundles
		try {
			Files.newDirectoryStream(localBaseDir, "*.properties").forEach(path -> {
				String name = path.getFileName().toString();
				name = name.substring(0, name.indexOf("."));
				if (name.lastIndexOf("_")>0)
					name = name.substring(0, name.lastIndexOf("_"));

				CustomDescriptionHandle handle = bundleNames.get(name);
				if (handle==null) {
					handle = new CustomDescriptionHandle(name);
					bundleNames.put(name, handle);
				}

				// Try to load content
				try {
					if (Files.size(path)>(1024*1024*4)) {
						System.err.println("The custom description file '"+path+"' is very large - ignoring it");
						logger.log(Level.ERROR, "The custom description file ``{0}`` is very large - ignoring it", path);
					} else {					
						byte[] data = Files.readAllBytes(path);
						handle.addLanguage(path.getFileName().toString(), data);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		} catch (IOException e) {
			logger.log(Level.ERROR, "Error listing directory at "+localBaseDir,e);
		}
		hasBeenCheckedLast = Instant.now();

		return bundleNames.values();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CustomResourceManager#getProperty(de.rpgframework.core.RoleplayingSystem, java.lang.String, java.util.Locale)
	 */
	@Override
	public String getProperty(RoleplayingSystem rules, String key, Locale loc) {
		for (CustomDescriptionHandle propertySet : getPropertiesFiles(rules)) {
			String result = propertySet.get(key, loc);
			if (result!=null) return result;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CustomResourceManager#setProperty(de.rpgframework.core.RoleplayingSystem, java.lang.String, java.util.Locale, java.lang.String)
	 */
	@Override
	public boolean setProperty(RoleplayingSystem rules, String key, Locale loc, String value) {
		logger.log(Level.INFO, "setProperty({0},{1},{2})", rules, loc, key);
		CustomDescriptionHandle handle = null;
		for (CustomDescriptionHandle propertySet : getPropertiesFiles(rules)) {
			String result = propertySet.get(key, loc);
			if (result!=null) {
				handle = propertySet;
			}
		}

		if (handle==null)
			handle = bundleNames.get("input");
		if (handle==null) {
			handle = new CustomDescriptionHandle("input");
			bundleNames.put("input", handle);
		}

		handle.put(key, loc, value, localBaseDir);
		return true;
	}

}
