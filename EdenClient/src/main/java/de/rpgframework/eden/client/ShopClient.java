package de.rpgframework.eden.client;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.http.HttpResponse;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.api.EdenStatus;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.CatalogItem.Category;

/**
 * @author prelle
 *
 */
public class ShopClient {

	private final static Logger logger = System.getLogger(ShopClient.class.getPackageName());

	private EdenConnection eden;

	//-------------------------------------------------------------------
	public ShopClient(EdenConnection con) {
		this.eden = con;
	}

	//-------------------------------------------------------------------
	public String getLicensedDatasets(RoleplayingSystem ruleSystem) throws IOException {
		String url_s = eden.getBaseURL()+"/license/datasets";
		HttpResponse<String> response = eden.sendGET(new URL(url_s));
		if (response.statusCode()>=400) {
			throw new EdenAPIException(EdenStatus.INTERNAL_ERROR.code(), "Received HTTP code "+response.statusCode());
		}
		return response.body();
	}

	//-------------------------------------------------------------------
	public List<BoughtItem> listBought(RoleplayingSystem ruleSystem, String lang, Category... categories) throws IOException {
		String urlS = eden.getBaseURL()+"/license";
		List<String> params = new ArrayList<>();
		if (ruleSystem!=null)
			params.add("rules="+ruleSystem);
		if (lang!=null)
			params.add("lang="+lang);
		if (categories.length>0) {
			List<String> cNames = new ArrayList<>();
			for (Category cat : categories) {
				cNames.add(cat.name());
			}
			params.add("category="+URLEncoder.encode(String.join(",", cNames), Charset.forName("UTF-8")));
		}
		if (!params.isEmpty()) {
			urlS+="?"+String.join("&", params);
		}

		URL url = new URL(urlS);
		logger.log(Level.INFO, "listBought: "+url);
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(eden.getAuthenticator());
		con.setRequestMethod("GET");
		if (con.getResponseCode()!=200)
			throw new IOException("Failed listing bought items: "+con.getResponseCode()+" "+con.getResponseMessage());

		logger.log(Level.DEBUG, "Cookie: "+con.getHeaderField("Set-Cookie"));
 		java.lang.reflect.Type typeOfObjectsList = new TypeToken<ArrayList<BoughtItem>>() {}.getType();
		List<BoughtItem> ret = new ArrayList<BoughtItem>();
		ret = (new Gson()).fromJson(new InputStreamReader(con.getInputStream()), typeOfObjectsList);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<String> listPlugins(RoleplayingSystem ruleSystem, String lang, Category... categories) throws IOException {
		String urlS = eden.getBaseURL()+"/license?plugins=true";
		List<String> params = new ArrayList<>();
		if (ruleSystem!=null)
			params.add("rules="+ruleSystem);
		if (lang!=null)
			params.add("lang="+lang);
		if (categories.length>0) {
			List<String> cNames = new ArrayList<>();
			for (Category cat : categories) {
				cNames.add(cat.name());
			}
			params.add("category="+URLEncoder.encode(String.join(",", cNames), Charset.forName("UTF-8")));
		}
		if (!params.isEmpty()) {
			urlS+="&"+String.join("&", params);
		}

		URL url = new URL(urlS);
		logger.log(Level.INFO, "listPlugins: "+url);
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(eden.getAuthenticator());
		con.setRequestMethod("GET");
		if (con.getResponseCode()!=200)
			throw new IOException("Failed listing plugins: "+con.getResponseCode()+" "+con.getResponseMessage());

		logger.log(Level.DEBUG, "Cookie: "+con.getHeaderField("Set-Cookie"));
 		java.lang.reflect.Type typeOfObjectsList = new TypeToken<ArrayList<String>>() {}.getType();
		List<String> ret = new ArrayList<String>();
		ret = (new Gson()).fromJson(new InputStreamReader(con.getInputStream()), typeOfObjectsList);
		return ret;
	}

	// -------------------------------------------------------------------
	public BoughtItem useActivationKey(UUID actKey) throws EdenAPIException {
		logger.log(Level.DEBUG, "BEGIN: useActivationKey(" + actKey + ")");
		try {
			String urlS = eden.getBaseURL() + "/license?activationKey=" + actKey;
			URL url = new URL(urlS);
			logger.log(Level.INFO, "useActivationKey: " + url);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setAuthenticator(eden.getAuthenticator());
			con.setRequestMethod("POST");
			int code = con.getResponseCode();
			logger.log(Level.DEBUG, "Code = "+code);
			switch (code) {
			case 403:
				throw new EdenAPIException(code,"Invalid code or code already used");
			case 406:
				throw new EdenAPIException(code,"You already own the brought this code can be used for");
			case 409:
				throw new EdenAPIException(code,"code valid points to unknown product");
			default:
				if (code != 200)
					throw new EdenAPIException(code,"Failed consuming activation key: " + con.getResponseCode() + " "
							+ con.getResponseMessage());
			}
			BoughtItem ret = (new Gson()).fromJson(new InputStreamReader(con.getInputStream()), BoughtItem.class);
			return ret;
		} catch (IOException e) {
			if (e instanceof EdenAPIException)
				throw (EdenAPIException)e;
			throw new EdenAPIException(e);
		} finally {
			logger.log(Level.DEBUG, "END  : useActivationKey(" + actKey + ")");
		}
	}

}
