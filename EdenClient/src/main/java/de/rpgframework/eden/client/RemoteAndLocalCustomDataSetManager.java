package de.rpgframework.eden.client;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.character.CharacterIOException;
import de.rpgframework.character.CharacterIOException.ErrorCode;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.data.CustomDataSetHandle;
import de.rpgframework.genericrpg.data.CustomDataSetHandle.DataSetEntry;
import de.rpgframework.genericrpg.data.CustomDataSetManager;
import de.rpgframework.genericrpg.data.DataSet;

/**
 * @author prelle
 *
 */
public class RemoteAndLocalCustomDataSetManager implements CustomDataSetManager {

	//private CustomResourceManagerListener callback;
	private RoleplayingSystem rules;

	protected Logger logger = System.getLogger(RemoteAndLocalCustomDataSetManager.class.getPackageName());

	private EdenConnection eden;
	//private EdenCustomResourceManager onlineCharProv;

	private Path localBaseDir;

	private Map<String,CustomDataSetHandle> bundleNames;
	private Instant hasBeenCheckedLast;

	//-------------------------------------------------------------------
	/**
	 * @throws IOException
	 */
	public RemoteAndLocalCustomDataSetManager(Path customDir, EdenConnection eden, RoleplayingSystem rules) throws IOException {
		this.eden = eden;
		this.rules = rules;
		bundleNames = new LinkedHashMap<>();

		localBaseDir = customDir;

		logger.log(Level.INFO, "Expect custom data at "+localBaseDir);
		System.setProperty("customDir", localBaseDir.toString());

		// Ensure that directory exists
		try {
			Files.createDirectories(localBaseDir);
		} catch (IOException e) {
			logger.log(Level.ERROR, "Could not create custom data directory: "+e);
			throw new CharacterIOException(ErrorCode.FILESYSTEM_WRITE, null, "Could not create directories", localBaseDir.toString(), e);
		}
		List<Path> subDirs = new ArrayList<Path>();

		Path defaultDir = localBaseDir.resolve("default");
		subDirs.add(defaultDir);

		// Ensure that directory exists

		try {
			Files.createDirectories(localBaseDir);
		} catch (IOException e) {
			logger.log(Level.ERROR, "Could not create custom data directory: "+e);
			throw new CharacterIOException(ErrorCode.FILESYSTEM_WRITE, null, "Could not create directories", localBaseDir.toString(), e);
		}
	}


	//-------------------------------------------------------------------
	public void start() {
		// TODO Auto-generated method stub
//		onlineCharProv = new EdenCustomResourceManager(eden);

		logger.log(Level.WARNING, "TODO: synchronize with cloud\n\n\n");
		synchronize();
	}

	//-------------------------------------------------------------------
	private void synchronize() {
//		try {
//			List<CharacterHandle> charsOnline = onlineCharProv.getMyCharacters(rules);
//			List<CharacterHandle> charsLocal  = getMyCharacters(rules);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			System.exit(1);
//		}
	}

	//-------------------------------------------------------------------
	private CustomDataSetHandle createDataSetHandle(Path directory) throws IOException {
		Path dir = directory.resolve("i18n");
		String name = directory.getFileName().toString();
		MultiLanguageResourceBundle res = new MultiLanguageResourceBundle(dir, name, Locale.getDefault());
		DataSet set = new DataSet(this, rules, name, null, res, Locale.getDefault());

		List<DataSetEntry<?>> entries = new ArrayList<>();
		dir = directory.resolve("data");

		logger.log(Level.DEBUG, "Search for custom data in {0}", dir);
		if (Files.exists(dir)) {
			DirectoryStream<Path> xmlStream = Files.newDirectoryStream(dir, f -> Files.isRegularFile(f) && f.getFileName().toString().endsWith(".xml"));
			xmlStream.forEach(xml -> {
				String key = xml.getFileName().toString();
				key = key.substring(0, key.lastIndexOf("."));
				DataSetEntry<?> entry = new DataSetEntry<>(key, null, null);
				entries.add(entry);
				logger.log(Level.INFO, "Found custom data {0}", xml.getFileName());
			});
			xmlStream.close();
		}

		return new CustomDataSetHandle(set, entries);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.CustomDataSetManager#getCustomDataProducts()
	 */
	@Override
	public List<CustomDataSetHandle> getCustomDataProducts() {
		logger.log(Level.INFO, "Search for custom data in {0}", localBaseDir);
		List<CustomDataSetHandle> ret = new ArrayList<>();
		// Default dataset
//		ret.add(createDefault());

		Path defaultDir = localBaseDir.resolve("default");
		try {
			ret.add(createDataSetHandle(defaultDir));
		} catch (IOException e) {
			logger.log(Level.ERROR, "Error loading custom data",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, "Error loading custom data for \"default\"!\n"+e.getMessage(),e);
		}

		try {
			Map<String,String> errorsByHandle = new HashMap<>();
			DirectoryStream<Path> stream = Files.newDirectoryStream(localBaseDir, (e) -> Files.isDirectory(e));
			for (Path directory : stream) {
				logger.log(Level.INFO, "Checking directory: {0}", directory);
				String productName = directory.getFileName().toString();
//				try {
//					ret.add(createDataSetHandle(directory));
//				} catch (Exception e1) {
//					errorsByHandle.put(productName, "Error in '"+productName+"': "+e1.getMessage());
//					logger.log(Level.ERROR, "Error loading custom data for '"+directory.getFileName(),e1);
//				}
			}
			stream.close();

			if (!errorsByHandle.isEmpty()) {
				logger.log(Level.ERROR, "Error loading custom data\n"+String.join("\n",errorsByHandle.values()));
			}
		} catch (IOException e) {
			logger.log(Level.ERROR, "Error loading custom data",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, "Error loading custom data!\n"+e.getMessage(),e);
		}

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.CustomDataSetManager#getDataFile(de.rpgframework.genericrpg.data.CustomDataSetHandle, java.lang.String)
	 */
	@Override
	public InputStream getDataFile(CustomDataSetHandle product, String fileKey) {
		logger.log(Level.DEBUG, "Load custom data {0} from {1}", fileKey, product.getName().getID());

		DataSet set = product.getName();
		Path dir = localBaseDir.resolve(set.getID()).resolve("data");
		Path file = dir.resolve(fileKey+".xml");
		logger.log(Level.DEBUG, "  try to load {0}", file);

		try {
			if (Files.exists(file) && Files.isRegularFile(file)) {
				return new FileInputStream(file.toFile());
			} else
				logger.log(Level.WARNING, "No such file {0}",file);
		} catch (IOException e) {
			logger.log(Level.ERROR, "IO error accessing "+file,e);
		}
		return null;
	}

}
