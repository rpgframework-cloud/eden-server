package de.rpgframework.eden.client;

import de.rpgframework.core.StartupStep;

/**
 *
 */
public interface OnlineStartupStep extends StartupStep {

	public default void updateConnection(EdenConnection con) {}

}
