package de.rpgframework.eden.client;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.character.Attachment;
import de.rpgframework.character.Attachment.Format;
import de.rpgframework.character.Attachment.Type;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterIOException;
import de.rpgframework.character.CharacterIOException.ErrorCode;
import de.rpgframework.character.CharacterProvider;
import de.rpgframework.character.CharacterProviderListener;
import de.rpgframework.character.FileBasedCharacterHandle;
import de.rpgframework.character.IUserDatabase;
import de.rpgframework.character.LocalUserDatabase;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.eden.client.EdenConnection.StateFlag;

/**
 * @author prelle
 *
 */
public class SyncingCharacterProvider<H extends CharacterHandle> implements CharacterProvider, Consumer<List<StateFlag>> {

	private Logger logger = System.getLogger(SyncingCharacterProvider.class.getPackageName());

	private static MultiLanguageResourceBundle RES = new MultiLanguageResourceBundle(SyncingCharacterProvider.class.getName(), Locale.ENGLISH, Locale.GERMAN);

	private CharacterProviderListener callback;
	private RoleplayingSystem rules;

	private EdenConnection eden;
	private IUserDatabase local;
	private IUserDatabase remote;

	private boolean alreadySynchronized = false;

	//-------------------------------------------------------------------
	/**
	 */
	public SyncingCharacterProvider(Path playerDir, EdenConnection eden, RoleplayingSystem rules) throws CharacterIOException {
		this.eden = eden;
		this.eden.addListener(this);
		this.rules = rules;
		local = new LocalUserDatabase(playerDir, rules);
		eden.addListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#setListener(de.rpgframework.character.CharacterProviderListener)
	 */
	@Override
	public void setListener(CharacterProviderListener callback) {
		this.callback = callback;
	}

	//-------------------------------------------------------------------
	private void fireADD(CharacterHandle handle) {
		if (callback!=null)
			callback.characterAdded(handle);
	}

	//-------------------------------------------------------------------
	private void fireMODIFY(CharacterHandle handle) {
		if (callback!=null)
			callback.characterModified(handle);
	}

	//-------------------------------------------------------------------
	private void fireDELETE(CharacterHandle handle) {
		if (callback!=null)
			callback.characterRemoved(handle);
	}

	//-------------------------------------------------------------------
	public void start() {
		logger.log(Level.WARNING, "ENTER start()");
		try {
			remote= new RemoteUserDatabase(eden);
//			onlineCharProv = new EdenCharacterProvider(eden);

			logger.log(Level.WARNING, "TODO: synchronize with cloud\n\n\n");
			synchronize();
		} finally {
			logger.log(Level.ERROR, "LEAVE start()");
		}
	}

	//-------------------------------------------------------------------
	private void synchronize() {
		logger.log(Level.DEBUG, "ENTER: synchronize()");
		String lastCharacter = null;
		try {
			Map<UUID,CharacterHandle> charsOnline = remote.getCharacters().stream()
					.collect(Collectors.toMap(CharacterHandle::getUUID, Function.identity()));
			Map<UUID,CharacterHandle> charsLocal  = local.getCharacters().stream()
					.collect(Collectors.toMap(CharacterHandle::getUUID, Function.identity()));
//			for (Entry<UUID,CharacterHandle> entry : charsLocal.entrySet()) {
//				logger.log(Level.INFO, "X Character {0} = {1}", entry.getKey(), entry.getValue().getName());
//			}
			logger.log(Level.WARNING, "{0} characters local, {1} characters online", charsLocal.size(), charsOnline.size());

			// Check if remote UUIDs are missing locally
			for (UUID key : charsOnline.keySet()) {
				CharacterHandle remoteH = charsOnline.get(key);
				if (!remoteH.getRuleIdentifier().equals(rules)) {
					logger.log(Level.TRACE, "Wrong roleplaying system: {0} != {1}", remoteH.getRuleIdentifier(), rules);
					continue;
				}
				if (!charsLocal.containsKey(key)) {
					// Remote server has a character not present locally
					// Ignore it, when it is marked deleted on the server
					if (remoteH.isDeleted())
						continue;
					logger.log(Level.WARNING, "Character {0}/{1} not found locally", charsOnline.get(key).getName(), key);
					local.createCharacter(remoteH);
					logger.log(Level.INFO, "Character {0} metadata has been downloaded from server", charsOnline.get(key).getName());
					fireADD(remoteH);
					logger.log(Level.INFO, "Character {0} : now synchronize attachments", charsOnline.get(key).getName());
					synchronizeAttachments((FileBasedCharacterHandle) remoteH);
				} else {
					// Remote server has a character - it may be newer
					CharacterHandle localH  = charsLocal.get(key);
					if (remoteH.getLastModified().after(localH.getLastModified())) {
						// Remote server has a character that is newer
						logger.log(Level.WARNING, "Local character {0} needs to be updated", charsOnline.get(key).getName());
						localH.setShortDescription(remoteH.getShortDescription());
						localH.setLastModified(remoteH.getLastModified());
						localH.setName(remoteH.getName());
						local.modifyCharacter(localH);
					} else
						logger.log(Level.INFO, "Local Character {0} is up to date", localH.getName());
					synchronizeAttachments((FileBasedCharacterHandle) localH);
				}
			}
			// Check if local files need to be uploaded
			try {
				for (UUID key : charsLocal.keySet()) {
					CharacterHandle localH  = charsLocal.get(key);
					CharacterHandle remoteH = charsOnline.get(key);
					if (!localH.getRuleIdentifier().equals(rules)) {
						logger.log(Level.TRACE, "Wrong roleplaying system: {0} != {1}", remoteH.getRuleIdentifier(), rules);
						continue;
					}
					lastCharacter = localH.getName()+" ("+localH.getUUID()+")";
					if (remoteH==null) {
						// Local server has a character not present online
						logger.log(Level.WARNING, "Character {0} not found online", charsLocal.get(key).getName());
						remote.createCharacter(localH);
					} else {
						if (localH.getLastModified().after(remoteH.getLastModified()) && !String.valueOf(localH.getLastModified()).equals(String.valueOf(remoteH.getLastModified()))) {
							// Local copy is newer than remote copy
							logger.log(Level.WARNING, "Remote character {0} needs to be updated", charsOnline.get(key).getName());
						} else
							logger.log(Level.INFO, "Remote character {0} is up to date", localH.getName());
					}
					synchronizeAttachments((FileBasedCharacterHandle) localH);
				}
			} catch (EdenAPIException e) {
				logger.log(Level.ERROR, "Unexpected API error: "+e.getStatus()+"/"+e.getCode(),e);
				String message = RES.getString("error.synchronizing_character.upload", Locale.getDefault())+"\n"+
						RES.getString("error.api."+String.valueOf(e.getStatus()).toLowerCase(), Locale.getDefault());
				message += "\nCharacter: "+lastCharacter;
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, message, e);
			}

			alreadySynchronized = true;
		} catch (EdenAPIException e) {
			logger.log(Level.ERROR, "Unexpected API error: "+e.getStatus()+"/"+e.getCode(),e);
			String message = RES.getString("error.api."+String.valueOf(e.getStatus()).toLowerCase(), Locale.getDefault());
			message += "\nCharacter: "+lastCharacter;
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, message, e);
		} catch (Exception e) {
			logger.log(Level.ERROR, "Unexpected error",e);
		} finally {
			logger.log(Level.DEBUG, "LEAVE: synchronize()");
		}
	}

	//-------------------------------------------------------------------
	private void synchronizeAttachments(FileBasedCharacterHandle handle) {
		logger.log(Level.DEBUG, "ENTER: synchronizeAttachments({0})", handle.getName());
		try {
			Map<UUID,Attachment> charsOnline = remote.getAttachments(handle)
					.stream()
					.collect(Collectors.toMap(Attachment::getID, Function.identity()));
			Map<UUID,Attachment> charsLocal  = new HashMap<UUID,Attachment>();
			local.getAttachments(handle).forEach(attach -> charsLocal.put(attach.getID(), attach));
//			Map<UUID,Attachment> charsLocal  = local.getAttachments(handle).stream()
//					.collect(Collectors.toMap(Attachment::getID, Function.identity()));
//			for (Entry<UUID,Attachment> entry : charsLocal.entrySet()) {
//				logger.log(Level.INFO, "  Local Attachment {0} = {1}  {2}  last={3}", entry.getKey(), entry.getValue().getFilename(), entry.getValue().getFormat(), entry.getValue().getLastModified());
//			}
			logger.log(Level.DEBUG, "{0} attachments local, {1} attachments online", charsLocal.size(), charsOnline.size());


			// Check if remote UUIDs are missing locally
			for (UUID key : charsOnline.keySet()) {
				Attachment remoteA = charsOnline.get(key);
				Attachment localA  = charsLocal.get(key);
				if (localA==null) {
					// Remote server has a character not present locally
					logger.log(Level.DEBUG, "Character {0}/{1} not found locally", charsOnline.get(key).getFilename(), key);
					local.createAttachment(handle, remoteA);

					byte[] content = remote.retrieveAttachment(handle, remoteA);
					if (content!=null) {
						local.modifyAttachment(handle, remoteA);
						logger.log(Level.INFO, "Successfully added attachment {0} to {1}", remoteA.getFilename(), handle.getName());
					}
				} else {
					// Remote server has a character - it may be newer
					if (remoteA.getLastModified().after(localA.getLastModified())) {
						// Remote server has a character that is newer
						logger.log(Level.DEBUG, "Local attachments {0} needs to be updated", charsOnline.get(key).getFilename());
						byte[] content = remote.retrieveAttachment(handle, remoteA);
						if (content!=null) {
							localA.setData(content);
							localA.setLastModified(remoteA.getLastModified());
							local.modifyAttachment(handle, localA);
							logger.log(Level.INFO, "Successfully udpdated attachment {0}", localA.getFilename());
						}
					} else
						logger.log(Level.DEBUG, "Local attachment {0} is up to date", localA.getLocalFile());
				}
			}
			// Check if local files need to be uploaded
			for (UUID key : charsLocal.keySet()) {
				Attachment localA  = charsLocal.get(key);
				Attachment remoteA  = charsOnline.get(key);
				if (remoteA==null) {
					// Local server has an attachment not present online
					logger.log(Level.WARNING, "Attachment {0} not found online", charsLocal.get(key).getFilename());
					remote.createAttachment(handle, localA);
					remote.modifyAttachmentData(handle, localA);
//					if (!remoteA.getID().equals(localA.getID())) {
//						logger.log(Level.WARNING, "Update attachment UUID of {0} from {1} to {2}", localA.getFilename(), localA.getID(), remoteA.getID());
//						localA.setID(remoteA.getID());
//						localA.setLastModified(remoteA.getLastModified());
//						local.modifyAttachment(handle, localA);
//						//this.setAttachment(handle, localA);
//					}
				} else {
					if (localA.getLastModified().after(remoteA.getLastModified()) && !String.valueOf(localA.getLastModified()).equals(String.valueOf(remoteA.getLastModified()))) {
						// Local copy is newer than remote copy
						logger.log(Level.WARNING, "Remote attachment {0} needs to be updated, local={1}, remote={2}", charsOnline.get(key).getFilename(), localA.getLastModified(), remoteA.getLastModified());
					} else
						logger.log(Level.DEBUG, "Remote attachment {0} is up to date", localA.getFilename());
				}
				//synchronizeAttachments((FileBasedCharacterHandle) local);
			}

		} catch (Exception e) {
			logger.log(Level.ERROR, "Unexpected error",e);
		} finally {
			logger.log(Level.DEBUG, "LEAVE: synchronizeAttachments({0})", handle.getName());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#getCharacter(java.lang.String, de.rpgframework.core.RoleplayingSystem)
	 */
	@Override
	public CharacterHandle getCharacter(String charName, RoleplayingSystem ruleSystem) throws CharacterIOException {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#createCharacter(java.lang.String, de.rpgframework.core.RoleplayingSystem)
	 */
	@Override
	public CharacterHandle createCharacter(String charName, RoleplayingSystem ruleSystem) throws IOException {
		logger.log(Level.DEBUG, "ENTER: createCharacter()");

		FileBasedCharacterHandle handle = new FileBasedCharacterHandle(null, ruleSystem);
		handle.setName(charName);

		if (remote!=null) {
			// Create the character remotely - this should create a UUID
			try {
				remote.createCharacter(handle);
				logger.log(Level.DEBUG, "Server defined UUID {0}", handle.getUUID());
			} catch (EdenAPIException e) {
				handle.setUUID(UUID.randomUUID());
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, RES.format("error.creating_character.remote_with_reason", e.getMessage()), e);
			} catch (IOException e) {
				handle.setUUID(UUID.randomUUID());
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, RES.getString("error.creating_character.remote"), e);
			}
		} else {
			handle.setUUID(UUID.randomUUID());
			logger.log(Level.DEBUG, "Offline - create UUID myself: {0}", handle.getUUID());
		}

		local.createCharacter(handle);
		// Inform listener
		fireADD(handle);
		return handle;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#listAttachments(de.rpgframework.character.CharacterHandle)
	 */
	@Override
	public List<Attachment> listAttachments(CharacterHandle rawHandle) throws CharacterIOException {
		logger.log(Level.DEBUG, "ENTER: listAttachments("+rawHandle.getName()+")");
		try {
			return local.getAttachments(rawHandle);
		} catch (IOException e) {
			logger.log(Level.ERROR, "Failed getting attachments",e);
			return List.of();
		} finally {
			logger.log(Level.TRACE, "LEAVE: listAttachments("+rawHandle.getName()+")");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#getFirstAttachment(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment.Type, de.rpgframework.character.Attachment.Format)
	 */
	@Override
	public Attachment getFirstAttachment(CharacterHandle rawHandle, Type type, Format format) throws CharacterIOException {
		if (!rawHandle.getAttachments().isEmpty()) {
			return rawHandle.getAttachments().stream().filter(attach -> attach.getType()==type && attach.getFormat()==format).findFirst().orElse(null);
		}

		return listAttachments(rawHandle).stream()
				.filter(attach -> attach.getType()==type && attach.getFormat()==format).findFirst().orElse(null);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#addAttachment(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment.Type, de.rpgframework.character.Attachment.Format, java.lang.String, byte[])
	 */
	@Override
	public Attachment addAttachment(CharacterHandle rawHandle, Type type, Format format, String suggestedName, byte[] data) throws IOException {
		logger.log(Level.INFO, "addAttachment");
		FileBasedCharacterHandle handle = (FileBasedCharacterHandle) rawHandle;

		if (data==null)
			throw new NullPointerException("No data in attachment");
		logger.log(Level.INFO, "Add attachment "+type+"  "+format+"  "+suggestedName );

		String guessedname = suggestedName;
		// Eventually override with default values
		switch (type) {
		case CHARACTER:
			switch (format) {
			case RULESPECIFIC: guessedname = handle.getName()+".xml"; break;
			case HTML        : guessedname = handle.getName()+".html"; break;
			case PDF         : guessedname = handle.getName()+".pdf"; break;
			case IMAGE       : guessedname = handle.getName()+".img"; break;
			case TEXT        : guessedname = handle.getName()+".txt"; break;
			case JSON        : guessedname = handle.getName()+".json"; break;
			case BBCODE      : guessedname = handle.getName()+".bbcode"; break;
			case RULESPECIFIC_EXTERNAL:
				break;
			}
			break;
		case BACKGROUND:
			switch (format) {
			case HTML        : guessedname = "background.html"; break;
			case PDF         : guessedname = "background.pdf"; break;
			case TEXT        : guessedname = "background.txt"; break;
			default:
				throw new IllegalArgumentException("Format "+format+" not accepted for character background");
			}
			break;
		default:
		}
		// If suggested name misses a suffix and guessed name is different, overrule
		// suggested name
		if (suggestedName!=null && suggestedName.indexOf(".")<0 && !suggestedName.equals(guessedname)) {
			suggestedName = guessedname;
			logger.log(Level.DEBUG, "  prefer filename "+suggestedName);
		}

		Attachment attach = new Attachment(handle, null, type, format);
		attach.setFilename(suggestedName);
		attach.setData(data);
		// Eventually sync to server
		if (remote!=null && handle.hasSyncFlag()) {
			logger.log(Level.DEBUG, "Upload attachment to server");
			remote.createAttachment(handle, attach);
			remote.modifyAttachmentData(handle, attach);
			logger.log(Level.INFO, "Attachment '"+guessedname+"' successfully uploaded to server");
		} else {
			logger.log(Level.WARNING, "Create local only Attachment");
			attach.setID(UUID.randomUUID());
		}
		logger.log(Level.DEBUG, "Create local attachment");
		local.createAttachment(handle, attach);
		local.modifyAttachmentData(handle, attach);

		logger.log(Level.INFO, "fireMODIFY");
		fireMODIFY(rawHandle);
		return attach;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#modifyAttachment(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment)
	 */
	@Override
	public void modifyAttachment(CharacterHandle rawHandle, Attachment attach) throws CharacterIOException {
		logger.log(Level.DEBUG, "ENTER modifyAttachment");
		try {
			FileBasedCharacterHandle handle = (FileBasedCharacterHandle) rawHandle;

			try {
				local.modifyAttachment(handle, attach);
				local.modifyAttachmentData(handle, attach);
			} catch (IOException e) {
				throw new CharacterIOException(ErrorCode.FILESYSTEM_WRITE, "", "Could not modify attachment", String.valueOf(handle.getPath()), e);
			}

			// Eventually sync to server
			try {
				if (remote != null && handle.hasSyncFlag()) {
					logger.log(Level.DEBUG, "Upload modified attachment to server");
					remote.modifyAttachment(handle, attach);
					remote.modifyAttachmentData(handle, attach);
					logger.log(Level.INFO, "Attachment ''{0}'' successfully uploaded to server", attach.getFilename());
				}
			} catch (IOException e) {
				throw new CharacterIOException(ErrorCode.SERVER_ERROR, "Error uploading character to online storage", e);
			}
		} finally {
			logger.log(Level.DEBUG, "LEAVE modifyAttachment");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#deleteAttachment(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment)
	 */
	@Override
	public void deleteAttachment(CharacterHandle rawHandle, Attachment attach) throws CharacterIOException {
		FileBasedCharacterHandle handle = (FileBasedCharacterHandle)rawHandle;
		logger.log(Level.INFO, "deleteAttachment: "+handle+" = "+attach);

		// Eventually sync to server
		if (remote!=null && handle.hasSyncFlag()) {
			logger.log(Level.DEBUG, "Remove attachment from server");
			try {
				remote.deleteAttachment(handle, attach);
			} catch (IOException e) {
				logger.log(Level.ERROR, "Failed removing attachment on remote server",e);
				throw new CharacterIOException(ErrorCode.SERVER_ERROR, "Failed deleting attachment from server", e);
			}
		}

		try {
			local.deleteAttachment(handle, attach);
		} catch (IOException e) {
			throw new CharacterIOException(ErrorCode.FILESYSTEM_WRITE, "", "Could not delete attachment", String.valueOf(handle.getPath()), e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#deleteCharacter(de.rpgframework.character.CharacterHandle)
	 */
	@Override
	public void deleteCharacter(CharacterHandle rawHandle) throws IOException {
		logger.log(Level.INFO, "deleteCharacter: "+rawHandle);
		FileBasedCharacterHandle handle = (FileBasedCharacterHandle)rawHandle;
		if (remote!=null) {
			try {
				remote.deleteCharacter(handle);
			} catch (EdenAPIException e) {
				handle.setUUID(UUID.randomUUID());
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, RES.format("error.deleting_character.remote_with_reason", e.getMessage()), e);
			} catch (IOException e) {
				handle.setUUID(UUID.randomUUID());
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, RES.getString("error.deleting_character.remote"), e);
			}
		}

		try {
			local.deleteCharacter(handle);
			fireDELETE(handle);
		} catch (IOException e) {
			throw new CharacterIOException(ErrorCode.FILESYSTEM_WRITE, "", "Could not delete character", String.valueOf(handle.getPath()), e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#renameCharacter(de.rpgframework.character.CharacterHandle, java.lang.String)
	 */
	@Override
	public void renameCharacter(CharacterHandle handle, String newName) throws CharacterIOException {
		// TODO Auto-generated method stub
		throw new RuntimeException("Renaming not implemented");
	}

	//--------------------------------------------------------------)-----
	/**
	 * @see de.rpgframework.character.CharacterProvider#getMyCharacters()
	 */
	@Override
	public List<CharacterHandle> getMyCharacters() throws CharacterIOException {
		logger.log(Level.WARNING, "ENTER getMyCharacters");
		try {
			return local.getCharacters().stream().filter(handle -> handle.getRuleIdentifier()==rules).toList();
		} catch (IOException e) {
			throw new CharacterIOException(ErrorCode.FILESYSTEM_READ, "", "Could not read characters", e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#getMyCharacters(de.rpgframework.core.RoleplayingSystem)
	 */
	@Override
	public List<CharacterHandle> getMyCharacters(RoleplayingSystem ruleSystem) throws CharacterIOException {
		logger.log(Level.DEBUG, "ENTER getMyCharacters(" + ruleSystem + ")");
		try {
			return local.getCharacters().stream().filter(handle -> handle.getRuleIdentifier()==rules).toList();
		} catch (IOException e) {
			throw new CharacterIOException(ErrorCode.FILESYSTEM_READ, "", "Could not read characters", e);
		} finally {
			logger.log(Level.DEBUG, "LEAVE getMyCharacters(" + ruleSystem + ")");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.util.function.Consumer#accept(java.lang.Object)
	 */
	@Override
	public void accept(List<StateFlag> flags) {
		logger.log(Level.WARNING, "TODO: received {0}", flags);
		if (flags.contains(StateFlag.ACCOUNT_VERIFIED) && !alreadySynchronized) {
			logger.log(Level.INFO, "Not synchronized yet - do it now");
			start();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#isSynchronizeSupported()
	 */
	@Override
	public boolean isSynchronizeSupported() {
		return eden.hasStateFlag(StateFlag.ACCOUNT_VERIFIED);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#initiateCharacterSynchronization()
	 */
	@Override
	public void initiateCharacterSynchronization() {
		logger.log(Level.INFO, "User requested character synchronization");
		synchronize();
	}

}
