package de.rpgframework.eden.client;

import java.io.IOException;
import java.lang.System.Logger.Level;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;

public class DummyTest {

	public DummyTest() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		Authenticator.setDefault(new java.net.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				System.err.println("getPasswordAuthentication   "+getRequestingHost());
				return new PasswordAuthentication ("Hallo", "Welt".toCharArray());
			}
		});

		HttpClient httpClient = HttpClient.newBuilder()
	            .version(HttpClient.Version.HTTP_1_1)
	            .authenticator(Authenticator.getDefault())
	            .connectTimeout(Duration.ofSeconds(10))
	            .build();

		  HttpRequest request = HttpRequest.newBuilder()
        .PUT(HttpRequest.BodyPublishers.ofString("{}", StandardCharsets.UTF_8))
        .uri(URI.create("http://localhost:8721/account/verify"))
        .setHeader("User-Agent", "Java 11 HttpClient Bot") // add request header
        .build();

HttpResponse<String> response = httpClient
.send(request, HttpResponse.BodyHandlers.ofString());

// print response headers
HttpHeaders headers = response.headers();
headers.map().forEach((k, v) -> System.out.println(k + ":::" + v));

// print status code
System.out.println(response.statusCode());

// print response body
System.out.println(response.body());

	}

}
