package de.rpgframework.eden.client;

import java.io.IOException;

import de.rpgframework.eden.api.EdenStatus;

/**
 * @author prelle
 *
 */
public class EdenAPIException extends IOException {

	private static final long serialVersionUID = -2463120547121304665L;

	private int code;
	private EdenStatus status;

	//-------------------------------------------------------------------
	public EdenAPIException(int code) {
		super("EDEN Server reports code "+code);
		this.code = code;
	}

	//-------------------------------------------------------------------
	public EdenAPIException(int code, String message) {
		super(message);
		this.code = code;
		this.status = EdenStatus.fromCode(code);
	}

	//-------------------------------------------------------------------
	public EdenAPIException(EdenStatus code, String message) {
		super(message);
		this.code = code.code();
		this.status = code;
	}

	//-------------------------------------------------------------------
	public EdenAPIException(Throwable e) {
		super(e);
		this.code = -1;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Throwable#getLocalizedMessage()
	 */
	@Override
    public String getLocalizedMessage() {
		switch (code) {

		}
        return getMessage();
    }

	//-------------------------------------------------------------------
	/**
	 * @return the status
	 */
	public EdenStatus getStatus() {
		return status;
	}

}
