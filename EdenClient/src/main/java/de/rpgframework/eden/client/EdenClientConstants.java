package de.rpgframework.eden.client;

public interface EdenClientConstants {

	public final static String PREF_USER = "eden.user";
	public final static String PREF_PASS = "eden.pass";

}
