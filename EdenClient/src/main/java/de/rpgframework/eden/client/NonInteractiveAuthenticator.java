package de.rpgframework.eden.client;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 * @author prelle
 *
 */
public class NonInteractiveAuthenticator extends Authenticator {

	private final static Logger logger = System.getLogger(NonInteractiveAuthenticator.class.getPackageName());

	private InteractiveAuthenticator interactiveAuth;
	private EdenConnection connection;

	//-------------------------------------------------------------------
	public NonInteractiveAuthenticator(InteractiveAuthenticator interactiveAuth, EdenConnection parent) {
		this.interactiveAuth = interactiveAuth;
		this.connection = parent;
	}

    //-------------------------------------------------------------------
    /**
     * @see java.net.Authenticator#getPasswordAuthentication()
     */
	@Override
    protected PasswordAuthentication getPasswordAuthentication() {
//		System.err.println("NonInteractiveAuthenticator.getPasswordAuthentication   "+getRequestingHost());
		logger.log(Level.DEBUG, "getPasswordAuthentication for {0} and previously successful={1}", getRequestingHost(), connection.getCredentialsSuccessful());

		String cachedUser = connection.getLogin();
		String cachedPass = connection.getPassword();
		if (connection.getCredentialsSuccessful()) {
			logger.log(Level.INFO, "Use stored login/password");
			connection.setCredentialsSuccessful(false);
			return new PasswordAuthentication (cachedUser, cachedPass.toCharArray());
		} else {
			logger.log(Level.INFO, "Ask for login/password: "+interactiveAuth);
			if (interactiveAuth!=null) {
				logger.log(Level.WARNING, "Cached credentials for {0} not successful - ask interactively", cachedUser);
				String[] credentials = interactiveAuth.requestCredentials();
				if (credentials==null) {
					logger.log(Level.WARNING, "User rejected entering credentials");
					return null;
				}
				cachedUser = credentials[0];
				cachedPass = credentials[1];
				if (cachedPass==null) return null;
				return new PasswordAuthentication (cachedUser, cachedPass.toCharArray());
			} else {
				logger.log(Level.ERROR, "Cached credentials for {0} not successful - but no interactive fallback configured", cachedUser);
				connection.clearStateFlag(EdenConnection.StateFlag.CREDENTIALS_CORRECT);
				return null;
			}
		}
    }

//    //-------------------------------------------------------------------
//	public void setWasSuccessful(boolean successful) {
//		this.cacheSuccessful = successful;
//	}
//
//    //-------------------------------------------------------------------
//	public void setCredentials(String login, String pass) {
//		logger.log(Level.INFO, "setCredentials({0}) and assume they are successful", login);
//		this.cachedUser = login;
//		this.cachedPass = pass;
//		this.cacheSuccessful = true;
//	}

    //-------------------------------------------------------------------
	public void setInteractiveFallback(InteractiveAuthenticator fallback) {
		this.interactiveAuth = fallback;
	}

}
