package de.rpgframework.eden.client;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.rpgframework.character.Attachment;
import de.rpgframework.character.Attachment.Format;
import de.rpgframework.character.Attachment.Type;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterIOException;
import de.rpgframework.character.CharacterIOException.ErrorCode;
import de.rpgframework.character.CharacterProvider;
import de.rpgframework.character.CharacterProviderListener;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class EdenCharacterProvider implements CharacterProvider {

	private final static String API_PATH = "api/";

	private final static Logger logger = System.getLogger(EdenCharacterProvider.class.getPackageName());

	private EdenConnection eden;

	private CharacterProviderListener callback;

	//-------------------------------------------------------------------
	public EdenCharacterProvider(EdenConnection con) {
		this.eden = con;
		if (eden==null)
			throw new NullPointerException();
//		if (eden.getAuthenticator()==null)
//			throw new NullPointerException("Auth");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#setListener(de.rpgframework.character.CharacterProviderListener)
	 */
	@Override
	public void setListener(CharacterProviderListener callback) {
		this.callback = callback;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#createCharacter(java.lang.String, de.rpgframework.core.RoleplayingSystem)
	 */
	@Override
	public CharacterHandle createCharacter(String charName, RoleplayingSystem ruleSystem) throws IOException {
		return createCharacter(charName, ruleSystem, UUID.randomUUID());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#createCharacter(java.lang.String, de.rpgframework.core.RoleplayingSystem, java.util.UUID)
	 */
	@Override
	public CharacterHandle createCharacter(String charName, RoleplayingSystem ruleSystem, UUID uuid) throws IOException {
		BaseCharacterHandleLight handle = new BaseCharacterHandleLight(null, ruleSystem, uuid);
		handle.setName(charName);
//		handle.setRuleIdentifier(ruleSystem);
		return createCharacter(handle);
	}

	//-------------------------------------------------------------------
	public CharacterHandle createCharacter(BaseCharacterHandleLight handle) throws IOException {
		URL url = new URL(String.format("http://%s/"+API_PATH+"character", eden.getHostPort()));
		logger.log(Level.INFO, "createCharacter: "+url);
		logger.log(Level.INFO, "  Name "+handle.getName());
		logger.log(Level.INFO, "  UUID "+handle.getUUID());

		Gson gson = (new Gson());
		String json = gson.toJson(handle);
		logger.log(Level.INFO, "  JSON "+json);

		try {
			HttpRequest request = HttpRequest.newBuilder(url.toURI())
					.headers("Content-Type", "application/json;charset=UTF-8")
					.POST(HttpRequest.BodyPublishers.ofString(json, StandardCharsets.UTF_8))
					.timeout(Duration.ofSeconds(3))
					.build();

			HttpResponse<String> response = eden.getClient().send(request, BodyHandlers.ofString());

			int code = response.statusCode(); //con.getResponseCode();
			logger.log(Level.INFO, "Server returned code {0} for /create", code);
//		HttpURLConnection con = (HttpURLConnection)url.openConnection();
//		con.setAuthenticator(eden.getAuthenticator());
//		con.setRequestMethod("POST");
//		if (con.getResponseCode()!=200)
		if (code!=200)
			throw new EdenAPIException(code,"Failed creating character: "+code);

		json = response.body();
//		logger.log(Level.DEBUG, "Cookie: "+con.getHeaderField("Set-Cookie"));
		handle = (new Gson()).fromJson(json, BaseCharacterHandleLight.class);
		return handle;
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error creating character: "+url,e);
			throw new IOException(e);
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#getMyCharacters()
	 */
	@Override
	public List<CharacterHandle> getMyCharacters() throws IOException {
		URL url = new URL(String.format("http://%s/"+API_PATH+"character/", eden.getHostPort()));
		logger.log(Level.INFO, "open GET "+url);
//			HttpRequest request = HttpRequest.newBuilder(url.toURI())
//				.headers("Content-Type", "application/octet-stream")
//				.GET()
//				.timeout(Duration.ofSeconds(3))
//				.build();

			HttpResponse<String> response = eden.sendGET(url);
			int code = response.statusCode(); //con.getResponseCode();

			if (code!=200)
				throw new IOException("Failed listing character: code "+code);

			//logger.log(Level.DEBUG, "Cookie: "+con.getHeaderField("Set-Cookie"));
			java.lang.reflect.Type typeOfObjectsList = new TypeToken<ArrayList<BaseCharacterHandleLight>>() {}.getType();
			List<CharacterHandle> handles = new ArrayList<CharacterHandle>();
			handles = (new Gson()).fromJson(response.body(), typeOfObjectsList);
			return handles;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#getMyCharacters(de.rpgframework.core.RoleplayingSystem)
	 */
	@Override
	public List<CharacterHandle> getMyCharacters(RoleplayingSystem ruleSystem) throws IOException {
		URL url = new URL(String.format("http://%s/"+API_PATH+"character/?rules=%s", eden.getHostPort(),  ruleSystem.name()));
		logger.log(Level.DEBUG, "Send via "+eden);
		HttpResponse<String> response = eden.sendGET(url);
		int code = response.statusCode(); //con.getResponseCode();

		if (code!=200)
			throw new CharacterIOException(ErrorCode.SERVER_ERROR, "Failed listing character: "+code);

 		java.lang.reflect.Type typeOfObjectsList = new TypeToken<ArrayList<BaseCharacterHandleLight>>() {}.getType();
		List<CharacterHandle> handles = new ArrayList<CharacterHandle>();
		handles = (new Gson()).fromJson(response.body(), typeOfObjectsList);
		return handles;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#getCharacter(java.lang.String, de.rpgframework.core.RoleplayingSystem)
	 */
	@Override
	public CharacterHandle getCharacter(String charName, RoleplayingSystem ruleSystem) throws IOException {
		List<CharacterHandle> chars = getMyCharacters(ruleSystem);
		for (CharacterHandle handle : chars) {
			if (handle.getName().equalsIgnoreCase(charName)) {
				return handle;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	@Override
	public List<Attachment> listAttachments(CharacterHandle handle) throws IOException {
		List<Attachment> handles = new ArrayList<Attachment>();
		try {
			URL url = new URL(String.format("http://%s/"+API_PATH+"character/%s/attachment", eden.getHostPort(),  handle.getUUID().toString()));
			logger.log(Level.DEBUG, "listAttachments: "+url);
			HttpResponse<String> response = eden.sendGET(url);
			int code = response.statusCode(); //con.getResponseCode();

			if (code!=200)
				throw new CharacterIOException(ErrorCode.SERVER_ERROR, "Failed listing attachments: "+code);

			java.lang.reflect.Type typeOfObjectsList = new TypeToken<ArrayList<Attachment>>() {}.getType();
			handles = (new Gson()).fromJson(response.body(), typeOfObjectsList);
		} finally {

		}
		logger.log(Level.DEBUG, "attachments = {0}",handles.size());
		return handles;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#getAttachmentContent(de.rpgframework.character.CharacterHandle, java.util.UUID)
	 */
	@Override
	public byte[] getAttachmentContent(CharacterHandle handle, Attachment attach) throws IOException {
		logger.log(Level.DEBUG, "ENTER: getAttachment {0}",attach.getID());
		URL url = new URL(String.format("http://%s/"+API_PATH+"character/%s/attachment/%s", eden.getHostPort(), handle.getUUID().toString(), attach.getID()));
		try {
			byte[] data = eden.sendGETRaw(url);
			logger.log(Level.INFO, "Parse " + data.length + " bytes");
			attach.setData(data);
			return data;
		} catch (IOException e) {
			logger.log(Level.ERROR, "Error getting attachment: "+e);
			throw e;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error getting attachment: "+url,e);
			throw new IOException(e);
		} finally {
			logger.log(Level.DEBUG, "LEAVE: getAttachment");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#getFirstAttachment(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment.Type, de.rpgframework.character.Attachment.Format)
	 */
	@Override
	public Attachment getFirstAttachment(CharacterHandle handle, Type type, Format format) throws IOException {
		for (Attachment attach : listAttachments(handle)) {
			if (attach.getType()==type && attach.getFormat()==format)
				return attach;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#addAttachment(de.rpgframework.character.CharacterHandle, de.rpgframework.character.CharacterHandle.Type, de.rpgframework.character.CharacterHandle.Format, java.lang.String, byte[])
	 */
	@Override
	public Attachment addAttachment(CharacterHandle handle, Type type, Format format, String filename, byte[] data) throws IOException {
		URL url = new URL(String.format("http://%s/"+API_PATH+"character/"+handle.getUUID()+"/attachment/?char=%s&name=%s&type=%s&format=%s", eden.getHostPort(),
				handle.getUUID().toString(), URLEncoder.encode(filename, "UTF-8"), type.name(), format.name()));
		logger.log(Level.INFO, "addAttachment POST "+url);
		Attachment attach = new Attachment(handle, null, type, format);
		attach.setFilename(filename);
		attach.setData(data);

		return addAttachment(handle, attach);
	}

	//-------------------------------------------------------------------
	public Attachment addAttachment(CharacterHandle handle, Attachment attach) throws IOException {
		URL url = new URL(String.format("http://%s/"+API_PATH+"character/%s/attachment", eden.getHostPort(), handle.getUUID().toString()));
		logger.log(Level.INFO, "addAttachment POST "+url);

		try {
			Gson gson = (new Gson());
			String json = gson.toJson(attach);
			logger.log(Level.INFO, "  JSON "+json);

			HttpRequest request = HttpRequest.newBuilder(url.toURI())
				.headers("Content-Type", "application/octet-stream")
				.POST(HttpRequest.BodyPublishers.ofString(json, StandardCharsets.UTF_8))
				.timeout(Duration.ofSeconds(3))
				.build();

			HttpResponse<String> response = eden.getClient().send(request, BodyHandlers.ofString());
			int code = response.statusCode(); //con.getResponseCode();
			logger.log(Level.INFO, "Server returned code {0} for /create", code);
			if (code>=400)
				throw new EdenAPIException(code, response.headers().firstValue("Reason").orElse("Unknown"));

			UUID uuid = (new Gson()).fromJson(response.body(), UUID.class);
			attach.setID(uuid);
			attach.setParent(handle);
			return attach;
		} catch (IOException e) {
			logger.log(Level.ERROR, "Error adding attachment: "+e);
			throw new EdenAPIException(e);
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error adding attachment: "+url,e);
			throw new EdenAPIException(e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#modifyAttachment(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment)
	 */
	@Override
	public void modifyAttachment(CharacterHandle handle, Attachment attach) throws IOException {
		if (attach.getID()==null) throw new IllegalArgumentException("Missing UUID in Attachment");
		if (attach.getFilename()==null) throw new IllegalArgumentException("Missing filename in Attachment");
		if (attach.getData()==null) throw new IllegalArgumentException("Missing data in Attachment");
		URL url = new URL(String.format("http://%s/"+API_PATH+"character/%s/attachment/%s", eden.getHostPort(),
				handle.getUUID(), attach.getID()));
		logger.log(Level.INFO, "modifyAttachment PUT "+url);

		Gson gson = (new Gson());
		String json = gson.toJson(attach);
		try {
			HttpRequest request = HttpRequest.newBuilder(url.toURI())
				.headers("Content-Type", "application/json;charset=UTF-8")
				.PUT(HttpRequest.BodyPublishers.ofString(json, StandardCharsets.UTF_8))
				.timeout(Duration.ofSeconds(3))
				.build();
			HttpResponse<String> response = eden.getClient().send(request, BodyHandlers.ofString());

			int code = response.statusCode(); //con.getResponseCode();
			logger.log(Level.INFO, "Server returned code {0} for /create", code);
			if (code!=200)
				throw new EdenAPIException(code,"Failed creating character: "+code);

//		HttpURLConnection con = (HttpURLConnection)url.openConnection();
//		con.setAuthenticator(eden.getAuthenticator());
//		con.setRequestMethod("PUT");
//		con.setDoOutput(true);
//		con.setRequestProperty("Content-Type", "application/octet-stream");
//		con.getOutputStream().write( (new Gson()).toJson(attach).getBytes("UTF-8"));
//		con.getOutputStream().flush();
//		if (con.getResponseCode()!=200)
//			throw new IOException("Failed updating attachment: "+con.getResponseCode()+" "+con.getResponseMessage());
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error creating character: "+url,e);
			throw new IOException(e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#deleteAttachment(de.rpgframework.character.CharacterHandle, de.rpgframework.character.Attachment)
	 */
	@Override
	public void deleteAttachment(CharacterHandle handle, Attachment attach) throws IOException {
		URL url = new URL(String.format("http://%s/"+API_PATH+"character/%s/attachment/%s", eden.getHostPort(), handle.getUUID(), attach.getID()));
		logger.log(Level.INFO, "deleteAttachment: "+url);
		try {
			HttpRequest request = HttpRequest.newBuilder(url.toURI())
				.headers("Content-Type", "application/json;charset=UTF-8")
				.DELETE()
				.timeout(Duration.ofSeconds(3))
				.build();

			HttpResponse<String> response = eden.getClient().send(request, BodyHandlers.ofString());

			if (response.statusCode()!=200)
				throw new IOException("Failed deleting attachment: "+response.statusCode()+" "+response.toString());
			logger.log(Level.DEBUG, "Delete successful");
		} catch (URISyntaxException | InterruptedException e) {
			logger.log(Level.ERROR, "Error deleting character: "+url,e);
			throw new IOException(e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#deleteCharacter(de.rpgframework.character.CharacterHandle)
	 */
	@Override
	public void deleteCharacter(CharacterHandle handle) throws IOException {
		URL url = new URL(String.format("http://%s/"+API_PATH+"character/%s", eden.getHostPort(), handle.getUUID()));
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setAuthenticator(eden.getAuthenticator());
		con.setRequestMethod("DELETE");

		if (con.getResponseCode()!=200)
			throw new IOException("Failed deleting character: "+con.getResponseCode()+" "+con.getResponseMessage());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.CharacterProvider#renameCharacter(de.rpgframework.character.CharacterHandle, java.lang.String)
	 */
	@Override
	public void renameCharacter(CharacterHandle handle, String newName) throws IOException {
		// TODO Auto-generated method stub

	}

}
